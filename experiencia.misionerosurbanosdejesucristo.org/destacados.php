<?php
session_start();
require_once('include/functions_connect.php');

require_once('cl/Mobile_Detect.php');
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
    $hexigMAcima = '100%';
} else {
    $hexigMAcima = '200px';
    $is_mobile = false;
}


$obtenerCategorias = obtener_categorias_data();
//var_dump($obtenerCategorias);
$palabraBusquedaFinderDi = '';
$categoriaFiltradaInfoD1 = '';
$mostrarCategoriasEnMovil = true;

if ($is_mobile == true) {
    if (isset($_GET["category"])) {
        if (!empty($_GET["category"])) {
            $categoriaData = $_GET["category"];
            $mostrarCategoriasEnMovil = false;
            //echo "Buscar por categoria : " . $categoriaData;
        }
    }
}

if (isset($_GET["tipoList"])) {
    if (!empty($_GET["tipoList"])) {
        $tipoList = $_GET["tipoList"];
    }
} else {
    if ($is_mobile == false) {
        $tipoList = 0;
    } else {
        $tipoList = 0;
    }
}

$keyWords = '';
if (isset($_GET["keywords"])) {
    if (!empty($_GET["keywords"])) {
        $keyWords = $_GET["keywords"];
        $palabraBusquedaFinderDi = 'sobre ' . $keyWords;
        //echo "Buscar por palabra : " . $keyWords;
    }
}

$categoriaData = '';
if (isset($_GET["category"])) {
    if (!empty($_GET["category"])) {
        $categoriaData = $_GET["category"];
        $categoriaFiltradaInfoD1 = $categoriaData;
        //echo "Buscar por categoria : " . $categoriaData;
    }
}


// aqui hacer el select option 
$buscarOptions = '';
foreach ($obtenerCategorias as $key => $value) {

    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }
}

$buscarOptions2 = '';
$buscarOptions2Movil = '';
$contadorAllcategory = 0;
$contadorAllcategory = 0;
foreach ($obtenerCategorias as $key => $value) {

    //
    $categoriaConsultar = total_contenido_categoria($value);
    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }

    $contadorAllcategory = $contadorAllcategory + 1;

    // <span>(' . $categoriaConsultar . ')</span>
    $categoriaExperiencias = $categoriaExperiencias . '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;"  href="index.php?category=' . $value . '" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-praying-hands mr-3 text-black" style="color:#009BDF;"></i> ' . $value . ' </a></li>';
}
$categoriaExperiencias = '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;" href="index.php?category=" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-praying-hands mr-3 text-black" style="color:#009BDF;"></i> Todas las experiencias espirituales <span style="margin-right: 10px;">&nbsp;&nbsp;(' . $contadorAllcategory . ')</a></li>' . $categoriaExperiencias;



$buscarOptions2 = '<li><h4 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas las experiencias espirituales <span>(' . $contadorAllcategory . ')</span></a></h4></li>' . $buscarOptions2;

$buscarOptions2Movil = '<h3 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas las experiencias espirituales <span>(' . $contadorAllcategory . ')</span></a></h3>' . $buscarOptions2Movil;
//echo $contadorAllcategory;

$con = conection_database();
// aqui hacer la pagincacion 


if (empty($categoriaData) && empty($keyWords)) {
    $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2')";
} else {

    //echo "Hree";
    if (!empty($keyWords) && !empty($categoriaData)) {

        // $separar palabras 
        $palabrasBusqueda = explode(' ', $keyWords);
        $finderWord = '';
        foreach ($palabrasBusqueda as $key => $value) {


            if ($value != 'el' && $value != 'la' && $value != 'lo' && $value != 'los' && $value != 'las' && $value != 'por' && $value != 'y' && $value != 'que' && $value != 'ellos' && $value != 'tu' && $value != 'yo' && $value != 'mi' && $value != 'de' && $value != ' ') {

                if (empty($finderWord)) {
                    $finderWord = $finderWord . "titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                } else {
                    $finderWord = $finderWord . " OR titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                }
            }
        }

        $finderWord = $finderWord . " OR titulo LIKE '%" . $keyWords . "%' OR titulo LIKE '%" . $keyWords . "' OR titulo LIKE '" . $keyWords . "%' OR titulo= '%" . $keyWords . "%'  OR contenido LIKE '%" . $keyWords . "%' OR contenido LIKE '%" . $keyWords . "' OR contenido LIKE '" . $keyWords . "%' OR contenido= '%" . $keyWords . "%'   OR resumen LIKE '%" . $keyWords . "%' OR resumen LIKE '%" . $keyWords . "' OR resumen LIKE '" . $keyWords . "%' OR resumen= '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "' OR adjuntos LIKE '" . $keyWords . "%' OR adjuntos= '%" . $keyWords . "%'";

        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' ) AND (" . $finderWord . ")";
    } else if (!empty($categoriaData)) {
        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' )";
    } else if (!empty($keyWords)) {
        //echo "HEre";

        // $separar palabras 
        $palabrasBusqueda = explode(' ', $keyWords);
        $finderWord = '';
        foreach ($palabrasBusqueda as $key => $value) {

            if ($value != 'el' && $value != 'la' && $value != 'lo' && $value != 'los' && $value != 'las' && $value != 'por' && $value != 'y' && $value != 'que' && $value != 'ellos' && $value != 'tu' && $value != 'yo' && $value != 'mi' && $value != 'de' && $value != ' ') {

                if (empty($finderWord)) {
                    $finderWord = $finderWord . "titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                } else {
                    $finderWord = $finderWord . " OR titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                }
            }
        }

        $finderWord = $finderWord . " OR titulo LIKE '%" . $keyWords . "%' OR titulo LIKE '%" . $keyWords . "' OR titulo LIKE '" . $keyWords . "%' OR titulo= '%" . $keyWords . "%'  OR contenido LIKE '%" . $keyWords . "%' OR contenido LIKE '%" . $keyWords . "' OR contenido LIKE '" . $keyWords . "%' OR contenido= '%" . $keyWords . "%'   OR resumen LIKE '%" . $keyWords . "%' OR resumen LIKE '%" . $keyWords . "' OR resumen LIKE '" . $keyWords . "%' OR resumen= '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "' OR adjuntos LIKE '" . $keyWords . "%' OR adjuntos= '%" . $keyWords . "%'";

        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (" . $finderWord . ")";

        //var_dump($finderWord);
    }
}

//echo $paginacion1;

$sqlPaginate1 = mysqli_query($con, $paginacion1);
$row1Paginate1 = mysqli_fetch_assoc($sqlPaginate1);

if (!empty($categoriaData)) {
    $num_total_rows = $row1Paginate1["total"];
} else {
    $num_total_rows = $contadorAllcategory;
}



if ($tipoList == 0) {
    $numItmeByPage = 15;
} else if ($tipoList == 1) {
    $numItmeByPage = 15;
}

if (isset($_GET["resultpaginatios"])) {
    if ($_GET["resultpaginatios"] > 0) {
        $numItmeByPage = $_GET["resultpaginatios"];
    }
}

$contenidoHtmlInformativo = '';
$navegacionContenido = '';
$totalResultadosShow = 0;

if ($num_total_rows > 0) {
    $page = false;

    //examino la pagina a mostrar y el inicio del registro a mostrar
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    }

    if (!$page) {
        $start = 0;
        $page = 1;
    } else {
        $start = ($page - 1) * $numItmeByPage;
    }
    //calculo el total de paginas
    $total_pages = ceil($num_total_rows / $numItmeByPage);


    //pongo el numero de registros total, el tamano de pagina y la pagina que se muestra
    //echo '<h3>Numero de articulos: '.$num_total_rows.'</h3>';
    //echo '<h3>En cada pagina se muestra '.$numItmeByPage.' articulos ordenados por fecha en formato descendente.</h3>';
    //echo '<h3>Mostrando la pagina '.$page.' de ' .$total_pages.' paginas.</h3>';

    //echo $total_pages;

    $diaActual = date('N');





    $paginacion2 = "SELECT * FROM `contenidos` WHERE ( destacados='" . $diaActual . "' OR destacados LIKE '%" . $diaActual . "' OR destacados LIKE '" . $diaActual . "%' OR destacados LIKE '%" . $diaActual . "%') AND status='1' AND (tipo='1' OR tipo='2') ORDER BY posicionamiento ASC ";






    $sqlPaginate2 = mysqli_query($con, $paginacion2);
    $categoriasExistentes = array();


    $conteoPaginasMostradas = 0;
    $paginaActualUsuario = $page;
    $paginacionRequerida = $numItmeByPage;
    if ($paginaActualUsuario == 1) {
        $mostrarDespuesDe = 0;
    } else {
        $mostrarDespuesDe = ($paginaActualUsuario - 1) * $paginacionRequerida;
    }

    $mostrarHasta = $mostrarDespuesDe + $paginacionRequerida;
    $mostrarHasta =  $mostrarHasta - 1;

    // echo $mostrarDespuesDe;
    // echo $mostrarHasta;
    // echo $inicioMuestreo = 0;

    while ($row1Paginate2 = mysqli_fetch_assoc($sqlPaginate2)) {

        if (1 == 1) {

            $icono = '';
            if ($row1Paginate2["tipo"] == 1) {

                $categoria1 = '<p><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion"] . '</p>';
                $categoria2 = '<li style="margin-bottom: 0px;"><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion"] . '</li>';

                $categoria1 = '';
                $categoria2 = '';

                $numberRand = rand(7, 10);
                $icono = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
                $icono22 = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
            } elseif ($row1Paginate2["tipo"] == 2) {


                $categoria1 = '<p><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion2"] . '</p>';
                $categoria2 = '<li style="margin-bottom: 0px;"><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion2"] . '</li>';

                $categoria1 = '';
                $categoria2 = '';

                $numberRand = rand(3, 6);
                $icono = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
                $icono22 = '<div  style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
            }

            if (!empty($row1Paginate2["imagen_portada"])) {
                $imagenPortada = '<img style="border-radius: 7px;" src="' . $row1Paginate2["imagen_portada"] . '" alt="Book">';
                $imagenPortada2 = $row1Paginate2["imagen_portada"];
            } else {
                $rutaDefault = 'images/default.jpg';
                $imagenPortada2 = 'images/default.jpg';
                $imagenPortada = '<img style="border-radius: 7px;" src="' . $rutaDefault . '" alt="Book">';
            }


            $titule = $row1Paginate2["titulo"];
            $author = '';
            $author2 = '';
            if (!empty($row1Paginate2["author"])) {
                $wordAutho = 'Autor';
                $author = '<p><strong>' . $wordAutho . ':</strong> ' . $row1Paginate2["author"] . '</p>';

                $author2 = '<li style="margin-bottom: 0px;"><strong>' . $wordAutho . ':</strong> ' . $row1Paginate2["author"] . '</li>';
            }

            $fechaPublicacion = '';
            $fechaPublicacion2 = '';
            if (!empty($row1Paginate2["fecha_contenido"]) && $row1Paginate2["fecha_contenido"] != '0000-00-00 00:00:00') {
                $fechaPublicacion = '<p><strong>Publicado :</strong> ' . date('Y-m-d', strtotime($row1Paginate2["fecha_contenido"])) . '</p>';
                $fechaPublicacion2 = '<li style="margin-bottom: 0px;"><strong>Publicado :</strong> ' . date('Y-m-d', strtotime($row1Paginate2["fecha_contenido"])) . '</li>';
            }


            //echo $fechaPublicacion;
            //echo "<br>";

            $mainContent = $row1Paginate2["resumen"];
            $pqueDescription = $mainContent;


            if ($row1Paginate2["tipo"] == 1) {
                $preguntarCategoria = $row1Paginate2["tipo_publicacion"];
            } else if ($row1Paginate2["tipo"] == 2) {

                $preguntarCategoria = $row1Paginate2["tipo_publicacion2"];
            }

            if (!empty($preguntarCategoria)) {


                $preguntarCuantosExistenConLaMismaCategoria = total_contenido_categoria($preguntarCategoria);
            }

            if (1 == 2) {

                if (in_array($preguntarCategoria, $categoriasExistentes)) {
                    //echo "El string '' está presente en el array.";

                } else {
                    $inicioMuestreo = $inicioMuestreo + 1;
                    $totalResultadosShow = $totalResultadosShow + 1;
                    $conteoPaginasMostradas = $conteoPaginasMostradas + 1;
                    array_push($categoriasExistentes, $preguntarCategoria);

                    // crear contenido 
                    if ($tipoList == 0) {
                        if (!empty($row1Paginate2["imagen_portada"])) {
                            $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $row1Paginate2["imagen_portada"] . '" alt="Book">';
                            $imagenPortada2 = $row1Paginate2["imagen_portada"];
                        } else {
                            $rutaDefault = 'images/default.jpg';
                            $imagenPortada2 = 'images/default.jpg';
                            $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $rutaDefault . '" alt="Book">';
                        }

                        // ' . $icono . '
                        /*
                <ul>
                                                                ' . $author2 . '
                                                                ' . $fechaPublicacion2 . '
                                                                ' . $categoria2 . '
                                                            </ul>
                                                            */


                        $numeroCaracteres = '210';
                        $conteoTitulo = strlen($row1Paginate2["titulo"]);
                        // echo $row1Paginate2["titulo"];
                        // echo "<br>";
                        if ($conteoTitulo <= 30) {
                            //$numeroCaracteres = '180';
                            $titule = $preguntarCategoria . '<p style="margin-bottom: 9px;">&nbsp;</p>';
                        } else {
                            //$numeroCaracteres = '180';

                        }
                        $titule = $preguntarCategoria;

                        $contenidoHtmlInformativo = $contenidoHtmlInformativo . '
                
            
                
                                <li class="zoom" onclick="redireccionarInfo(\'' . 'index.php?category=' . $preguntarCategoria . '\')" style="box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border-radius:25px;padding: 0px;cursor:pointer;border-left-width: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;">
                                                
                                            
                                                
                                            
                                                <div class="single-book-box" style="padding-top: 0px;height: 425px;">

                                                ' . $imagenPortada . '


                                                    <div class="post-detail" style="border-radius: 7px;padding-top: 15px;z-index: 2;padding-bottom: 0px;background: transparent;">
                                                    
                                                
                                                    
                                                        <header class="entry-header" style="width: 90%;margin-left: 5%;margin-right: 5%;height: 45px;word-break:break-word;">
                                                            <h3 class="entry-title" style="margin-bottom: 5px;text-align: center;"><a style="color:#009BDF;word-break:break-word;" href="index.php?category=' . $preguntarCategoria . '">' . $titule . '</a></h3>
                                                            
                                                        </header>
                                                        <div class="entry-content" style="word-break:break-word;margin-top: 5px;height: 100px;">
                                                        <p style="margin-bottom: 0px;word-break:break-word;text-align:justify;">
                                                            ' . (strlen($pqueDescription) > $numeroCaracteres ? substr($pqueDescription, 0, $numeroCaracteres) . "..." : $pqueDescription) . '
                                                            </p>
                                                        </div>
                                                        <footer class="entry-footer" style="margin-bottom:20px">
                                                        <div class="d-flex justify-content-center text-center">

                                                            <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="index.php?category=' . $preguntarCategoria . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                            </div>
                                                        </footer>
                                                    </div>
                                                </div>
                                                
                                            </li>
                
                                            ';
                    } else if ($tipoList == 1) {

                        $maximoWid = '260px';
                        if ($is_mobile == true) {
                            $maximoWid = '100%';
                        }
                        // ' . $icono22 . '

                        $imagenSeccion = ' <div class="post-thumbnail" style="display: inline-block;margin: -10px 0 -10px -10px">
                                                    
                                                        <a href="index.php?category=' . $preguntarCategoria . '"><img style="max-width: ' . $maximoWid . ';border-radius: 7px;max-height: 180px;" alt="Book" src="' . $imagenPortada2 . '"></a>                                                                 </div>';



                        /*
                                                        <ul>
                                                                            ' . $author2 . '
                                                                            ' . $fechaPublicacion2 . '
                                                                            ' . $categoria2 . '
                                                                        </ul>
                                                        */
                        $contenidoHtmlInformativo = $contenidoHtmlInformativo . '<article class="zoom2" onclick="redireccionarInfo(\'' . 'index.php?category=' . $preguntarCategoria . '\')" style="cursor:pointer;width: 98%;"> 
                                                <div class="single-book-box" style="margin-bottom: 15px;border-radius: 15px;padding-bottom: 0px;box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border: 0px solid #f3f3f3;padding-top: 20px;background: white;">                                                
                                                
                                                    <div class="post-detail" style="max-height: ' . $hexigMAcima . ';border-radius: 27px;width: 100%;right: 0px;position: relative;top: 0px;transform: initial;z-index: 2;padding-top: 10px;">
                                                        <div class="row">
                                                        <div class="col-lg-4 col-12"> <div class="d-flex justify-content-center">' . $imagenSeccion . '</div></div>
                                                        <div class="col-lg-8 col-12"> 
                                                            
                                                            <header class="entry-header">
                                                                <div class="row">
                                                                    <div class="col-sm-12" style="padding-left: 0px;">
                                                                        <h3 class="entry-title" style="margin-bottom: 5px;"><a style="color:#009BDF;" href="index.php?category=' . $preguntarCategoria . '">' . $titule . '</a></h3>
                                                                        
                                                                    </div>
                                                                
                                                                </div>
                                                            </header>
                                                            <div class="entry-content" style="word-break:break-word;height: 60px;text-align:justify;">
                                                           ' . (strlen($pqueDescription) > 260 ? substr($pqueDescription, 0, 260) . "..." : $pqueDescription) . '
                                                            </div>
                                                            <footer class="entry-footer" >
                                                            <br>
                                                                <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="index.php?category=' . $preguntarCategoria . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                            </footer>

                                                            <div class="clear"></div>
                                                        </div>
                                                        
                                                        </div>
                                                    
                                                        
                                                    </div>
                                                
                                                </div>
                                            </article>';
                    }
                }
                //var_dump($categoriasExistentes);
            } else {
                $inicioMuestreo = $inicioMuestreo + 1;
                $totalResultadosShow = $totalResultadosShow + 1;
                $conteoPaginasMostradas = $conteoPaginasMostradas + 1;

                if ($tipoList == 0) {
                    if (!empty($row1Paginate2["imagen_portada"])) {
                        $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $row1Paginate2["imagen_portada"] . '" alt="Book">';
                        $imagenPortada2 = $row1Paginate2["imagen_portada"];
                    } else {
                        $rutaDefault = 'images/default.jpg';
                        $imagenPortada2 = 'images/default.jpg';
                        $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $rutaDefault . '" alt="Book">';
                    }

                    // ' . $icono . '
                    /*
                <ul>
                                                                ' . $author2 . '
                                                                ' . $fechaPublicacion2 . '
                                                                ' . $categoria2 . '
                                                            </ul>
                                                            */


                    $numeroCaracteres = '210';
                    $conteoTitulo = strlen($row1Paginate2["titulo"]);
                    // echo $row1Paginate2["titulo"];
                    // echo "<br>";
                    if ($conteoTitulo <= 30) {
                        //$numeroCaracteres = '180';
                        $titule = $row1Paginate2["titulo"] . '<p style="margin-bottom: 9px;">&nbsp;</p>';
                    } else {
                        //$numeroCaracteres = '180';

                    }
                    $titule = $row1Paginate2["titulo"];

                    $contenidoHtmlInformativo = $contenidoHtmlInformativo . '
                
                
                
                                <li class="zoom" onclick="redireccionarInfo(\'' . 'post.php?id=' . $row1Paginate2["id"] . '\')" style="box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border-radius:25px;padding: 0px;cursor:pointer;border-left-width: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;">
                                                
                                            
                                                
                                            
                                                <div class="single-book-box" style="padding-top: 0px;height: 425px;">

                                                ' . $imagenPortada . '


                                                    <div class="post-detail" style="border-radius: 7px;padding-top: 15px;z-index: 2;padding-bottom: 0px;background: transparent;">
                                                    
                                                
                                                    
                                                        <header class="entry-header" style="width: 90%;margin-left: 5%;margin-right: 5%;height: 45px;word-break:break-word;">
                                                            <h3 class="entry-title" style="margin-bottom: 5px;text-align: center;"><a style="color:#009BDF;word-break:break-word;" href="post.php?id=' . $row1Paginate2["id"] . '">' . $titule . '</a></h3>
                                                            
                                                        </header>
                                                        <div class="entry-content" style="word-break:break-word;margin-top: 5px;height: 100px;">
                                                        <p style="margin-bottom: 0px;word-break:break-word;text-align:justify;">
                                                            ' . (strlen($pqueDescription) > $numeroCaracteres ? substr($pqueDescription, 0, $numeroCaracteres) . "..." : $pqueDescription) . '
                                                            </p>
                                                        </div>
                                                        <footer class="entry-footer" style="margin-bottom:20px">
                                                        <div class="d-flex justify-content-center text-center">

                                                            <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="post.php?id=' . $row1Paginate2["id"] . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                            </div>
                                                        </footer>
                                                    </div>
                                                </div>
                                                
                                            </li>
                
                                            ';
                } else if ($tipoList == 1) {

                    $maximoWid = '260px';
                    if ($is_mobile == true) {
                        $maximoWid = '100%';
                    }
                    // ' . $icono22 . '

                    $imagenSeccion = ' <div class="post-thumbnail" style="display: inline-block;margin: -10px 0 -10px -10px">
                                                    
                                                        <a href="post.php?id=' . $row1Paginate2["id"] . '"><img style="max-width: ' . $maximoWid . ';border-radius: 7px;max-height: 180px;" alt="Book" src="' . $imagenPortada2 . '"></a>                                                                 </div>';



                    /*
                                                        <ul>
                                                                            ' . $author2 . '
                                                                            ' . $fechaPublicacion2 . '
                                                                            ' . $categoria2 . '
                                                                        </ul>
                                                        */
                    $contenidoHtmlInformativo = $contenidoHtmlInformativo . '<article class="zoom2" onclick="redireccionarInfo(\'' . 'post.php?id=' . $row1Paginate2["id"] . '\')" style="cursor:pointer;width: 98%;"> 
                                                <div class="single-book-box" style="margin-bottom: 15px;border-radius: 15px;padding-bottom: 0px;box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border: 0px solid #f3f3f3;padding-top: 20px;background: white;">                                                
                                                
                                                    <div class="post-detail" style="max-height: ' . $hexigMAcima . ';border-radius: 27px;width: 100%;right: 0px;position: relative;top: 0px;transform: initial;z-index: 2;padding-top: 10px;">
                                                        <div class="row">
                                                        <div class="col-lg-4 col-12"> <div class="d-flex justify-content-center">' . $imagenSeccion . '</div></div>
                                                        <div class="col-lg-8 col-12"> 
                                                            
                                                            <header class="entry-header">
                                                                <div class="row">
                                                                    <div class="col-sm-12" style="padding-left: 0px;">
                                                                        <h3 class="entry-title" style="margin-bottom: 5px;"><a style="color:#009BDF;" href="post.php?id=' . $row1Paginate2["id"] . '">' . $titule . '</a></h3>
                                                                        
                                                                    </div>
                                                                
                                                                </div>
                                                            </header>
                                                            <div class="entry-content" style="word-break:break-word;height: 60px;text-align:justify;">
                                                            ' . (strlen($pqueDescription) > 260 ? substr($pqueDescription, 0, 260) . "..." : $pqueDescription) . '
                                                            </div>
                                                            <footer class="entry-footer" >
                                                            <br>
                                                                <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="post.php?id=' . $row1Paginate2["id"] . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                            </footer>

                                                            <div class="clear"></div>
                                                        </div>
                                                        
                                                        </div>
                                                    
                                                        
                                                    </div>
                                                
                                                </div>
                                            </article>';
                }
            }
        } else {
            $inicioMuestreo = $inicioMuestreo + 1;

            if ($row1Paginate2["tipo"] == 1) {
                $preguntarCategoria = $row1Paginate2["tipo_publicacion"];
            } else if ($row1Paginate2["tipo"] == 2) {

                $preguntarCategoria = $row1Paginate2["tipo_publicacion2"];
            }

            if (!empty($preguntarCategoria)) {


                $preguntarCuantosExistenConLaMismaCategoria = total_contenido_categoria($preguntarCategoria);
            }
            if ($preguntarCuantosExistenConLaMismaCategoria > 1 && empty($categoriaData)) {

                if (in_array($preguntarCategoria, $categoriasExistentes)) {
                    //echo "El string '' está presente en el array.";
                } else {
                    array_push($categoriasExistentes, $preguntarCategoria);
                }
            }
        }
    }




    /*
    
    if($conteoPaginasMostradas== $numItmeByPage){
            echo "All full";
        }else {
            echo "Faltan aun documentos";
        }
    */



    //echo $contenidoHtmlInformativo;

    //echo $total_pages;


    if ($total_pages > 1) {


        //echo "Aqui";
        if ($page != 1) {
            $anteriorPage = '<a style="border-style: solid;border-color: #116ed1;border-width: 1px;padding: 10px;color: white;" class="prev page-numbers btn btn-primary" href="index.php?page=' . ($page - 1) . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '">Anterior</a>';
            //echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page-1).'"><span aria-hidden="true">&laquo;</span></a></li>';
        }

        $paginaActuales = '';

        for ($i = 1; $i <= $total_pages; $i++) {
            //echo $i;
            if ($page == $i) {
                $paginaActuales = $paginaActuales . '<span class="page-numbers current">' . $page . '</span>';
                //echo '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
            } else {
                $paginaActuales = $paginaActuales . '<a class="page-numbers" href="index.php?page=' . $i . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '">' . $i . '</a>';
                //echo '<li class="page-item"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
            }
        }


        if ($page != $total_pages) {
            $siguientePage = '<a style="border-style: solid;border-color: #116ed1;border-width: 1px;padding: 10px;color: white;" class="next page-numbers btn btn-primary" href="index.php?page=' . ($page + 1) . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '">Siguiente</a>';

            //echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page+1).'"><span aria-hidden="true">&raquo;</span></a></li>';
        }


        if ($is_mobile == false) {
            $navegacionContenido = '  <nav style="margin-top:20px;" class="navigation pagination text-center">
                                    <h2 class="screen-reader-text">Navegacion</h2>
                                    <div class="nav-links">
                                        ' . $anteriorPage . '
                                        ' . $paginaActuales . '                                        
                                        ' . $siguientePage . '
                                    </div>
                                </nav>';
        } else {

            $navegacionContenido = '  <nav style="margin-top:20px;" class="navigation pagination text-center">
                                    <h2 class="screen-reader-text">Navegacion</h2>
                                    <div class="nav-links">

                                        <div class="row">
                                        
                                            <div class="col-lg-4 col-4 col-md-4"> ' . $anteriorPage . '</div>
                                            <div class="col-lg-4 col-4 col-md-4">' . $paginaActuales . '   </div>
                                            <div class="col-lg-4 col-4 col-md-4">' . $siguientePage . '</div>
                                        </div>
                                       
                                                                             
                                        
                                    </div>
                                </nav>';
        }
    } else {
        $navegacionContenido = '';
    }
}

include_once 'include/ip.php';
$obterIpDelUsuario = getRealIPAfiliadoVi();
$obterIpDelUsuario = explode(',', $obterIpDelUsuario);
$obterIpDelUsuario = $obterIpDelUsuario[0];

$registraAcceso = registrar_acceeso_ip($obterIpDelUsuario, 0);

close_database($con);

// include mobile detect




?>


<!DOCTYPE html>
<html lang="es" translate="no">

<head>
    <!-- Required meta tags   -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-language" content="es_ES">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Experiencias</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="css/data_image.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="swal/sweetalert2.min.css">
    <link href="lib/fonti/css/all.css" rel="stylesheet">
    <!--load all styles -->
    <!-- Arribaema -->
    <link rel="icon" href="images/favicon.png" sizes="192x192" />


    <style>
        @import url('https://fonts.googleapis.com/css?family=Nunito');


        .slim-header.with-sidebar .slim-sidebar-menu {
            margin-right: 0px !important;
        }


        .zoom:hover {
            transform: scale(1.1);
            /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }

        .zoom2:hover {
            transform: scale(1.06);
            /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }


        .itemdonar .sidebar-nav-link:hover,
        .sidebar-nav-link:focus {
            background-color: #f8f9fa;
        }




        #breadcrumb li:last-child a {
            padding-left: 5px;
            padding-right: 5px;
        }

        .section-wrapper {
            border-radius: 15px;
        }

        .sidebar-nav-sub .nav-sub-link.active {
            background-color: #ffffff;
        }


        .margenLis {
            border-style: solid;
            border-color: #d3d3d3;
            border-width: 1.15px;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }
    </style>
    <!-- Stylesheet -->
    <link href="style.css" rel="stylesheet" type="text/css" />


    <link href="admin/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="lib/fullcall/fullcalendar.css">
    <style>
        .slim-header.with-sidebar .slim-logo {
            width: 205px;
            margin-right: 10px;
            margin-left: -340px;
        }

        .container-fluid {
            margin-right: 0px;
            margin-left: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }


        .nav-links .page-numbers.prev:hover {
            border-radius: 15px;
            background-color: #00adef;
            padding-right: 15px;
            color: #fff;
        }


        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .single-book-box {
            border: 10px solid #c2c2c2;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .books-gird .single-book-box {
            opacity: 1;
        }

        .books-gird ul li:nth-child(3n) .single-book-box .post-detail {
            left: 0px;
            right: 66px;
        }

        .books-gird .single-book-box .post-detail {
            top: 0;
            transform: none;
            width: 100%;
            left: 0;
            position: relative;
            padding: 30px 15px;
        }

        table.dataTable tbody tr {
            background-color: transparent;
        }

        .has-fixed.is-shrink .header-main {
            padding: 0px 0;
        }

        .header-main {
            padding: 2px 0;
        }

        #home-v1-header-carousel .carousel-caption {
            top: 36%;
        }

        label {
            color: #fff;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: #fff !important;
        }

        .sidebar .widget-sub-title::after {
            content: '';
        }

        input {
            border-radius: 15px;
        }

        .form-control {
            border-radius: 15px;
        }

        .single-book-box {
            padding-top: 5px;
        }

        .single-book-box .post-detail .btn {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }

        .row {
            margin-left: 0px;
            margin-right: 0px;
        }

        .form-control {
            border: 3px solid #f4f4f4 !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            height: 56px !important;
            padding: 5px 10px !important;
        }

        html {
            font-family: "Nunito" !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-weight: 500;
        }

        body {
            font-family: "Nunito" !important;
            margin: 0;
            font-weight: 500;
            color: #808080;
        }

        a,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        li,
        font {
            color: #808080;
            font-family: "Nunito" !important;
        }

        .text-dark {
            color: #808080 !important;
        }

        * {
            font-family: "Nunito";
        }

        .btnSearch:hover {
            background-color: #116ed1 !important;
            border-color: #116ed1 !important;
            color: #fff;
        }

        .btnSearch:active {
            background-color: #116ed1 !important;
            border-color: #116ed1 !important;
            color: #fff;
        }

        .books-list {
            margin-top: 100px
        }

        .nav-links .page-numbers:hover,
        .nav-links .page-numbers.current {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .nav-links .page-numbers.next:hover {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .nav-links .page-numbers.prev:hover {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .btn-primary:hover {
            background-color: #116ed1;
            border-color: #116ed1;
            color: #fff;
        }

        .nav-links .page-numbers:hover,
        .nav-links .page-numbers.current {
            color: #fff !important;
            background-color: #009BDF !important;
            border-color: #009BDF !important;
        }

        .margenLis:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-sub .nav-sub-link.active:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-item.with-sub>.sidebar-nav-link::after {
            content: '\f3d0';
            font-family: 'Ionicons';
            margin-left: auto;
            position: relative;
            opacity: 1;
            color: #000;
        }

        .linunito {
            font-family: "Nunito" !important;
            /*color: #009BDF !important;*/
            list-style-type: none;
        }

        .linunito:hover {
            font-family: "Nunito" !important;
            color: #009BDF !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" />

    <script type="text/javascript" defer>
        // (function() {
        //   var options = {
        //     whatsapp: "573194475236", // WhatsApp number
        //     call_to_action: "Contáctenos", // Call to action
        //     position: "right", // Position may be 'right' or 'left'
        //   };
        //   var proto = document.location.protocol,
        //     host = "whatshelp.io",
        //     url = proto + "//static." + host;
        //   var s = document.createElement('script');
        //   s.type = 'text/javascript';
        //   s.async = true;
        //   s.src = url + '/widget-send-button/js/init.js';
        //   s.onload = function() {
        //     WhWidgetSendButton.init(host, proto, options);
        //   };
        //   var x = document.getElementsByTagName('script')[0];
        //   x.parentNode.insertBefore(s, x);
        // })(); 
    </script>


    <link rel="manifest" href="manifest.json">




</head>

<body>

    <div class="slim-header with-sidebar" style="background-color: #fff;color:#000;">
        <div class="container-fluid">
            <div class="slim-header-left">


                <?php if ($is_mobile == true) { ?>
                    <h2 class="slim-logo"><a class="navbar-brand" href="https://diplomado.misionerosurbanosdejesucristo.org" style="font-size:2.6rem;color:#fff;">
                            <img style="max-height: 70px;" src="img/logo.png">
                        </a></h2>

                    <?php if ($is_mobile == true) { ?>
                        <a href="" id="slimSidebarMenu" class="slim-sidebar-menu" style="background-color: #fff;color: #000;margin-right: 0px !important;margin-left: 20px;"><span></span></a>
                        <font onclick="abrirMenu()" id="textoMenuShow2">&nbsp;&nbsp;Mostrar Menú</font>


                    <?php } ?>

                <?php } else { ?>
                    <h2 class="slim-logo" style="
    position: absolute;
    left: -15px;
    top: -18px;
    margin: 0px;
"><a class="navbar-brand" href="https://diplomado.misionerosurbanosdejesucristo.org" style="font-size:2.6rem;color:#fff;">
                            <img style="max-height: 70px;" src="img/logo.png">
                        </a></h2>

                <?php } ?>





            </div><!-- slim-header-left -->
            <div class="slim-header-right">


                <?php if ($is_mobile == false) { ?>

                    <li class="linunito" style="margin: 3px;"><a href="index.php">Inicio</a></li>

                    <li class="linunito" style="margin: 3px;"><a href="destacados.php">Destacados</a></li>

                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/acompanamiento/">Acompañamiento</a></li>
                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/formacion/">Formación</a></li>
                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/testimonios/">Testimonios</a></li>
                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/una-experiencia-espiritual/">Una Experiencia Espiritual</a></li>

                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/contenido-de-interes/">Contenidos de Interés</a></li>

                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/quienes-somos/">Quiénes Somos</a></li>

                    <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/blog/">Blog</a></li>
                    <li class="linunito" style="margin: 3px;"><a href="https://edu.misionerosurbanosdejesucristo.org/login.php">Acceso al Diplomado</a></li>

                    <li class="linunito" style="margin: 3px;"><a style="
    
    width: 181.08px;
    height: 47px;
    color: #025373;
    background: #FADB0A;
    padding: 12px;
    z-index: 1000;
    border-radius: 30px;
    text-align: center;
    cursor: pointer;
    transition: all 0.5s;
    margin-left: 30px;
    display: grid;
    " href="https://misionerosurbanosdejesucristo.org/donar/" target="_blank">
                            <span>
                                Quiero donar
                            </span>
                        </a></li>

                <?php } ?>




            </div><!-- header-right -->
        </div><!-- container-fluid -->
    </div><!-- slim-header -->


    <div class="slim-body">
        <div class="slim-sidebar">
            <label class="sidebar-label" style="text-transform:initial;font-size: 15px;">Menú</label>

            <ul class="nav nav-sidebar">

                <li class="sidebar-nav-item">
                    <a href="index.php" id="cuadroRoundData1" class="sidebar-nav-link ">
                        <i class="fas fa-home mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Inicio </span></a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="destacados.php" id="cuadroRoundData1" class="sidebar-nav-link ">
                        <i class="fas fa-star mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Destacados </span></a>
                </li>


                <li class="sidebar-nav-item with-sub">


                    <a href="" class="sidebar-nav-link "><i class="fas fa-school  mr-3 text-black"></i>
                        <span class="text-black" style="color: #000;font-size: 1.3rem;">Experiencias Espirituales&nbsp;&nbsp;</span></a>

                    <ul class="nav sidebar-nav-sub" style="display: block;margin-left: 0px;">
                        <?php echo $categoriaExperiencias; ?>










                    </ul>
                </li>





                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/acompanamiento/" id="cuadroRoundData2" class="sidebar-nav-link ">
                        <i class="fas fa-people-carry mr-3 text-black mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Acompañamiento </span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/formacion/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-user-graduate mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Formación</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/testimonios/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-users mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Testimonios</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/una-experiencia-espiritual/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-hands-helping mr-3 text-black mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Una Experiencia Espiritual</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/contenido-de-interes/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-video mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Contenidos de Interés</span></a>
                </li>



                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/quienes-somos/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-info-circle mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Quiénes Somos</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/blog/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-blog mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Blog</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://edu.misionerosurbanosdejesucristo.org/login.php" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-graduation-cap mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Acceso al Diplomado</span></a>
                </li>




                <div class="sidebar-nav-item" style="background-color: #FADB0A !important;color: #025373 !important;">
                    <a target="_blank" href=" https://misionerosurbanosdejesucristo.org/donar/" id="cuadroRoundData3" class="sidebar-nav-link2 " style="padding: 0 20px;height: 42px;display: flex;align-items: center;justify-content: flex-start;color: #025373;border-bottom: 1px solid #dee2e6;">
                        <i class="fas fa-hand-holding-medical mr-3 text-black" style="color: #025373 !important;"></i>
                        <span class="text-black" style="color: #025373 !important; cursor: pointer;">Quiero donar</span></a>
                </div>

















            </ul>
        </div><!-- slim-sidebar -->

        <div class="slim-mainpanel" style="background: #f0f2f7;">

            <?php if ($is_mobile == true) { ?>
                <div class="container" style="padding-top: 5px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 26px;padding-left: 5px !important;padding-right: 5px !important;padding-bottom: 5px !important;">
                <?php } else { ?>
                    <div class="container" style="padding-top: 25px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 50px;">
                    <?php } ?>




                    <div id="cuadrocentralcontenido" class="cuadrocentralcontenido" style="border-radius:15px;">

                        <div id="content" class="site-content">
                            <div id="primary" class="content-area">
                                <main id="main" class="site-main">
                                    <div class="books-media-gird">
                                        <div class="container-fluid">
                                            <div style="display:none;" class="row">
                                                <!-- Start: Search Section -->
                                                <section class="search-filters" style="margin-bottom: 20px;">
                                                    <div class="container-fluid">

                                                    </div>
                                                </section>
                                                <!-- End: Search Section -->
                                            </div>
                                            <div class="row">

                                                <?php if ($is_mobile == true) { ?>

                                                    <?php if (1 == 2 && $mostrarCategoriasEnMovil == true) { ?>
                                                        <div class="col-md-3">
                                                            <div class="widget widget_related_search open" data-accordion>
                                                                <!--<h4 class="widget-title" data-control>Búsqueda relacionada</h4>
                                             -->
                                                                <div data-accordion>
                                                                    <h1 style="margin-bottom: 1;padding-bottom: 0px;margin-bottom: 0px;" class="widget-sub-title" data-control>Experiencias</h1>
                                                                    <h6 style="margin-bottom: 10px;color:#000;">Selecciona una experiencia</h6>
                                                                    <?php echo $buscarOptions2Movil; ?>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>


                                                        </div>
                                                    <?php } ?>
                                                    <div class="col-md-9" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 20px;margin: 15px;">
                                                        <div class="filter-options margin-list">
                                                            <div class="row">


                                                                <?php if ($is_mobile == true) { ?>

                                                                    <div class="col-md-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                                                                        <form action="index.php" method="get">



                                                                            <div class="col-12" style="padding-left: 0px;margin-top: 30px;margin-bottom: 30px;">
                                                                                <h1 style="word-break: break-word;font-size:3rem;text-align:center;color:#009BDF;">Destacados </h1>
                                                                            </div>






                                                                        </form>

                                                                    </div>







                                                                <?php } else if ($is_mobile == false) { ?>


                                                                <?php } ?>
                                                                <?php if ($is_mobile == false) { ?>



                                                                <?php } ?>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12  text-center" style="bottom: 50px;top: -40;top: -50;top: -50;">
                                                            <h1 style="text-transform:initial;font-weight: 500;color:#009BDF;margin-top: 0px;margin-bottom: 0px;word-break: break-word;"><?php echo $categoriaFiltradaInfoD1; ?></h1>
                                                        </div>

                                                        <?php if (!empty($categoriaFiltradaInfoD1)) { ?>

                                                            <a onclick="regresarAtras()" style="border-radius: 25px;padding-top: 6px;padding-bottom: 6px;padding-left: 15px;padding-right: 15px;color:#fff;border-left-width: 10px;margin-left: 0px;margin-bottom: 30px;" class="btn btn-primary">Regresar a todas las categorías</a>

                                                        <?php } ?>

                                                        <div class="<?php echo ($tipoList == 0) ? 'books-gird' : 'books-list'; ?>">
                                                            <ul style="width: 100%;padding-left: 0px;">

                                                                <?php if ($totalResultadosShow > 0) { ?>
                                                                    <?php echo $contenidoHtmlInformativo; ?>
                                                                <?php } else { ?>

                                                                    Resultados no encontrados <?php echo $palabraBusquedaFinderDi; ?>
                                                                <?php } ?>

                                                            </ul>
                                                        </div>


                                                    </div>


                                                <?php } else { ?>
                                                    <div class="col-md-12" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 0px;padding-left: 25px;padding-top: 20px;padding-bottom: 20px;">
                                                        <div class="filter-options margin-list">
                                                            <div class="row">

                                                                <div class="col-12" style="padding-left: 0px;margin-top: 30px;margin-bottom: 30px;">
                                                                    <h1 style="word-break: break-word;font-size:7rem;text-align:center;color:#009BDF;">Destacados </h1>
                                                                </div>








                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12  text-center" style="bottom: 50px;top: -40;top: -50;top: -50;">
                                                            <h1 style="text-transform:initial;font-weight: 500;color:#009BDF;margin-top: 20px;margin-bottom: 10px;word-break: break-word;"><?php echo $categoriaFiltradaInfoD1; ?></h1>
                                                        </div>

                                                        <?php if (!empty($categoriaFiltradaInfoD1)) { ?>

                                                            <div class="col-md-12 col-sm-12 " style="padding-left: 0px;">
                                                                <a onclick="regresarAtras()" style="border-radius: 25px;padding-top: 6px;padding-bottom: 6px;padding-left: 15px;padding-right: 15px;color:#fff;border-left-width: 10px;margin-left: 0px;margin-bottom: 30px;" class="btn btn-primary">Regresar a todas las categorías</a>
                                                            </div>



                                                        <?php } ?>

                                                        <div class="<?php echo ($tipoList == 0) ? 'books-gird' : 'books-list'; ?>">
                                                            <ul style="width: 100%;padding-left: 0px;">

                                                                <?php if ($totalResultadosShow > 0) { ?>
                                                                    <?php echo $contenidoHtmlInformativo; ?>
                                                                <?php } else { ?>

                                                                    Resultados no encontrados <?php echo $palabraBusquedaFinderDi; ?>
                                                                <?php } ?>

                                                            </ul>
                                                        </div>

                                                    </div>


                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                </main>
                            </div>
                        </div>
                    </div>


                    </div><!-- container -->



                    <div class="slim-footer mg-t-0" style="position: static;margin-top: 40px;padding-top: 0px;bottom: 0px;width: 100%;border-top-width: 0px;top: auto;">


                        <footer class="site-footer">

                            <div class="sub-footer" style="background-color: #F6F2F0;">
                                <div class="container">
                                    <div class="row" style="text-align:center;">

                                        <div class="footer-text col-12">

                                            <div class="d-flex justify-content-center">

                                                <h3 style="color: #000;">¿Necesitas ayuda con esta Experiencia? Escríbenos</h2>



                                            </div>

                                            <div class="d-flex justify-content-center">

                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-shrink elementor-repeater-item-b682d3e" href="https://api.whatsapp.com/send/?phone=573005782938&amp;text=Hola+Misioneros%2C+%2Aquiero+conocer+información+del+contenido+de+interes+%2C+por+favor%2A.+Mi+nombre+es%3A&amp;app_absent=0" target="_blank">
                                                    <span class="elementor-screen-only"></span>
                                                    <i style="color:#fff;font-size: 30px;background-color: #00d084;border-radius: 50%;padding: 7px;" class="fab fa-whatsapp"></i> </a>

                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-envelope elementor-animation-shrink elementor-repeater-item-d6b2487" href="mailto:info@misionerosurbanosdejesucristo.org" target="_blank">
                                                    <span class="elementor-screen-only"></span>
                                                    <i style="color:#fff;font-size: 30px;background-color: #EA4335;border-radius: 50%;padding: 7px;" class="fas fa-envelope"></i> </a>
                                            </div>








                                        </div>


                                    </div>
                                </div>
                        </footer>


                        <footer class="site-footer">

                            <div class="sub-footer">
                                <div class="container">
                                    <div class="row" style="text-align:center;">




                                        <div class="footer-text col-12">

                                            <div class="d-flex justify-content-center">

                                                <a href="https://misionerosurbanosdejesucristo.org/">
                                                    <img style="text-align: center;max-height: 80px;" src="images/logo.png" alt="">
                                                </a>



                                            </div>

                                            <p><a style="text-align: center;color:#000;" target="_blank" href="https://misionerosurbanosdejesucristo.org/">Copyright © 2022 Misioneros Urbanos de Jesucristo</a></p>


                                        </div>
                                    </div>
                                </div>
                        </footer>



                    </div><!-- slim-footer -->
                </div><!-- slim-mainpanel -->
        </div><!-- slim-body -->




        <script src="lib/jquery/js/jquery.js"></script>

        <script src="lib/popper.js/js/popper.js"></script>
        <script src="lib/bootstrap/js/bootstrap.js"></script>
        <script src="lib/jquery.cookie/js/jquery.cookie.js"></script>
        <script src="lib/d3/js/d3.js"></script>
        <script src="lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>
        <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

        <script src="js/ResizeSensor.js"></script>
        <script src="js/slim.js"></script>

        <script src="swal/swalpersonalizado.js"></script>
        <script src="swal/sweetalert2.min.js"></script>


        <script>
            function regresarAtras() {


                history.back();


            }
        </script>

        <script>
            function cambiarpaginacionresultados() {
                var paginaciontotal = $("#paginaciontotal").val();

                var queryString = window.location.search;
                var busqueda = "resultpaginatios";

                var existe = queryString.indexOf(busqueda);
                if (existe == -1) {
                    // buscar si es primero o no 
                    var busqueda2 = "?";
                    var existe2 = queryString.indexOf(busqueda2);
                    if (existe2 == -1) {
                        queryString = queryString + '?resultpaginatios=' + paginaciontotal;
                    } else {
                        queryString = queryString + '&resultpaginatios=' + paginaciontotal;

                    }

                    var existe2 = queryString.indexOf('page');
                    // alert(existe2)
                    if (existe2 == -1) {
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    } else {
                        // existe pagina y debo re
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    }


                    var urlFinal = 'index.php' + queryString;
                    window.location.href = urlFinal;

                } else {


                    queryString = queryString.replace("resultpaginatios=15", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=25", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=50", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=100", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=00", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=00000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=000000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0", "resultpaginatios=" + paginaciontotal);


                    var existe2 = queryString.indexOf('page');
                    //alert(existe2)
                    if (existe2 == -1) {

                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    } else {
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    }

                    var urlFinal = 'index.php' + queryString;
                    //alert(urlFinal);
                    window.location.href = urlFinal;
                    //alert(urlFinal);
                }






            }
        </script>


        <script>
            function redireccionarInfo(infoUrl) {
                window.location.href = infoUrl;
            }

            function abrirMenu() {
                if ($("body").hasClass("show-sidebar")) {
                    //alert('1');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Mostrar Menú');
                    $("body").removeClass("show-sidebar");

                } else {
                    //alert('2');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Ocultar Menu')
                    $("body").addClass("show-sidebar");

                }
            }

            $("#slimSidebarMenu").on("click", function() {
                if ($("body").hasClass("show-sidebar")) {
                    //alert('1');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Mostrar Menú');
                    // $("body").removeClass("show-sidebar");

                } else {
                    //alert('2');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Ocultar Menu')
                    // $("body").addClass("show-sidebar");

                }
            });

            function urlAddType(tipoUrl) {

                var queryString = window.location.search;
                var busqueda = "tipoList";

                var existe = queryString.indexOf(busqueda);
                if (existe == -1) {
                    // buscar si es primero o no
                    var busqueda2 = "?";
                    var existe2 = queryString.indexOf(busqueda2);
                    if (existe2 == -1) {

                        if (tipoUrl == 1) {
                            queryString = queryString + '?tipoList=' + 1;
                        } else if (tipoUrl == 0) {
                            queryString = queryString + '?tipoList=' + 0;
                        }
                    } else {


                        if (tipoUrl == 1) {
                            queryString = queryString + '&tipoList=' + 1;
                        } else if (tipoUrl == 0) {
                            queryString = queryString + '&tipoList=' + 0;
                        }

                    }

                    var urlFinal = 'index.php' + queryString;
                    window.location.href = urlFinal;

                } else {


                    if (tipoUrl == 1) {
                        queryString = queryString.replace("tipoList=0", "tipoList=1");
                        queryString = queryString.replace("tipoList=1", "tipoList=1");

                    } else if (tipoUrl == 0) {
                        queryString = queryString.replace("tipoList=1", "tipoList=0");
                        queryString = queryString.replace("tipoList=0", "tipoList=0");

                    }



                    var urlFinal = 'index.php' + queryString;
                    window.location.href = urlFinal;
                    //alert(urlFinal);
                }



            }

            function cambiarpaginacionresultados() {
                var paginaciontotal = $("#paginaciontotal").val();

                var queryString = window.location.search;
                var busqueda = "resultpaginatios";

                var existe = queryString.indexOf(busqueda);
                if (existe == -1) {
                    // buscar si es primero o no
                    var busqueda2 = "?";
                    var existe2 = queryString.indexOf(busqueda2);
                    if (existe2 == -1) {
                        queryString = queryString + '?resultpaginatios=' + paginaciontotal;
                    } else {
                        queryString = queryString + '&resultpaginatios=' + paginaciontotal;

                    }

                    var existe2 = queryString.indexOf('page');
                    // alert(existe2)
                    if (existe2 == -1) {
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    } else {
                        // existe pagina y debo re
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    }


                    var urlFinal = 'index.php' + queryString;
                    window.location.href = urlFinal;

                } else {


                    queryString = queryString.replace("resultpaginatios=15", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=25", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=50", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=100", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=00", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=00000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=000000", "resultpaginatios=0");
                    queryString = queryString.replace("resultpaginatios=0", "resultpaginatios=" + paginaciontotal);


                    var existe2 = queryString.indexOf('page');
                    //alert(existe2)
                    if (existe2 == -1) {

                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    } else {
                        queryString = queryString.replace("page=1", "page=1");
                        queryString = queryString.replace("page=2", "page=1");
                        queryString = queryString.replace("page=3", "page=1");
                        queryString = queryString.replace("page=4", "page=1");
                        queryString = queryString.replace("page=5", "page=1");
                        queryString = queryString.replace("page=6", "page=1");
                        queryString = queryString.replace("page=7", "page=1");
                        queryString = queryString.replace("page=8", "page=1");
                        queryString = queryString.replace("page=9", "page=1");
                        queryString = queryString.replace("page=10", "page=1");
                    }

                    var urlFinal = 'index.php' + queryString;
                    //alert(urlFinal);
                    window.location.href = urlFinal;
                    //alert(urlFinal);
                }






            }
        </script>

        <div id="smart-button-container">
            <div style="text-align: center;">
                <div id="paypal-button-container"></div>
            </div>
        </div>

        <div class="modal bd-example-modal-lg" id="modalHorariosUser" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 style="text-align:center;">¿Deseas recibir notificaciones?</h2>


                    </div>
                    <div class="modal-body">

                        <div id="notification-dialog">

                            <h1 style="text-align:center;">Aprovecha al máximo nuestras expericiencias y mantente informado.</h1>
                            <div class="button-container">
                                <div class="d-flex justify-content-center">
                                    <button onclick="requestNotificationPermission()" style="border-radius: 15px;" class="form-control btn btn-primary" id="allow-button">Sí, permitir</button>
                                    <button onclick="cerrarModaLnotif()" style="border-radius: 15px;" class="form-control btn btn-primary" id="deny-button">No, gracias</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal bd-example-modal-lg" id="modalDeNotificacion" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 id="textoModalNotificaciones" style="text-align:center;color:#009BDF;word-break:break-word;"></h2>


                    </div>
                    <div class="modal-body">

                        <div id="notification_dialog_contenido">


                        </div>

                        <div id="notification_dialog_contenido_notification"></div>

                    </div>

                </div>
            </div>
        </div>

        <div class="modal bd-example-modal-lg" id="modalAgregarServiceWorker" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">

                        <div id="notification-dialog">

                            <h1 style="text-align:center;color:#009BDF;word-break:break-word;">¿Quieres agregar MUJ a tu pantalla de inicio?</h1>
                            <div class="button-container">
                                <div class="d-flex justify-content-center">
                                    <button style="border-radius: 15px;" class="accept_button_pwa form-control btn btn-primary" id="accept_button_pwa">Sí, permitir</button>
                                    <button onclick="cerrarModaLnotif2()" style="border-radius: 15px;" class="form-control btn btn-primary" id="deny_button_pwa">No, gracias</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <button id="showButtonAddToHome" class="accept_button_pwa btn btn-primary" style="cursor:pointer;position: absolute;right: 30px;border-radius: 25px;height: 35px;padding-top: 1px;padding-bottom: 1px;border-style: solid;border-color: #009BDF;top: 110px;display:none;"> Agregar a pantalla de inicio</button>

        <div class="modal bd-example-modal-lg" id="modalMostrarRecomendacionesDelDia" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <h2 style="text-align:center;color:#009BDF;word-break:break-word;">Programación del día</h2>


                    </div>
                    <div class="modal-body">

                        <div id="programacion_del_dia_semanal">


                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var mostrandoAlgunModalActualmente = 0;
        </script>



        <script>
            var SePreguntoPorAccesoAnotificaciones = sessionStorage.getItem("preguntadoPorNotificaaciones");
            var SePreguntoPorAccesoAnotificaciones2 = localStorage.getItem("preguntadoPorNotificaacionesHome");

            if (
                SePreguntoPorAccesoAnotificaciones == "" ||
                SePreguntoPorAccesoAnotificaciones == "undefined" ||
                SePreguntoPorAccesoAnotificaciones == "null" ||
                SePreguntoPorAccesoAnotificaciones == null ||
                SePreguntoPorAccesoAnotificaciones == undefined ||
                SePreguntoPorAccesoAnotificaciones == 0
            ) {
                SePreguntoPorAccesoAnotificaciones = 0;
            } else {
                SePreguntoPorAccesoAnotificaciones = SePreguntoPorAccesoAnotificaciones;
            }

            if (
                SePreguntoPorAccesoAnotificaciones2 == "" ||
                SePreguntoPorAccesoAnotificaciones2 == "undefined" ||
                SePreguntoPorAccesoAnotificaciones2 == "null" ||
                SePreguntoPorAccesoAnotificaciones2 == null ||
                SePreguntoPorAccesoAnotificaciones2 == undefined ||
                SePreguntoPorAccesoAnotificaciones2 == 0
            ) {
                SePreguntoPorAccesoAnotificaciones2 = 0;
            } else {
                SePreguntoPorAccesoAnotificaciones2 = SePreguntoPorAccesoAnotificaciones2;
            }

            var mostarPrimerMensaje = SePreguntoPorAccesoAnotificaciones2;
            var conteoIntentos = 0;
            var compatible = 0;

            // Verificar si el navegador es compatible con las notificaciones
            if ('Notification' in window) {
                // Verificar si ya se ha solicitado el permiso
                compatible = 1;
            } else {
                compatible = 0;
            }

            var seniegaingreso = 0;
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', function() {
                    navigator.serviceWorker.register('service-worker.js').then(function(registration) {

                            if (registration.active && registration.active.state === 'activated') {
                                // PWA ya instalada
                                // console.log('La PWA ya está instalada');
                                localStorage.setItem("preguntadoPorNotificaacionesHome", 1);
                                $("#showButtonAddToHome").css("display", "none");
                                if (compatible == 1) {
                                    showNotificationDialog();
                                    mostarPrimerMensaje = 1;

                                }
                            } else {
                                if (mostarPrimerMensaje == 0) {
                                    localStorage.setItem("preguntadoPorNotificaacionesHome", 0);
                                    $("#showButtonAddToHome").css("display", "none");
                                    console.log('Service Worker registrado con éxito:', registration.scope);
                                } else if (mostarPrimerMensaje == 1) {
                                    mostarPrimerMensaje = 2;
                                }


                            }


                        },
                        function(error) {
                            mostarPrimerMensaje = 1;
                            localStorage.setItem("preguntadoPorNotificaacionesHome", 1);
                            $("#showButtonAddToHome").css("display", "none");
                            if (compatible == 1) {
                                showNotificationDialog();
                            }
                            console.log('Fallo al registrar el Service Worker:', error);
                        });
                });
            }
            //alert(mostarPrimerMensaje);

            if (mostarPrimerMensaje == 0) {
                mostarPrimerMensaje = 1;
                localStorage.setItem("preguntadoPorNotificaacionesHome", 2);
                $("#showButtonAddToHome").css("display", "none");
                let deferredPrompt;
                var btnAdd = document.getElementById("accept_button_pwa");
                $("#modalAgregarServiceWorker").modal('hide');

                window.addEventListener('beforeinstallprompt', (e) => {
                    // Prevent Chrome 67 and earlier from automatically showing the prompt
                    e.preventDefault();
                    // Stash the event so it can be triggered later.
                    deferredPrompt = e;
                    if (seniegaingreso == 0) {
                        $("#modalAgregarServiceWorker").modal('show');
                    }

                    mostrandoAlgunModalActualmente = 1;
                    //alert(seniegaingreso);
                });

                btnAdd.addEventListener('click', (e) => {
                    // hide our user interface that shows our A2HS button
                    $("#modalAgregarServiceWorker").modal('hide');
                    // Show the prompt
                    deferredPrompt.prompt();
                    // Wait for the user to respond to the prompt
                    deferredPrompt.userChoice
                        .then((choiceResult) => {
                            if (choiceResult.outcome === 'accepted') {
                                localStorage.setItem("preguntadoPorNotificaacionesHome", 1);
                                $("#showButtonAddToHome").css("display", "none");
                                console.log('User accepted the A2HS prompt');
                                $("#modalAgregarServiceWorker").modal('hide');
                                if (compatible == 1) {
                                    showNotificationDialog();
                                }
                            } else {
                                seniegaingreso = 1;
                                console.log('User dismissed the A2HS prompt');
                                localStorage.setItem("preguntadoPorNotificaacionesHome", 2);
                                $("#showButtonAddToHome").css("display", "none");
                                $("#modalAgregarServiceWorker").modal('hide');
                                if (compatible == 1) {
                                    showNotificationDialog();
                                }
                            }
                            deferredPrompt = null;
                        });
                });
            } else if (mostarPrimerMensaje == 2) {

                mostarPrimerMensaje = 1;
                localStorage.setItem("preguntadoPorNotificaacionesHome", 2);
                $("#showButtonAddToHome").css("display", "none");
                let deferredPrompt;
                var btnAdd = document.getElementById("showButtonAddToHome");
                $("#modalAgregarServiceWorker").modal('hide');

                window.addEventListener('beforeinstallprompt', (e) => {
                    // Prevent Chrome 67 and earlier from automatically showing the prompt
                    e.preventDefault();
                    // Stash the event so it can be triggered later.
                    deferredPrompt = e;
                    mostrandoAlgunModalActualmente = 1;
                    $("#showButtonAddToHome").css('display', 'block');
                });

                btnAdd.addEventListener('click', (e) => {
                    // hide our user interface that shows our A2HS button
                    $("#modalAgregarServiceWorker").modal('hide');
                    // Show the prompt
                    deferredPrompt.prompt();
                    // Wait for the user to respond to the prompt
                    deferredPrompt.userChoice
                        .then((choiceResult) => {
                            if (choiceResult.outcome === 'accepted') {
                                localStorage.setItem("preguntadoPorNotificaacionesHome", 1);
                                $("#showButtonAddToHome").css("display", "none");
                                console.log('User accepted the A2HS prompt');
                                $("#modalAgregarServiceWorker").modal('hide');
                                if (compatible == 1) {
                                    showNotificationDialog();
                                }
                            } else {
                                console.log('User dismissed the A2HS prompt');
                                localStorage.setItem("preguntadoPorNotificaacionesHome", 2);
                                $("#showButtonAddToHome").css("display", "block");
                                $("#modalAgregarServiceWorker").modal('hide');
                                if (compatible == 1) {
                                    showNotificationDialog();
                                }
                            }
                            deferredPrompt = null;
                        });
                });
            }




            function cerrarModaLnotif2() {
                $("#modalAgregarServiceWorker").modal('hide');
                localStorage.setItem("preguntadoPorNotificaacionesHome", 2);
                if (compatible == 1) {
                    showNotificationDialog();
                }
            }
        </script>


        <script src="https://www.gstatic.com/firebasejs/9.14.0/firebase-app-compat.js"></script>
        <script src="https://www.gstatic.com/firebasejs/9.14.0/firebase-messaging-compat.js"></script>


        <script>
            function obtenerTokenNotificacions() {
                conteoIntentos = conteoIntentos + 1;

                if (conteoIntentos <= 2) {

                    try {
                        // TODO: Add SDKs for Firebase products that you want to use
                        // https://firebase.google.com/docs/web/setup#available-libraries

                        // Your web app's Firebase configuration
                        const firebaseConfig = {
                            apiKey: "AIzaSyAx9dP8CYq86yGOa8npfBFqS7UGYZTm1HE",
                            authDomain: "misioneros-urbanos.firebaseapp.com",
                            projectId: "misioneros-urbanos",
                            storageBucket: "misioneros-urbanos.appspot.com",
                            messagingSenderId: "616290574309",
                            appId: "1:616290574309:web:bdab034518ffb5f292590c"
                        };

                        // Initialize Firebase
                        const app = firebase.initializeApp(firebaseConfig);

                        // Initialize Firebase Cloud Messaging and get a reference to the service
                        const messaging = firebase.messaging();

                        try {
                            messaging.getToken({
                                vapidKey: "BBR-Aaz0-3vZGF7ub0iFV7I20tXeScOWbQ2cQUAHovDLv8onOvMwD8izis62SCER2ggPnwG1OOVU3jyNZJyvkEU"
                            }).then((currentToken) => {
                                if (currentToken) {
                                    console.log(currentToken);
                                    cerrarModaLnotif()
                                    var topic = 'muj';
                                    var fcm_server_key = 'AAAAj33H3-U:APA91bF26FjKLAs-30g2rTkQmspmgpP7BOFBg_xYU8laCxLWXzy0P4EbPIk3MbYg_tnLtrL2HEmn9xslu-VX-1aHiCBSU2JnOuzik5_fvOqTqeEBzGdDyucHgn-yQPRORuuySUQSRwVW';
                                    fetch('https://iid.googleapis.com/iid/v1/' + currentToken + '/rel/topics/' + topic, {
                                        method: 'POST',
                                        headers: new Headers({
                                            'Authorization': 'key=' + fcm_server_key
                                        })
                                    }).then(response => {
                                        if (response.status < 200 || response.status >= 400) {
                                            throw 'Error subscribing to topic: ' + response.status + ' - ' + response.text();
                                        }
                                        console.log('Subscribed to "' + topic + '"');
                                    }).catch(error => {
                                        //console.error(error);
                                        showNotificationDialog();
                                    })

                                } else {
                                    //alert("Error  try");
                                    //showNotificationDialog();
                                    showNotificationDialog();
                                }
                            }).catch((err) => {
                                // alert("Error"+ err);
                                showNotificationDialog();
                                //showNotificationDialog();
                            });

                        } catch (error) {
                            showNotificationDialog();
                        }

                        messaging.onMessage((payload) => {
                            //console.log(payload);

                            if (typeof payload.data.click_action != 'undefined') {
                                cargarModalNotificacion(payload.data.title, payload.data.body, payload.data.click_action);
                            } else {
                                cargarModalNotificacion(payload.data.title, payload.data.body, '');
                            }



                        });

                    } catch (error) {
                        showNotificationDialog();
                    }




                }

            }





            function cargarModalNotificacion(title, body, notification) {
                $("#modalDeNotificacion").modal('show');
                $("#textoModalNotificaciones").html('').html(title);
                $("#notification_dialog_contenido").html('').html(body);

                if (notification != '') {
                    notifications = '<a style="color:#009BDF;word-break:break-word;text-align:center;" target="_blank" href="' + notification + '">' + notification + '</a>';
                    $("#notification_dialog_contenido_notification").html('').html(notifications);
                    $("#notification_dialog_contenido_notification").css('display', 'block');
                } else {

                    $("#notification_dialog_contenido_notification").html('').html('');
                    $("#notification_dialog_contenido_notification").css('display', 'none');
                }


            }

            function showNotificationDialog() {

                if (SePreguntoPorAccesoAnotificaciones == 0) {
                    sessionStorage.setItem("preguntadoPorNotificaaciones", 1);
                    try {
                        // TODO: Add SDKs for Firebase products that you want to use
                        // https://firebase.google.com/docs/web/setup#available-libraries

                        // Your web app's Firebase configuration
                        const firebaseConfig = {
                            apiKey: "AIzaSyAx9dP8CYq86yGOa8npfBFqS7UGYZTm1HE",
                            authDomain: "misioneros-urbanos.firebaseapp.com",
                            projectId: "misioneros-urbanos",
                            storageBucket: "misioneros-urbanos.appspot.com",
                            messagingSenderId: "616290574309",
                            appId: "1:616290574309:web:bdab034518ffb5f292590c"
                        };

                        // Initialize Firebase
                        const app = firebase.initializeApp(firebaseConfig);

                        // Initialize Firebase Cloud Messaging and get a reference to the service
                        const messaging = firebase.messaging();

                        try {
                            messaging.getToken({
                                vapidKey: "BBR-Aaz0-3vZGF7ub0iFV7I20tXeScOWbQ2cQUAHovDLv8onOvMwD8izis62SCER2ggPnwG1OOVU3jyNZJyvkEU"
                            }).then((currentToken) => {
                                if (currentToken) {
                                    console.log(currentToken);
                                    cerrarModaLnotif()
                                    var topic = 'muj';
                                    var fcm_server_key = 'AAAAj33H3-U:APA91bF26FjKLAs-30g2rTkQmspmgpP7BOFBg_xYU8laCxLWXzy0P4EbPIk3MbYg_tnLtrL2HEmn9xslu-VX-1aHiCBSU2JnOuzik5_fvOqTqeEBzGdDyucHgn-yQPRORuuySUQSRwVW';
                                    fetch('https://iid.googleapis.com/iid/v1/' + currentToken + '/rel/topics/' + topic, {
                                        method: 'POST',
                                        headers: new Headers({
                                            'Authorization': 'key=' + fcm_server_key
                                        })
                                    }).then(response => {
                                        if (response.status < 200 || response.status >= 400) {
                                            throw 'Error subscribing to topic: ' + response.status + ' - ' + response.text();
                                        }
                                        console.log('Subscribed to "' + topic + '"');
                                    }).catch(error => {
                                        //console.error(error);
                                        $("#modalHorariosUser").modal('show');
                                        mostrandoAlgunModalActualmente = 1;
                                    })

                                } else {
                                    //alert("Error  try");
                                    //showNotificationDialog();
                                    $("#modalHorariosUser").modal('show');
                                    mostrandoAlgunModalActualmente = 1;
                                }
                            }).catch((err) => {
                                // alert("Error"+ err);
                                $("#modalHorariosUser").modal('show');
                                mostrandoAlgunModalActualmente = 1;
                                //showNotificationDialog();
                            });

                        } catch (error) {
                            $("#modalHorariosUser").modal('show');
                            mostrandoAlgunModalActualmente = 1;
                        }

                        messaging.onMessage((payload) => {
                            //console.log(payload);

                            if (typeof payload.data.click_action != 'undefined') {
                                cargarModalNotificacion(payload.data.title, payload.data.body, payload.data.click_action);
                            } else {
                                cargarModalNotificacion(payload.data.title, payload.data.body, '');
                            }



                        });

                    } catch (error) {
                        $("#modalHorariosUser").modal('show');
                    }
                }





            }
        </script>
        <script>
            function cerrarModaLnotif() {
                $("#modalHorariosUser").modal('hide');
            }

            function requestNotificationPermission() {
                //cerrarModaLnotif()

                if (Notification.permission !== 'granted') {
                    Notification.requestPermission().then(function(permission) {
                        if (permission === 'granted') {
                            obtenerTokenNotificacions();
                            // Puedes enviar el token de registro o hacer otras acciones aquí
                        } else {
                            cargar_swal('info', 'Permiso de notificaciones denegado.', 'Advertencia');
                        }
                    });
                }
            }
        </script>

        <script>
            var iniciarModalRecomendaciones = sessionStorage.getItem("preguntarPorRecomendaciones");
            if (
                iniciarModalRecomendaciones == "" ||
                iniciarModalRecomendaciones == "undefined" ||
                iniciarModalRecomendaciones == "null" ||
                iniciarModalRecomendaciones == null ||
                iniciarModalRecomendaciones == undefined ||
                iniciarModalRecomendaciones == 0
            ) {
                iniciarModalRecomendaciones = 0;
            } else {
                iniciarModalRecomendaciones = SePreguntoPorAccesoAnotificaciones;
            }


            if (iniciarModalRecomendaciones == 0 && mostrandoAlgunModalActualmente == 0) {
                if (window.matchMedia('(display-mode: standalone)').matches) {
                    // alert('La aplicación se está ejecutando como PWA.');
                    mostarmodalconmisteriodeldia(1);
                } else {
                    //alert('La aplicación se está ejecutando en el navegador.');
                    mostarmodalconmisteriodeldia(2);
                }
            }



            function mostarmodalconmisteriodeldia(tipo) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "admin/ajax/contenidoDataRecomendaciones.php",
                    data: {
                        "tipo": tipo,
                        "obtenerRecomendacionParaUsuario": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {

                            if (loginData.existecontenido > 0) {
                                //alert('ok');
                                var contenidoDiario = '';
                                sessionStorage.setItem("preguntarPorRecomendaciones", 1);
                                for (var i = 0; i < loginData.datoscotenido.length; i++) {
                                    var item = loginData.datoscotenido[i];
                                    //console.log(item)


                                    var ExtraHorario = '';
                                    if (typeof item.horario != 'undefined' && item.horario != '' && item.horario != undefined && item.horario != 'undefined') {
                                        ExtraHorario = ` <p style="word-break: break-word;color:#009BDF;">Hora: ${item.horario}</p>`;
                                    }

                                    contenidoDiario = contenidoDiario + `<a href="post.php?id=${item.id}"><div style="box-shadow: rgba(0 107 174) 0px 1px 4px;padding: 15px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 25px;word-break: break-word;text-align: justify;cursor:pointer;" class="zoom2 table-tabs" id="responsiveTabs">
                                                                                            <h4 style="margin-bottom: 0px;word-break: break-word;color:#009BDF;">${item.titulo}</h5>    <br>

                                                                                           ${ExtraHorario}
                                                                                            

                                                                                            <p style="word-break: break-word;">${item.resumen}</p>

                                                                                            
                                                                                            </div></a>`;
                                }


                                $('#modalMostrarRecomendacionesDelDia').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                })

                                $("#programacion_del_dia_semanal").html('').html(contenidoDiario);
                            }


                        }
                    }
                });
            }
        </script>







</body>

</html>