importScripts(
  "https://www.gstatic.com/firebasejs/9.14.0/firebase-app-compat.js"
);
importScripts(
  "https://www.gstatic.com/firebasejs/9.14.0/firebase-messaging-compat.js"
);

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAx9dP8CYq86yGOa8npfBFqS7UGYZTm1HE",
  authDomain: "misioneros-urbanos.firebaseapp.com",
  projectId: "misioneros-urbanos",
  storageBucket: "misioneros-urbanos.appspot.com",
  messagingSenderId: "616290574309",
  appId: "1:616290574309:web:bdab034518ffb5f292590c",
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);

// Initialize Firebase Cloud Messaging and get a reference to the service
const messaging = firebase.messaging();
messaging.onBackgroundMessage(function (payload) {
  // Customize notification here
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    icon: payload.data.icon,
    body: payload.data.body,
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
  self.addEventListener("notificationclick", function (event) {
    const clickedNotification = event.notification;
    clickedNotification.close();
    event.waitUntil(clients.openWindow(payload.data.click_action));
  });

});