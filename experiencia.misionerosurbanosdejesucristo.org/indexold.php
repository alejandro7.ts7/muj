<?php
session_start();
require_once('include/functions_connect.php');
//var_dump($informacionContenido);
// aqui hacer el select option 
$obtenerCategorias = obtener_categorias_data();
$buscarOptions = '';
foreach ($obtenerCategorias as $key => $value) {

    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }
}
?>
<?php require_once('htmlinclude/head.php'); ?>

<style>
    @media (max-width: 767px) {
        .layout-v3 #home-v1-header-carousel .filter-box .form-group {
            width: 100%;
        }
    }

    @media (min-width: 768px) {
        .layout-v3 #home-v1-header-carousel .filter-box .form-group {
            width: 50%;
        }
    }
</style>

<body class="layout-v3">

    <!-- Start: Header Section -->
    <?php require_once('htmlinclude/header.php'); ?>



    <!-- Start: Slider Section -->
    <div data-ride="carousel" class="carousel slide" id="home-v1-header-carousel">
        <!-- Carousel slides -->
        <div class="carousel-inner">
            <div class="item active">
                <figure>
                    <img alt="Home Slide" src="images/header-slider/home-v3/header-slide.jpg" />
                </figure>
                <div class="container">
                    <div class="carousel-caption">
                        <h2>Que Estas Buscando ?</h2>
                        <p></p>
                        <div class="filter-box">
                            <form action="colecciones.php" class="banner-filter-box" method="get">
                                <div class="form-group">
                                    <label class="sr-only" for="keywords">Buscar Por Palabras Claves</label>
                                    <input class="form-control" placeholder="Buscar Por Palabras Claves" id="keywords" name="keywords" type="text">
                                </div>
                                <div class="form-group">
                                    <select name="category" id="category" class="form-control">
                                        <option value="">Todas Las Categorias</option>
                                        <?php echo $buscarOptions; ?>
                                    </select>
                                </div>

                                <div class="clearfix"></div>
                                <input class="form-control" type="submit" value="Buscar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End: Slider Section -->

   

  

   

    <?php require_once('htmlinclude/footer.php'); ?>

</body>


</html>