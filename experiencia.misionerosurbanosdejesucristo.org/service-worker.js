const CACHE_NAME = "misionerosurbanosdejesucristo-v3";
const urlsToCache = [
  "/",
  "/index.php",
  "/img/logo.png",
  "/manifest.json",
  "/firebase-messaging-sw.js",

  // Agrega aquí los archivos que deseas almacenar en caché
];

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener("fetch", function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      if (response) {
        return response;
      }
      return fetch(event.request);
    })
  );
});
