<header id="header" class="navbar-wrapper">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-default" style="border-bottom-width: 0px">
                <div class="menu-wrap" style="border-top-width: 0px;border-bottom-width: 0px;">
                    <div class="navbar-header">
                        <div class="navbar-brand">
                            <h1>
                                <a href="https://misionerosurbanosdejesucristo.org/">
                                    <img src="images/logo2.png" alt="LIBRARIA" style="max-height: 60px;width: auto;" />
                                </a>
                            </h1>
                        </div>
                    </div>
                    <!-- Navigation -->
                    <div class="navbar-collapse hidden-sm hidden-xs" style="border-right-width: 0px;border-left-width: 0px;">
                        <ul class="nav navbar-nav" style="float: left">

                            <!--
                            <li class="dropdown active">
                                <a data-toggle="dropdown" class="dropdown-toggle disabled" href="index-2.html">Inicio</a>
                                <ul class="dropdown-menu">
                                    <li><a href="index-2.html">Home V1</a></li>
                                    <li><a href="home-v2.html">Home V2</a></li>
                                    <li><a href="home-v3.html">Home V3</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle disabled" href="books-media-list-view.html">Books &amp; Media</a>
                                <ul class="dropdown-menu">
                                    <li><a href="books-media-list-view.html">Books &amp; Media List View</a></li>
                                    <li><a href="books-media-gird-view-v1.html">Books &amp; Media Grid View V1</a></li>
                                    <li><a href="books-media-gird-view-v2.html">Books &amp; Media Grid View V2</a></li>
                                    <li><a href="books-media-detail-v1.html">Books &amp; Media Detail V1</a></li>
                                    <li><a href="books-media-detail-v2.html">Books &amp; Media Detail V2</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle disabled" href="news-events-list-view.html">News &amp; Events</a>
                                <ul class="dropdown-menu">
                                    <li><a href="news-events-list-view.html">News &amp; Events List View</a></li>
                                    <li><a href="news-events-detail.html">News &amp; Events Detail</a></li>
                                </ul>
                            </li>
                            

                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle disabled" href="#">Pages</a>
                                <ul class="dropdown-menu">
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="signin.html">Signin/Register</a></li>
                                    <li><a href="404.html">404/Error</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle disabled" href="blog.html">Blog</a>
                                <ul class="dropdown-menu">
                                    <li><a href="blog.html">Blog Grid View</a></li>
                                    <li><a href="blog-detail.html">Blog Detail</a></li>
                                </ul>
                            </li>

                             -->
                            <li><a href="index.php">Inicio</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/acompanamiento/">Acompañamiento</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/formacion/">Formación</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/testimonios/">Testimonios</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/una-experiencia-espiritual/">Una Experiencia Espiritual</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/contenido-de-interes/">Contenidos de Interés</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/quienes-somos/">Quiénes Somos</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/blog/">Blog</a></li>
                            <li><a href="https://edu.misionerosurbanosdejesucristo.org/login.php">Acceso al Diplomado</a></li>

                            <li><a style="position: fixed;
    right: 20px;
    top: 18px;
    width: 181.08px;
    height: 47px;
    color: #025373;
    background: #FADB0A;
    padding: 12px;
    z-index: 1000;
    border-radius: 30px;
    text-align: center;
    cursor: pointer;
    transition: all 0.5s;" href="https://misionerosurbanosdejesucristo.org/donar/" target="_blank">
                                    <span>
                                        Quiero donar
                                    </span>
                                </a></li>



                            <!-- 
                                <li><a href="services.html">Calendatio Eclesial</a></li>
                                <li><a href="contact.html">Calendario Actividades</a></li>
                                -->


                        </ul>

                    </div>

                </div>
                <div class="mobile-menu hidden-md hidden-lg">
                    <a href="#mobile-menu"><i class="fa fa-navicon"></i></a>
                    <div id="mobile-menu">
                        <ul>
                            <div class="mobile-title" style="background-color: #009BDF;height: 30px;padding-left: 15px;margin-bottom: 10px;padding-right: 15px;padding-top: 5px;">
                                <h4 style="color:#fff;font-size: 2.0rem;">Menú</h4>
                                <a href="#" class="close"></a>
                            </div>

                            <li><a href="index.php">Inicio</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/acompanamiento/">Acompañamiento</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/formacion/">Formación</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/testimonios/">Testimonios</a></li>
                            <li><a href="https://misionerosurbanosdejesucristo.org/una-experiencia-espiritual/">Una Experiencia Espiritual</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/contenido-de-interes/">Contenidos de Interés</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/quienes-somos/">Quiénes Somos</a></li>

                            <li><a href="https://misionerosurbanosdejesucristo.org/blog/">Blog</a></li>
                            <li><a href="https://edu.misionerosurbanosdejesucristo.org/login.php">Acceso al Diplomado</a></li>

                            <li><a style="background: #FADB0A;color: #025373;" href="https://misionerosurbanosdejesucristo.org/donar/" target="_blank">Quiero Donar </a></li>












                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- End: Header Section -->