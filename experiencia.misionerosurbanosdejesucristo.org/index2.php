<?php
session_start();
require_once('include/functions_connect.php');

require_once('cl/Mobile_Detect.php');
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
    $hexigMAcima = '100%';
} else {
    $hexigMAcima = '200px';
    $is_mobile = false;
}


$obtenerCategorias = obtener_categorias_data();
//var_dump($obtenerCategorias);
$palabraBusquedaFinderDi = '';
$categoriaFiltradaInfoD1 = '';
$mostrarCategoriasEnMovil = true;

if ($is_mobile == true) {
    if (isset($_GET["category"])) {
        if (!empty($_GET["category"])) {
            $categoriaData = $_GET["category"];
            $mostrarCategoriasEnMovil = false;
            //echo "Buscar por categoria : " . $categoriaData;
        }
    }
}

if (isset($_GET["tipoList"])) {
    if (!empty($_GET["tipoList"])) {
        $tipoList = $_GET["tipoList"];
    }
} else {
    if ($is_mobile == false) {
        $tipoList = 0;
    } else {
        $tipoList = 0;
    }
}

$keyWords = '';
if (isset($_GET["keywords"])) {
    if (!empty($_GET["keywords"])) {
        $keyWords = $_GET["keywords"];
        $palabraBusquedaFinderDi = 'sobre ' . $keyWords;
        //echo "Buscar por palabra : " . $keyWords;
    }
}

$categoriaData = '';
if (isset($_GET["category"])) {
    if (!empty($_GET["category"])) {
        $categoriaData = $_GET["category"];
        $categoriaFiltradaInfoD1 = $categoriaData;
        //echo "Buscar por categoria : " . $categoriaData;
    }
}


// aqui hacer el select option 
$buscarOptions = '';
foreach ($obtenerCategorias as $key => $value) {

    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }
}

$buscarOptions2 = '';
$buscarOptions2Movil = '';
$contadorAllcategory = 0;

$categoriaExperiencias='';
foreach ($obtenerCategorias as $key => $value) {


    // consultar cuantos resultados existen con cada categoria 
    $categoriaConsultar = total_contenido_categoria($value);
    //echo $categoriaConsultar;
    //echo "<br>";
    $buscarOptions2 = $buscarOptions2 . '<li><h4 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=' . $value . '">' . $value . ' <span>(' . $categoriaConsultar . ')</span></a></h4></li>';

    $buscarOptions2Movil = $buscarOptions2Movil . '<h3 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=' . $value . '">' . $value . ' <span>(' . $categoriaConsultar . ')</span></a></h3>';

    $contadorAllcategory = $contadorAllcategory + $categoriaConsultar;

    $categoriaExperiencias = $categoriaExperiencias. '<li class="nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;"  href="index.php?category=' . $value . '" class="nav-sub-link active">' . $value . ' <span>(' . $categoriaConsultar . ')</span></a></li>';
}


$categoriaExperiencias = '<li class="nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;" href="index.php?category=" class="nav-sub-link active">Todas las experiencias espirituales <span>(' . $contadorAllcategory . ')</a></li>'. $categoriaExperiencias;

$buscarOptions2 = '<li><h4 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas las experiencias espirituales <span>(' . $contadorAllcategory . ')</span></a></h4></li>' . $buscarOptions2;

$buscarOptions2Movil = '<h3 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas las experiencias espirituales <span>(' . $contadorAllcategory . ')</span></a></h3>' . $buscarOptions2Movil;
//echo $contadorAllcategory;

$con = conection_database();
// aqui hacer la pagincacion 


if (empty($categoriaData) && empty($keyWords)) {
    $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2')";
} else {

    //echo "Hree";
    if (!empty($keyWords) && !empty($categoriaData)) {

        // $separar palabras 
        $palabrasBusqueda = explode(' ', $keyWords);
        $finderWord = '';
        foreach ($palabrasBusqueda as $key => $value) {


            if ($value != 'el' && $value != 'la' && $value != 'lo' && $value != 'los' && $value != 'las' && $value != 'por' && $value != 'y' && $value != 'que' && $value != 'ellos' && $value != 'tu' && $value != 'yo' && $value != 'mi' && $value != 'de' && $value != ' ') {

                if (empty($finderWord)) {
                    $finderWord = $finderWord . "titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                } else {
                    $finderWord = $finderWord . " OR titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                }
            }
        }

        $finderWord = $finderWord . " OR titulo LIKE '%" . $keyWords . "%' OR titulo LIKE '%" . $keyWords . "' OR titulo LIKE '" . $keyWords . "%' OR titulo= '%" . $keyWords . "%'  OR contenido LIKE '%" . $keyWords . "%' OR contenido LIKE '%" . $keyWords . "' OR contenido LIKE '" . $keyWords . "%' OR contenido= '%" . $keyWords . "%'   OR resumen LIKE '%" . $keyWords . "%' OR resumen LIKE '%" . $keyWords . "' OR resumen LIKE '" . $keyWords . "%' OR resumen= '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "' OR adjuntos LIKE '" . $keyWords . "%' OR adjuntos= '%" . $keyWords . "%'";

        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' ) AND (" . $finderWord . ")";
    } else if (!empty($categoriaData)) {
        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' )";
    } else if (!empty($keyWords)) {
        //echo "HEre";

        // $separar palabras 
        $palabrasBusqueda = explode(' ', $keyWords);
        $finderWord = '';
        foreach ($palabrasBusqueda as $key => $value) {

            if ($value != 'el' && $value != 'la' && $value != 'lo' && $value != 'los' && $value != 'las' && $value != 'por' && $value != 'y' && $value != 'que' && $value != 'ellos' && $value != 'tu' && $value != 'yo' && $value != 'mi' && $value != 'de' && $value != ' ') {

                if (empty($finderWord)) {
                    $finderWord = $finderWord . "titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                } else {
                    $finderWord = $finderWord . " OR titulo LIKE '%" . $value . "%' OR titulo LIKE '%" . $value . "' OR titulo LIKE '" . $value . "%' OR titulo= '%" . $value . "%'  OR contenido LIKE '%" . $value . "%' OR contenido LIKE '%" . $value . "' OR contenido LIKE '" . $value . "%' OR contenido= '%" . $value . "%'   OR resumen LIKE '%" . $value . "%' OR resumen LIKE '%" . $value . "' OR resumen LIKE '" . $value . "%' OR resumen= '%" . $value . "%' OR adjuntos LIKE '%" . $value . "%' OR adjuntos LIKE '%" . $value . "' OR adjuntos LIKE '" . $value . "%' OR adjuntos= '%" . $value . "%'";
                }
            }
        }

        $finderWord = $finderWord . " OR titulo LIKE '%" . $keyWords . "%' OR titulo LIKE '%" . $keyWords . "' OR titulo LIKE '" . $keyWords . "%' OR titulo= '%" . $keyWords . "%'  OR contenido LIKE '%" . $keyWords . "%' OR contenido LIKE '%" . $keyWords . "' OR contenido LIKE '" . $keyWords . "%' OR contenido= '%" . $keyWords . "%'   OR resumen LIKE '%" . $keyWords . "%' OR resumen LIKE '%" . $keyWords . "' OR resumen LIKE '" . $keyWords . "%' OR resumen= '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "%' OR adjuntos LIKE '%" . $keyWords . "' OR adjuntos LIKE '" . $keyWords . "%' OR adjuntos= '%" . $keyWords . "%'";

        $paginacion1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (" . $finderWord . ")";

        //var_dump($finderWord);
    }
}


$sqlPaginate1 = mysqli_query($con, $paginacion1);
$row1Paginate1 = mysqli_fetch_assoc($sqlPaginate1);
$num_total_rows = $row1Paginate1['total'];

if ($tipoList == 0) {
    $numItmeByPage = 15;
} else if ($tipoList == 1) {
    $numItmeByPage = 15;
}

if (isset($_GET["resultpaginatios"])) {
    if ($_GET["resultpaginatios"] > 0) {
        $numItmeByPage = $_GET["resultpaginatios"];
    }
}

$contenidoHtmlInformativo = '';
$navegacionContenido = '';
$totalResultadosShow = 0;

if ($num_total_rows > 0) {
    $page = false;

    //examino la pagina a mostrar y el inicio del registro a mostrar
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    }

    if (!$page) {
        $start = 0;
        $page = 1;
    } else {
        $start = ($page - 1) * $numItmeByPage;
    }
    //calculo el total de paginas
    $total_pages = ceil($num_total_rows / $numItmeByPage);


    //pongo el numero de registros total, el tamano de pagina y la pagina que se muestra
    //echo '<h3>Numero de articulos: '.$num_total_rows.'</h3>';
    //echo '<h3>En cada pagina se muestra '.$numItmeByPage.' articulos ordenados por fecha en formato descendente.</h3>';
    //echo '<h3>Mostrando la pagina '.$page.' de ' .$total_pages.' paginas.</h3>';

    //echo $total_pages;





    if (empty($categoriaData) && empty($keyWords)) {
        $paginacion2 = "SELECT * FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') ORDER BY posicionamiento ASC LIMIT " . $start . ", " . $numItmeByPage . "";
    } else {

        if (!empty($keyWords) && !empty($categoriaData)) {

            $paginacion2 = "SELECT * FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' ) AND (" . $finderWord . ") ORDER BY posicionamiento ASC LIMIT " . $start . ", " . $numItmeByPage . "";
        } else if (!empty($categoriaData)) {
            $paginacion2 = "SELECT * FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (tipo_publicacion='" . $categoriaData . "' OR tipo_publicacion2='" . $categoriaData . "' ) ORDER BY posicionamiento ASC LIMIT " . $start . ", " . $numItmeByPage . "";
        } else if (!empty($keyWords)) {

            $paginacion2 = "SELECT * FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') AND (" . $finderWord . ") ORDER BY posicionamiento ASC LIMIT " . $start . ", " . $numItmeByPage . "";
        }
    }




    //echo $paginacion2;


    $sqlPaginate2 = mysqli_query($con, $paginacion2);
    while ($row1Paginate2 = mysqli_fetch_assoc($sqlPaginate2)) {
        $totalResultadosShow = $totalResultadosShow + 1;

        $icono = '';


        if ($row1Paginate2["tipo"] == 1) {

            $categoria1 = '<p><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion"] . '</p>';
            $categoria2 = '<li style="margin-bottom: 0px;"><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion"] . '</li>';

            $categoria1 = '';
            $categoria2 = '';

            $numberRand = rand(7, 10);
            $icono = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
            $icono22 = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
        } elseif ($row1Paginate2["tipo"] == 2) {


            $categoria1 = '<p><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion2"] . '</p>';
            $categoria2 = '<li style="margin-bottom: 0px;"><strong>Categoria :</strong> ' . $row1Paginate2["tipo_publicacion2"] . '</li>';

            $categoria1 = '';
            $categoria2 = '';

            $numberRand = rand(3, 6);
            $icono = '<div style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
            $icono22 = '<div  style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;" style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
        }

        if (!empty($row1Paginate2["imagen_portada"])) {
            $imagenPortada = '<img style="border-radius: 7px;" src="' . $row1Paginate2["imagen_portada"] . '" alt="Book">';
            $imagenPortada2 = $row1Paginate2["imagen_portada"];
        } else {
            $rutaDefault = 'images/default.jpg';
            $imagenPortada2 = 'images/default.jpg';
            $imagenPortada = '<img style="border-radius: 7px;" src="' . $rutaDefault . '" alt="Book">';
        }


        $titule = $row1Paginate2["titulo"];
        $author = '';
        $author2 = '';
        if (!empty($row1Paginate2["author"])) {
            $wordAutho = 'Autor';
            $author = '<p><strong>' . $wordAutho . ':</strong> ' . $row1Paginate2["author"] . '</p>';

            $author2 = '<li style="margin-bottom: 0px;"><strong>' . $wordAutho . ':</strong> ' . $row1Paginate2["author"] . '</li>';
        }

        $fechaPublicacion = '';
        $fechaPublicacion2 = '';
        if (!empty($row1Paginate2["fecha_contenido"]) && $row1Paginate2["fecha_contenido"] != '0000-00-00 00:00:00') {
            $fechaPublicacion = '<p><strong>Publicado :</strong> ' . date('Y-m-d', strtotime($row1Paginate2["fecha_contenido"])) . '</p>';
            $fechaPublicacion2 = '<li style="margin-bottom: 0px;"><strong>Publicado :</strong> ' . date('Y-m-d', strtotime($row1Paginate2["fecha_contenido"])) . '</li>';
        }


        //echo $fechaPublicacion;
        //echo "<br>";

        $mainContent = $row1Paginate2["resumen"];
        $pqueDescription = '<p>' . $mainContent . '</p>';



        if ($tipoList == 0) {
            if (!empty($row1Paginate2["imagen_portada"])) {
                $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $row1Paginate2["imagen_portada"] . '" alt="Book">';
                $imagenPortada2 = $row1Paginate2["imagen_portada"];
            } else {
                $rutaDefault = 'images/default.jpg';
                $imagenPortada2 = 'images/default.jpg';
                $imagenPortada = '<img style="border-top-left-radius: 17px;border-top-right-radius: 17px;border- bottom-left-radius: 7px;border- bottom-right-radius: 7px;height: 200px;width: 100%;" src="' . $rutaDefault . '" alt="Book">';
            }

            // ' . $icono . '
            /*
            <ul>
                                                            ' . $author2 . '
                                                            ' . $fechaPublicacion2 . '
                                                            ' . $categoria2 . '
                                                        </ul>
                                                         */
            $contenidoHtmlInformativo = $contenidoHtmlInformativo . '<li style="box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border-radius:25px;padding: 0px;">
                                            
                                          
                                            
                                           
                                            <div class="single-book-box" style="padding-top: 0px;">

                                              ' . $imagenPortada . '


                                                <div class="post-detail" style="border-radius: 7px;padding-top: 15px;z-index: 2;">
                                                   
                                              
                                                 
                                                    <header class="entry-header">
                                                        <h3 class="entry-title" style="margin-bottom: 5px;text-align: center;"><a style="color:#009BDF;" href="post.php?id=' . $row1Paginate2["id"] . '">' . $titule . '</a></h3>
                                                        
                                                    </header>
                                                    <div class="entry-content" style="word-break:break-word;margin-top: 5px;height: 120px;">
                                                        ' . (strlen($pqueDescription) > 220 ? substr($pqueDescription, 0, 220) . "..." : $pqueDescription) . '
                                                    </div>
                                                    <footer class="entry-footer">
                                                    <div class="d-flex justify-content-center text-center">

                                                        <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="post.php?id=' . $row1Paginate2["id"] . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                            
                                        </li>
                                        ';
        } else if ($tipoList == 1) {

            $maximoWid = '260px';
            if ($is_mobile == true) {
                $maximoWid = '100%';
            }
            // ' . $icono22 . '

            $imagenSeccion = ' <div class="post-thumbnail">
                                                   
                                                    <a href="post.php?id=' . $row1Paginate2["id"] . '"><img style="max-width: ' . $maximoWid . ';border-radius: 7px;max-height: 180px;" alt="Book" src="' . $imagenPortada2 . '"></a>                                                                 </div>';



            /*
                                                    <ul>
                                                                        ' . $author2 . '
                                                                        ' . $fechaPublicacion2 . '
                                                                        ' . $categoria2 . '
                                                                    </ul>
                                                    */
            $contenidoHtmlInformativo = $contenidoHtmlInformativo . '<article > 
                                            <div class="single-book-box" style="margin-bottom: 15px;border-radius: 15px;padding-bottom: 0px;box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border: 10px solid #f3f3f3;">                                                
                                              
                                                <div class="post-detail" style="max-height: ' . $hexigMAcima . ';border-radius: 7px;width: 100%;right: 0px;position: relative;top: 0px;transform: initial;z-index: 2;">
                                                    <div class="row">
                                                    <div class="col-lg-4 col-12"> <div class="d-flex justify-content-center">' . $imagenSeccion . '</div></div>
                                                    <div class="col-lg-8 col-12"> 
                                                        
                                                        <header class="entry-header">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <h3 class="entry-title" style="margin-bottom: 5px;"><a style="color:#009BDF;" href="post.php?id=' . $row1Paginate2["id"] . '">' . $titule . '</a></h3>
                                                                    
                                                                </div>
                                                            
                                                            </div>
                                                        </header>
                                                        <div class="entry-content" style="word-break:break-word;height: 60px;">
                                                        ' . (strlen($pqueDescription) > 220 ? substr($pqueDescription, 0, 220) . "..." : $pqueDescription) . '
                                                        </div>
                                                        <footer class="entry-footer" >
                                                            <a style="border-radius: 15px;border: 0px;" class="btn btn-primary" href="post.php?id=' . $row1Paginate2["id"] . '" style="padding-left: 8px;padding-right: 8px;">Acceder al contenido</a>
                                                        </footer>

                                                        <div class="clear"></div>
                                                    </div>
                                                     
                                                    </div>
                                                   
                                                    
                                                </div>
                                               
                                            </div>
                                        </article>';
        }
    }


    //echo $contenidoHtmlInformativo;

    //echo $total_pages;


    if ($total_pages > 1) {


        //echo "Aqui";
        if ($page != 1) {
            $anteriorPage = '<a class="prev page-numbers" href="index.php?page=' . ($page - 1) . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '"><i class="fa fa-long-arrow-left"></i> Anterior</a>';
            //echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page-1).'"><span aria-hidden="true">&laquo;</span></a></li>';
        }

        $paginaActuales = '';

        for ($i = 1; $i <= $total_pages; $i++) {
            //echo $i;
            if ($page == $i) {
                $paginaActuales = $paginaActuales . '<span class="page-numbers current">' . $page . '</span>';
                //echo '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
            } else {
                $paginaActuales = $paginaActuales . '<a class="page-numbers" href="index.php?page=' . $i . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '">' . $i . '</a>';
                //echo '<li class="page-item"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
            }
        }


        if ($page != $total_pages) {
            $siguientePage = '<a class="next page-numbers" href="index.php?page=' . ($page + 1) . '&tipoList=' . $tipoList . '&resultpaginatios=' . $numItmeByPage . '">Siguiente <i class="fa fa-long-arrow-right"></i></a>';

            //echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page+1).'"><span aria-hidden="true">&raquo;</span></a></li>';
        }


        $navegacionContenido = '  <nav class="navigation pagination text-center">
                                    <h2 class="screen-reader-text">Navegacion</h2>
                                    <div class="nav-links">
                                        ' . $anteriorPage . '
                                        ' . $paginaActuales . '                                        
                                        ' . $siguientePage . '
                                    </div>
                                </nav>';
    } else {
        $navegacionContenido = '';
    }
}




close_database($con);

// include mobile detect




require_once('cl/Mobile_Detect.php');
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
$mobilNumero = 0;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
    $mobilNumero = 1;
}
?>

<!DOCTYPE html>
<html lang="es" translate="no">

<head>
    <!-- Required meta tags   -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-language" content="es_ES">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Diplomado y Cursos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="css/data_image.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="swal/sweetalert2.min.css">
    <link href="lib/fonti/css/all.css" rel="stylesheet">
    <!--load all styles -->
    <!-- Arribaema -->
    <link rel="icon" href="images/favicon.png" sizes="192x192" />


    <style>
        @import url('https://fonts.googleapis.com/css?family=Nunito');


        html {
            font-family: "Nunito";
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-weight: 500;
        }

        body {
            font-family: "Nunito";
            margin: 0;
            font-weight: 500;
            color: #808080;
        }

        .itemdonar .sidebar-nav-link:hover,
        .sidebar-nav-link:focus {
            background-color: #f8f9fa;
        }

        a,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        font {
            color: #808080;
            font-family: "Nunito";
        }

        .text-dark {
            color: #808080 !important;
        }

        * {
            font-family: "Nunito";
        }

        #breadcrumb li:last-child a {
            padding-left: 5px;
            padding-right: 5px;
        }

        .section-wrapper {
            border-radius: 15px;
        }
    </style>
    <!-- Stylesheet -->
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" integrity="sha512-BnbUDfEUfV0Slx6TunuB042k9tuKe3xrD6q4mg5Ed72LTgzDIcLPxg6yI2gcMFRyomt+yJJxE+zJwNmxki6/RA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="admin/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="lib/fullcall/fullcalendar.css">
    <style>
        .slim-header.with-sidebar .slim-logo {
            width: 205px;
            margin-right: 10px;
            margin-left: -340px;
        }

        .container-fluid {
            margin-right: 0px;
            margin-left: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }


        .nav-links .page-numbers.prev:hover {
            border-radius: 15px;
            background-color: #00adef;
            padding-right: 15px;
            color: #fff;
        }


        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .single-book-box {
            border: 10px solid #c2c2c2;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .books-gird .single-book-box {
            opacity: 1;
        }

        .books-gird ul li:nth-child(3n) .single-book-box .post-detail {
            left: 0px;
            right: 66px;
        }

        .books-gird .single-book-box .post-detail {
            top: 0;
            transform: none;
            width: 100%;
            left: 0;
            position: relative;
            padding: 30px 15px;
        }

        table.dataTable tbody tr {
            background-color: transparent;
        }

        .has-fixed.is-shrink .header-main {
            padding: 0px 0;
        }

        .header-main {
            padding: 2px 0;
        }

        #home-v1-header-carousel .carousel-caption {
            top: 36%;
        }

        label {
            color: #fff;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: #fff !important;
        }

        .sidebar .widget-sub-title::after {
            content: '';
        }

        input {
            border-radius: 15px;
        }

        .form-control {
            border-radius: 15px;
        }

        .single-book-box {
            padding-top: 5px;
        }

        .single-book-box .post-detail .btn {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }

        .row {
            margin-left: 0px;
            margin-right: 0px;
        }

        .form-control {
            border: 3px solid #f4f4f4 !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            height: 56px !important;
            padding: 5px 10px !important;
        }
    </style>


    <script type="text/javascript" defer>
        // (function() {
        //   var options = {
        //     whatsapp: "573194475236", // WhatsApp number
        //     call_to_action: "Contáctenos", // Call to action
        //     position: "right", // Position may be 'right' or 'left'
        //   };
        //   var proto = document.location.protocol,
        //     host = "whatshelp.io",
        //     url = proto + "//static." + host;
        //   var s = document.createElement('script');
        //   s.type = 'text/javascript';
        //   s.async = true;
        //   s.src = url + '/widget-send-button/js/init.js';
        //   s.onload = function() {
        //     WhWidgetSendButton.init(host, proto, options);
        //   };
        //   var x = document.getElementsByTagName('script')[0];
        //   x.parentNode.insertBefore(s, x);
        // })(); 
    </script>

</head>

<body>

    <div class="slim-header with-sidebar" style="background-color: #fff;color:#000;">
        <div class="container-fluid">
            <div class="slim-header-left">
                <h2 class="slim-logo"><a class="navbar-brand" href="https://diplomado.misionerosurbanosdejesucristo.org" style="font-size:2.6rem;color:#fff;">
                        <img style="max-height: 70px;" src="img/logo.png">
                    </a></h2>

                <?php if ($is_mobile == true) { ?>
                    <a href="" id="slimSidebarMenu" class="slim-sidebar-menu" style="background-color: #fff;color: #000;"><span></span></a>
                    <font id="textoMenuShow">Mostrar Menú</font>


                <?php } ?>


                <!--  <div class="search-box">
            <input type="text" class="form-control" placeholder="Search">
            <button class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>-->


            </div><!-- slim-header-left -->
            <div class="slim-header-right">





            </div><!-- header-right -->
        </div><!-- container-fluid -->
    </div><!-- slim-header -->


    <div class="slim-body">
        <div class="slim-sidebar">
            <label class="sidebar-label" style="text-transform:initial;font-size: 15px;">Menú</label>

            <ul class="nav nav-sidebar">




                <li class="sidebar-nav-item">
                    <a href="index.php" id="cuadroRoundData1" class="sidebar-nav-link ">
                        <i class="fas fa-home mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Inicio </span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/acompanamiento/" id="cuadroRoundData2" class="sidebar-nav-link ">
                        <i class="fas fa-people-carry mr-3 text-black mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Acompañamiento </span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/formacion/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-user-graduate mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Formación</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/testimonios/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-users mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Testimonios</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/una-experiencia-espiritual/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-hands-helping mr-3 text-black mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Una Experiencia Espiritual</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/contenido-de-interes/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-video mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Contenidos de Interés</span></a>
                </li>



                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/quienes-somos/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-info-circle mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Quiénes Somos</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://misionerosurbanosdejesucristo.org/blog/" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-blog mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Blog</span></a>
                </li>

                <li class="sidebar-nav-item">
                    <a href="https://edu.misionerosurbanosdejesucristo.org/login.php" id="cuadroRoundData3" class="sidebar-nav-link ">
                        <i class="fas fa-graduation-cap mr-3 text-black"></i>
                        <span class="text-black" style="color: #000; cursor: pointer;">Acceso al Diplomado</span></a>
                </li>





                <li class="sidebar-nav-item with-sub">


                    <a href="" class="sidebar-nav-link "><i class="fas fa-school  mr-3 text-black"></i>
                        <span class="text-black" style="color: #000;">Experiencias</span></a>

                    <ul class="nav sidebar-nav-sub" style="display: none;">
                       <?php echo $categoriaExperiencias;?>

                       






                    </ul>
                </li>














            </ul>
        </div><!-- slim-sidebar -->

        <div class="slim-mainpanel">


            <div class="container" style="padding-top: 0px;">





                <div id="cuadrocentralcontenido" class="cuadrocentralcontenido" style="border-radius:15px;">

                    <div id="content" class="site-content">
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="books-media-gird">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <!-- Start: Search Section -->
                                            <section class="search-filters" style="margin-bottom: 20px;">
                                                <div class="container-fluid">

                                                </div>
                                            </section>
                                            <!-- End: Search Section -->
                                        </div>
                                        <div class="row">

                                            <?php if ($is_mobile == true) { ?>

                                                <?php if (1 == 2 && $mostrarCategoriasEnMovil == true) { ?>
                                                    <div class="col-md-3">
                                                        <div class="widget widget_related_search open" data-accordion>
                                                            <!--<h4 class="widget-title" data-control>Búsqueda relacionada</h4>
                                             -->
                                                            <div data-accordion>
                                                                <h1 style="margin-bottom: 1;padding-bottom: 0px;margin-bottom: 0px;" class="widget-sub-title" data-control>Experiencias</h1>
                                                                <h6 style="margin-bottom: 10px;color:#000;">Selecciona una experiencia</h6>
                                                                <?php echo $buscarOptions2Movil; ?>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>


                                                    </div>
                                                <?php } ?>
                                                <div class="col-md-9" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 30px;">
                                                    <div class="filter-options margin-list">
                                                        <div class="row">


                                                            <?php if ($is_mobile == true) { ?>

                                                                <div class="col-md-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                                                                    <form action="index.php" method="get">





                                                                        <div class="col-md-12 col-sm-12">


                                                                            <div style="display:none;" class="form-group">
                                                                                <select style="border-radius: 15px;" name="category" id="category" class="idCategoria form-control" fdprocessedid="x27p3n">
                                                                                    <option value="">Todas las experiencias espirituales</option>
                                                                                    <option value="Libros">Libros</option>
                                                                                    <option value="Noticias">Noticias</option>
                                                                                </select>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="sr-only" for="keywords">Buscar por palabras claves</label>
                                                                                <input style="border-radius: 15px;" class="form-control" placeholder="Buscar por palabras claves" id="keywords" name="keywords" value="" type="text" fdprocessedid="gaiifr">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input style="border-radius: 15px;" class="form-control" type="submit" value="Buscar" fdprocessedid="v71pdq">
                                                                            </div>

                                                                        </div>



                                                                    </form>

                                                                </div>


                                                                <div class="col-md-12 col-sm-12 ">
                                                                    Resultados por página <?php echo $palabraBusquedaFinderDi; ?>
                                                                    <select onchange="cambiarpaginacionresultados()" id="paginaciontotal" style="border-radius: 15px;" class="form-control">
                                                                        <option <?php echo ($numItmeByPage == 15) ? 'selected' : ''; ?>>15</option>
                                                                        <option <?php echo ($numItmeByPage == 25) ? 'selected' : ''; ?>>25</option>
                                                                        <option <?php echo ($numItmeByPage == 50) ? 'selected' : ''; ?>>50</option>
                                                                        <option <?php echo ($numItmeByPage == 100) ? 'selected' : ''; ?>>100</option>
                                                                    </select>




                                                                </div>




                                                            <?php } else if ($is_mobile == false) { ?>
                                                                <div class="col-md-4 col-sm-8 pull-left ">
                                                                    Resultados por página <?php echo $palabraBusquedaFinderDi; ?>
                                                                    <select onchange="cambiarpaginacionresultados()" id="paginaciontotal" style="border-radius: 15px;" class="form-control">
                                                                        <option <?php echo ($numItmeByPage == 15) ? 'selected' : ''; ?>>15</option>
                                                                        <option <?php echo ($numItmeByPage == 25) ? 'selected' : ''; ?>>25</option>
                                                                        <option <?php echo ($numItmeByPage == 50) ? 'selected' : ''; ?>>50</option>
                                                                        <option <?php echo ($numItmeByPage == 100) ? 'selected' : ''; ?>>100</option>
                                                                    </select>


                                                                </div>

                                                                <div class="col-md-4">
                                                                    <a style="border-radius: 15px;" href="index.php" class="btn btn-primary">
                                                                        <font style="color:#fff;">Hacer una busqueda nueva</font>
                                                                    </a>
                                                                </div>

                                                            <?php } ?>
                                                            <?php if ($is_mobile == false) { ?>

                                                                <div class="col-md-4 col-sm-4 pull-right">
                                                                    <div class="filter-toggle">

                                                                        <a style="cursor:pointer;" onclick="urlAddType(0)" <?php echo ($tipoList == 0) ? 'class="active"' : ''; ?>><i class="fas fa-th-list"></i></a>
                                                                        <a style="cursor:pointer;" <?php echo ($tipoList == 1) ? 'class="active"' : ''; ?> onclick="urlAddType(1)"><i class="fas fa-list"></i></a>


                                                                    </div>
                                                                </div>

                                                            <?php } ?>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12  text-center" style="bottom: 50px;top: -40;top: -50;top: -50;">
                                                        <h1 style="text-transform:initial;font-weight: 500;color:#009BDF;"><?php echo $categoriaFiltradaInfoD1; ?></h1>
                                                    </div>

                                                    <div class="<?php echo ($tipoList == 0) ? 'books-gird' : 'books-list'; ?>">
                                                        <ul style="width: 100%;padding-left: 0px;">

                                                            <?php if ($totalResultadosShow > 0) { ?>
                                                                <?php echo $contenidoHtmlInformativo; ?>
                                                            <?php } else { ?>

                                                                Resultados no encontrados <?php echo $palabraBusquedaFinderDi; ?>
                                                            <?php } ?>

                                                        </ul>
                                                    </div>

                                                    <div class="col-md-12 col-sm-8">
                                                        <div class="filter-result">Mostrando <?php echo $totalResultadosShow; ?> de <?php echo ($num_total_rows == 1) ? $num_total_rows . ' resultado' : $num_total_rows . ' resultados'; ?> <?php echo $palabraBusquedaFinderDi; ?></div>
                                                    </div>

                                                    <?php echo $navegacionContenido; ?>

                                                    <div class="col-md-12 col-sm-8">
                                                        <div class="d-flex justify-content-center text-center">
                                                            <br>
                                                            <a style="border-radius: 15px;margin-top: 5px;" href="index.php" class="btn btn-primary">
                                                                <font style="color:#fff;">Hacer una busqueda nueva</font>
                                                            </a>


                                                        </div>
                                                    </div>
                                                </div>


                                            <?php } else { ?>
                                                <div class="col-md-9 col-md-push-3" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 30px;">
                                                    <div class="filter-options margin-list">
                                                        <div class="row">

                                                            <div class="col-md-2 col-sm-2">
                                                                Resultados por página <?php echo $palabraBusquedaFinderDi; ?>
                                                                <select onchange="cambiarpaginacionresultados()" id="paginaciontotal" style="border-radius: 15px;" class="form-control">
                                                                    <option <?php echo ($numItmeByPage == 15) ? 'selected' : ''; ?>>15</option>
                                                                    <option <?php echo ($numItmeByPage == 25) ? 'selected' : ''; ?>>25</option>
                                                                    <option <?php echo ($numItmeByPage == 50) ? 'selected' : ''; ?>>50</option>
                                                                    <option <?php echo ($numItmeByPage == 100) ? 'selected' : ''; ?>>100</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-md-8 col-sm-8">
                                                                <div class="container-fluid">
                                                                    <div class="filter-box" style="border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;">
                                                                        <h3><br></h3>
                                                                        <form action="index.php" method="get">
                                                                            <div class="col-md-5 col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label class="sr-only" for="keywords">Buscar por palabras claves</label>
                                                                                    <input style="border-radius: 15px;" class="form-control" placeholder="Buscar por palabras claves" id="keywords" name="keywords" value="<?php echo $keyWords; ?>" type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-6">
                                                                                <div class="form-group">
                                                                                    <select style="border-radius: 15px;" name="category" id="category" class="idCategoria form-control">
                                                                                        <option value="">Todas las experiencias espirituales</option>
                                                                                        <?php echo $buscarOptions; ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2 col-sm-6">
                                                                                <div class="form-group">
                                                                                    <input style="border-radius: 15px;" class="form-control" type="submit" value="Buscar">
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-2 col-sm-2 pull-right">
                                                                <br>
                                                                <div class="filter-toggle" style="margin-top: 5px;">

                                                                    <a style="cursor:pointer;" onclick="urlAddType(0)" <?php echo ($tipoList == 0) ? 'class="active"' : ''; ?>><i class="fas fa-th-list"></i></a>
                                                                    <a style="cursor:pointer;" <?php echo ($tipoList == 1) ? 'class="active"' : ''; ?> onclick="urlAddType(1)"><i class="fas fa-list"></i></a>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12  text-center" style="bottom: 50px;top: -40;top: -50;top: -50;">
                                                        <h1 style="text-transform:initial;font-weight: 500;color:#009BDF;"><?php echo $categoriaFiltradaInfoD1; ?></h1>
                                                    </div>

                                                    <div class="<?php echo ($tipoList == 0) ? 'books-gird' : 'books-list'; ?>">
                                                        <ul style="width: 100%;padding-left: 0px;">

                                                            <?php if ($totalResultadosShow > 0) { ?>
                                                                <?php echo $contenidoHtmlInformativo; ?>
                                                            <?php } else { ?>

                                                                Resultados no encontrados <?php echo $palabraBusquedaFinderDi; ?>
                                                            <?php } ?>

                                                        </ul>
                                                    </div>
                                                    <div class="col-md-12 col-sm-8">
                                                        <div class="filter-result">Mostrando <?php echo $totalResultadosShow; ?> de <?php echo ($num_total_rows == 1) ? $num_total_rows . ' resultado' : $num_total_rows . ' resultados'; ?> <?php echo $palabraBusquedaFinderDi; ?></div>
                                                    </div>

                                                    <?php echo $navegacionContenido; ?>

                                                    <div class="col-md-12 col-sm-8">
                                                        <div class="d-flex justify-content-center text-center">
                                                            <br>
                                                            <a style="border-radius: 15px;margin-top: 5px;" href="index.php" class="btn btn-primary">
                                                                <font style="color:#fff;">Hacer una busqueda nueva</font>
                                                            </a>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-md-pull-9">
                                                    <aside id="secondary" class="sidebar widget-area" data-accordion-group>
                                                        <div class="widget widget_related_search open" data-accordion>
                                                            <div data-content>
                                                                <div data-accordion>
                                                                    <h1 style="margin-bottom: 1;padding-bottom: 0px;margin-bottom: 0px;" class="widget-sub-title" data-control>Experiencias</h2>
                                                                        <h6 style="margin-bottom: 10px;color:#000;">Selecciona una experiencia</h6>
                                                                        <div class="widget_categories" style="margin-left: 0px;" data-content>
                                                                            <ul style="margin-left: 0px;">
                                                                                <?php echo $buscarOptions2; ?>

                                                                            </ul>
                                                                        </div>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>



                                                    </aside>
                                                </div>

                                            <?php } ?>
                                        </div>
                                    </div>


                                </div>
                            </main>
                        </div>
                    </div>
                </div>

            </div><!-- container -->


            <div class="slim-footer mg-t-0" style="margin-top: 25px;">


                <footer class="site-footer">

                    <div class="sub-footer" style="background-color: #F6F2F0;">
                        <div class="container">
                            <div class="row" style="text-align:center;">

                                <div class="footer-text col-12">

                                    <div class="d-flex justify-content-center">

                                        <h3 style="color: #000;">¿Necesitas ayuda con esta Experiencia? Escríbenos</h2>



                                    </div>

                                    <div class="d-flex justify-content-center">

                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-shrink elementor-repeater-item-b682d3e" href="https://api.whatsapp.com/send/?phone=573005782938&amp;text=Hola+Misioneros%2C+%2Aquiero+conocer+información+del+contenido+de+interes+%2C+por+favor%2A.+Mi+nombre+es%3A&amp;app_absent=0" target="_blank">
                                            <span class="elementor-screen-only"></span>
                                            <i style="color:#fff;font-size: 30px;background-color: #00d084;border-radius: 50%;padding: 7px;" class="fab fa-whatsapp"></i> </a>

                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-envelope elementor-animation-shrink elementor-repeater-item-d6b2487" href="mailto:info@misionerosurbanosdejesucristo.org" target="_blank">
                                            <span class="elementor-screen-only"></span>
                                            <i style="color:#fff;font-size: 30px;background-color: #EA4335;border-radius: 50%;padding: 7px;" class="fas fa-envelope"></i> </a>
                                    </div>








                                </div>


                            </div>
                        </div>
                </footer>


                <footer class="site-footer">

                    <div class="sub-footer">
                        <div class="container">
                            <div class="row" style="text-align:center;">




                                <div class="footer-text col-12">

                                    <div class="d-flex justify-content-center">

                                        <a href="https://misionerosurbanosdejesucristo.org/">
                                            <img style="text-align: center;max-height: 80px;" src="images/logo.png" alt="">
                                        </a>



                                    </div>

                                    <p><a style="text-align: center;color:#000;" target="_blank" href="https://misionerosurbanosdejesucristo.org/">Copyright © 2022 Misioneros Urbanos de Jesucristo</a></p>


                                </div>
                            </div>
                        </div>
                </footer>



            </div><!-- slim-footer -->
        </div><!-- slim-mainpanel -->
    </div><!-- slim-body -->




    <script src="lib/jquery/js/jquery.js"></script>

    <script src="lib/popper.js/js/popper.js"></script>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script src="lib/jquery.cookie/js/jquery.cookie.js"></script>
    <script src="lib/d3/js/d3.js"></script>
    <script src="lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

    <script src="js/ResizeSensor.js"></script>
    <script src="js/slim.js"></script>

    <script src="swal/swalpersonalizado.js"></script>
    <script src="swal/sweetalert2.min.js"></script>






    <div id="smart-button-container">
        <div style="text-align: center;">
            <div id="paypal-button-container"></div>
        </div>
    </div>






</body>

</html>