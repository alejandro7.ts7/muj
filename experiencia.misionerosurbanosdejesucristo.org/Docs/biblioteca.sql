-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-07-2022 a las 22:30:27
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `profile_image` mediumtext,
  `fecha_registro` datetime DEFAULT NULL,
  `segundo_factor` varchar(500) NOT NULL,
  `Pais` varchar(500) DEFAULT NULL,
  `whatsapp` longtext,
  `messenger` longtext,
  `telegram` longtext,
  `facebook` longtext,
  `instagram` longtext,
  `youtube` longtext,
  `tiktok` longtext,
  `twitter` longtext,
  `modenight` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPRESSED ;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `username`, `name`, `lastname`, `email`, `phone`, `password`, `profile_image`, `fecha_registro`, `segundo_factor`, `Pais`, `whatsapp`, `messenger`, `telegram`, `facebook`, `instagram`, `youtube`, `tiktok`, `twitter`, `modenight`) VALUES
(1, 'admin', 'Empresaname', 'empresalast', 'demo@gmail.com', '4216754678', '$2y$10$gNYySNzWumvIWWJiPLFituHM3vCWg/5SrENm0yltJSAybyC5NZwqu', NULL, NULL, 'hola', 'Bahamas', '', '', '', '', '', '', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidos`
--

CREATE TABLE `contenidos` (
  `id` int(11) NOT NULL,
  `fecha_publicacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_contenido` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo` int(11) NOT NULL COMMENT '1-> documental 2-> audiovisual 3-> calendario eclesiastico 4-> paginas de interes 5-> actividades muj',
  `titulo` mediumtext,
  `contenido` longtext,
  `video1` varchar(1000) DEFAULT NULL,
  `video2` varchar(1000) DEFAULT NULL,
  `video3` varchar(1000) DEFAULT NULL,
  `adjuntos` longtext,
  `url` varchar(5000) DEFAULT NULL,
  `meta_title` varchar(1000) DEFAULT NULL,
  `meta_description` varchar(5000) DEFAULT NULL,
  `meta_keywords` mediumtext,
  `meta_author` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `tipo_publicacion` varchar(2000) DEFAULT NULL,
  `imagen_portada` varchar(2000) DEFAULT NULL,
  `tipo_pagina_interes` varchar(2000) DEFAULT NULL,
  `tipo_publicacion2` varchar(2000) DEFAULT NULL,
  `creador` int(11) DEFAULT '1',
  `author` varchar(2000) DEFAULT NULL,
  `resumen` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contenidos`
--
ALTER TABLE `contenidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
