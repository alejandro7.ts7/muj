<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 10;
//require_once('include/function_admin.php');

?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Enviar notificaciones</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <h4>+ Crear notificación (Recordar que despues de enviadas ya no es posible eliminar las notificaciones)</h4>

                            <div class="col-lg-12 col-md-12 mb-4">

                                <br>
                                <label class="form-control-label text-uppercase">Titulo </label>
                                <input type="text" placeholder="Titulo" value="" id="usuario_subadministrador" name="usuario_subadministrador" class="form-control">


                                <br>
                                <label class="form-control-label text-uppercase">Mensaje </label>
                                <textarea rows="5" type="text" placeholder="Mensaje" value="" id="password_1" name="password_1" class="form-control"></textarea>

                                <br>
                                <label class="form-control-label text-uppercase">URL (Opcional) </label>
                                <input type="text" value="https://experiencia.misionerosurbanosdejesucristo.org" placeholder="URL (Opcional)" value="" id="password_2" name="password_2" class="form-control">

                                <br>
                                <button onclick="guardar_info_notificacion()" type="submit" class="btn btn-primary">Crear notificación </button>
                            </div>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <table id="datatable1_28_notificaciones" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Mensaje</th>
                                        <th>Url </th>
                                        <th>Fecha </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();




    $('#datatable1_28_notificaciones').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/scripts/server_processing_subadmin_notificaciones.php",
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: '_MENU_ Resultados/por página',
        },
        "order": [
            [0, "DESC"]
        ],
    });


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */




    function guardar_info_notificacion() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        var usuario_subadministrador = $("#usuario_subadministrador").val();
        var password_1 = $("#password_1").val();
        var password_2 = $("#password_2").val();

        if (usuario_subadministrador != '' && password_1 != '' && password_2 != '') {
            Swal({
                title: 'Crear notificación',
                text: 'Estas seguro de crear esta notificación ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Crear',
                cancelButtonText: 'Cancelar',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {


                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/subadministradores.php",
                        data: {
                            "newpassword2": password_2,
                            "newpassword": password_1,
                            "usuario_subadministrador": usuario_subadministrador,
                            "userid": userid,
                            "CreateUserNotificacion": 'SMD69'
                        },
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                notificacionesausuarios('');
                                Swal({
                                    title: 'Notificación enviada',
                                    text: 'Notificación enviada correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {

                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('info', 'Recuerda Que Toda La Informacion Es Necesaria', 'Error');
        }


    }
</script>