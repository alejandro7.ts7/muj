<?php 

/*Funcion que devuelve el Navegador Actual*/
function obtenerNavegadorWeb()
{
$agente = $_SERVER['HTTP_USER_AGENT'];
$navegador = 'Unknown';
$platforma = 'Unknown';
$version= "";
#Obtenemos la Plataforma
if (preg_match('/linux/i', $agente)) {
$platforma = 'linux';
}
elseif (preg_match('/macintosh|mac os x/i', $agente)) {
$platforma = 'mac';
}
elseif (preg_match('/windows|win32/i', $agente)) {
$platforma = 'windows';
}
#Obtener el UserAgente
if(preg_match('/MSIE/i',$agente) && !preg_match('/Opera/i',$agente))
{
$navegador = 'Internet Explorer';
$navegador_corto = "MSIE";
}
elseif(preg_match('/Firefox/i',$agente))
{
$navegador = 'Mozilla Firefox';
$navegador_corto = "Firefox";
}
elseif(preg_match('/Chrome/i',$agente))
{
$navegador = 'Google Chrome';
$navegador_corto = "Chrome";
}
elseif(preg_match('/Safari/i',$agente))
{
$navegador = 'Apple Safari';
$navegador_corto = "Safari";
}
elseif(preg_match('/Opera/i',$agente))
{
$navegador = 'Opera';
$navegador_corto = "Opera";
}
elseif(preg_match('/Netscape/i',$agente))
{
$navegador = 'Netscape';
$navegador_corto = "Netscape";
}
#Obtenemos la Version
$known = array('Version', $navegador_corto, 'other');
$pattern = '#(?' . join('|', $known) .
')[/ ]+(?[0-9.|a-zA-Z.]*)#';
if (!preg_match_all($pattern, $agente, $matches)) {
#No se obtiene la version simplemente continua
}
$i = count($matches['browser']);
if ($i != 1) {
if (strripos($agente,"Version") < strripos($agente,$navegador_corto)){ $version= $matches['version'][0]; } else { $version= $matches['version'][1]; } } else { $version= $matches['version'][0]; } /*Verificamos si tenemos Version*/ if ($version==null || $version=="") {$version="?";} 

/*Resultado final del Navegador Web que Utilizamos*/ 
    return array(
'agente' => $agente,
'nombre'      => $navegador,
'version'   => $version,
'platforma'  => $platforma
);
}

function getRealIPAfiliadoVi()
		{

		    if (isset($_SERVER["HTTP_CLIENT_IP"]))
		    {
		        return $_SERVER["HTTP_CLIENT_IP"];
		    }
		    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		    {
		        return $_SERVER["HTTP_X_FORWARDED_FOR"];
		    }
		    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
		    {
		        return $_SERVER["HTTP_X_FORWARDED"];
		    }
		    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
		    {
		        return $_SERVER["HTTP_FORWARDED_FOR"];
		    }
		    elseif (isset($_SERVER["HTTP_FORWARDED"]))
		    {
		        return $_SERVER["HTTP_FORWARDED"];
		    }
		    else
		    {
		        return $_SERVER["REMOTE_ADDR"];
		    }

		}

	function devolver_id_from_country($ip_data){
		//$ip_data = getRealIPAfiliadoVi();
		//$ip_data = "190.70.133.104";

		$con = conection_database();
		$Sql_Query = "SELECT 
	            c.country,c.iso_country,iso_code_2,c.iso_code_3 
	        FROM 
	            ip2nationcountries c,
	            ip2nation i
	        WHERE 
	            i.ip < INET_ATON('".$ip_data."') 
	            AND 
	            c.code = i.country 
	        ORDER BY 
	            i.ip DESC 
	        LIMIT 0,1 ";
		$check = mysqli_fetch_assoc(mysqli_query($con,$Sql_Query));


		$Sql_Query_d = "SELECT id,pais  FROM `paises` WHERE `iso` = '" . $check["iso_code_2"] . "' ";
		$checkD = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d));


		$Sql_Query_d2 = "SELECT *  FROM `paises_timezone` WHERE `iso` = '" . $check["iso_code_2"] . "' ORDER BY `paises_timezone`.`id` DESC LIMIT 1";
		$checkD2 = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d2));

		if (isset($check)) {
			$result = array("pais" => $checkD["pais"], "iso" => $check["iso_code_2"], "idCountry" => $checkD["id"], "pais2" => $check["iso_country"], 'timezone' => $checkD2["utc"]
			);
		} else {
			$result = array("pais" => "", "iso" => "", "idCountry" => "");
		}

		close_database($con);

		return $result;

	}



function guardarLoginActivityAdmin($idUsuarioCrypt , $idUsuarioN){
    if($idUsuarioCrypt == encriptar_datos_id($idUsuarioN)){

        // primero obtener la ip 
        $ip =  getRealIPAfiliadoVi();
        //$ip = "190.70.133.104";
        // obtener pais de login 
        $paisLogin = devolver_id_from_country($ip);
        $coutryL =  $paisLogin["pais"];
        // obtener datos del navegador 
        $ew = obtenerNavegadorWeb();
        $navegador = $ew['nombre']; //Nombre del Navegador en Uso
        $plataforma= $ew['platforma']; //Plataforma

        // fecha 
        $date = date('Y-m-d H:i:s');
        // insertar el log en db 
        $con = conection_database();
        $Sql_Query = "INSERT INTO `adminlogactivity` ( `ip`,`browser`,`system`,`country`, `time`,`idU`) VALUES ('". $ip. "','" . $navegador . "','" . $plataforma . "','" . $coutryL . "', '". $date."','". $idUsuarioN."');";
        $check = mysqli_query($con, $Sql_Query);
        close_database($con);
        if ($check) {
            return true;
        } else {
            return false;
        }
       
    }else {
        return false;
    }
}
