<?php
date_default_timezone_set('America/Bogota');

function encriptar($string, $key){
	$result = '';
	for($i=0; $i<strlen($string); $i++) {
	   	$char = substr($string, $i, 1);
	    $keychar = substr($key, ($i % strlen($key))-1, 1);
	    $char = chr(ord($char)+ord($keychar));
	    $result.=$char;
	}

	return base64_encode($result);
}

function desencriptar($string, $key){
   	$result = '';
   	$string = base64_decode($string);
   	for($i=0; $i<strlen($string); $i++) {
      	$char = substr($string, $i, 1);
      	$keychar = substr($key, ($i % strlen($key))-1, 1);
      	$char = chr(ord($char)-ord($keychar));
      	$result.=$char;
   	}
   	return $result;
}

function encriptar_datos_id($data_a_encriptar){

      $algorithm = MCRYPT_BLOWFISH;
      $key = 'GHFGF78DFTGHJ67';
      $key_2 = 'SYUEKLODUJI';
      $data = $data_a_encriptar;
      $mode = MCRYPT_MODE_CBC;
      $iv = "REWSDGFH";
      $encrypted_data = mcrypt_encrypt($algorithm, $key, $data, $mode, $iv);
      $plain_text = base64_encode($encrypted_data);

      $plain_text = encriptar($plain_text, $key_2).$key_2. base64_encode(base64_encode($iv)) ;
      return $plain_text; 
}
 
function desencriptar_datos_id($data_string){

   $algorithm = MCRYPT_BLOWFISH;
   $key = 'GHFGF78DFTGHJ67';
   $key_2 = 'SYUEKLODUJI';
   $mode = MCRYPT_MODE_CBC;

   $plain_text = explode($key_2, $data_string);
   $iv =  "REWSDGFH";
   $plain_text = desencriptar($plain_text[0], $key_2);
   $encrypted_data = base64_decode($plain_text);
   $decoded = mcrypt_decrypt($algorithm, $key, $encrypted_data, $mode, $iv);
   $decoded = (int) $decoded;
   return $decoded ;
}

/*
function encriptar_datos_id($id){

	$result =$id;
	$result = $result.'jhDSMD69';
	
	return $result;
};
function desencriptar_datos_id($id){

	$idresultante = explode('jhDSMD69', $id);
	$result =$idresultante[0];
	
	return $result;
};
*/

function encriptar_contrasena($string){

  

	$result = password_hash($string, PASSWORD_DEFAULT );

	return $result;
};




function FechaGloabl(){
	$date = date('Y-m-d H:i:s');
	return $date;
}

function gen_uuid() { 

	 return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x', // 32 bits for "time_low"
	 mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), // 16 bits for "time_mid" 
	 mt_rand( 0, 0xffff ), // 16 bits for "time_hi_and_version", // four most significant bits holds version number 4 
	 mt_rand( 0, 0x0fff ) | 0x4000, // 16 bits, 8 bits for "clk_seq_hi_res", // 8 bits for "clk_seq_low", // two most significant bits holds zero and one for variant DCE1.1 
	 mt_rand( 0, 0x3fff ) | 0x8000, // 48 bits for "node"
	 mt_rand( 0, 0xffff ),mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) ); 
}



function acte_wallet_coinb($tringData){
  
   $keyData = "UTJKLOHJYTECGFHRTECV";
   $data = encriptar_wallet_coinba($tringData,$keyData);
   

   return $data ;
}



function encriptar_wallet_coinba($string,$key){

   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);

}



function decryptDataWalle_coin_ba_final($tringData){

   $keyData = "UTJKLOHJYTECGFHRTECV";
   $data = decryptDataWalle_coin_ba($tringData,$keyData);
   

   return $data ;
}


function decryptDataWalle_coin_ba($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}




?>