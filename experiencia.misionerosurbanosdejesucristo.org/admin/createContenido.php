<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 2;
require_once('include/function_admin.php');
$optionContenidoCreado = obtener_select_contenidos($_SESSION["adminMisionerosUrbanos"][0]);
?>

<style>
    .dropdown-menu.note-check a i {
        visibility: visible !important;
        color: black;
    }

    .note-popover .popover-content .dropdown-menu.note-check a i,
    .card-header.note-toolbar .dropdown-menu.note-check a i {
        visibility: visible !important;
        color: black;
    }

    .note-icon-menu-check:before {
        content: "";
    }

    .note-icon-menu-check span {
        color: #000 !important;
        visibility: visible !important;
    }

    .note-icon-menu-check {
        visibility: visible !important;
    }
</style>
<link href="lib/summernote/css/summernote-bs4.css" rel="stylesheet">

<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Crear contenido</h3>
                        <div class="nk-block-des text-soft">

                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <div class="row g-gs">
                    <div class="col-12">
                        <div class="card  card-full">
                            <div class="col-lg-12 col-md-12 mb-4">
                                <label class="form-control-label" style="text-transform:initial;">Tipo de contenido</label>
                                <select onchange="tipo_contenido_a_crear()" id="tipo_contenido" name="tipo_contenido" class="form-control">
                                    <option value="0">Selecciona un tipo de contenido</option>
                                    <option value="1">Colección documental</option>
                                    <option value="2">Colección audiovisual</option>
                                    <option value="3">Noticias MUJ</option>
                                    <option value="5">Programación actividades MUJ</option>
                                    <option value="4">Páginas de interés</option>
                                </select>

                                <label class="form-control-label" style="text-transform:initial;margin-bottom: 0px;margin-top: 5px;">Posicionar contenido después de </label>
                                <select id="posicion_contenido_usuario" name="posicion_contenido_usuario" class="form-control">
                                    <option value="0">Posicionar contenido después de </option>
                                    <?php echo $optionContenidoCreado; ?>
                                </select>
                            </div>





                        </div><!-- .card -->
                    </div><!-- .col -->


                    <div id="cuadroModal1" style="display: none;" class="col-12">

                        <form enctype="multipart/form-data" method="post">

                            <div class="card card-full">
                                <div class="col-lg-12 col-md-12 mb-4">
                                    <label class="form-control-label text-uppercase">Categorías<br>
                                        (Ejemplo Artículos científicos – Revistas – Textos – Noticias – Escuelas de Padres – Libros – Publicaciones – Etc )</label>
                                    <input type="text" placeholder="Categorías" value="" id="tipo_coleccion" name="tipo_coleccion" class="form-control">


                                    <br>
                                    <label class="form-control-label text-uppercase">Titulo</label>
                                    <input type="text" placeholder="Titulo" value="" id="titulo_coleccion" name="titulo_coleccion" class="form-control">

                                    <br>
                                    <label class="form-control-label text-uppercase">Fecha cuando fue publicado</label>
                                    <input type="date" placeholder="Fecha Cuando Fue Publicado" value="" id="fecha_coleccion" name="fecha_coleccion" class="form-control">


                                    <br>
                                    <label class="form-control-label text-uppercase">Autor</label>
                                    <input type="text" placeholder="Nombre Del Autor O Autores" value="" id="autores_coleccion" name="autores_coleccion" class="form-control">


                                    <br>
                                    <label class="form-control-label text-uppercase">Resumen de contenido (Maximo 500 caractres)</label>
                                    <textarea type="date" placeholder="Resumen De Contenido" value="" id="desc_collecion_resumen" name="desc_collecion_resumen" class="form-control" rows="4"></textarea>


                                    <br>
                                    <div class="form-check">
                                        <input onchange="mostrarCamposEdit()" class="form-check-input" type="checkbox" value="" id="activaFromulario1">
                                        <label class="form-check-label" for="activaFromulario1">
                                            Activar fomulario
                                        </label>
                                    </div>
                                    <br>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="activaFromulario2_repguntas1">
                                        <label class="form-check-label" for="activaFromulario2_repguntas1">
                                            Activar Seccion Preguntas Y Respuestas
                                        </label>
                                    </div>

                                    <br>

                                    <div id="mostrarcamposEditables" style="display:none;" class="col-lg-12 col-md-12 mb-4">

                                    </div>

                                    <br>


                                    <label class="form-control-label text-uppercase">Descripcion</label>


                                    <div class="summernote" id="desc_collecion"></div>


                                    <?php /*<textarea type="date" placeholder="Descripcion" value="" id="desc_collecion" name="desc_collecion" class="form-control" rows="8"></textarea> */ ?>



                                    <br>
                                    <label class="form-control-label text-uppercase">Videos de contenido (Recordar que se pueden usar links de vimeo o de youtube y que cada link se deberia separar por una coma ejemplo youbelink1,youtubelink2,youtubelink3)</label>
                                    <textarea type="date" placeholder="Videos de contenido" value="" id="links_video" name="links_video" class="form-control" rows="4"></textarea>


                                    <!-- Contenido o opcion par subir archivos -->
                                    <br>
                                    <label class="form-control-label text-uppercase">Imagen portada</label>
                                    <input type="file" placeholder="Imagen Portada" value="" id="imagen_portada" name="imagen_portada" class="form-control">



                                    <!-- Contenido o opcion par subir archivos -->
                                    <br>
                                    <label class="form-control-label text-uppercase">Cargar adjuntos</label>
                                    <input type="file" placeholder="Contenido" value="" id="files" name="files" class="form-control" multiple>

                                    <br>
                                    <button onclick="guardar_info2(event)" type="submit" class="btn btn-primary">Guardar coleccion documental </button>


                                </div>



                            </div><!-- .card -->


                        </form>


                    </div><!-- .col -->


                    <div id="cuadroModal2" style="display: none;" class="col-12">

                        <form enctype="multipart/form-data" method="post">

                            <div class="card card-full">
                                <div class="col-lg-12 col-md-12 mb-4">
                                    <label class="form-control-label text-uppercase">Categoria<br>
                                        (Ejemplo Galería de imágenes – Música – Películas – conferencias – Videos – Audios - Podcats)</label>
                                    <input type="text" placeholder="Categoria" value="" id="tipo_coleccion2" name="tipo_coleccion2" class="form-control">


                                    <br>
                                    <label class="form-control-label text-uppercase">Titulo</label>
                                    <input type="text" placeholder="Titulo" value="" id="titulo_coleccion2" name="titulo_coleccion2" class="form-control">

                                    <br>
                                    <label class="form-control-label text-uppercase">Fecha cuando fue publicado</label>
                                    <input type="date" placeholder="Fecha Cuando Fue Publicado" value="" id="fecha_coleccion2" name="fecha_coleccion2" class="form-control">

                                    <br>
                                    <label class="form-control-label text-uppercase">Autor</label>
                                    <input type="text" placeholder="Nombre Del Autor O Autores" value="" id="autores2_coleccion" name="autores2_coleccion" class="form-control">



                                    <br>
                                    <label class="form-control-label text-uppercase">Resumen de contenido (Maximo 500 caracteres)</label>
                                    <textarea type="date" placeholder="Resumen De Contenido" value="" id="desc_collecion2_resumen" name="desc_collecion2_resumen" class="form-control" rows="4"></textarea>


                                    <br>
                                    <div class="form-check">
                                        <input onchange="mostrarCamposEdit2()" class="form-check-input" type="checkbox" value="" id="activaFromulario2">
                                        <label class="form-check-label" for="activaFromulario2">
                                            Activar fomulario
                                        </label>
                                    </div>

                                    <br>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="activaFromulario2_repguntas2">
                                        <label class="form-check-label" for="activaFromulario2_repguntas2">
                                            Activar Seccion Preguntas Y Respuestas
                                        </label>
                                    </div>



                                    <br>

                                    <div id="mostrarcamposEditables2" style="display:none;" class="col-lg-12 col-md-12 mb-4">

                                    </div>





                                    <br>
                                    <label class="form-control-label text-uppercase">Descripcion </label>
                                    <div class="summernote" id="desc_collecion2"></div>


                                    <?php /* <textarea type="date" placeholder="Descripcion" value="" id="desc_collecion2" name="desc_collecion2" class="form-control" rows="8"></textarea> */ ?>



                                    <br>
                                    <label class="form-control-label text-uppercase">Videos de contenido (Recordar que se pueden usar links de vimeo o de youtube y que cada link se deberia separar por una coma ejemplo youbelink1,youtubelink2,youtubelink3)</label>
                                    <textarea type="date" placeholder="Videos de contenido" value="" id="links_video2" name="links_video2" class="form-control" rows="4"></textarea>




                                    <!-- Contenido o opcion par subir archivos -->
                                    <br>
                                    <label class="form-control-label text-uppercase">Imagen portada</label>
                                    <input type="file" placeholder="Imagen Portada" value="" id="imagen_portada2" name="imagen_portada2" class="form-control">



                                    <!-- Contenido o opcion par subir archivos -->
                                    <br>
                                    <label class="form-control-label text-uppercase">Cargar adjuntos</label>
                                    <input type="file" placeholder="Contenido" value="" id="files2" name="files2" class="form-control" multiple>

                                    <br>
                                    <button onclick="guardar_info3(event)" type="submit" class="btn btn-primary">Guardar Coleccion Audiovisual </button>


                                </div>



                            </div><!-- .card -->


                        </form>


                    </div><!-- .col -->


                    <div id="cuadroModal4" style="display: none;" class="col-12">
                        <div class="card card-full">
                            <div class="col-lg-12 col-md-12 mb-4">

                                <label class="form-control-label text-uppercase">Tipo pagina <br> ejemplo (Iglesia,Informativas,Aliados)</label>
                                <input type="text" placeholder="Tipo Pagina" value="" id="tipo_pagina_interes" name="tipo_pagina_interes" class="form-control">

                                <br>


                                <label class="form-control-label text-uppercase">Link de pagina de interes</label>
                                <input type="text" placeholder="Link De Pagina De Interes" value="" id="pagina_interes" name="pagina_interes" class="form-control">


                                <!-- Contenido o opcion par subir archivos -->
                                <br>
                                <label class="form-control-label text-uppercase">Imagen </label>
                                <input type="file" placeholder="Imagen Portada" value="" id="imagen_portada3" name="imagen_portada3" class="form-control">


                                <br>
                                <button onclick="guardar_info4()" type="submit" class="btn btn-primary">Guardar pagina de interes </button>

                            </div>



                        </div><!-- .card -->
                    </div><!-- .col -->

                    <div id="cuadroModal5" style="display: none;" class="col-12">
                        <div class="card card-full">
                            <div class="col-lg-12 col-md-12 mb-4">
                                <label class="form-control-label text-uppercase">Fecha actividad</label>
                                <input type="date" placeholder="Fecha Evento" value="" id="fecha_evento" name="fecha_evento" class="form-control">

                                <br>
                                <label class="form-control-label text-uppercase">Hora actividad</label>
                                <input type="time" placeholder="Hora Evento" value="" id="hora_evento" name="hora_evento" class="form-control">


                                <br>
                                <label class="form-control-label text-uppercase">Nombre de la actividad</label>
                                <input type="text" placeholder="Nombre Del Evento" value="" id="nombre_evento" name="nombre_evento" class="form-control">


                                <br>
                                <label class="form-control-label text-uppercase">Url video, poster, imagen, etc.</label>
                                <input type="text" placeholder="Url video, poster, imagen, etc." value="" id="url_videoinfo1" name="url_videoinfo1" class="form-control">

                                <br>
                                <button onclick="guardar_info()" type="submit" class="btn btn-primary">Guardar actividad MUJ </button>
                            </div>







                        </div><!-- .card -->
                    </div><!-- .col -->


                    <div id="cuadroModal6" style="display: none;" class="col-12">
                        <div class="card card-full">
                            <div class="col-lg-12 col-md-12 mb-4">
                                <label class="form-control-label text-uppercase">Fecha noticia</label>
                                <input type="date" placeholder="Fecha Evento" value="" id="fecha_evento2" name="fecha_evento2" class="form-control">

                                <br>
                                <label class="form-control-label text-uppercase">Hora noticia</label>
                                <input type="time" placeholder="Hora Evento" value="" id="hora_evento2" name="hora_evento2" class="form-control">


                                <br>
                                <label class="form-control-label text-uppercase">Nombre de la noticia</label>
                                <input type="text" placeholder="Nombre Del Evento" value="" id="nombre_evento2" name="nombre_evento2" class="form-control">


                                <br>
                                <label class="form-control-label text-uppercase">Url video, poster, imagen, etc.</label>
                                <input type="text" placeholder="Url video, poster, imagen, etc." value="" id="url_videoinfo2" name="url_videoinfo2" class="form-control">

                                <br>
                                <button onclick="guardar_info()" type="submit" class="btn btn-primary">Guardar noticia </button>
                            </div>







                        </div><!-- .card -->
                    </div><!-- .col -->


                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->
<script type="text/javascript" src="js/typeahead.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".note-color-all .dropdown-toggle").html('▼');
    });


    var informacion = $('#desc_collecion').summernote({
        height: 650,
        tooltip: false,
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                that = $(this);
                sendFile(files[0], editor, welEditable, that);
            },
            onFileUpload: function(file) {
                that = $(this);
                myOwnCallBack(file[0], that, 1);
            },
        },
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        lang: 'es-ES', // Change to your chosen language
        imageAttributes: {
            icon: '<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '32', '36', '40', '48', '50', '60', '72', '80', '90', '100', '120', '150', '200', '240'],
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video', 'file']],
            ['view', ['fullscreen']],

        ],
    });

    // ['table', ['table']],
    // ['view', ['fullscreen', 'codeview', 'help']],

    var informacion2 = $('#desc_collecion2').summernote({
        height: 650,
        tooltip: false,
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                that = $(this);
                sendFile(files[0], editor, welEditable, that);
            },
            onFileUpload: function(file) {
                //alert('ok');
                that = $(this);
                myOwnCallBack(file[0], that, 2);
            },
        },
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        lang: 'es-ES', // Change to your chosen language
        imageAttributes: {
            icon: '<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '32', '36', '40', '48', '50', '60', '72', '80', '90', '100', '120', '150', '200', '240'],
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video', 'file']],
            ['view', ['fullscreen']],

        ],
    });

    function sendFile(file, editor, welEditable, edicionSumer) {
        data = new FormData();
        data.append("file", file);
        data.append("SubirImagenTarjeta", "SMD69");
        data.append("userid", '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>');
        $.ajax({
            data: data,
            type: "POST",
            url: "ajax/subidaimagentarjetapersonal.php",
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(url) {
                if (url.suceso == 'ok') {
                    edicionSumer.summernote('insertImage', url.mensaje)
                } else {
                    cargar_swal('error', url.mensaje, 'Error');
                }
            }
        });
    }

    function myOwnCallBack(file, edicionSumer, edito2ver) {

        let data = new FormData();
        data.append("file", file);
        data.append("SubirImagenTarjetaFileAcrhivo", "SMD69");
        data.append("userid", '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>');

        if (edito2ver == 1) {
            var varData = 'desc_collecion';
        } else if (edito2ver == 2) {
            var varData = 'desc_collecion2';
        }

        $.ajax({
            data: data,
            type: "POST",
            url: "ajax/subidaimagentarjetapersonalExtras.php", //Your own back-end uploader
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            xhr: function() { //Handle progress upload

                let myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                return myXhr;
            },
            success: function(reponse) {
                console.log(reponse);
                if (reponse.suceso === 'ok') {
                    let listMimeImg = ['image/png', 'image/jpeg', 'image/webp', 'image/gif', 'image/svg'];
                    let listMimeAudio = ['audio/mpeg', 'audio/ogg'];
                    let listMimeVideo = ['video/mpeg', 'video/mp4', 'video/webm'];
                    let elem;



                    if (listMimeImg.indexOf(file.type) > -1) {
                        //Picture
                        //alert('aqui2');
                        //alert(reponse.mensaje);
                        $("#" + varData).summernote('editor.insertImage', reponse.mensaje);
                        //edicionSumer.summernote('insertImage', reponse.mensaje)
                    } else if (listMimeAudio.indexOf(file.type) > -1) {
                        //Audio
                        elem = document.createElement("audio");
                        elem.setAttribute("src", reponse.mensaje);
                        elem.setAttribute("controls", "controls");
                        elem.setAttribute("preload", "metadata");
                        $("#" + varData).summernote('editor.insertNode', elem);
                    } else if (listMimeVideo.indexOf(file.type) > -1) {
                        //Video
                        elem = document.createElement("video");
                        elem.setAttribute("src", reponse.mensaje);
                        elem.setAttribute("controls", "controls");
                        elem.style = "width:100%";
                        elem.setAttribute("preload", "metadata");
                        $("#" + varData).summernote('editor.insertNode', elem);
                    } else {
                        // console.log(file.type);
                        //Other file type
                        elem = document.createElement("a");
                        let linkText = document.createTextNode(file.name);
                        elem.appendChild(linkText);
                        elem.title = file.name;
                        elem.href = reponse.mensaje;
                        $("#" + varData).summernote('editor.insertNode', elem);
                    }
                }
            }
        });
    }

    function progressHandlingFunction(e) {
        if (e.lengthComputable) {
            //Log current progress
            //console.log((e.loaded / e.total * 100) + '%');

            //Reset progress on complete
            if (e.loaded === e.total) {
                //console.log("Upload finished.");
            }
        }
    }

    $('#tipo_coleccion').typeahead({
        source: function(query, result) {
            $.ajax({
                url: "ajax/server_tipos.php",
                data: 'query=' + query,
                dataType: "json",
                type: "POST",
                success: function(data) {
                    result($.map(data, function(item) {
                        return item;
                    }));
                }
            });
        }
    });

    $('#tipo_coleccion2').typeahead({
        source: function(query, result) {
            $.ajax({
                url: "ajax/server_tipos3.php",
                data: 'query=' + query,
                dataType: "json",
                type: "POST",
                success: function(data) {
                    result($.map(data, function(item) {
                        return item;
                    }));
                }
            });
        }
    });

    $('#tipo_pagina_interes').typeahead({
        source: function(query, result) {
            $.ajax({
                url: "ajax/server_tipos2.php",
                data: 'query=' + query,
                dataType: "json",
                type: "POST",
                success: function(data) {
                    result($.map(data, function(item) {
                        return item;
                    }));
                }
            });
        }
    });
</script>


<script>
    function tipo_contenido_a_crear() {
        var tipo_contenido = $("#tipo_contenido").val();
        //alert(tipo_contenido);

        if (tipo_contenido == 1) {
            cuadroModal1 = $("#cuadroModal1").css('display', 'block');
        } else {
            cuadroModal1 = $("#cuadroModal1").css('display', 'none');
        }

        if (tipo_contenido == 2) {
            cuadroModal2 = $("#cuadroModal2").css('display', 'block');
        } else {
            cuadroModal2 = $("#cuadroModal2").css('display', 'none');
        }

        if (tipo_contenido == 3) {
            cuadroModal6 = $("#cuadroModal6").css('display', 'block');
        } else {
            cuadroModal6 = $("#cuadroModal6").css('display', 'none');
        }

        if (tipo_contenido == 4) {
            cuadroModal4 = $("#cuadroModal4").css('display', 'block');
        } else {
            cuadroModal4 = $("#cuadroModal4").css('display', 'none');
        }

        if (tipo_contenido == 5) {
            cuadroModal5 = $("#cuadroModal5").css('display', 'block');
        } else {
            cuadroModal5 = $("#cuadroModal5").css('display', 'none');
        }





        // alert(tipo_contenido);
    }

    function mostrarCamposEdit() {
        $("#mostrarcamposEditables").html('');
        $("#mostrarcamposEditables2").html('');

        $("#mostrarcamposEditables").html(` <div class="row">

                                            <div class="col-12">
                                                <label class="form-control-label text-uppercase">Descripción de formulario (máximo 300 caracteres)</label>
                                                <input type="text" placeholder="" value="" id="desc_form" name="desc_form" class="form-control">
                                            </div>


                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 1</label>
                                                <input type="text" placeholder="Nombre" value="Nombre" id="c1" name="c1" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c1_activo" name="c1_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 2</label>
                                                <input type="text" placeholder="Apellido" value="Apellido" id="c2" name="c2" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c2_activo" name="c2_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 3</label>
                                                <input type="text" placeholder="Correo Electr&oacute;nico" value="Correo Electr&oacute;nico" id="c3" name="c3" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c3_activo" name="c3_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 4</label>
                                                <input type="text" placeholder="WhatsApp / M&oacute;vil	" value="WhatsApp / M&oacute;vil" id="c4" name="c4" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c4_activo" name="c4_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 5</label>
                                                <input type="text" placeholder="Pa&iacute;s" value="Pa&iacute;s" id="c5" name="c5" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c5_activo" name="c5_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 6</label>
                                                <input type="text" placeholder="Ciudad" value="Ciudad" id="c6" name="c6" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c6_activo" name="c6_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Campo 7</label>
                                                <input type="text" placeholder="Campo 7" value="Campo 7" id="c7" name="c7" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c7_activo" name="c7_activo" class="form-control">

                                                    <option selected value="0">No </option>
                                                    <option value="1">Si </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Campo 8</label>
                                                <input type="text" placeholder="Campo 8" value="Campo 8" id="c8" name="c8" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c8_activo" name="c8_activo" class="form-control">
                                                    <option selected value="0">No </option>
                                                    <option value="1">Si </option>


                                                </select>
                                            </div>
                                        </div>`);
        if ($('#activaFromulario1').is(':checked')) {
            var activaFromulario = 1;
            $("#mostrarcamposEditables").css('display', 'block');

        } else {
            var activaFromulario = 0;
            $("#mostrarcamposEditables").css('display', 'none');
        }
        // alert(activaFromulario);
    }

    function mostrarCamposEdit2() {
        $("#mostrarcamposEditables2").html('');
        $("#mostrarcamposEditables").html('');

        $("#mostrarcamposEditables2").html(` <div class="row">

                                            <div class="col-12">
                                                <label class="form-control-label text-uppercase">Descripción de formulario (máximo 300 caracteres)</label>
                                                <input type="text" placeholder="" value="" id="desc_form" name="desc_form" class="form-control">
                                            </div>


                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 1</label>
                                                <input type="text" placeholder="Nombre" value="Nombre" id="c1" name="c1" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c1_activo" name="c1_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 2</label>
                                                <input type="text" placeholder="Apellido" value="Apellido" id="c2" name="c2" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c2_activo" name="c2_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 3</label>
                                                <input type="text" placeholder="Correo Electr&oacute;nico" value="Correo Electr&oacute;nico" id="c3" name="c3" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c3_activo" name="c3_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 4</label>
                                                <input type="text" placeholder="WhatsApp / M&oacute;vil	" value="WhatsApp / M&oacute;vil" id="c4" name="c4" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c4_activo" name="c4_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 5</label>
                                                <input type="text" placeholder="Pa&iacute;s" value="Pa&iacute;s" id="c5" name="c5" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c5_activo" name="c5_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Nombre Campo 6</label>
                                                <input type="text" placeholder="Ciudad" value="Ciudad" id="c6" name="c6" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c6_activo" name="c6_activo" class="form-control">
                                                    <option selected value="1">Si </option>
                                                    <option value="0">No </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Campo 7</label>
                                                <input type="text" placeholder="Campo 7" value="Campo 7" id="c7" name="c7" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c7_activo" name="c7_activo" class="form-control">

                                                    <option selected value="0">No </option>
                                                    <option value="1">Si </option>

                                                </select>
                                            </div>

                                            <div class="col-8">
                                                <label class="form-control-label text-uppercase">Campo 8</label>
                                                <input type="text" placeholder="Campo 8" value="Campo 8" id="c8" name="c8" class="form-control">
                                            </div>

                                            <div class="col-4">
                                                <label class="form-control-label text-uppercase">Campo Activo </label>
                                                <select id="c8_activo" name="c8_activo" class="form-control">
                                                    <option selected value="0">No </option>
                                                    <option value="1">Si </option>


                                                </select>
                                            </div>
                                        </div>`);
        if ($('#activaFromulario2').is(':checked')) {
            var activaFromulario = 1;
            $("#mostrarcamposEditables2").css('display', 'block');

        } else {
            var activaFromulario = 0;
            $("#mostrarcamposEditables2").css('display', 'none');
        }
        // alert(activaFromulario);
    }



    function guardar_info2() {
        event.preventDefault();




        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var tipo_contenido = $("#tipo_contenido").val();
        var pagina_interes = $("#pagina_interes").val();


        var fecha_evento = $("#fecha_evento").val();
        var hora_evento = $("#hora_evento").val();
        var nombre_evento = $("#nombre_evento").val();

        var fecha_evento2 = $("#fecha_evento2").val();
        var hora_evento2 = $("#hora_evento2").val();
        var nombre_evento2 = $("#nombre_evento2").val();


        var fecha_coleccion = $("#fecha_coleccion").val();
        var tipo_coleccion = $("#tipo_coleccion").val();
        var titulo_coleccion = $("#titulo_coleccion").val();
        var desc_collecion = informacion.summernote('code');
        var tipo_pagina_interes = $("#tipo_pagina_interes").val();
        var posicion_contenido_usuario = $("#posicion_contenido_usuario").val();



        if ($('#activaFromulario1').is(':checked')) {
            var activaFromulario = 1;
        } else {
            var activaFromulario = 0;
        }

        if ($('#activaFromulario2_repguntas1').is(':checked')) {
            var activaFromularioPreguntas = 1;
        } else {
            var activaFromularioPreguntas = 0;
        }

        //alert(activaFromulario)

        var imagen_portada = $("#imagen_portada")[0].files[0];
        var form_data = new FormData();
        var files = $("#files").get(0).files;
        for (var i = 0; i < files.length; i++) {
            form_data.append("files[" + i + "]", files[i]);
        }

        var desc_collecion_resumen = $("#desc_collecion_resumen").val();
        var autores_coleccion = $("#autores_coleccion").val();



        var desc_form = $("#desc_form").val();
        var links_video = $("#links_video").val();
        var c1 = $("#c1").val();
        var c1_activo = $("#c1_activo").val();
        var c2 = $("#c2").val();
        var c2_activo = $("#c2_activo").val();
        var c3 = $("#c3").val();
        var c3_activo = $("#c3_activo").val();
        var c4 = $("#c4").val();
        var c4_activo = $("#c4_activo").val();
        var c5 = $("#c5").val();
        var c5_activo = $("#c5_activo").val();
        var c6 = $("#c6").val();
        var c6_activo = $("#c6_activo").val();
        var c7 = $("#c7").val();
        var c7_activo = $("#c7_activo").val();
        var c8 = $("#c8").val();
        var c8_activo = $("#c8_activo").val();
        form_data.append("desc_form", desc_form);
        form_data.append("links_video", links_video);
        form_data.append("c1", c1);
        form_data.append("c1_activo", c1_activo);
        form_data.append("c2", c2);
        form_data.append("c2_activo", c2_activo);
        form_data.append("c3", c3);
        form_data.append("c3_activo", c3_activo);
        form_data.append("c4", c4);
        form_data.append("c4_activo", c4_activo);
        form_data.append("c5", c5);
        form_data.append("c5_activo", c5_activo);
        form_data.append("c6", c6);
        form_data.append("c6_activo", c6_activo);
        form_data.append("c7", c7);
        form_data.append("c7_activo", c7_activo);
        form_data.append("c8", c8);
        form_data.append("c8_activo", c8_activo);


        form_data.append("posicion_contenido_usuario", posicion_contenido_usuario);
        form_data.append("desc_collecion_resumen", desc_collecion_resumen);
        form_data.append("autores_coleccion", autores_coleccion);
        form_data.append("fecha_coleccion", fecha_coleccion);
        form_data.append("tipo_coleccion", tipo_coleccion);
        form_data.append("titulo_coleccion", titulo_coleccion);
        form_data.append("desc_collecion", desc_collecion);
        form_data.append("nombre_evento", nombre_evento);
        form_data.append("hora_evento", hora_evento);
        form_data.append("fecha_evento", fecha_evento);
        form_data.append("nombre_evento2", nombre_evento2);
        form_data.append("hora_evento2", hora_evento2);
        form_data.append("fecha_evento2", fecha_evento2);
        form_data.append("tipo_contenido", tipo_contenido);
        form_data.append("pagina_interes", pagina_interes);
        form_data.append("imagen_portada", imagen_portada);
        form_data.append("tipo_pagina_interes", tipo_pagina_interes);
        form_data.append("activaFromulario", activaFromulario);
        form_data.append("activaFromularioPreguntas", activaFromularioPreguntas);
        form_data.append("userid", userid);
        form_data.append("CrearContenidoInformacion", 'SMD69');

        // alert Crear contenido 

        var continuar = true;
        var mensajeError = '';

        if (tipo_contenido == 4) {
            if (pagina_interes == null || pagina_interes == '' || pagina_interes == 0 || pagina_interes == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que La Url Es Necesaria';
            }
        }

        if (tipo_contenido == 5) {
            if (fecha_evento == null || fecha_evento == '' || fecha_evento == 0 || fecha_evento == undefined || hora_evento == null || hora_evento == '' || hora_evento == 0 || hora_evento == undefined || nombre_evento == null || nombre_evento == '' || nombre_evento == 0 || nombre_evento == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que Toda La Informacion Es Necesaria';
            }
        }

        if (continuar == true) {
            Swal({
                title: 'Crear contenido',
                text: 'Estas seguro de crear este contenido ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Contenido creado',
                                    text: 'Contenido creado correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('error', mensajeError, 'Error');
        }

    }

    function guardar_info3() {
        event.preventDefault();




        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var tipo_contenido = $("#tipo_contenido").val();
        var pagina_interes = $("#pagina_interes").val();


        var fecha_evento = $("#fecha_evento").val();
        var hora_evento = $("#hora_evento").val();
        var nombre_evento = $("#nombre_evento").val();

        var fecha_evento2 = $("#fecha_evento2").val();
        var hora_evento2 = $("#hora_evento2").val();
        var nombre_evento2 = $("#nombre_evento2").val();


        var fecha_coleccion = $("#fecha_coleccion2").val();
        var tipo_coleccion = $("#tipo_coleccion2").val();
        var titulo_coleccion = $("#titulo_coleccion2").val();
        var desc_collecion = informacion2.summernote('code');
        var tipo_pagina_interes = $("#tipo_pagina_interes2").val();


        var imagen_portada = $("#imagen_portada2")[0].files[0];
        var posicion_contenido_usuario = $("#posicion_contenido_usuario").val();


        if ($('#activaFromulario2').is(':checked')) {
            var activaFromulario = 1;
        } else {
            var activaFromulario = 0;
        }

        if ($('#activaFromulario2_repguntas2').is(':checked')) {
            var activaFromularioPreguntas = 1;
        } else {
            var activaFromularioPreguntas = 0;
        }

        var form_data = new FormData();



        var files = $("#files2").get(0).files;
        for (var i = 0; i < files.length; i++) {
            form_data.append("files[" + i + "]", files[i]);
        }


        var desc_collecion_resumen = $("#desc_collecion2_resumen").val();
        var autores_coleccion = $("#autores2_coleccion").val();


        var desc_form = $("#desc_form").val();
        var links_video = $("#links_video2").val();
        var c1 = $("#c1").val();
        var c1_activo = $("#c1_activo").val();
        var c2 = $("#c2").val();
        var c2_activo = $("#c2_activo").val();
        var c3 = $("#c3").val();
        var c3_activo = $("#c3_activo").val();
        var c4 = $("#c4").val();
        var c4_activo = $("#c4_activo").val();
        var c5 = $("#c5").val();
        var c5_activo = $("#c5_activo").val();
        var c6 = $("#c6").val();
        var c6_activo = $("#c6_activo").val();
        var c7 = $("#c7").val();
        var c7_activo = $("#c7_activo").val();
        var c8 = $("#c8").val();
        var c8_activo = $("#c8_activo").val();
        form_data.append("desc_form", desc_form);
        form_data.append("links_video", links_video);
        form_data.append("c1", c1);
        form_data.append("c1_activo", c1_activo);
        form_data.append("c2", c2);
        form_data.append("c2_activo", c2_activo);
        form_data.append("c3", c3);
        form_data.append("c3_activo", c3_activo);
        form_data.append("c4", c4);
        form_data.append("c4_activo", c4_activo);
        form_data.append("c5", c5);
        form_data.append("c5_activo", c5_activo);
        form_data.append("c6", c6);
        form_data.append("c6_activo", c6_activo);
        form_data.append("c7", c7);
        form_data.append("c7_activo", c7_activo);
        form_data.append("c8", c8);
        form_data.append("c8_activo", c8_activo);



        form_data.append("posicion_contenido_usuario", posicion_contenido_usuario);
        form_data.append("desc_collecion_resumen", desc_collecion_resumen);
        form_data.append("autores_coleccion", autores_coleccion);
        form_data.append("fecha_coleccion", fecha_coleccion);
        form_data.append("tipo_coleccion", tipo_coleccion);
        form_data.append("titulo_coleccion", titulo_coleccion);
        form_data.append("desc_collecion", desc_collecion);
        form_data.append("nombre_evento", nombre_evento);
        form_data.append("hora_evento", hora_evento);
        form_data.append("fecha_evento", fecha_evento);
        form_data.append("nombre_evento2", nombre_evento2);
        form_data.append("hora_evento2", hora_evento2);
        form_data.append("fecha_evento2", fecha_evento2);
        form_data.append("tipo_contenido", tipo_contenido);
        form_data.append("pagina_interes", pagina_interes);
        form_data.append("imagen_portada", imagen_portada);
        form_data.append("tipo_pagina_interes", tipo_pagina_interes);
        form_data.append("activaFromulario", activaFromulario);
        form_data.append("activaFromularioPreguntas", activaFromularioPreguntas);
        form_data.append("userid", userid);
        form_data.append("CrearContenidoInformacion", 'SMD69');



        // alert Crear contenido 

        var continuar = true;
        var mensajeError = '';

        if (tipo_contenido == 4) {
            if (pagina_interes == null || pagina_interes == '' || pagina_interes == 0 || pagina_interes == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que La Url Es Necesaria';
            }
        }

        if (tipo_contenido == 5) {
            if (fecha_evento == null || fecha_evento == '' || fecha_evento == 0 || fecha_evento == undefined || hora_evento == null || hora_evento == '' || hora_evento == 0 || hora_evento == undefined || nombre_evento == null || nombre_evento == '' || nombre_evento == 0 || nombre_evento == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que Toda La Informacion Es Necesaria';
            }
        }

        if (continuar == true) {
            Swal({
                title: 'Crear contenido',
                text: 'Estas seguro de crear este contenido ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    // creando contenido 
                    Swal.fire({
                        title: 'info',
                        html: `Creando Contenido...`,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        type: '',
                    });
                    Swal.showLoading();

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(loginData) {
                            swal.close();
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Contenido creado',
                                    text: 'Contenido creado correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('error', mensajeError, 'Error');
        }

    }

    function guardar_info4() {
        event.preventDefault();




        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var tipo_contenido = $("#tipo_contenido").val();
        var pagina_interes = $("#pagina_interes").val();


        var fecha_evento = $("#fecha_evento").val();
        var hora_evento = $("#hora_evento").val();
        var nombre_evento = $("#nombre_evento").val();

        var fecha_evento2 = $("#fecha_evento2").val();
        var hora_evento2 = $("#hora_evento2").val();
        var nombre_evento2 = $("#nombre_evento2").val();


        var fecha_coleccion = $("#fecha_coleccion2").val();
        var tipo_coleccion = $("#tipo_coleccion2").val();
        var titulo_coleccion = $("#titulo_coleccion2").val();
        var desc_collecion = informacion2.summernote('code');
        var tipo_pagina_interes = $("#tipo_pagina_interes").val();
        var posicion_contenido_usuario = $("#posicion_contenido_usuario").val();


        var imagen_portada = $("#imagen_portada3")[0].files[0];

        var form_data = new FormData();



        var files = $("#files2").get(0).files;
        for (var i = 0; i < files.length; i++) {
            form_data.append("files[" + i + "]", files[i]);
        }


        var desc_collecion_resumen = $("#desc_collecion2_resumen").val();
        var autores_coleccion = $("#autores2_coleccion").val();



        form_data.append("posicion_contenido_usuario", posicion_contenido_usuario);
        form_data.append("desc_collecion_resumen", desc_collecion_resumen);
        form_data.append("autores_coleccion", autores_coleccion);
        form_data.append("fecha_coleccion", fecha_coleccion);
        form_data.append("tipo_coleccion", tipo_coleccion);
        form_data.append("titulo_coleccion", titulo_coleccion);
        form_data.append("desc_collecion", desc_collecion);
        form_data.append("nombre_evento", nombre_evento);
        form_data.append("hora_evento", hora_evento);
        form_data.append("fecha_evento", fecha_evento);
        form_data.append("nombre_evento2", nombre_evento2);
        form_data.append("hora_evento2", hora_evento2);
        form_data.append("fecha_evento2", fecha_evento2);
        form_data.append("tipo_contenido", tipo_contenido);
        form_data.append("pagina_interes", pagina_interes);
        form_data.append("imagen_portada", imagen_portada);
        form_data.append("tipo_pagina_interes", tipo_pagina_interes);
        form_data.append("userid", userid);
        form_data.append("CrearContenidoInformacion", 'SMD69');



        // alert Crear contenido 

        var continuar = true;
        var mensajeError = '';

        if (tipo_contenido == 4) {
            if (pagina_interes == null || pagina_interes == '' || pagina_interes == 0 || pagina_interes == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que La Url Es Necesaria';
            }
        }

        if (tipo_contenido == 5) {
            if (fecha_evento == null || fecha_evento == '' || fecha_evento == 0 || fecha_evento == undefined || hora_evento == null || hora_evento == '' || hora_evento == 0 || hora_evento == undefined || nombre_evento == null || nombre_evento == '' || nombre_evento == 0 || nombre_evento == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que Toda La Informacion Es Necesaria';
            }
        }

        if (continuar == true) {
            Swal({
                title: 'Crear contenido',
                text: 'Estas seguro de crear este contenido ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    // creando contenido 
                    Swal.fire({
                        title: 'info',
                        html: `Creando Contenido...`,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        type: '',
                    });
                    Swal.showLoading();

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(loginData) {
                            swal.close();
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Contenido creado',
                                    text: 'Contenido creado correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('error', mensajeError, 'Error');
        }

    }


    function guardar_info() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var tipo_contenido = $("#tipo_contenido").val();
        var pagina_interes = $("#pagina_interes").val();


        var fecha_evento = $("#fecha_evento").val();
        var hora_evento = $("#hora_evento").val();
        var nombre_evento = $("#nombre_evento").val();

        var fecha_evento2 = $("#fecha_evento2").val();
        var hora_evento2 = $("#hora_evento2").val();
        var nombre_evento2 = $("#nombre_evento2").val();
        var tipo_pagina_interes = $("#tipo_pagina_interes").val();

        var url_videoinfo2 = $("#url_videoinfo2").val();
        var url_videoinfo1 = $("#url_videoinfo1").val();
        var posicion_contenido_usuario = $("#posicion_contenido_usuario").val();



        // alert Crear contenido 

        var continuar = true;
        var mensajeError = '';

        if (tipo_contenido == 4) {
            if (pagina_interes == null || pagina_interes == '' || pagina_interes == 0 || pagina_interes == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que La Url Es Necesaria';
            }
        }

        if (tipo_contenido == 5) {
            if (fecha_evento == null || fecha_evento == '' || fecha_evento == 0 || fecha_evento == undefined || hora_evento == null || hora_evento == '' || hora_evento == 0 || hora_evento == undefined || nombre_evento == null || nombre_evento == '' || nombre_evento == 0 || nombre_evento == undefined) {
                continuar = false;
                mensajeError = 'Recuerda Que Toda La Informacion Es Necesaria';
            }
        }

        if (continuar == true) {
            Swal({
                title: 'Crear contenido',
                text: 'Estas seguro de crear este contenido ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        data: {
                            "tipo_pagina_interes": tipo_pagina_interes,
                            "nombre_evento2": nombre_evento2,
                            "hora_evento2": hora_evento2,
                            "fecha_evento2": fecha_evento2,
                            "nombre_evento": nombre_evento,
                            "hora_evento": hora_evento,
                            "fecha_evento": fecha_evento,
                            "tipo_contenido": tipo_contenido,
                            "pagina_interes": pagina_interes,
                            "url_videoinfo1": url_videoinfo1,
                            "url_videoinfo2": url_videoinfo2,
                            "posicion_contenido_usuario": posicion_contenido_usuario,
                            "userid": userid,
                            "CrearContenidoInformacion": 'SMD69'
                        },
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Contenido creado',
                                    text: 'Contenido creado correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('error', mensajeError, 'Error');
        }


    }
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();
</script>