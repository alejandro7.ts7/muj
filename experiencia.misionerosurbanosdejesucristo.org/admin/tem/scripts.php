<?php
session_start();
?>
<script>
    function loading() {
        $("#loadingDataShowLoader").css('display', 'block');
    }

    function unloading() {
        $("#loadingDataShowLoader").css('display', 'none');
    }

    function removeClass(idDontRemove) {
        for (let i = 1; i <= 29; i++) {
            if (idDontRemove != i) {
                $("#menuboxLoader" + i).removeClass("active");
            }
        }

        // close open menu mobile
        $(".as-mobile").removeClass("nav-shown");
        $(".nk-sidebar-mobile").removeClass("nk-sidebar-active");
        $(".nk-sidebar-overlay").remove();
    }

    function home(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('home.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader1").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(1);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function createContenido(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('createContenido.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader2").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(2);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function hisotirialContenido(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('hisotirialContenido.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader3").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(3);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function subaministradores(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('subaministradores.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader4").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(4);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }



    function editarContenido(e, id) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('editarContenido.php?id=' + id);
        $("html, body").scrollTop("0");
        $("#menuboxLoader3").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(3);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function editarEmail(e, id) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('editarEmail.php?id=' + id);
        $("html, body").scrollTop("0");
        $("#menuboxLoader3").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(3);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }


    function palabrasofensivas(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('palabrasofensivas.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader4").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(4);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function hisotirialIntenciones(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('hisotirialIntenciones.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader4").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(4);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function horariospaises(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('horariospaises.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader4").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(4);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }

    function notificacionesausuarios(e) {
        $("#boxContentMainCentral").html('');
        $("#boxContentMainCentral").load('notificacionesausuarios.php');
        $("html, body").scrollTop("0");
        $("#menuboxLoader4").addClass("active");
        if (e != 'undefined' && e != '' && e != null) {
            e.preventDefault();
        }
        loading();
        removeClass(4);
        $('#desc_collecion').summernote('destroy');
        $('#desc_collecion2').summernote('destroy');
        return false;
    }



    var pointNowDataSession = '<?php echo $data = (!empty($_SESSION["adminHistorialPointMUJ"])) ? $_SESSION["adminHistorialPointMUJ"] : 1; ?>';


    $(document).ready(function() {

        if (pointNowDataSession == 1) {
            home();
        } else if (pointNowDataSession == 2) {
            createContenido();
        } else if (pointNowDataSession == 3) {
            hisotirialContenido();
        } else if (pointNowDataSession == 4) {
            var id = '<?php echo $_SESSION["adminHistorialPointMUJUser"]; ?>'
            editarContenido('', id);
        } else if (pointNowDataSession == 5) {
            subaministradores();
        } else if (pointNowDataSession == 6) {
            var id = '<?php echo $_SESSION["adminHistorialPointMUJUser"]; ?>'
            editarEmail('', id);
        } else if (pointNowDataSession == 7) {
            palabrasofensivas('');
        } else if (pointNowDataSession == 8) {
            hisotirialIntenciones('');
        } else if (pointNowDataSession == 9) {
            horariospaises('');
        } else if (pointNowDataSession == 10) {
            notificacionesausuarios('');
        } else {
            home();
        }


    });
</script>