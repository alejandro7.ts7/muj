<?php

require_once('../../connection/conect.php');
require_once('ssp2.class.php');


$SQLVista = "CREATE OR REPLACE VIEW viewcontenidos3 AS SELECT c.*,a.username FROM `contenidos` AS c LEFT JOIN admin AS a ON a.id = c.creador WHERE c.tipo='3' ORDER BY c.fecha_contenido ASC";

$conVista = conection_database();
$checkVista = mysqli_query($conVista, $SQLVista);
close_database($conVista);

function sanear_string($string)
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    /*
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("¨", "º", "-", "~",
             "#", "@", "|", "!",
             "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $string
    );
    */


    return $string;
}


function cleanString($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "a", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "i", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "e", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "o", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "u", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = str_replace("&aacute;", "a", $String);
    $String = str_replace("&Aacute;", "a", $String);
    $String = str_replace("&eacute;", "e", $String);
    $String = str_replace("&Eacute;", "e", $String);
    $String = str_replace("&iacute;", "i", $String);
    $String = str_replace("&Iacute;", "i", $String);
    $String = str_replace("&oacute;", "o", $String);
    $String = str_replace("&Oacute;", "o", $String);
    $String = str_replace("&uacute;", "u", $String);
    $String = str_replace("&Uacute;", "u", $String);
    return $String;
}


$sql_details = array(
    'user' => $usuarioBaseDatosEnvio,
    'pass' => $passwordBaseDatosEnvio,
    'db'   => $tablaeBaseDatosEnvio,
    'host' => $urlBaseDatosEnvio
);
$table = 'viewcontenidos3';




$primaryKey = 'id';
$columns = array(
    array(
        'db'        => 'fecha_contenido',
        'dt'        => 0,
        'formatter' => function ($d, $rowinfo) {

            if(!empty($rowinfo[2])){
                $info ='<a style="cursor:pointer;" target="_blank" href="'. $rowinfo[2].'">'. date('Y-m-d H:i:s', strtotime($d)).'</a>';
            }else {
                $info = '<font >' .  date('Y-m-d H:i:s', strtotime($d)) . '</font>';
            }

            return $info;
        }
    ),
    array(
        'db'        => 'contenido',
        'dt'        => 1,
        'formatter' => function ($d, $rowinfo) {

            if (!empty($rowinfo[2])) {
                $info = '<a style="cursor:pointer;" target="_blank" href="' . $rowinfo[2] . '">' . $d. '</a>';
            } else {
                $info = '<font >' . $d . '</font>';
            }
            return $info;
        }
    ),
    array(
        'db'        => 'url_new',
        'dt'        => 2,
        'formatter' => function ($d, $rowinfo) {

            return $d;
        }
    )
);
$fecha_actual = date('Y-m-d H:i:s');
// . ' 00:00:00'
$fecha_horarios = $fecha_actual;
$fecha_horarios = date("Y-m-d H:i:s", strtotime($fecha_horarios . "- 365 days")); 

$where = "fecha_contenido>='". $fecha_horarios. "' AND tipo='3' AND status='1'";
$info = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $where);
//var_dump($info);

echo json_encode($info);
