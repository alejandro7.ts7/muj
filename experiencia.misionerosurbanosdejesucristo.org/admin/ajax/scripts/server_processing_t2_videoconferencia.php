<?php
session_start();
require_once('../../connection/conectutf8.php');
require_once('../../include/functions.php');
require_once('ssp2.class.php');


$idReadl = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);

//

$SQLVista = "";

$idformulario = $_GET["idformulario"];
//$conVista = conection_database();
//$checkVista = mysqli_query($conVista, $SQLVista);
//close_database($conVista);

//echo $idReadl;

function sanear_string($string)
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    /*
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("¨", "º", "-", "~",
             "#", "@", "|", "!",
             "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $string
    );
    */


    return $string;
}


function cleanString($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "a", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "i", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "e", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "o", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "u", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = str_replace("&aacute;", "a", $String);
    $String = str_replace("&Aacute;", "a", $String);
    $String = str_replace("&eacute;", "e", $String);
    $String = str_replace("&Eacute;", "e", $String);
    $String = str_replace("&iacute;", "i", $String);
    $String = str_replace("&Iacute;", "i", $String);
    $String = str_replace("&oacute;", "o", $String);
    $String = str_replace("&Oacute;", "o", $String);
    $String = str_replace("&uacute;", "u", $String);
    $String = str_replace("&Uacute;", "u", $String);
    return $String;
}


$sql_details = array(
    'user' => $usuarioBaseDatosEnvio,
    'pass' => $passwordBaseDatosEnvio,
    'db'   => $tablaeBaseDatosEnvio,
    'host' => $urlBaseDatosEnvio
);
$table = 'horarios_video';




$primaryKey = 'id';
$columns = array(
    array(
        'db'        => 'fecha_begin',
        'dt'        => 0,
        'formatter' => function ($d, $rowinfo) {
            return date('Y-m-d', strtotime($d));
        }
    ), array(
        'db'        => 'fecha_begin',
        'dt'        => 1,
        'formatter' => function ($d, $rowinfo) {
            return date('H:i:s', strtotime($d));
        }
    ),array(
        'db'        => 'fecha_end',
        'dt'        => 2,
        'formatter' => function ($d, $rowinfo) {

            if($d== '0000-00-00 00:00:00' || $d == '1969-12-31 19:00:00' || empty($d) || $d ==null || $d=='NULL'){
                $info ='No aplica';
            }else {
                $info = date('Y-m-d', strtotime($d));
            }
            return $info;
        }
    ), array(
        'db'        => 'fecha_end',
        'dt'        => 3,
        'formatter' => function ($d, $rowinfo) {

            if ($d == '0000-00-00 00:00:00' || $d == '1969-12-31 19:00:00' || empty($d) || $d ==null || $d=='NULL') {
                $info = 'No aplica';
            } else {
                $info = date('H:i:s', strtotime($d));
            }
            return $info;

        }
    ), array(
        'db'        => 'id',
        'dt'        => 4,
        'formatter' => function ($d, $rowinfo) {

            $contenidoinformacion = $rowinfo[8];
            $contenidoinformacion = trim($contenidoinformacion);
            $contenidoinformacion = str_replace('"','', $contenidoinformacion);
            $contenidoinformacion = str_replace("'","", $contenidoinformacion);
            $contenidoinformacion = preg_replace('/"/', '', $contenidoinformacion);


            //echo $contenidoinformacion;

            //$editarUsuario = '<button type="button" onclick=editarHorarios_envivo_2(\'' . $d . '\',\'' . $rowinfo[5] . '\',\'' . (date('Y-m-d', strtotime($rowinfo[0]))) . '\',\'' . (date('H:i:s', strtotime($rowinfo[0]))) . '\',\'' . (date('Y-m-d', strtotime($rowinfo[2]))) . '\',\'' . (date('H:i:s', strtotime($rowinfo[2]))) . '\',\''. $contenidoinformacion. '\') class="btn btn-primary btn-block mg-b-10" >Editar   </button>';

            $editarUsuario = '<button type="button" onclick=editarHorarios_envivo_2(\'' . $d . '\',\'' . $rowinfo[5] . '\') class="btn btn-primary btn-block mg-b-10" >Editar   </button>';


            return $editarUsuario;
        }
    ), array(
        'db'        => 'id_formulario',
        'dt'        => 5,
        'formatter' => function ($d, $rowinfo) {
            $editarUsuario = "<button type='button' onclick=eliminarHorariosVideoConferencia('" . $rowinfo[4] . "','" . $d . "') class='btn btn-primary btn-block mg-b-10' >Eliminar   </button>";
            return $editarUsuario;
        }
    ), array(
        'db'        => 'fecha_begin',
        'dt'        => 6,
        'formatter' => function ($d, $rowinfo) {

            $info ='Pendiente de iniciar';
            // fecha de inicio
            $fechaActual = date('Y-m-d H:i:s');
            $fechaInicio = $d ;
            //echo $fechaInicio;
            if(strtotime($fechaInicio)>strtotime($fechaActual)){
                $info = 'Pendiente de iniciar';  
            }else {

                $info = 'Iniciada';
                if ($rowinfo[3] == '0000-00-00 00:00:00' || $rowinfo[3] == '1969-12-31 19:00:00' || empty($rowinfo[3]) || $rowinfo[3] == null || $rowinfo[3] == 'NULL') {

                    $info = 'Iniciada y en espera de finalización manual';
                } else {
                    $fechaFinal = $rowinfo[3];
                    if (strtotime($fechaFinal) < strtotime($fechaActual)) {
                        $info = 'Finalizada';
                    } else {
                        $info = 'Iniciada';
                    }
                }
                              
            }
            return $info;
        }
    ), array(
        'db'        => 'descHorario',
        'dt'        => 7,
        'formatter' => function ($d, $rowinfo) {
          
            return (strlen($d) > 30 ? substr($d, 0, 30) . "..." : $d);
        }
    ), array(
        'db'        => 'descHorario',
        'dt'        => 8,
        'formatter' => function ($d, $rowinfo) {

            return $d;
        }
    )



);

$where = "id_formulario='" . $idformulario . "'";

$info = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $where);
//var_dump($info);

echo json_encode($info);
