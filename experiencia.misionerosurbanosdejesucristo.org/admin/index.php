<?php
session_start();

if (isset($_SESSION["adminMisionerosUrbanos"][0])) {
    # code...
    //echo "Dejar Aqui";
    include('include/function_admin.php');
    $idReal = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);
    if (empty($idReal)) {
        header("Location:login.php");
    }
} else {
    header("Location:login.php");
}


?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Misioneros Urbanos de Jesucristo">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Misioneros Urbanos de Jesucristo Admin">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="images/favicon.png">
    <!-- Page Title  -->
    <title>Misioneros Urbanos de Jesucristo</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="assets/css/dashlite.css?ver=2.4.0">
    <link id="skin-default" rel="stylesheet" href="assets/css/theme.css?ver=2.4.0">
    <link href="swal/sweetalert2.min.css" rel="stylesheet">

    <link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <link href="lib/summernote/css/summernote-bs4.css" rel="stylesheet">


    <style>
        .borderBotSide {
            margin-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom: 1px;
            border-left: 0px;
            border-top: 0px;
            border-right: 0px;
            border-style: solid;
            border-color: #d1d1d1;
        }
    </style>

    <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
    <style>
        body {
            font-family: 'Nunito';
        }


        .dropdown-toggle::after {
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: 0.255em;
            vertical-align: 0.255em;
            content: "";
            border-top: 0.3em solid;
            border-right: 0.3em solid transparent;
            border-bottom: 0;
            border-left: 0.3em solid transparent;
        }

        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .card-header.note-toolbar>.btn-group.note-color .dropdown-toggle::after {
            margin-left: -3px;
        }

        .card-header.note-toolbar>.btn-group .dropdown-toggle::after {
            margin-left: 8px;
        }

        .dropdown-toggle:empty::after {
            margin-left: 0;
        }

        .dropdown-toggle::after {
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: 0.255em;
            vertical-align: 0.255em;
            content: "";
            border-top: 0.3em solid;
            border-right: 0.3em solid transparent;
            border-bottom: 0;
            border-left: 0.3em solid transparent;
        }

        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }
    </style>
</head>

<body class="nk-body bg-white has-sidebar <?php echo (($_SESSION["adminMisionerosUrbanos"][4] == 1) ? 'dark-mode' : ''); ?>  ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php
            include('htmlInclude/sidebar.php');
            ?>
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">

                <?php
                include('htmlInclude/header.php');
                ?>
                <!-- content @s -->
                <div id="boxContentMainCentral" class="nk-content nk-content-fluid" style="padding-top: 0px;padding-left: 0px;">

                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <?php
                include('htmlInclude/footer.php');
                ?>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="assets/js/bundle.js?ver=2.4.0"></script>
    <script src="assets/js/scripts.js?ver=2.4.0"></script>
    <script src="swal/swalpersonalizado.js"></script>
    <script src="swal/sweetalert2.min.js"></script>
    <script src="lib/datatables/js/jquery.dataTables.js"></script>
    <script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>

    <script src="lib/summernote/js/summernote-bs4.min.js"></script>
    <script src="lib/summernote/js/summernote-file.js"></script>
    <script src="lib/smclas/summernote-image-attributes.js"></script>
    <script src="lib/smclas/lang/es-ES.js"></script>

    <?php
    include('tem/scripts.php');
    ?>
</body>

</html>