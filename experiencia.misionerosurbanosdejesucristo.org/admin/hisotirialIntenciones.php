<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 8;
//require_once('include/function_admin.php');

?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Historial intenciones</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">


                            <div class="col-lg-12 col-md-12 mb-4">


                                <button onclick="maracarIntencionescomoAtendidas()" type="submit" class="btn btn-primary">Marcar intenciones como atendidas (Esto reiniciará el listado de intenciones) </button>
                            </div>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <table id="datatable1_28_intenciones" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Intención</th>
                                        <th>Fecha intención</th>
                                        <th>Estatus</th>
                                        <th>Eliminar </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();




    $('#datatable1_28_intenciones').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/scripts/server_processing_subadmin_intenciones.php",
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: '_MENU_ Resultados/por página',
        },
        "order": [
            [0, "DESC"]
        ],
    });


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */


    function eliminarUsuarioSubadminIntenciones(idAdmin) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Eliminar intención',
            text: 'Estas seguro de eliminar esta intención ?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores.php",
                    data: {
                        "idAdmin": idAdmin,
                        "userid": userid,
                        "EliminarSubadministradorIntencion": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Intención eliminada',
                                text: 'Intención eliminado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    hisotirialIntenciones('');
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }

    function maracarIntencionescomoAtendidas(idAdmin) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Marcar intenciones como atendidas',
            text: 'Estas seguro de marcar intenciones como atendidas ?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores.php",
                    data: {
                        "idAdmin": idAdmin,
                        "userid": userid,
                        "MarcarIntencionesSubadministradorIntencion": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Intenciones atendidas',
                                text: '',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    hisotirialIntenciones('');
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }


    function guardar_info() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        var usuario_subadministrador = $("#usuario_subadministrador").val();
        var password_1 = $("#password_1").val();
        var password_2 = $("#password_2").val();

        if (usuario_subadministrador != '' && password_1 != '' && password_2 != '') {
            Swal({
                title: 'Crear Subadministrador',
                text: 'Estas Seguro De Crear El Subadministrador',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Crear',
                cancelButtonText: 'Cancelar',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {


                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/subadministradores.php",
                        data: {
                            "newpassword2": password_2,
                            "newpassword": password_1,
                            "usuario_subadministrador": usuario_subadministrador,
                            "userid": userid,
                            "CreateUser": 'SMD69'
                        },
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Subadministrador Creado',
                                    text: 'Subadministrador Creado Correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('info', 'Recuerda Que Toda La Informacion Es Necesaria', 'Error');
        }


    }

    function changePassword(idAdmin) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Cambiar Contraseña Subadministrador',
            html: '<label>Nueva Contraseña 1</label><br><input type="text" name="newpassword" id="newpassword" class="form-control" /><br> <label>Nueva Contraseña 2</label><br><input type="text" name="newpassword2" id="newpassword2" class="form-control" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Modificar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var newpassword = $("#newpassword").val();
                var newpassword2 = $("#newpassword2").val();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores.php",
                    data: {
                        "newpassword2": newpassword2,
                        "newpassword": newpassword,
                        "idAdmin": idAdmin,
                        "userid": userid,
                        "NewPassword": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Contraseña Modificada',
                                text: 'Contraseña Modificada Correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }
</script>