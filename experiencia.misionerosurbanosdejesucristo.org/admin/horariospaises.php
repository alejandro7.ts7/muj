<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 9;
require_once('include/function_admin.php');
$minimoPopu = obtenerMinimoParaMosrarPopup($_SESSION["adminMisionerosUrbanos"][0]);

?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<!-- content @s -->

<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title ">Conexiones minimas para mostrar popup de programacion diaria</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">


                            <div class="col-lg-12 col-md-12 mb-4">

                                <br>
                                <label class="form-control-label">Cantidad</label>
                                <input type="text" placeholder="Conexiones mínimas" value="<?php echo $minimoPopu["plabra"]; ?>" id="palabra_ofensiva" name="palabra_ofensiva" class="form-control"></input>

                                <label class="form-control-label">Definido por:</label>
                                <select type="text"  id="define_by" name="define_by" class="form-control">
                                <option <?php echo ($minimoPopu["define_by"]=="1")?'selected':''; ?> value="1">Dias ingresando a plataforma</option>
                                <option <?php echo ($minimoPopu["define_by"]=="2")?'selected':''; ?> value="2">Conexiones a plataforma (Es decir cada vez que ingresa)</option>
                            </select>


                                <br>
                                <button onclick="guardar_info()" type="submit" class="btn btn-primary">Editar dato </button>
                            </div>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>



            </div>
        </div>
    </div>
</div>


<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Horarios paises</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">




                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <table id="datatable1_28_horarios_paises" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Pais</th>
                                        <th>Horario UTC </th>
                                        <th>Editar </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();

    function guardar_info() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        var palabra_ofensiva = $("#palabra_ofensiva").val();
        var define_by = $("#define_by").val();


        if (palabra_ofensiva != '') {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "ajax/subadministradores_palabras.php",
                data: {
                    "palabra_ofensiva": palabra_ofensiva,
                    "userid": userid,
                    "define_by": define_by,
                    "CreateUserPalabra_time_popup": 'SMD69'
                },
                success: function(loginData) {
                    if (loginData.suceso == 'ok') {
                        Swal({
                            title: 'Dato editado',
                            text: 'Dato editado correctamente',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#6baafe',
                            cancelButtonColor: '#6baafe',
                            confirmButtonText: 'ok',
                            cancelButtonText: 'No',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            if (result.value) {
                                // cargar_palabras_ofensivas();
                            }
                        });
                    } else {
                        cargar_swal('error', loginData.mensaje, 'Error');
                    }
                }
            });
        } else {
            cargar_swal('info', 'Recuerda Que Toda La Informacion Es Necesaria', 'Error');
        }


    }



    $('#datatable1_28_horarios_paises').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/scripts/server_processing_subadmin_horrios_paises.php",
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: '_MENU_ Resultados/por página',
        },
        "order": [
            [0, "DESC"]
        ],
    });


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */


    function cambiarHorarioUTC(idPais, horario) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Cambiar horario',
            html: '<label>Nuevo horario </label><br><input type="text" value="' + horario + '" name="horario_time" id="horario_time" class="form-control" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Modificar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var horario_time = $("#horario_time").val();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores.php",
                    data: {
                        "horario_time": horario_time,
                        "idPais": idPais,
                        "userid": userid,
                        "NewPasswordHorario": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Horario modificado',
                                text: 'Horario modificado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    // location.reload();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }
</script>