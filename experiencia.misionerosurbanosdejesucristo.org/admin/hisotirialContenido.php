<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 3;
require_once('include/function_admin.php');

?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
<style>
    @media (min-width: 992px) {

        .modal-lg,
        .modal-xl {
            max-width: 1020px
        }
    }
</style>

<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Historial de contenido</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <table id="datatable1_28" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Fecha creación</th>
                                        <th>Tipo contenido</th>
                                        <th>Título</th>
                                        <th>Posteador </th>
                                        <th>Horarios de experiencias </th>
                                        <th>Horarios En Vivos </th>
                                        <th>Email </th>
                                        <th>Información de registrados </th>
                                        <th>Destacados y misterio del día</th>
                                        <th>Preguntas </th>
                                        <th>Editar </th>
                                        <th>Eliminar </th>


                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<div class="modal bd-example-modal-lg" id="modalHorariosUser" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Horarios<br></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button style="cursor:pointer;" onclick="create_new_horary()" type="button" class="btn btn-primary">+ Crear nuevo horario</button>
                <br>

                <div class="col-md-12">
                    <br>
                    <div class="card  card-full">
                        <table id="datatable1_28Horarios" class="table card-text">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div><!-- .card -->
                </div><!-- .col -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="idFormulario" id="idFormulario" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal bd-example-modal-lg" id="modalHorariosUserVideoConferencias" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Horarios videoconferencias<br></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button style="cursor:pointer;" onclick="create_new_horary_videoconferencia()" type="button" class="btn btn-primary">+ Crear nuevo horario de videoconferencia</button>
                <br>
                <div id="aquiContenidoOnlineActivoPermanente"></div>

                <div class="row g-gs">

                    <div class="col-md-12">
                        <br>
                        <div class="card  card-full">
                            <table id="datatable1_28Horarios_video_coferencias" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Fecha inicio</th>
                                        <th>Hora inicio</th>
                                        <th>Fecha finalización</th>
                                        <th>Hora finalización</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                        <th>Status Videoconferencia</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="idFormulario_videoconferencia" id="idFormulario_videoconferencia" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal bd-example-modal-lg" id="modalregistradosExperienciasUser" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Usuarios registrados <br></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row g-gs">

                    <div class="col-md-12">
                        <br>
                        <div class="card  card-full">
                            <table id="fomularioDeRegistradosPorUsuario" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Fecha de experiencia</th>
                                        <th>Hora de experiencia</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Correo Electronico</th>
                                        <th>WhatsApp/Móvil</th>
                                        <th>Pais</th>
                                        <th>Ciudad</th>
                                        <th>Campo 1</th>
                                        <th>Campo 2</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="idFormularioEdicionRegistradosHidden" id="idFormularioEdicionRegistradosHidden" />
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal bd-example-modal-lg" id="modalregistradosExperienciasUser_preguntas" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Preguntas de personas <br></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row g-gs">

                    <div class="col-md-12">
                        <br>
                        <div class="card  card-full">
                            <table id="fomularioDeRegistradosPorUsuario_repguntas" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Pregunta</th>
                                        <th>Status</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Correo</th>
                                        <th>WhatsApp/Móvil</th>
                                        <th>Fecha pregunta</th>

                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="idFormularioEdicionRegistradosHiddenPreguntas" id="idFormularioEdicionRegistradosHiddenPreguntas" />
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal bd-example-modal-lg" id="modalregistradosExperienciasUser_destacados" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Destacados <br></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row g-gs">

                    <div class="col-md-12">
                        <br>
                        <div class="card  card-full">

                            <h4>Es misterio del día , este campo sirve para identificar que misterios se les muestra a los usuarios en la pagina principal </h4>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaMisterio">
                                <label class="form-check-label" for="diaMisterio">
                                    Misterio del día
                                </label>
                            </div>
                            <hr>


                            <h4>Días en los cuales aparecerá este contenido en destacados</h4>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaLunes">
                                <label class="form-check-label" for="diaLunes">
                                    Lunes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaMartes">
                                <label class="form-check-label" for="diaMartes">
                                    Martes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaMiercoles">
                                <label class="form-check-label" for="diaMiercoles">
                                    Miércoles
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaJueves">
                                <label class="form-check-label" for="diaJueves">
                                    Jueves
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaViernes">
                                <label class="form-check-label" for="diaViernes">
                                    Viernes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaSabado">
                                <label class="form-check-label" for="diaSabado">
                                    Sábado
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="diaDomingo">
                                <label class="form-check-label" for="diaDomingo">
                                    Domingo
                                </label>
                            </div>





                            <div class="form-check">
                                <button onclick="guardar_dias_destacados()" type="button" class="btn btn-primary">Guardar</button>
                            </div>

                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="idFormularioEdicionRegistradosHiddenDestacados" id="idFormularioEdicionRegistradosHiddenDestacados" />
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();

    var tablaUserNew;
    var tablaUserNewregistrados;
    var tablaUserNewregistrados2;

    function marcar_respondida(idFormulario, idFormSend) {
        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("RespondidaPreguntaUsuario", 'SMD69');


        Swal({
            title: 'Estas seguro ?',
            html: 'Realmente deseas marcar esta pregunta como respondida ? ',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {


                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Guardando informacion...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openmodalPreguntas(idFormSend);
                            Swal({
                                title: 'Respuesta respondida',
                                text: 'Respuesta marcada como respondida correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }

    function eliminarHorarios(idFormulario, idForm) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("EliminarHorario", 'SMD69');


        Swal({
            title: 'Eliminar horario ?',
            html: 'Estas seguro de eliminar este horario ? ',
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {


                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Eliminando Horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormulario(idForm);
                            Swal({
                                title: 'Horario eliminado',
                                text: 'Horario eliminado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });

    }

    function eliminarHorariosVideoConferencia(idFormulario, idForm) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("EliminarHorario_video", 'SMD69');


        Swal({
            title: 'Eliminar horario ?',
            html: 'Estas seguro de eliminar este horario ? ',
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {


                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Eliminando Horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormularioEnVivos(idForm);
                            Swal({
                                title: 'Horario eliminado',
                                text: 'Horario eliminado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });

    }

    function editarHorarios(idFormulario, idForm, fecha, hora) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("EditarHorario", 'SMD69');


        Swal({
            title: 'Editar horario ?',
            html: '<label>Seleciona una fecha y una hora</label><br><label>Fecha</label><br><input type="date" class="form-control" name="horarioDia" id="horarioDia" value="' + fecha + '" /> <br><label>Hora</label><input value="' + hora + '" type="time" class="form-control" name="horarioHora" id="horarioHora" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Editar',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var horarioHora = $("#horarioHora").val();
                var horarioDia = $("#horarioDia").val();
                form_data.append("horarioDia", horarioDia);
                form_data.append("horarioHora", horarioHora);

                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Editando horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormulario(idForm);
                            Swal({
                                title: 'Horario editado',
                                text: 'Horario editado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });

    }

    function editarHorarios_envivo(idFormulario, idForm, fecha, hora, fecha2, hora2, descripcion) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("EditarHorario_video", 'SMD69');

        // alert(fecha2);
        // alert(hora2);

        // alert(descripcion);

        if (fecha2 == '1969-12-31' && hora2 == '19:00:00') {
            var html2 = '<label>Fecha finalización </label><br><input type="date" class="form-control" value="" name="horarioDia2" id="horarioDia2" /> <br><label>Hora finalización </label><input type="time" class="form-control" value="" name="horarioHora2" id="horarioHora2" />'
        } else {
            var html2 = '<label>Fecha finalización </label><br><input type="date" class="form-control" value="' + fecha2 + '" name="horarioDia2" id="horarioDia2" /> <br><label>Hora finalización </label><input type="time" class="form-control" value="' + hora2 + '" name="horarioHora2" id="horarioHora2" />';
        }



        Swal({
            title: 'Editar horario ?',
            html: '<label>Seleciona una fecha y una hora</label><br><label>Fecha</label><br><input type="date" class="form-control" name="horarioDia" id="horarioDia" value="' + fecha + '" /> <br><label>Hora</label><input value="' + hora + '" type="time" class="form-control" name="horarioHora" id="horarioHora" /><br><br> <label>Descripción de la videoconferencia </label><textarea class="form-control" name="descHorario2" id="descHorario2" rows="5" />' + descripcion + '</textarea> <br> <label>Seleciona una fecha y una hora de finalización (si no se selecciona una hora de finalizacion la videoconferencia estara abierta hasta que manualmente sea eliminado el horario)</label><br>' + html2,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Editar',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var horarioHora = $("#horarioHora").val();
                var horarioDia = $("#horarioDia").val();
                form_data.append("horarioDia", horarioDia);
                form_data.append("horarioHora", horarioHora);

                var horarioHora2 = $("#horarioHora2").val();
                var horarioDia2 = $("#horarioDia2").val();


                var descHorario2 = $("#descHorario2").val();
                form_data.append("horarioDia2", horarioDia2);
                form_data.append("horarioHora2", horarioHora2);
                form_data.append("descHorario", descHorario2);

                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Editando horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormularioEnVivos(idForm);
                            Swal({
                                title: 'Horario editado',
                                text: 'Horario editado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });

    }


    function editarHorarios_envivo_2(idFormulario, idForm) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';



        // fecha, hora, fecha2, hora2, descripcion
        $("#modalHorariosUserVideoConferencias").modal('hide');

        // alert('ajax to get data');
        var form_data2 = new FormData();
        form_data2.append("idFormulario", idFormulario);
        form_data2.append("userid", userid);
        form_data2.append("ObtenerHorario_video", 'SMD69');

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "ajax/contenidoData.php",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data2,
            success: function(loginData) {
                //swal.close();
                if (loginData.suceso == 'ok') {
                    // alert('datos obtenidos');

                    var fecha = loginData.fecha;
                    var hora = loginData.hora;
                    var fecha2 = loginData.fecha2;
                    var hora2 = loginData.hora2;
                    var descripcion = loginData.descripcion;




                    var form_data = new FormData();
                    form_data.append("idFormulario", idFormulario);
                    form_data.append("userid", userid);
                    form_data.append("EditarHorario_video", 'SMD69');

                    // alert(fecha2);
                    // alert(hora2);

                    // alert(descripcion);

                    if (fecha2 == '1969-12-31' && hora2 == '19:00:00') {
                        var html2 = '<label>Fecha finalización </label><br><input type="date" class="form-control" value="" name="horarioDia2" id="horarioDia2" /> <br><label>Hora finalización </label><input type="time" class="form-control" value="" name="horarioHora2" id="horarioHora2" />'
                    } else {
                        var html2 = '<label>Fecha finalización </label><br><input type="date" class="form-control" value="' + fecha2 + '" name="horarioDia2" id="horarioDia2" /> <br><label>Hora finalización </label><input type="time" class="form-control" value="' + hora2 + '" name="horarioHora2" id="horarioHora2" />';
                    }



                    Swal({
                        title: 'Editar horario ?',
                        html: '<label>Seleciona una fecha y una hora</label><br><label>Fecha</label><br><input type="date" class="form-control" name="horarioDia" id="horarioDia" value="' + fecha + '" /> <br><label>Hora</label><input value="' + hora + '" type="time" class="form-control" name="horarioHora" id="horarioHora" /><br><br> <label>Descripción de la videoconferencia </label><textarea class="form-control" name="descHorario2" id="descHorario2" rows="5" />' + descripcion + '</textarea> <br> <label>Seleciona una fecha y una hora de finalización (si no se selecciona una hora de finalizacion la videoconferencia estara abierta hasta que manualmente sea eliminado el horario)</label><br>' + html2,
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonColor: '#6baafe',
                        cancelButtonColor: '#6baafe',
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'No',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then((result) => {
                        if (result.value) {

                            var horarioHora = $("#horarioHora").val();
                            var horarioDia = $("#horarioDia").val();
                            form_data.append("horarioDia", horarioDia);
                            form_data.append("horarioHora", horarioHora);

                            var horarioHora2 = $("#horarioHora2").val();
                            var horarioDia2 = $("#horarioDia2").val();


                            var descHorario2 = $("#descHorario2").val();
                            form_data.append("horarioDia2", horarioDia2);
                            form_data.append("horarioHora2", horarioHora2);
                            form_data.append("descHorario", descHorario2);

                            // creando contenido 
                            Swal.fire({
                                title: 'info',
                                html: `Editando horario...`,
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                type: '',
                            });
                            Swal.showLoading();

                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                url: "ajax/contenidoData.php",
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: form_data,
                                success: function(loginData) {
                                    swal.close();
                                    if (loginData.suceso == 'ok') {
                                        openHorariosFormularioEnVivos(idForm);
                                        Swal({
                                            title: 'Horario editado',
                                            text: 'Horario editado correctamente',
                                            type: 'success',
                                            showCancelButton: false,
                                            confirmButtonColor: '#6baafe',
                                            cancelButtonColor: '#6baafe',
                                            confirmButtonText: 'ok',
                                            cancelButtonText: 'No',
                                            allowOutsideClick: false,
                                            allowEscapeKey: false
                                        }).then((result) => {
                                            if (result.value) {

                                            }
                                        });
                                    } else {
                                        cargar_swal('error', loginData.mensaje, 'Error');
                                    }
                                }
                            });


                        } else {
                            $("#modalHorariosUserVideoConferencias").modal('show');
                        }
                    });


                } else {
                    cargar_swal('error', loginData.mensaje, 'Error');
                }
            }
        });





    }

    function create_new_horary() {

        var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("CreateHorario", 'SMD69');

        Swal({
            title: 'Crear horario',
            html: '<label>Seleciona una fecha y una hora</label><br><label>Fecha</label><br><input type="date" class="form-control" name="horarioDia" id="horarioDia" /> <br><label>Hora</label><input type="time" class="form-control" name="horarioHora" id="horarioHora" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Crear',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var horarioHora = $("#horarioHora").val();
                var horarioDia = $("#horarioDia").val();
                form_data.append("horarioDia", horarioDia);
                form_data.append("horarioHora", horarioHora);

                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Creando Horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormulario(idFormulario);
                            Swal({
                                title: 'Horario creado',
                                text: 'Horario creado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }


    function create_new_horary_videoconferencia() {

        var idFormulario = $("#idFormulario_videoconferencia").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var form_data = new FormData();
        form_data.append("idFormulario", idFormulario);
        form_data.append("userid", userid);
        form_data.append("CreateHorario_video", 'SMD69');

        $("#modalHorariosUserVideoConferencias").modal('hide');

        Swal({
            title: 'Crear horario',
            html: '<label>Seleciona una fecha y una hora de inicio </label><br><label>Fecha inicio </label><br><input type="date" class="form-control" name="horarioDia" id="horarioDia" /> <br><label>Hora inicio </label><input type="time" class="form-control" name="horarioHora" id="horarioHora" /><br> <br> <label>Descripción de la videoconferencia </label><textarea class="form-control" name="descHorario" id="descHorario" rows="5" /></textarea> <br><labelSelecciona una fecha y una hora de finalización (si no se selecciona una hora de finalización, la videoconferencia estará abierta hasta que manualmente sea eliminado el horario)</label><br><label>Fecha finalización </label><br><input type="date" class="form-control" name="horarioDia2" id="horarioDia2" /> <br><label>Hora finalización </label><input type="time" class="form-control" name="horarioHora2" id="horarioHora2" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Crear',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var horarioHora = $("#horarioHora").val();
                var horarioDia = $("#horarioDia").val();
                form_data.append("horarioDia", horarioDia);
                form_data.append("horarioHora", horarioHora);

                var horarioHora2 = $("#horarioHora2").val();
                var horarioDia2 = $("#horarioDia2").val();
                var descHorario = $("#descHorario").val();
                form_data.append("horarioDia2", horarioDia2);
                form_data.append("horarioHora2", horarioHora2);
                form_data.append("descHorario", descHorario);

                // creando contenido 
                Swal.fire({
                    title: 'info',
                    html: `Creando Horario...`,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    type: '',
                });
                Swal.showLoading();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        swal.close();
                        if (loginData.suceso == 'ok') {
                            openHorariosFormularioEnVivos(idFormulario);
                            Swal({
                                title: 'Horario creado',
                                text: 'Horario creado correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {

                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            } else {
                $("#modalHorariosUserVideoConferencias").modal('show');
            }
        });
    }





    function openmodalInformacionRegistrados(idFormulario) {
        //alert(idFormulario);
        $("#modalregistradosExperienciasUser").modal('show');
        //tablaUserNew.destroy();
        $("#idFormularioEdicionRegistradosHidden").val(idFormulario);

        try {
            tablaUserNewregistrados2.DataTable().destroy();
        } catch (error) {

        }


        tablaUserNewregistrados2 = $('#fomularioDeRegistradosPorUsuario').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "ajax/scripts/server_processing_t3.php?idformulario=" + idFormulario,
            responsive: true,
            language: {
                searchPlaceholder: 'Buscar...',
                sSearch: '',
                lengthMenu: '_MENU_ Resultados/por página',
            },
            "order": [
                [0, "DESC"]
            ],
            destroy: true,
        });

    }

    function guardar_dias_destacados() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var idFormulario = $("#idFormularioEdicionRegistradosHiddenDestacados").val();

        var lunes = 0;
        var martes = 0;
        var miercoles = 0;
        var jueves = 0;
        var viernes = 0;
        var sabado = 0;
        var domingo = 0;
        var misterio = 0;

        if ($("#diaLunes").is(":checked")) {
            lunes = 1;
        }
        if ($("#diaMartes").is(":checked")) {
            martes = 2;
        }
        if ($("#diaMiercoles").is(":checked")) {
            miercoles = 3;
        }
        if ($("#diaJueves").is(":checked")) {
            jueves = 4;
        }
        if ($("#diaViernes").is(":checked")) {
            viernes = 5;
        }
        if ($("#diaSabado").is(":checked")) {
            sabado = 6;
        }
        if ($("#diaDomingo").is(":checked")) {
            domingo = 7;
        }
        if ($("#diaMisterio").is(":checked")) {
            misterio = 8;
        }

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "ajax/contenidoData.php",
            data: {
                "idFormulario": idFormulario,
                "userid": userid,
                "lunes": lunes,
                "martes": martes,
                "miercoles": miercoles,
                "jueves": jueves,
                "viernes": viernes,
                "sabado": sabado,
                "domingo": domingo,
                "misterio": misterio,
                "GuardarDatosDescatados": 'SMD69'
            },
            success: function(loginData) {
                if (loginData.suceso == 'ok') {

                    //$("#modalregistradosExperienciasUser_destacados").modal('hide');
                    //openmodalDestacados(idFormulario)
                    Swal({
                        title: 'Datos guardados',
                        text: 'Datos guardados correctamente',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#6baafe',
                        cancelButtonColor: '#6baafe',
                        confirmButtonText: 'ok',
                        cancelButtonText: 'No',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then((result) => {
                        if (result.value) {
                            // location.reload();
                        }
                    });

                } else {
                    cargar_swal('error', loginData.mensaje, 'Error');
                }
            }
        });
    }

    function openmodalDestacados(idFormulario) {
        //alert(idFormulario);
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "ajax/contenidoData.php",
            data: {
                "idFormulario": idFormulario,
                "userid": userid,
                "ObtenerDatosDestacados": 'SMD69'
            },
            success: function(loginData) {
                if (loginData.suceso == 'ok') {
                    //alert("ok");
                    $("#modalregistradosExperienciasUser_destacados").modal('show');

                    if (loginData.lunes == 1) {
                        $("#diaLunes").prop("checked", true);
                    } else {
                        $("#diaLunes").prop("checked", false);
                    }

                    if (loginData.martes == 1) {
                        $("#diaMartes").prop("checked", true);
                    } else {
                        $("#diaMartes").prop("checked", false);
                    }

                    if (loginData.miercoles == 1) {
                        $("#diaMiercoles").prop("checked", true);
                    } else {
                        $("#diaMiercoles").prop("checked", false);
                    }

                    if (loginData.jueves == 1) {
                        $("#diaJueves").prop("checked", true);
                    } else {
                        $("#diaJueves").prop("checked", false);
                    }

                    if (loginData.viernes == 1) {
                        $("#diaViernes").prop("checked", true);
                    } else {
                        $("#diaViernes").prop("checked", false);
                    }

                    if (loginData.sabado == 1) {
                        $("#diaSabado").prop("checked", true);
                    } else {
                        $("#diaSabado").prop("checked", false);
                    }

                    if (loginData.domingo == 1) {
                        $("#diaDomingo").prop("checked", true);
                    } else {
                        $("#diaDomingo").prop("checked", false);
                    }

                    if (loginData.misterio == 1) {
                        $("#diaMisterio").prop("checked", true);
                    } else {
                        $("#diaMisterio").prop("checked", false);
                    }



                    //tablaUserNew.destroy();
                    $("#idFormularioEdicionRegistradosHiddenDestacados").val(idFormulario);
                } else {
                    cargar_swal('error', loginData.mensaje, 'Error');
                }
            }
        });
    }

    function openmodalPreguntas(idFormulario) {
        //alert(idFormulario);
        $("#modalregistradosExperienciasUser_preguntas").modal('show');
        //tablaUserNew.destroy();
        $("#idFormularioEdicionRegistradosHiddenPreguntas").val(idFormulario);

        try {
            tablaUserNewregistrados.DataTable().destroy();
        } catch (error) {

        }


        tablaUserNewregistrados = $('#fomularioDeRegistradosPorUsuario_repguntas').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "ajax/scripts/server_processing_t4.php?idformulario=" + idFormulario,
            responsive: true,
            language: {
                searchPlaceholder: 'Buscar...',
                sSearch: '',
                lengthMenu: '_MENU_ Resultados/por página',
            },
            "order": [
                [0, "DESC"]
            ],
            destroy: true,
        });
    }

    function openHorariosFormulario(idFormulario) {
        //alert(idFormulario);
        $("#modalHorariosUser").modal('show');
        //tablaUserNew.destroy();
        $("#idFormulario").val(idFormulario);

        tablaUserNew = $('#datatable1_28Horarios').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "ajax/scripts/server_processing_t2.php?idformulario=" + idFormulario,
            responsive: true,
            language: {
                searchPlaceholder: 'Buscar...',
                sSearch: '',
                lengthMenu: '_MENU_ Resultados/por página',
            },
            "order": [
                [0, "DESC"]
            ],
            destroy: true,
        });

    }


    function cambiar_statusGlobal(tipoCambio, idFormulario) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        if (tipoCambio == 1) {
            $("#modalHorariosUserVideoConferencias").modal('hide');
            Swal({
                title: 'Activar videoconferencia permanente',
                html: '<label>Estas Seguro ?</label><br><label>Descripción de la videoconferencia </label><textarea class="form-control" name="descHorario3" id="descHorario3" rows="5" /></textarea>',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {


                    var descHorario3 = $("#descHorario3").val();

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        data: {
                            "descHorario3": descHorario3,
                            "idFormulario": idFormulario,
                            "userid": userid,
                            "ActivarConferenciaPermanente": 'SMD69'
                        },
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                openHorariosFormularioEnVivos(idFormulario)
                                Swal({
                                    title: 'Videoconferencia permanente activada',
                                    text: 'Recuerda que se mostraran la conferencia permanentemente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        // location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                } else {
                    $("#modalHorariosUserVideoConferencias").modal('show');
                }
            });

        } else if (tipoCambio == 0) {
            Swal({
                title: 'Inactivar videoconferencia permanente',
                text: 'Estas Seguro ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        data: {
                            "idFormulario": idFormulario,
                            "userid": userid,
                            "InactivarConferenciaPermanente": 'SMD69'
                        },
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                openHorariosFormularioEnVivos(idFormulario)
                                Swal({
                                    title: 'Videoconferencia permanente inactivada',
                                    text: 'Recuerda que se mostraran las conferencias solo en los horarios establecidos',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        // location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });

        }
    }

    function openHorariosFormularioEnVivos(idFormulario) {

        //var idFormulario = $("#idFormulario").val();
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';



        // fecha, hora, fecha2, hora2, descripcion
        $("#modalHorariosUserVideoConferencias").modal('hide');

        // alert('ajax to get data');
        var form_data2 = new FormData();
        form_data2.append("idFormulario", idFormulario);
        form_data2.append("userid", userid);
        form_data2.append("ObtenerHorario_video_contenido_online", 'SMD69');

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "ajax/contenidoData.php",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data2,
            success: function(loginData) {
                //swal.close();
                if (loginData.suceso == 'ok') {
                    // alert('datos obtenidos');

                    // alert('ok');

                    if (loginData.online_activo == 1) {
                        var botonStatusActivo = '<font style="color:#24a63c">Activo</font>';
                        var tipoBotonStatus = `<button type="button" style="width: 100px;" onclick="cambiar_statusGlobal('0','${idFormulario}')" class="btn btn-primary btn-block mg-b-10">Inactivar </button>`;
                        var descBotonStatus = `<br>Descripción de la videoconferencia<br><div style="color:#000">${loginData.desc_activo}</div>`;
                    } else {
                        var botonStatusActivo = '<font style="color:#245aa6">Inactivo</font>';
                        var tipoBotonStatus = `<button type="button" style="width: 100px;" onclick="cambiar_statusGlobal('1','${idFormulario}')" class="btn btn-primary btn-block mg-b-10">Activar </button>`;
                        var descBotonStatus = '';
                    }

                    $("#aquiContenidoOnlineActivoPermanente").html(``).html(`<br>Estatus videoconferencia permanente: ${botonStatusActivo}<br>${tipoBotonStatus}<br>${descBotonStatus}`);

                    //alert(idFormulario);
                    $("#modalHorariosUserVideoConferencias").modal('show');
                    //tablaUserNew.destroy();
                    $("#idFormulario_videoconferencia").val(idFormulario);

                    tablaUserNew = $('#datatable1_28Horarios_video_coferencias').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": "ajax/scripts/server_processing_t2_videoconferencia.php?idformulario=" + idFormulario,
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Buscar...',
                            sSearch: '',
                            lengthMenu: '_MENU_ Resultados/por página',
                        },
                        "order": [
                            [0, "DESC"]
                        ],
                        destroy: true,
                    });

                } else {
                    cargar_swal('error', loginData.mensaje, 'Error');
                }
            }
        });





    }

    $('#datatable1_28').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/scripts/server_processing_t1.php",
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: '_MENU_ Resultados/por página',
        },
        "order": [
            [0, "DESC"]
        ],
    });


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */


    function eliminarContenido(idContenido) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Eliminar Contenido',
            text: 'Estas Seguro De Eliminar Este Contenido ?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    data: {
                        "idContenido": idContenido,
                        "userid": userid,
                        "EliminarContenido": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Contenido Eliminado',
                                text: 'Contenido Eliminado Correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }
</script>