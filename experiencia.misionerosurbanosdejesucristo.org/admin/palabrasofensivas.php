<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 7;
require_once('include/function_admin.php');
$palabras = obetenerPalabrasOfensivas($_SESSION["adminMisionerosUrbanos"][0]);
?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Palabras ofensivas</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">


                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <h4>+ Crear palabra</h4>

                            <div class="col-lg-12 col-md-12 mb-4">

                                <br>
                                <label class="form-control-label text-uppercase">Palabras (Recordar cada palabra debe ir separada por una coma ejemplo palabra1,palabra2,palabra3)</label>
                            <textarea type="text" placeholder="Palabras" rows="5" value="" id="palabra_ofensiva" name="palabra_ofensiva" class="form-control"><?php echo $palabras["plabra"];?></textarea>


                                <br>
                                <button onclick="guardar_info()" type="submit" class="btn btn-primary">Editar palabras ofensivas </button>
                            </div>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>


            
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();



    var  tablaUserNewregistrados ;
    function cargar_palabras_ofensivas() {
        /*
        try {
            tablaUserNewregistrados.DataTable().destroy();
        } catch (error) {

        }

        tablaUserNewregistrados = $("#datatable1_28").html('');
        $('#datatable1_28').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "ajax/scripts/server_processing_subadmin_palabras.php",
            responsive: true,
            language: {
                searchPlaceholder: 'Buscar...',
                sSearch: '',
                lengthMenu: '_MENU_ Resultados/por página',
            },
            "order": [
                [1, "DESC"]
            ],
        });
        */

    }
    cargar_palabras_ofensivas();


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */


    function eliminarUsuarioSubadmin_palabras(idAdmin) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Eliminar palabra',
            text: 'Estas seguro de eliminar esta palabra ?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores_palabras.php",
                    data: {
                        "idAdmin": idAdmin,
                        "userid": userid,
                        "EliminarSubadministradorPalabras": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Palabra eliminada',
                                text: 'Palabra eliminada correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    cargar_palabras_ofensivas();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }


    function guardar_info() {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';

        var palabra_ofensiva = $("#palabra_ofensiva").val();


        if (palabra_ofensiva != '') {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "ajax/subadministradores_palabras.php",
                data: {
                    "palabra_ofensiva": palabra_ofensiva,
                    "userid": userid,
                    "CreateUserPalabra": 'SMD69'
                },
                success: function(loginData) {
                    if (loginData.suceso == 'ok') {
                        Swal({
                            title: 'Palabra creada',
                            text: 'Palabra creada correctamente',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#6baafe',
                            cancelButtonColor: '#6baafe',
                            confirmButtonText: 'ok',
                            cancelButtonText: 'No',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            if (result.value) {
                                cargar_palabras_ofensivas();
                            }
                        });
                    } else {
                        cargar_swal('error', loginData.mensaje, 'Error');
                    }
                }
            });
        } else {
            cargar_swal('info', 'Recuerda Que Toda La Informacion Es Necesaria', 'Error');
        }


    }

    function changePassword(idAdmin) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Cambiar Contraseña Subadministrador',
            html: '<label>Nueva Contraseña 1</label><br><input type="text" name="newpassword" id="newpassword" class="form-control" /><br> <label>Nueva Contraseña 2</label><br><input type="text" name="newpassword2" id="newpassword2" class="form-control" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Modificar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                var newpassword = $("#newpassword").val();
                var newpassword2 = $("#newpassword2").val();

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/subadministradores.php",
                    data: {
                        "newpassword2": newpassword2,
                        "newpassword": newpassword,
                        "idAdmin": idAdmin,
                        "userid": userid,
                        "NewPassword": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Contraseña Modificada',
                                text: 'Contraseña Modificada Correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }
</script>