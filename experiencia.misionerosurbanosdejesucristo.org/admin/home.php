<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 1;
require_once('include/function_admin.php');
$post = conto_posts($_SESSION["adminMisionerosUrbanos"][0]);
?>

<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Inicio</h3>
                        <div class="nk-block-des text-soft">
                            <p>Bienvenido <?php echo $_SESSION["adminMisionerosUrbanos"][3]; ?> </p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card card-bordered card-full">
                            <div class="card-inner">


                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h3 class="subtitle" style="font-size: 22px;"><em class="icon ni ni-pen2"></em> Experiencias espirituales creadas</h3>
                                    </div>

                                </div>

                                <div class="invest-data">
                                    <div class="invest-data-amount g-2">
                                        <h1><?php echo $post; ?></h1>

                                    </div>
                                    <div class="invest-data-ck">
                                        <canvas class="iv-data-chart" id="totalDeposit"></canvas>
                                    </div>
                                </div>


                            </div>
                        </div><!-- .card -->
                    </div><!-- .col -->


                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();
</script>