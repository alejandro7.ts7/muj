ALTER TABLE `contenidos` ADD `activaFromularioPreguntas` INT NULL DEFAULT '0' AFTER `desc_activo`;



-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2023 a las 00:48:43
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `biblioteca2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_usuarios`
--

CREATE TABLE `preguntas_usuarios` (
  `id` int(11) NOT NULL,
  `id_formulario` int(11) NOT NULL,
  `fecha_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefono` varchar(50) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `pais` varchar(100) CHARACTER SET latin1 NOT NULL,
  `estado` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(100) CHARACTER SET latin1 NOT NULL,
  `fecha_exp` datetime NOT NULL,
  `campo_editable_1` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campo_editable_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hora_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ex` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `preguntas_usuarios`
--
ALTER TABLE `preguntas_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `preguntas_usuarios`
--
ALTER TABLE `preguntas_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `preguntas_usuarios` ADD `preguntar_usuario` VARCHAR(5000) NULL AFTER `name_ex`;  

ALTER TABLE `preguntas_usuarios` ADD `respondida` INT NULL DEFAULT '0' AFTER `preguntar_usuario`;


-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2023 a las 01:42:43
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `biblioteca2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabra_ofensiva`
--

CREATE TABLE `palabra_ofensiva` (
  `id` int(11) NOT NULL,
  `plabra` longtext COLLATE latin1_general_ci NOT NULL,
  `fechat_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `palabra_ofensiva`
--

INSERT INTO `palabra_ofensiva` (`id`, `plabra`, `fechat_create`) VALUES
(1, '', '2023-07-16 18:33:07');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `palabra_ofensiva`
--
ALTER TABLE `palabra_ofensiva`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `palabra_ofensiva`
--
ALTER TABLE `palabra_ofensiva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;