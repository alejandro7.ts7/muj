ALTER TABLE `registrados` DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ROW_FORMAT = COMPACT;
ALTER TABLE `emails` DEFAULT CHARSET=utf8 COLLATE utf8_spanish_ci;
ALTER TABLE `contenidos` DEFAULT CHARSET=latin1 COLLATE latin1_swedish_ci;

/*
ALTER TABLE contenidos ADD FULLTEXT (titulo);
ALTER TABLE `contenidos` CHANGE `url` `url` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `contenidos` CHANGE `meta_description` `meta_description` MEDIUMTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
*/