-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2023 a las 00:44:22
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `biblioteca2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metadata_adjuntos`
--

CREATE TABLE `metadata_adjuntos` (
  `id` int(11) NOT NULL,
  `idContenido` int(11) NOT NULL,
  `Ruta` varchar(10000) NOT NULL,
  `miniatura_imagen_adjunto` varchar(1000) NOT NULL,
  `nombre_adjunto_info` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `metadata_adjuntos`
--
ALTER TABLE `metadata_adjuntos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `metadata_adjuntos`
--
ALTER TABLE `metadata_adjuntos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;