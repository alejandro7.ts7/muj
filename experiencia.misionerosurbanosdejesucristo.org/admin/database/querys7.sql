-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2023 a las 04:34:07
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `biblioteca2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recomendaciones_usuarios`
--

CREATE TABLE `recomendaciones_usuarios` (
  `id` int(11) NOT NULL,
  `id_formulario` int(11) NOT NULL,
  `fecha_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefono` varchar(50) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `pais` varchar(100) CHARACTER SET latin1 NOT NULL,
  `estado` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(100) CHARACTER SET latin1 NOT NULL,
  `fecha_exp` datetime NOT NULL,
  `campo_editable_1` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campo_editable_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hora_2` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ex` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preguntar_usuario` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `respondida` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `recomendaciones_usuarios`
--
ALTER TABLE `recomendaciones_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `recomendaciones_usuarios`
--
ALTER TABLE `recomendaciones_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;