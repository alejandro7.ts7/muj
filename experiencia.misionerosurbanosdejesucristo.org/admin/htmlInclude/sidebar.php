<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head" style="background-color: white;">
        <div class="nk-sidebar-brand">


            <a class="navbar-brand" onclick="home(event)" style="color:#000;font-weight:800;font-size:10px"><img style="width: auto;max-height: 65px;" src="../img/logo.png" alt="logo"> </a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-body" data-simplebar>
            <div class="nk-sidebar-content">
                <div class="nk-sidebar-menu">
                    <ul class="nk-menu">

                        <li class="nk-menu-heading">
                            <h6 class="overline-title text-primary-alt">Menús</h6>
                        </li><!-- .nk-menu-heading -->



                        <li id="menuboxLoader1" class="nk-menu-item ">
                            <a onclick="home(event)" href="#" class="nk-menu-link ">
                                <span class="nk-menu-icon"><em class="icon ni ni-home "></em></span>
                                <span class="nk-menu-text ">Inicio</span>
                            </a>
                        </li>



                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="createContenido(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-pen2"></em></span>
                                <span class="nk-menu-text">Crear contenido</span>
                            </a>
                        </li>

                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="hisotirialContenido(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-file-text"></em></span>
                                <span class="nk-menu-text">Historial de contenido</span>
                            </a>
                        </li>

                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="hisotirialIntenciones(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-file-text"></em></span>
                                <span class="nk-menu-text">Intenciones</span>
                            </a>
                        </li>

                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="palabrasofensivas(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-file-text"></em></span>
                                <span class="nk-menu-text">Listado palabras ofensivas</span>
                            </a>
                        </li>

                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="horariospaises(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-clock"></em></span>
                                <span class="nk-menu-text">Configuración Horario países y desplegable de recomendaciones </span>
                            </a>
                        </li>

                        <li id="menuboxLoader2" class="nk-menu-item">
                            <a href="#" onclick="notificacionesausuarios(event)" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-chat"></em></span>
                                <span class="nk-menu-text">Notificaciones</span>
                            </a>
                        </li>

                        <?php if ($idReal == 1) { ?>

                            <li id="menuboxLoader4" class="nk-menu-item">
                                <a href="#" onclick="subaministradores(event)" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                                    <span class="nk-menu-text">Subaministradores</span>
                                </a>
                            </li>

                        <?php } ?>


                        <li class="nk-menu-item">
                            <a href="logout.php" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-signout"></em></span>
                                <span class="nk-menu-text">Cerrar sesión</span>
                            </a>
                        </li>







                    </ul><!-- .nk-menu -->
                </div><!-- .nk-sidebar-menu -->
                <div class="nk-sidebar-footer">

                </div><!-- .nk-sidebar-footer -->
            </div><!-- .nk-sidebar-content -->
        </div><!-- .nk-sidebar-body -->
    </div><!-- .nk-sidebar-element -->
</div>