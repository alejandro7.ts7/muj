<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 6;
require_once('include/function_admin.php');
$id = $_GET["id"];
$_SESSION["adminHistorialPointMUJUser"] = $id;
$obtenerInfo = obtener_info_contenido_email($_SESSION["adminMisionerosUrbanos"][0], $id);
$idInformacion = $obtenerInfo["id_formulario"];


?>
<style>
    .dropdown-menu.note-check a i {
        visibility: visible;
        color: black;
    }

    .note-popover .popover-content .dropdown-menu.note-check a i,
    .card-header.note-toolbar .dropdown-menu.note-check a i {
        visibility: visible;
        color: black;
    }

    .note-icon-menu-check:before {
        content: "";
    }
</style>
<link href="lib/summernote/css/summernote-bs4.css" rel="stylesheet">

<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">
                            <a style="cursor: pointer;" onclick="hisotirialContenido()"><em class="icon ni ni-arrow-left-round-fill"></em> Historial De Contenido </a>

                        </h3>
                        <div class="nk-block-des text-soft">

                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <div class="row g-gs">




                    <div class="col-12">
                        <div class="card card-full">



                            <div class="col-lg-12 col-md-12 mb-4">
                                <br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Variables : </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">nombre : //%NOMBRE//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">apellido : //%APELLIDO//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Correo Electronico : //%EMAIL//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">WhatsApp : //%WHATSAPP//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">País : //%PAIS//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Ciudad : //%CIUDAD//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Nombre Experiencia : //%NAMEEXPERIENCIA//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Fecha Experiencia : //%FECHAEXPERIENCIA//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Hora Experiencia : //%HORAEXPERIENCIA//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Hora Experiencia : //%HORAEXPERIENCIA//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Campo Editable 1 : //%CAMPO1//% </label><br>
                                <label style="color:#000;" class="form-control-label text-uppercase">Campo Editable 2 : //%CAMPO2//% </label><br>




                                <label class="form-control-label text-uppercase">Email </label>


                                <div class="summernote" id="desc_collecion"><?php echo $obtenerInfo["contenido"]; ?></div>
                                <br>


                                <button onclick="guardar_info2Email()" type="submit" class="btn btn-primary">Editar Email </button>
                            </div>







                        </div><!-- .card -->
                    </div><!-- .col -->


                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->
<script type="text/javascript" src="js/typeahead.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".note-color-all .dropdown-toggle").html('▼');
    });


    var informacion = $('#desc_collecion').summernote({
        height: 650,
        tooltip: false,
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                that = $(this);
                sendFile(files[0], editor, welEditable, that);
            },
            onFileUpload: function(file) {
                that = $(this);
                myOwnCallBack(file[0], that, 1);
            },
        },
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        lang: 'es-ES', // Change to your chosen language
        imageAttributes: {
            icon: '<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '32', '36', '40', '48', '50', '60', '72', '80', '90', '100', '120', '150', '200', '240'],
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video', 'file']],
            ['view', ['fullscreen']],

        ],
    });



    function sendFile(file, editor, welEditable, edicionSumer) {
        data = new FormData();
        data.append("file", file);
        data.append("SubirImagenTarjeta", "SMD69");
        data.append("userid", '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>');
        $.ajax({
            data: data,
            type: "POST",
            url: "ajax/subidaimagentarjetapersonal.php",
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(url) {
                if (url.suceso == 'ok') {
                    edicionSumer.summernote('insertImage', url.mensaje)
                } else {
                    cargar_swal('error', url.mensaje, 'Error');
                }
            }
        });
    }

    function myOwnCallBack(file, edicionSumer, edito2ver) {

        let data = new FormData();
        data.append("file", file);
        data.append("SubirImagenTarjetaFileAcrhivo", "SMD69");
        data.append("userid", '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>');

        var varData = 'desc_collecion';

        $.ajax({
            data: data,
            type: "POST",
            url: "ajax/subidaimagentarjetapersonalExtras.php", //Your own back-end uploader
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            xhr: function() { //Handle progress upload

                let myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                return myXhr;
            },
            success: function(reponse) {
                console.log(reponse);
                if (reponse.suceso === 'ok') {
                    let listMimeImg = ['image/png', 'image/jpeg', 'image/webp', 'image/gif', 'image/svg'];
                    let listMimeAudio = ['audio/mpeg', 'audio/ogg'];
                    let listMimeVideo = ['video/mpeg', 'video/mp4', 'video/webm'];
                    let elem;



                    if (listMimeImg.indexOf(file.type) > -1) {
                        //Picture
                        //alert('aqui2');
                        //alert(reponse.mensaje);
                        $("#" + varData).summernote('editor.insertImage', reponse.mensaje);
                        //edicionSumer.summernote('insertImage', reponse.mensaje)
                    } else if (listMimeAudio.indexOf(file.type) > -1) {
                        //Audio
                        elem = document.createElement("audio");
                        elem.setAttribute("src", reponse.mensaje);
                        elem.setAttribute("controls", "controls");
                        elem.setAttribute("preload", "metadata");
                        $("#" + varData).summernote('editor.insertNode', elem);
                    } else if (listMimeVideo.indexOf(file.type) > -1) {
                        //Video
                        elem = document.createElement("video");
                        elem.setAttribute("src", reponse.mensaje);
                        elem.setAttribute("controls", "controls");
                        elem.style = "width:100%";
                        elem.setAttribute("preload", "metadata");
                        $("#" + varData).summernote('editor.insertNode', elem);
                    } else {
                        // console.log(file.type);
                        //Other file type
                        elem = document.createElement("a");
                        let linkText = document.createTextNode(file.name);
                        elem.appendChild(linkText);
                        elem.title = file.name;
                        elem.href = reponse.mensaje;
                        $("#" + varData).summernote('editor.insertNode', elem);
                    }
                }
            }
        });
    }

    function progressHandlingFunction(e) {
        if (e.lengthComputable) {
            //Log current progress
            //console.log((e.loaded / e.total * 100) + '%');

            //Reset progress on complete
            if (e.loaded === e.total) {
                //console.log("Upload finished.");
            }
        }
    }



    function guardar_info2Email() {
        event.preventDefault();




        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        var codigo_email = informacion.summernote('code');
        var form_data = new FormData();
        var idInfo = <?php echo $idInformacion; ?>;

        form_data.append("idInfo", idInfo);
        form_data.append("codigo_email", codigo_email);
        form_data.append("userid", userid);
        form_data.append("EditarEmailInformacionHtml", 'SMD69');



        // alert crear contenido 

        var continuar = true;

        if (continuar == true) {
            Swal({
                title: 'Editar Email',
                text: 'Estas Seguro De Editar Este Email ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#6baafe',
                cancelButtonColor: '#6baafe',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "ajax/contenidoData.php",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                Swal({
                                    title: 'Email Editado',
                                    text: 'Email Editado Correctamente',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#6baafe',
                                    cancelButtonColor: '#6baafe',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            } else {
                                cargar_swal('error', loginData.mensaje, 'Error');
                            }
                        }
                    });


                }
            });
        } else {
            cargar_swal('error', mensajeError, 'Error');
        }

    }
</script>


<script>
    $(function() {
        'use strict';
    });


    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();
</script>