window.fbAsyncInit = function() {
    FB.init({
      appId      : '2269729386453544',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.3' // The Graph API version to use for the call
    });

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));



  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      testAPI();
    } else {
        FB.login(function(response) {
		  checkLoginState();
		}, {scope: 'public_profile,email'});
    }
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  function testAPI() {
    FB.api('/me?fields=id,name,email,permissions', function(response) {
    	login_uxuario_facebook(response.id);
    });
  }


  function statusChangeCallback_regixter(response) {
    if (response.status === 'connected') {
      testAPIRegixter();
    } else {
        FB.login(function(response) {
      checkLoginState_regixter();
    }, {scope: 'public_profile,email'});
    }
  }


  function testAPIRegixter() {
    var ruta ="";
      FB.api('/me?fields=id,name,email,permissions,first_name,last_name', function(response) {
      //login_uxuario_facebook(response.id);

     

      regixter_uxuario_facebook(response.id,response.first_name,response.last_name,"",response.email);
    });



  }


  function checkLoginState_regixter() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback_regixter(response);
    });
  }

  


 

  function cerrada_data(){
  	FB.logout(function(response) {
	   
	 });
  }

  function login_uxuario_facebook(id){
    
    $.ajax({
      type: "POST",
      url: "ajax/login_user.php",
      data : {"id":id,"loginDataFacebook":2},
      dataType : 'json',
      success: function(loginData) {
        if(loginData.data=='ok'){
          location.href = "index.php";
                        //window.open("index.php");

                      }else {
                        cargar_swal('error',loginData.data,'error');
                      }  
                    }
                  });

  }


