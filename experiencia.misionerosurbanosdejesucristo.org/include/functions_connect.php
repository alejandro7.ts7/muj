<?php
include('admin/connection/conect.php');

function informacion_calendario_clase_data_general_afiliado_general()
{


    $fecha_actual = date('Y-m-d H:i:s');
    // . ' 00:00:00'
    $fecha_horarios = $fecha_actual;

    // restar 1 a;o de noticias
    $fecha_horarios = date("Y-m-d H:i:s",strtotime($fecha_horarios."- 365 days")); 


    $con = conection_database();


    $Sql_Query = "SELECT * FROM `contenidos`  WHERE tipo='3' AND fecha_contenido>='". $fecha_horarios."' ORDER BY fecha_contenido ASC";
    $sql = mysqli_query($con, $Sql_Query);

    if ($sql) {
        //;
        $fecha_actualCmp = date('Y-m-d');
        $result = '';
        while ($row = mysqli_fetch_assoc($sql)) {
            // <strong>Módulo</strong>: ' . $row["nombre_modulo"] . '<br>
            //echo $row["id"];
            $fecha_comparacion = date("Y-m-d", strtotime($row["fecha_clase"]));
            
            $result = $result . '<div class="card">
              <div class="card-header" role="tab" id="headingOneData_' . $row["id"] . '">
                <h2 data-toggle="collapse" data-parent="#accordion3Data" href="#collapseOneData_' . $row["id"] . '" aria-expanded="false" aria-controls="collapseOneData_' . $row["id"] . '" class="tx-gray-800 transition collapsed">
                	' . $row["fecha_contenido"] . ' : ' . $row["contenido"] . '
                </h2><br><hr>
              </div><!-- card-header -->

              ';
        }
    } else {

        $result = array();
    }

    close_database($con);

    return $result;
}


function informacion_calendario_clase_data_general_afiliado_individual_generlizado()
{
    $fecha_actual = date('Y-m-d H:i:s');
    // . ' 00:00:00'
    $fecha_horarios = $fecha_actual;
    $fecha_horarios = date("Y-m-d H:i:s", strtotime($fecha_horarios . "- 365 days")); 


    $con = conection_database();
    $Sql_Query = "SELECT * FROM `contenidos`  WHERE tipo='3' AND fecha_contenido>='" . $fecha_horarios . "' ";
    $sql = mysqli_query($con, $Sql_Query);

    if ($sql) {
        //;
        $result = $sql;
    } else {

        $result = array();
    }

    close_database($con);

    return $result;
}


function informacion_calendario_clase_data_general_afiliado_general5()
{


    $fecha_actual = date('Y-m-d H:i:s');
    // . ' 00:00:00'
    $fecha_horarios = $fecha_actual;



    $con = conection_database();


    $Sql_Query = "SELECT * FROM `contenidos`  WHERE tipo='5' AND fecha_contenido>='" . $fecha_horarios . "' ORDER BY fecha_contenido ASC";
    $sql = mysqli_query($con, $Sql_Query);

    if ($sql) {
        //;
        $fecha_actualCmp = date('Y-m-d');
        $result = '';
        while ($row = mysqli_fetch_assoc($sql)) {
            // <strong>Módulo</strong>: ' . $row["nombre_modulo"] . '<br>
            //echo $row["id"];
            $fecha_comparacion = date("Y-m-d", strtotime($row["fecha_clase"]));

            $result = $result . '<div class="card">
              <div class="card-header" role="tab" id="headingOneData_' . $row["id"] . '">
                <h2 data-toggle="collapse" data-parent="#accordion3Data" href="#collapseOneData_' . $row["id"] . '" aria-expanded="false" aria-controls="collapseOneData_' . $row["id"] . '" class="tx-gray-800 transition collapsed">
                	' . $row["fecha_contenido"] . ' : ' . $row["contenido"] . '
                </h2><br><hr>
              </div><!-- card-header -->

              ';
        }
    } else {

        $result = array();
    }

    close_database($con);

    return $result;
}


function informacion_calendario_clase_data_general_afiliado_individual_generlizado5()
{
    $fecha_actual = date('Y-m-d H:i:s');
    // . ' 00:00:00'
    $fecha_horarios = $fecha_actual;

    $con = conection_database();
    $Sql_Query = "SELECT * FROM `contenidos`  WHERE tipo='5' AND fecha_contenido>='" . $fecha_horarios . "' ";
    $sql = mysqli_query($con, $Sql_Query);

    if ($sql) {
        //;
        $result = $sql;
    } else {

        $result = array();
    }

    close_database($con);

    return $result;
}

function obtener_categorias_data(){

    $con = conection_database();
    $categorias1 = "SELECT DISTINCT(tipo_publicacion) FROM `contenidos` WHERE  status='1' ORDER BY tipo_publicacion ASC"; 
    $categorias2 = "SELECT DISTINCT(tipo_publicacion2) FROM `contenidos` WHERE  status='1' ORDER BY tipo_publicacion ASC";


    $sql1 = mysqli_query($con, $categorias1);
    $sql2 = mysqli_query($con, $categorias2);
 

    $dataCategories = array();

    while ($row1 = mysqli_fetch_assoc($sql1)) {
        if(!empty($row1["tipo_publicacion"])){
            array_push($dataCategories, $row1["tipo_publicacion"]);
        }
    }


    while ($row2 = mysqli_fetch_assoc($sql2)) {
        if (!empty($row2["tipo_publicacion2"])) {
            array_push($dataCategories, $row2["tipo_publicacion2"]);
        }
    }

  

    
    $dataCategories = array_unique($dataCategories);
    sort($dataCategories);

    // 

    close_database($con);

    return $dataCategories;
}

function total_contenido_categoria($categoria)
{

    $con = conection_database();
    $categorias1 = "SELECT COUNT(id) AS total FROM `contenidos` WHERE  status='1' AND (tipo_publicacion='".$categoria. "' OR tipo_publicacion2='".$categoria."')";
   

    $sql1 = mysqli_query($con, $categorias1);
    

    $dataCategories = 0;

    $row1 = mysqli_fetch_assoc($sql1);

    if(!empty($row1["total"])){
        $dataCategories = $row1["total"];
    }



    close_database($con);

    return $dataCategories;
}


function consultar_contenido_informativo($idContenido)
{

    $con = conection_database();
    $categorias1 = "SELECT * FROM `contenidos` WHERE  status='1' AND id='". $idContenido."'";
    $sql1 = mysqli_query($con, $categorias1);
    $dataCategories = 0;
    $row1 = mysqli_fetch_assoc($sql1);

    if (!empty($row1["id"])) {
        $dataCategories = $row1;
    }



    close_database($con);

    return $dataCategories;
}
function getName($string)
{

    $string = explode('/', $string);
    $count = count($string);
    $string = $string[$count - 1];

    return $string;
}



function obtenerFechasExperieicnias($idExperieincia)
{

    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');
    $categorias1 = "SELECT * FROM `horarios` WHERE id_formulario='". $idExperieincia. "' AND fecha_begin>'".$fechaActual."' ORDER BY `horarios`.`fecha_begin` ASC";
    $sql1 = mysqli_query($con, $categorias1);
    $dataCategories = '';
   
    while ($row1 = mysqli_fetch_assoc($sql1)) {

        // Ejemplo (Jueves 16 De Agosto Del 2022 Hora : 02:00 PM )

        $dayName = getDayName(date('w', strtotime($row1["fecha_begin"])));
        $dataCategories = $dataCategories.'<option value="'. $row1["fecha_begin"]. '">'. $dayName.' '. date('j', strtotime($row1["fecha_begin"])).' De Agosto Del '. date('Y', strtotime($row1["fecha_begin"])).' Hora : '. date('H:i', strtotime($row1["fecha_begin"])).'  </option>';
    }



    close_database($con);

    return $dataCategories;
}

function getDayName($dayOfWeek)
{

    switch ($dayOfWeek) {
        case 6:
            return 'Sábado';
        case 0:
            return 'Domingo';
        case 1:
            return 'Lunes';
        case 2:
            return 'Martes';
        case 3:
            return 'Miércoles';
        case 4:
            return 'Jueves';
        case 5:
            return 'Viernes';
        default:
            return '';
    }
}


function obtenerFechasExperieicnias_v2($idExperieincia,$timeZone)
{
    
    /*echo $timeZone;
    $the_date = strtotime(date('Y-m-d H:i:s'));
    echo (date_default_timezone_get() . "<br />");
    echo (date("Y-m-d H:i:s", $the_date) . "<br />");
    echo (date_default_timezone_set($timeZone) . "<br />");
    echo (date("Y-m-d H:i:s", $the_date) . "<br />");
    */



    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');
    $categorias1 = "SELECT * FROM `horarios` WHERE id_formulario='" . $idExperieincia . "' AND fecha_begin>'" . $fechaActual . "' ORDER BY `horarios`.`fecha_begin` ASC";
    $sql1 = mysqli_query($con, $categorias1);
    $dataCategories = '';

    while ($row1 = mysqli_fetch_assoc($sql1)) {

        // Ejemplo (Jueves 16 De Agosto Del 2022 Hora : 02:00 PM )
        $fechaValue = $row1["fecha_begin"];
        $the_date = strtotime($row1["fecha_begin"]);        
        $row1["fecha_begin"] = date("Y-m-d H:i:s", $the_date);
        date_default_timezone_set('Etc/GMT'.$timeZone);
        $row1["fecha_begin"] = date("Y-m-d H:i:s", $the_date);
        date_default_timezone_set('America/Bogota');
        //echo "<br><br>";
        

        $dayName = getDayName(date('w', strtotime($row1["fecha_begin"])));
        $dataCategories = $dataCategories . '<option value="' . $fechaValue . '">' . $dayName . ' ' . date('j', strtotime($row1["fecha_begin"])) . ' De Agosto Del ' . date('Y', strtotime($row1["fecha_begin"])) . ' Hora : ' . date('H:i', strtotime($row1["fecha_begin"])) . '  </option>';
    }



    close_database($con);

    return $dataCategories;
}

function obtenerFechasExperieicnias_v3($idExperieincia, $timeZone)
{

    /*echo $timeZone;
    $the_date = strtotime(date('Y-m-d H:i:s'));
    echo (date_default_timezone_get() . "<br />");
    echo (date("Y-m-d H:i:s", $the_date) . "<br />");
    echo (date_default_timezone_set($timeZone) . "<br />");
    echo (date("Y-m-d H:i:s", $the_date) . "<br />");
    */



    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');
    $categorias1 = "SELECT * FROM `horarios` WHERE id_formulario='" . $idExperieincia . "' AND fecha_begin>'" . $fechaActual . "' ORDER BY `horarios`.`fecha_begin` ASC";
    $sql1 = mysqli_query($con, $categorias1);
    $dataCategories = '';

    while ($row1 = mysqli_fetch_assoc($sql1)) {

        // Ejemplo (Jueves 16 De Agosto Del 2022 Hora : 02:00 PM )
        $fechaValue = $row1["fecha_begin"];
        $the_date = strtotime($row1["fecha_begin"]);
        $row1["fecha_begin"] = date("Y-m-d H:i:s", $the_date);
        date_default_timezone_set('Etc/GMT'.$timeZone);
        $row1["fecha_begin"] = date("Y-m-d H:i:s", $the_date);
        date_default_timezone_set('America/Bogota');
        //echo "<br><br>";


        $dayName = getDayName(date('w', strtotime($row1["fecha_begin"])));
        
        /*$dataCategories = $dataCategories . '<option value="' . $fechaValue . '">' . $dayName . ' ' . date('j', strtotime($row1["fecha_begin"])) . ' De Agosto Del ' . date('Y', strtotime($row1["fecha_begin"])) . ' Hora : ' . date('H:i', strtotime($row1["fecha_begin"])) . '  </option>';
        */

        // date('F/m/Y', strtotime($row1["fecha_begin"]))

        
        $dataCategories = $dataCategories. '{
                    id: "'. $fechaValue.'",
                    name: "Hora : ' . date('H:i', strtotime($row1["fecha_begin"])) . '",
                    badge: "",
                    date: ["'. date('F/d/Y', strtotime($row1["fecha_begin"])). '"],
                    description: "' . $dayName . ' ' . date('j', strtotime($row1["fecha_begin"])) . ' De Agosto Del ' . date('Y', strtotime($row1["fecha_begin"])) . ' ",
                    type: "event",
                },';
    }



    close_database($con);

    return $dataCategories;
}

function consultarApiTimezone($ip){
    $urlIp = 'http://worldtimeapi.org/api/ip/'. $ip;
    //Peticion Curl Get
    $ch = curl_init();
    // lineas del ssl 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // Hasta Aqui Lineas Del SSL
    curl_setopt($ch, CURLOPT_URL, $urlIp);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $output = curl_exec($ch);
    if ($output === false) {
        return  curl_error($ch);
    }

    curl_close($ch);

    return json_decode($output,true);
}

function obtener_info_emetadata_usuario($idContenido, $ruta)
{

    $data = array();

    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');
    $Sql_Query = "SELECT * FROM `metadata_adjuntos` WHERE `idContenido` = '" . $idContenido . "' AND `Ruta` = '" . $ruta . "' ";
    $check = mysqli_query($con, $Sql_Query);
    $row = mysqli_fetch_assoc($check);

    if (!empty($row["id"])) {
        $data = $row;
    }

    // obtener_info_emetadata_usuario

    close_database($con);

    return $data;
}

function registrar_acceeso_ip($ip,$post){
    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');

    // Obtener el año
    $anio = date('Y', strtotime($fechaActual));

    // Obtener el mes
    $mes = date('m', strtotime($fechaActual));

    // Obtener el día
    $dia = date('d', strtotime($fechaActual));

    // Obtener la hora
    $hora = date('H', strtotime($fechaActual));

    // Obtener el minuto
    $minuto = date('i', strtotime($fechaActual));

    // Obtener el segundo
    $segundo = date('s', strtotime($fechaActual));


    $Sql_Query = "INSERT INTO logger_users (ip,post,fecha,anio,mes,dia,hora,minuto,segundo) VALUES ('". $ip."','". $post."','". $fechaActual."','". $anio."','". $mes."','". $dia."','". $hora."','". $minuto."','". $segundo."')";
    $check = mysqli_query($con, $Sql_Query);
    $row = mysqli_fetch_assoc($check);

    if (!empty($row["id"])) {
        $data = $row;
    }

    // obtener_info_emetadata_usuario

    close_database($con);
}