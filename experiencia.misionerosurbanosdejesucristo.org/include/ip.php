<?php

function getRealIPAfiliadoVi()
{


    //return $ip_data = '103.198.80.4';

    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}


function ConsultarPorApiPais($idDireccion){
    // https://api.iplocation.net/?ip=103.198.80.4
    //Peticion Curl Get
    $url = 'https://api.iplocation.net/?ip='.$idDireccion;
    $ch = curl_init();
    // lineas del ssl 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // Hasta Aqui Lineas Del SSL
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $output = curl_exec($ch);
    if ($output === false) {
        return  curl_error($ch);
    }

    curl_close($ch);
    return $output;
}

function devolver_id_from_country($ip_data)
{
    //$ip_data = getRealIPAfiliadoVi();
    //$ip_data = "190.70.133.104";
    // $ip_data = '191.95.158.96';

    
    $con = conection_database();
    mysqli_set_charset($con, "utf8");
    $Sql_Query = "SELECT 
	            c.country,c.iso_country,iso_code_2,c.iso_code_3 
	        FROM 
	            ip2nationcountries c,
	            ip2nation i
	        WHERE 
	            i.ip < INET_ATON('" . $ip_data . "') 
	            AND 
	            c.code = i.country 
	        ORDER BY 
	            i.ip DESC 
	        LIMIT 0,1 ";
    $check = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query));


    $Sql_Query_d = "SELECT id,pais  FROM `paises` WHERE `iso` = '" . $check["iso_code_2"] . "' ";
    $checkD = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d));


    $Sql_Query_d2 = "SELECT *  FROM `paises_timezone` WHERE `iso` = '" . $check["iso_code_2"] . "' ORDER BY `paises_timezone`.`id` DESC LIMIT 1";
    $checkD2 = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d2));

    if (isset($check)) {
        $result = array("pais" => $checkD["pais"], "iso" => $check["iso_code_2"], "idCountry" => $checkD["id"], "pais2" => $check["iso_country"],'timezone'=> $checkD2["utc"]);
    } else {
        $result = array("pais" => "", "iso" => "", "idCountry" => "");
    }

    close_database($con);

    return $result;
}

function devolver_id_from_country_by_code($IsoCode)
{
    //$ip_data = getRealIPAfiliadoVi();
    //$ip_data = "190.70.133.104";
    // $ip_data = '191.95.158.96';


    $con = conection_database();
    mysqli_set_charset($con, "utf8");
    /*
    $Sql_Query = "SELECT 
	            c.country,c.iso_country,iso_code_2,c.iso_code_3 
	        FROM 
	            ip2nationcountries c,
	            ip2nation i
	        WHERE 
	            i.ip < INET_ATON('" . $ip_data . "') 
	            AND 
	            c.code = i.country 
	        ORDER BY 
	            i.ip DESC 
	        LIMIT 0,1 ";
    $check = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query));
    */


    $Sql_Query_d = "SELECT id,pais  FROM `paises` WHERE `iso` = '" . $IsoCode . "' ";
    $checkD = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d));

    

    $Sql_Query_d2 = "SELECT *  FROM `paises_timezone` WHERE `iso` = '" . $IsoCode . "' ORDER BY `paises_timezone`.`id` DESC LIMIT 1";
    $checkD2 = mysqli_fetch_assoc(mysqli_query($con, $Sql_Query_d2));

    // var_dump($checkD2);

    if (isset($checkD)) {
        $result = array("pais" => $checkD["pais"], "iso" => $IsoCode, "idCountry" => $checkD["id"], "pais2" => $checkD["pais"], 'timezone' => $checkD2["timezone"]);
    } else {
        $result = array("pais" => "", "iso" => "", "idCountry" => "");
    }

    close_database($con);

    return $result;
}


   

?>