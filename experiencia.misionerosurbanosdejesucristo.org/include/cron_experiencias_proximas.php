<?php
include_once('../admin/connection/conect.php');
include_once('../admin/ajax/enviarmensaje.php');

function consultarExperienciasPrximas()
{
    $horaActual = date('Y-m-d H:i:s');
    // Suma 30 minutos a la fecha actual
    $nuevaFecha = date('Y-m-d H:i:s', strtotime($horaActual . ' +30 minutes'));

    $con = conection_database();
    $paginacion1 = "SELECT * FROM `horarios_video`  WHERE (fecha_begin  BETWEEN '". $horaActual."' AND  '". $nuevaFecha."') AND (envio_notificacion='0')";
    $sqlPaginate1 = mysqli_query($con, $paginacion1);
   


    while ($row1Paginate1 = mysqli_fetch_assoc($sqlPaginate1)) {
        
        $idInicial = $row1Paginate1["id_formulario"];
        $paginacion2 = "SELECT * FROM `contenidos`  WHERE id ='". $idInicial."'";
        $sqlPaginate2 = mysqli_query($con, $paginacion2);
        $row1Paginate2 = mysqli_fetch_assoc($sqlPaginate2);


        $fechaDeInicio = $row1Paginate1["fecha_begin"];
        $horaActualInicio = $horaActual;

        // Define las dos fechas
        $fecha1 = $fechaDeInicio;
        $fecha2 = $horaActualInicio;

        // Crea objetos DateTime para las dos fechas
        $datetime1 = new DateTime($fecha1);
        $datetime2 = new DateTime($fecha2);

        // Calcula la diferencia entre las dos fechas
        $diferencia = $datetime1->diff($datetime2);

        // Obtiene la diferencia en minutos
        $minutos = $diferencia->i;

        // Imprime la diferencia en minutos
        

        $tituloMensaje = $row1Paginate2["titulo"];
        $palabraInicio2 = ($minutos==1)? 'el': 'los';
        $palabraInicio1 = ($minutos==1)? 'próximo': 'próximos';
        $palabraInicio = ($minutos==1)?'minuto':'minutos';
        $minutos = ($minutos==1)?'': $minutos;

        $link = 'https://experiencia.misionerosurbanosdejesucristo.org/post.php?id='. $idInicial;
        $contenido = 'En '. $palabraInicio2.' '. $palabraInicio1.' '. $minutos.' '. $palabraInicio.' inicia ' . $tituloMensaje . ' Recuerda ingresar desde el siguiente link: ';

        $paginacion3 = "UPDATE `horarios_video` SET envio_notificacion='1'  WHERE id ='" . $row1Paginate1["id"] . "'";
        $sqlPaginate3 = mysqli_query($con, $paginacion3);

        if($sqlPaginate3){
            EnviarMensajeNotificacionFCB($tituloMensaje, $contenido, $link);
        }
        
    }

    echo "Finalizado";
    // var_dump($row1Paginate1);
}

consultarExperienciasPrximas();

?>