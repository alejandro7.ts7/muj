<?php
session_start();
date_default_timezone_set('America/Bogota');
include('../connection/conect.php');
include('../include/functions.php');
include('../../include/ip.php');


function consultar_horarios_video_conferencias($idHorario, $timeZone){
    $con = conection_database();
    date_default_timezone_set('America/Bogota');
    $horaActual  = date('Y-m-d H:i:s');
    $horaActual = date('Y-m-d H:i:s', strtotime('-15 minutes', strtotime($horaActual)));

    $Sql_Query22 = "SELECT *  FROM `horarios_video` WHERE `id_formulario` = '". $idHorario."' AND `fecha_begin` >= '". $horaActual."' ORDER BY `id`  DESC LIMIT 1 ";
    $check22 = mysqli_query($con, $Sql_Query22);
    $row22 = mysqli_fetch_assoc($check22);

    $horaRegreso ='';

    if(!empty($row22["fecha_begin"])){
        $stringHora = $row22["fecha_begin"];
        $horaActualVisitante2 = strtotime($stringHora);
        date_default_timezone_set('Etc/GMT' . $timeZone);
        $horaRegreso = date("H:i", $horaActualVisitante2);
        date_default_timezone_set('America/Bogota');
    }else {
        $horaRegreso = '';
    }


    close_database($con);

    return $horaRegreso;
}

if (isset($_POST["obtenerRecomendacionParaUsuario"]) && $_POST["obtenerRecomendacionParaUsuario"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    // obtener ip del usuario
    $obterIpDelUsuario = getRealIPAfiliadoVi();
    $obterIpDelUsuario = explode(',', $obterIpDelUsuario);
    $obterIpDelUsuario = $obterIpDelUsuario[0];
    $datosPaiRegistro = devolver_id_from_country($obterIpDelUsuario);
    $timeZone = $datosPaiRegistro["timezone"];
    $timezon = explode("UTC", $timeZone);
    $timezon2 = trim($timezon[1]);
    $signo = substr($timezon2, 0, 1);
    $tiempo = str_replace('-', '', $timezon2);
    $tiempo = str_replace('+', '', $timezon2);
    $tiempo =  explode(":", $tiempo);
    $tiempo = $tiempo[0];
    if (substr($tiempo, 0, 1) === "0") {
        $tiempo = substr($tiempo, 1);
    }
    if ($signo == '-') {
        $signo = '+';
    } elseif ($signo == '+') {
        $signo = '-';
    }

    $timeZone = $signo . $tiempo;

    date_default_timezone_set('Etc/GMT' . $timeZone);
    $horaActualVisitante = date('Y-m-d H:i:s');
    $horaActualVisitante2 = strtotime($horaActualVisitante);
    date_default_timezone_set('America/Bogota');
    $horaActualVisitante3 = date("N", $horaActualVisitante2);
    $horaActualServidor = date('Y-m-d H:i:s');
    // echo $horaActualVisitante3;



    $tipo = $_POST["tipo"];
    $diaActual = $horaActualVisitante3;
    $tipoME= 8;


    $con = conection_database();
    $consultarDatos = false;

    if($tipo==1){
        $consultarDatos = true;
    }else if($tipo==2){
        $consultarDatos = false;

        $Sql_Query22 = "SELECT * FROM `palabra_ofensiva` WHERE id='2' ";
        $check22 = mysqli_query($con, $Sql_Query22);
        $row22 = mysqli_fetch_assoc($check22);

        // forma de validar el usuario 

        if($row22["define_by"]==2){
            // conexiones
            $Sql_Query222 = "SELECT COUNT(id) AS total FROM `logger_users` WHERE ip='". $obterIpDelUsuario."' ";
            $check222 = mysqli_query($con, $Sql_Query222);
            $row222 = mysqli_fetch_assoc($check222);

            if($row222["total"]>= $row22["plabra"]){
                $consultarDatos = true;
            }
            

        } elseif ($row22["define_by"] == 1) {
            // dias conectados

            //echo "Aqui";
            $Sql_Query222 = "SELECT COUNT( DISTINCT CONCAT(anio, '-', mes, '-',dia)) AS total FROM `logger_users` WHERE ip='" . $obterIpDelUsuario . "' ";
            $check222 = mysqli_query($con, $Sql_Query222);
            $row222 = mysqli_fetch_assoc($check222);

            if ($row222["total"] >= $row22["plabra"]) {
                $consultarDatos = true;
               
            }
        }
    }

    if($consultarDatos==true){
        $mensaje = 'ok';
        $submensaje = 'ok';
        $Sql_Query2 = "SELECT id,titulo,imagen_portada,resumen FROM  contenidos  WHERE ( destacados='" . $tipoME . "' OR destacados LIKE '%" . $tipoME . "' OR destacados LIKE '" . $tipoME . "%' OR destacados LIKE '%" . $tipoME . "%') AND ( destacados='" . $diaActual . "' OR destacados LIKE '%" . $diaActual . "' OR destacados LIKE '" . $diaActual . "%' OR destacados LIKE '%" . $diaActual . "%') ORDER BY posicionamiento ASC ";
        $check2 = mysqli_query($con, $Sql_Query2);
        $row_cnt = mysqli_num_rows($check2);

        $existeContenidoParaEldia = 0;


        $arrayData = array();
        if($row_cnt>0){
            $existeContenidoParaEldia = $row_cnt;
            
            while ($row2 = mysqli_fetch_assoc($check2)) {

                $horarioDeEncuentro = consultar_horarios_video_conferencias($row2["id"], $timeZone);

                if(!empty($horarioDeEncuentro)){
                    $row2["horario"] = $horarioDeEncuentro;
                }else {
                    $row2["horario"] = '';
                }

                
                array_push($arrayData, $row2);
            }
        }else {
            $existeContenidoParaEldia = 0;
        }

        
        



        $result = array('suceso' => 'ok', 'existecontenido' => $existeContenidoParaEldia,'datoscotenido'=> $arrayData);
    }else {
        $result = array('suceso' => '');
    }

    

    

    close_database($con);
    $obj = json_encode($result);

    echo $obj;
};