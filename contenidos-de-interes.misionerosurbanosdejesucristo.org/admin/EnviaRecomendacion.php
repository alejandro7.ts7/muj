<?php
session_start();
include('../connection/conectutf8.php');
include('../include/functions.php');
include('../mail/notif_personalizadas.php');
error_reporting(E_ALL);
error_reporting(-1);
ini_set('error_reporting', E_ALL);
if (isset($_POST["registrarUsuarioExperiencia_v2_email"]) && $_POST["registrarUsuarioExperiencia_v2_email"] == 'SMD69') {

    $nameExperience = $_POST["nameExperience"];
    $nombreUsuarioRegistrar = $_POST["nombreUsuarioRegistrar"];
    $apellidoUsuarioRegistrar = $_POST["apellidoUsuarioRegistrar"];
    $emailUsuarioRegistrar = $_POST["emailUsuarioRegistrar"];
    $urlExperience = $_POST["urlExperience"];

    $to = $emailUsuarioRegistrar;
    $subject = "Recomendación de Contenido";

    $message = "¡Hola $nombreUsuarioRegistrar $apellidoUsuarioRegistrar!\n\n";
    $message .= "Alguien que te estima ha querido compartir contigo este contenido que puede ser interesante para ti '$nameExperience'.\n";
    $message .= "Puedes verlo en el siguiente link: $urlExperience.";

    $headers = "From: experiencia@misionerosurbanosdejesucristo.org"; // Cambia esto por tu dirección de correo

    if (mail($to, $subject, $message, $headers)) {
        $response["suceso"] = "ok";
    } else {
        $response["suceso"] = "error";
        $response["mensaje"] = "Error al enviar el correo.";
    }

    echo json_encode($response);
} else {
    header("HTTP/1.0 405 Method Not Allowed");
}
?>
