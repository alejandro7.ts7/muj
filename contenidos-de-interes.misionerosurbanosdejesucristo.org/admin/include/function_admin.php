<?php
include('connection/conect.php');
include('include/functions.php');

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);


function conto_posts($id_session)
{

    if (isset($id_session) && $id_session==$_SESSION["adminMisionerosUrbanos"][0]) {
        $conteo = 0;

        $con = conection_database();
        $fechaActual = date('Y-m-d H:i:s');

        $idReal = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);

        if ($idReal == 1) {
            $Sql_Query = "SELECT count(id) as total FROM contenidos WHERE status='1' ";
        } else {
            $Sql_Query = "SELECT count(id) as total FROM contenidos WHERE status='1' AND creador='".$idReal."'";
        }


        
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);
    
        if (!empty($row["total"])) {
            $conteo = $row["total"];
        }


        close_database($con);
    }else {
		$conteo= 0;
	}

	return $conteo;
}


function obtener_info_contenido($id_session,$idContenido)
{

    if (isset($id_session) && $id_session == $_SESSION["adminMisionerosUrbanos"][0]) {
        $data = array();

        $con = conection_database();
        $fechaActual = date('Y-m-d H:i:s');
        $Sql_Query = "SELECT * FROM contenidos WHERE id='". $idContenido."' ";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);

        if (!empty($row["id"])) {
            $data = $row;
        }


        close_database($con);
    } else {
        $data = array();
    }

    return $data;
}

function getName($string)
{

    $string = explode('/', $string);
    $count = count($string);
    $string = $string[$count - 1];

    return $string;
}

function obtener_select_contenidos($id_session)
{

    if (isset($id_session) && $id_session == $_SESSION["adminMisionerosUrbanos"][0]) {
        $contenido = '';

        $con = conection_database();
        $fechaActual = date('Y-m-d H:i:s');
        $Sql_Query = "SELECT * FROM `contenidos` WHERE status='1' ORDER BY `posicionamiento` ASC ";
        $check = mysqli_query($con, $Sql_Query);
        $row_cnt = mysqli_num_rows($check);
        $contador = 0;

        while($row = mysqli_fetch_assoc($check)){
            $contador = $contador+1;
            if($contador== $row_cnt){
                $contenido = $contenido . '<option selected value="' . $row["id"] . '">' . $row["titulo"] . ' (' . $row["tipo_publicacion"] . ')</option>';
            }else {
                $contenido = $contenido . '<option value="' . $row["id"] . '">' . $row["titulo"] . ' (' . $row["tipo_publicacion"] . ')</option>';
            }
            
        }


        close_database($con);
    } else {
        $contenido = '';
    }

    return $contenido;
}

function obtener_select_contenidos_2($id_session,$idPOsicionamiento,$idProducto)
{

    if (isset($id_session) && $id_session == $_SESSION["adminMisionerosUrbanos"][0]) {
        $contenido = '';

        $con = conection_database();
        $fechaActual = date('Y-m-d H:i:s');
        $Sql_Query = "SELECT * FROM `contenidos` WHERE status='1' AND id!='". $idProducto."' ORDER BY `posicionamiento` ASC ";
        $check = mysqli_query($con, $Sql_Query);
        $row_cnt = mysqli_num_rows($check);
        $contador = 0;

        while ($row = mysqli_fetch_assoc($check)) {

            //echo $idPOsicionamiento;
            if ($idPOsicionamiento == $row["id"]) {
                $contenido = $contenido . '<option selected value="' . $row["id"] . '">' . $row["titulo"] . ' (' . $row["tipo_publicacion"] . ')</option>';
            } else {
                $contenido = $contenido . '<option value="' . $row["id"] . '">' . $row["titulo"] . ' (' . $row["tipo_publicacion"] . ')</option>';
            }
        }


        close_database($con);
    } else {
        $contenido = '';
    }

    return $contenido;
}


function obtener_info_emetadata_usuario($idContenido, $ruta)
{

    $data = array();

    $con = conection_database();
    $fechaActual = date('Y-m-d H:i:s');
    $Sql_Query = "SELECT * FROM `metadata_adjuntos` WHERE `idContenido` = '" . $idContenido . "' AND `Ruta` = '" . $ruta . "' ";
    $check = mysqli_query($con, $Sql_Query);
    $row = mysqli_fetch_assoc($check);

    if (!empty($row["id"])) {
        $data = $row;
    }


    close_database($con);

    return $data;
}