<?php
session_start();
include('../connection/conect.php');
include('../include/functions.php');

if (isset($_POST["EliminarSubadministrador"]) && $_POST["EliminarSubadministrador"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idAdmin = $_POST["idAdmin"];


        $con = conection_database();
        $Sql_Query = "DELETE FROM  `admin`  WHERE `id` = '" . $idAdmin . "' AND id!='1';";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);
        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["NewPassword"]) && $_POST["NewPassword"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idAdmin = $_POST["idAdmin"];
        $newpassword = $_POST["newpassword"];
        $newpassword2 = $_POST["newpassword2"];


        $con = conection_database();
        // crypt password 
        $newPasword = encriptar_contrasena($newpassword);

        $Sql_Query = "UPDATE  `admin` SET password='".$newPasword. "' ,segundo_factor='". $newpassword2."'  WHERE `id` = '" . $idAdmin . "' ";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);
        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["CreateUser"]) && $_POST["CreateUser"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $usuario_subadministrador = $_POST["usuario_subadministrador"];
        $usuario_subadministrador = trim($usuario_subadministrador);
        $usuario_subadministrador = str_replace(' ','', $usuario_subadministrador);
        $newpassword = $_POST["newpassword"];
        $newpassword2 = $_POST["newpassword2"];


        $con = conection_database();
        // crypt password 
        $newPasword = encriptar_contrasena($newpassword);

        $Sql_Query = "SELECT * FROM admin WHERE username='". $usuario_subadministrador."' ";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);


        if(empty($row["id"])){
            // crear usuario 

            $Sql_Query2 = "INSERT INTO `admin` (`username`, `name`, `lastname`, `email`, `phone`, `password`, `profile_image`, `fecha_registro`, `segundo_factor`, `Pais`, `whatsapp`, `messenger`, `telegram`, `facebook`, `instagram`, `youtube`, `tiktok`, `twitter`, `modenight`) VALUES ('". $usuario_subadministrador."', 'Empresaname', 'empresalast', 'demo@gmail.com', '4216754678', '". $newPasword."', NULL, NULL, '". $newpassword2."', 'Bahamas', '', '', '', '', '', '', NULL, NULL, '0');";
            $check2 = mysqli_query($con, $Sql_Query2);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        }else {
            $result = array('suceso' => 'error', 'mensaje' => 'Usuario Ya Existe', 'Error');
        }

        close_database($con);

      
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};