<?php
session_start();
require_once('../../connection/conect.php');
require_once('../../include/functions.php');
require_once('ssp2.class.php');


$idReadl = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);

//

$SQLVista = "CREATE OR REPLACE VIEW viewcontenidos AS SELECT c.*,a.username FROM `contenidos` AS c LEFT JOIN admin AS a ON a.id = c.creador ";

$conVista = conection_database();
$checkVista = mysqli_query($conVista, $SQLVista);
close_database($conVista);

//echo $idReadl;

function sanear_string($string)
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    /*
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("¨", "º", "-", "~",
             "#", "@", "|", "!",
             "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $string
    );
    */


    return $string;
}


function cleanString($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "a", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "i", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "e", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "o", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "u", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = str_replace("&aacute;", "a", $String);
    $String = str_replace("&Aacute;", "a", $String);
    $String = str_replace("&eacute;", "e", $String);
    $String = str_replace("&Eacute;", "e", $String);
    $String = str_replace("&iacute;", "i", $String);
    $String = str_replace("&Iacute;", "i", $String);
    $String = str_replace("&oacute;", "o", $String);
    $String = str_replace("&Oacute;", "o", $String);
    $String = str_replace("&uacute;", "u", $String);
    $String = str_replace("&Uacute;", "u", $String);
    return $String;
}


$sql_details = array(
    'user' => $usuarioBaseDatosEnvio,
    'pass' => $passwordBaseDatosEnvio,
    'db'   => $tablaeBaseDatosEnvio,
    'host' => $urlBaseDatosEnvio
);
$table = 'viewcontenidos';




$primaryKey = 'id';
$columns = array(
    array(
        'db'        => 'fecha_publicacion',
        'dt'        => 0,
        'formatter' => function ($d, $rowinfo) {
            return date('Y-m-d H:i:s', strtotime($d));
        }
    ),
    array(
        'db'        => 'tipo',
        'dt'        => 1,
        'formatter' => function ($d, $rowinfo) {
            $info ='';
            if($d==1){
                $info= 'Coleccion Documental';
            } else if ($d == 2) {
                $info = 'Coleccion Audiovisual';
            } else if ($d == 3) {
                $info = 'Calendario Eclesiastico';
            } else if ($d == 4) {
                $info = 'Paginas De Interes';
            } else if ($d == 5) {
                $info = 'Calendario Actividades MUJ';
            }
            return $info;
        }
    ),
    array(
        'db'        => 'titulo',
        'dt'        => 2,
        'formatter' => function ($d, $rowinfo) {
            
            if($rowinfo[1]==1 OR $rowinfo[1]==2){
                $info = $d;
            }else  if($rowinfo[1]==3){
                $info = $rowinfo[6];
            } else  if ($rowinfo[1] == 4) {
                $info = $rowinfo[6];
            } else  if ($rowinfo[1] == 5) {
                $info = $rowinfo[6];
            }
            $info = $d;
            return $info;
        }
    ), array(
        'db'        => 'username',
        'dt'        => 3,
        'formatter' => function ($d, $rowinfo) {
           
            return $d;
        }
    ),
    array(
        'db'        => 'id',
        'dt'        => 4,
        'formatter' => function ($d, $rowinfo) {
            $editarUsuario = "<button type='button' onclick=editarContenido(event,'" . $d . "') class='btn btn-primary btn-block mg-b-10' >Editar contenido </button>";
            return $editarUsuario;
        }
    ), array(
        'db'        => 'id',
        'dt'        => 5,
        'formatter' => function ($d, $rowinfo) {
            $EliminarUsuario = "<button type='button' onclick=eliminarContenido('" . $d . "') class='btn btn-primary btn-block mg-b-10' >Eliminar contenido </button>";
            return $EliminarUsuario;
        }
    ), array(
        'db'        => 'contenido',
        'dt'        => 6,
        'formatter' => function ($d, $rowinfo) {

            return $d;
        }
    ),

    
);

if($idReadl>1){
    $where = "id>0 AND status='1' AND creador='".$idReadl."'";
} elseif ($idReadl == 1) {
    $where = "id>0 AND status='1'";
}


$info = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $where);
//var_dump($info);

echo json_encode($info);
