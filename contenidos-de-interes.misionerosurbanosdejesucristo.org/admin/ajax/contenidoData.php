<?php
session_start();
include('../connection/conect.php');
include('../include/functions.php');

if (isset($_POST["CrearContenidoInformacion"]) && $_POST["CrearContenidoInformacion"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';
    $errorMesasge = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $tipo_contenido = $_POST["tipo_contenido"];
        $pagina_interes = $_POST["pagina_interes"];
        $fecha_evento = $_POST["fecha_evento"];
        $hora_evento = $_POST["hora_evento"];
        $nombre_evento = $_POST["nombre_evento"];

        $fecha_evento2 = $_POST["fecha_evento2"];
        $hora_evento2 = $_POST["hora_evento2"];
        $nombre_evento2 = $_POST["nombre_evento2"];
        $tipo_pagina_interes = $_POST["tipo_pagina_interes"];
        $posicion_contenido_usuario = $_POST["posicion_contenido_usuario"];
        $links_video = $_POST["links_video"];
        

        $titulo = '';
        $tipo_publicacion = '';
        $imagen_portada = '';
        $tipo_publicacion2 = '';
        $author = '';
        $resumen = '';
        $url_new = '';


        if(!empty($tipo_pagina_interes)){
            $tipo_pagina_interes = $tipo_pagina_interes;
        }else {
            $tipo_pagina_interes ='';
        }


        $continuar = true; 

        if($tipo_contenido==4){
            if(empty($pagina_interes)){
                $continuar = false;
                $errorMesasge = 'Recuerda Que La Url Es Necesaria';
            }else {

                $urlparts = parse_url($pagina_interes);

                $scheme = $urlparts['scheme'];

                if ($scheme === 'https' OR $scheme === 'http') {
                    //echo ("$url es una URL valida");
                } else {
                    $continuar = false;
                    $errorMesasge = 'Recuerda Que La Url Debe Tener Http O Https';
                }
            }

            if($continuar==true){
                $contenido = $pagina_interes;
                $titulo = $contenido;
            }

            

        }

        

        $fecha2 = '';

        if ($tipo_contenido == 5) {
            $fecha2 = $fecha_evento.' '. $hora_evento;
            $contenido = $nombre_evento;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo1"];
        }

        if ($tipo_contenido == 3) {
            $fecha2 = $fecha_evento2 . ' ' . $hora_evento2;
            $contenido = $nombre_evento2;
            $titulo = $contenido;

            $url_new = $_POST["url_videoinfo2"];
        }

        if($continuar==true){

            $con = conection_database();

            $contenido = mysqli_real_escape_string($con, $contenido);
            $titulo = mysqli_real_escape_string($con, $titulo);
           
            $fecha = date('Y-m-d H:i:s');

            if(empty($fecha2)){
                $fecha2 = date('Y-m-d H:i:s');
            }


            if ($tipo_contenido == 1 OR $tipo_contenido == 2) {
                $titulo = $_POST["titulo_coleccion"];
                $titulo = mysqli_real_escape_string($con, $titulo);

                $author = $_POST["autores_coleccion"];
                $author = mysqli_real_escape_string($con, $author);

                $resumen = $_POST["desc_collecion_resumen"];
                $resumen = mysqli_real_escape_string($con, $resumen);
                


                if($tipo_contenido==2){
                    $tipo_publicacion = '';

                    $tipo_publicacion2 = $_POST["tipo_coleccion"];
                    $tipo_publicacion2 = mysqli_real_escape_string($con, $tipo_publicacion2);
                } else if ($tipo_contenido == 1) {
                    $tipo_publicacion2 = '';

                    $tipo_publicacion = $_POST["tipo_coleccion"];
                    $tipo_publicacion = mysqli_real_escape_string($con, $tipo_publicacion);
                }
               


                $fecha2 = $_POST["fecha_coleccion"];
                $contenido = $_POST["desc_collecion"];
                $contenido = nl2br($contenido);
                $contenido = mysqli_real_escape_string($con, $contenido);


                if($contenido== '<p><br></p>'){
                    $contenido='';
                }

                // upload imagen de portada 
               
            }



            $creador = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);
            

            $Sql_Query = "INSERT INTO `contenidos` (`fecha_publicacion`, `fecha_contenido`, `tipo`, `titulo`, `contenido`, `video1`, `video2`, `video3`, `adjuntos`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `meta_author`, `status`,`tipo_publicacion`,`imagen_portada`,`tipo_pagina_interes`,`tipo_publicacion2`,`creador`,`author`,`resumen`,`url_new`,`posicionamiento`,`links_video`) VALUES ( '". $fecha."', '". $fecha2."', '". $tipo_contenido."', '".$titulo."', '". $contenido."', '', '', '', '', '', '', '', '', '', '1','". $tipo_publicacion."','". $imagen_portada."','". $tipo_pagina_interes."','". $tipo_publicacion2."','". $creador."','". $author."','". $resumen."','". $url_new."','". $posicion_contenido_usuario."','". $links_video."');";
            $check = mysqli_query($con, $Sql_Query);
            //$row = mysqli_fetch_assoc($check);
            $idReal = mysqli_insert_id($con);


            if($tipo_contenido==1 OR $tipo_contenido == 2 OR $tipo_contenido == 4){
                if (!empty($_FILES["imagen_portada"]["tmp_name"])) {



                    $extension10 = $_FILES["imagen_portada"]["name"];
                    $extension10 = explode(".", $extension10);
                    $poxixion_extenxion10 = count($extension10);
                    $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                    $extension10 = $extension10[$poxixion_extenxion10];
                    $extension = strtolower($extension10);



                    $nombre = $idReal;
                    $filename = $nombre . "." . $extension;
                    $directorio = '../../contenido/' . $nombre;
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $directorio = $directorio.'/img_p';
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }


                    $filename = $filename . rand(1, 1000000000);

                    $source = $_FILES["imagen_portada"]["tmp_name"];
                    $target_path = $directorio . '/' . $filename;
                    $file = $_FILES['imagen_portada']['tmp_name'];
                    $source_properties = getimagesize($file);
                    $image_type = $source_properties[2];
                    $continuarUpcorrect = false;
                    if ($image_type == IMAGETYPE_JPEG) {

                        $xten = "jpg";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_GIF) {

                        $xten = "gif";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_PNG) {

                        $xten = "png";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } else {
                        $result = array('suceso' => 'Formato De Archivo No Permitido', 'ruta' => '');
                        $upCorrect = false;
                        $continuarUpcorrect = false;
                    }

                    if ($continuarUpcorrect == true) {

                        if ($upCorrect) {
                            //insertar nombre en db 
                            //require_once('rutaMain.php');

                            $directorio_final = str_replace('../', '', $directorio_final);
                            //$directorio_final = $rutaPrincipalArchivos . $directorio_final;
                            $data = $directorio_final;
                            $consultaDataIMAGEN = "UPDATE `contenidos` SET `imagen_portada` = '" . $directorio_final . "' WHERE id = '" . $idReal . "';";
                            $check2 = mysqli_query($con, $consultaDataIMAGEN);
                        } 
                    }
                }
            }

            if ($tipo_contenido == 1 OR $tipo_contenido == 2) {


                $nombre = $idReal;
                $filename = $nombre . "." . $extension;
                $directorio = '../../contenido/' . $nombre;
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

                $directorio = $directorio . '/files';
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }



                $contador =  0;
                $adjuntos = '';
                foreach ($_FILES["files"] as $file) {
                    $contador = $contador+1;
                    $file = $_FILES['files']["tmp_name"][$contador - 1];


                    if(!empty($file) && isset($file)){

                        $directorio2 = $directorio . '/'. uniqid() . uniqid();
                        if (!file_exists($directorio2)) {
                            mkdir($directorio2, 0777);
                        }

                     
                        $nombre_final = $_FILES['files']["name"][$contador - 1];


                        $extension101 = $_FILES["imagen_portada"]["name"];
                        $extension101 = explode(".", $extension101);
                        $poxixion_extenxion101 = count($extension101);
                        $poxixion_extenxion101 = $poxixion_extenxion101 - 1;
                        $extension101 = $extension101[$poxixion_extenxion101];
                        $extension1 = strtolower($extension101);


                        if($extension1 != 'php' && $extension1 != 'py' && $extension1 != 'pyw' && $extension1 != '.htaccess'
                        && $extension1 != 'htaccess' && $extension1 != 'html' ){
                            $nombre_final = str_replace(',', '', $nombre_final);
                            $directorio_final = $directorio2 . '/' . $nombre_final;
                            $upCorrect = move_uploaded_file($file, $directorio_final);


                            // adjuntar cada ruta para agregar a ruta de agregdos
                            if (empty($adjuntos)) {
                                $adjuntos = $directorio_final;
                            } else {
                                $adjuntos = $adjuntos . ',' . $directorio_final;
                            }
                        }


                        

                    }

                    $adjuntos = str_replace('../', '', $adjuntos);

                    if(!empty($adjuntos)){
                        $consultaDataIMAGEN2 = "UPDATE `contenidos` SET `adjuntos` = '" . $adjuntos . "' WHERE id = '" . $idReal . "';";
                        $check22 = mysqli_query($con, $consultaDataIMAGEN2);
                    }

                    
                    //var_dump($adjuntos);
                    //echo "<br>";
                    
                }

                //archivos de contenido 
            }


            close_database($con);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        }else {
            $result = array('suceso' => 'error', 'mensaje' => $errorMesasge);
        }

    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EditarContenidoInformacion"]) && $_POST["EditarContenidoInformacion"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';
    $errorMesasge = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idInfo = $_POST["idInfo"];
        $tipo_contenido = $_POST["tipo_contenido"];
        $pagina_interes = $_POST["pagina_interes"];
        $fecha_evento = $_POST["fecha_evento"];
        $hora_evento = $_POST["hora_evento"];
        $nombre_evento = $_POST["nombre_evento"];

        $fecha_evento2 = $_POST["fecha_evento2"];
        $hora_evento2 = $_POST["hora_evento2"];
        $nombre_evento2 = $_POST["nombre_evento2"];
        $tipo_pagina_interes = $_POST["tipo_pagina_interes"];
        $posicion_contenido_usuario = $_POST["posicion_contenido_usuario"];
        $links_video = $_POST["links_video"];


        $titulo = '';
        $tipo_publicacion = '';
        $imagen_portada = '';
        $tipo_publicacion2 = '';

        $author = '';
        $resumen = '';
        $url_new = '';


        if (!empty($tipo_pagina_interes)) {
            $tipo_pagina_interes = $tipo_pagina_interes;
        } else {
            $tipo_pagina_interes = '';
        }


        $continuar = true;

        if ($tipo_contenido == 4) {
            if (empty($pagina_interes)) {
                $continuar = false;
                $errorMesasge = 'Recuerda Que La Url Es Necesaria';
            } else {

                $urlparts = parse_url($pagina_interes);

                $scheme = $urlparts['scheme'];

                if ($scheme === 'https' or $scheme === 'http') {
                    //echo ("$url es una URL valida");
                } else {
                    $continuar = false;
                    $errorMesasge = 'Recuerda Que La Url Debe Tener Http O Https';
                }
            }

            if ($continuar == true) {
                $contenido = $pagina_interes;
                $titulo = $contenido;
            }
        }



        $fecha2 = '';

        if ($tipo_contenido == 5) {
            $fecha2 = $fecha_evento . ' ' . $hora_evento;
            $contenido = $nombre_evento;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo1"];
        }

        if ($tipo_contenido == 3) {
            $fecha2 = $fecha_evento2 . ' ' . $hora_evento2;
            $contenido = $nombre_evento2;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo2"];
        }

        if ($continuar == true) {

            $con = conection_database();

            $contenido = mysqli_real_escape_string($con, $contenido);
            $titulo = mysqli_real_escape_string($con, $titulo);
           
            $fecha = date('Y-m-d H:i:s');

            if (empty($fecha2)) {
                $fecha2 = date('Y-m-d H:i:s');
            }


            if ($tipo_contenido == 1 or $tipo_contenido == 2) {
                $titulo = $_POST["titulo_coleccion"];
                $titulo = mysqli_real_escape_string($con, $titulo);


                $author = $_POST["autores_coleccion"];
                $author = mysqli_real_escape_string($con, $author);

                $resumen = $_POST["desc_collecion_resumen"];
                $resumen = mysqli_real_escape_string($con, $resumen);



                if ($tipo_contenido == 2) {
                    $tipo_publicacion = '';

                    $tipo_publicacion2 = $_POST["tipo_coleccion"];
                    $tipo_publicacion2 = mysqli_real_escape_string($con, $tipo_publicacion2);
                } else if ($tipo_contenido == 1) {
                    $tipo_publicacion2 = '';

                    $tipo_publicacion = $_POST["tipo_coleccion"];
                    $tipo_publicacion = mysqli_real_escape_string($con, $tipo_publicacion);
                }


                $fecha2 = $_POST["fecha_coleccion"];
                $contenido = $_POST["desc_collecion"];
                $contenido = nl2br($contenido);
                $contenido = mysqli_real_escape_string($con, $contenido);

                if ($contenido == '<p><br></p>') {
                    $contenido = '';
                }

                // upload imagen de portada 

            }


            $Sql_Query2Adjuntos = "SELECT * FROM contenidos  WHERE id='" . $idInfo . "'";
            $checkAdjunots = mysqli_query($con, $Sql_Query2Adjuntos);
            $rowAdjuntos = mysqli_fetch_assoc($checkAdjunots);

            $Sql_Query2 = "UPDATE contenidos SET fecha_contenido='".$fecha2. "',tipo='".$tipo_contenido. "',titulo='". $titulo. "' , contenido='". $contenido. "',tipo_publicacion='". $tipo_publicacion. "',imagen_portada='". $imagen_portada. "' , tipo_pagina_interes='". $tipo_pagina_interes. "' , tipo_publicacion2='". $tipo_publicacion2. "',author='". $author. "',resumen='". $resumen. "', url_new='". $url_new. "' , posicionamiento='". $posicion_contenido_usuario. "', links_video='". $links_video."' WHERE id='". $idInfo."'";

            /*
            $Sql_Query = "INSERT INTO `contenidos` (`fecha_publicacion`, `fecha_contenido`, `tipo`, `titulo`, `contenido`, `video1`, `video2`, `video3`, `adjuntos`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `meta_author`, `status`,`tipo_publicacion`,`imagen_portada`,`tipo_pagina_interes`,`tipo_publicacion2`) VALUES ( '" . $fecha . "', '" . $fecha2 . "', '" . $tipo_contenido . "', '" . $titulo . "', '" . $contenido . "', '', '', '', '', '', '', '', '', '', '1','" . $tipo_publicacion . "','" . $imagen_portada . "','" . $tipo_pagina_interes . "','" . $tipo_publicacion2 . "');";
            */
            $check = mysqli_query($con, $Sql_Query2);
            //$row = mysqli_fetch_assoc($check);
            $idReal = $idInfo;


            if ($tipo_contenido == 1 or $tipo_contenido == 2  or $tipo_contenido == 4) {
                if (!empty($_FILES["imagen_portada"]["tmp_name"])) {



                    $extension10 = $_FILES["imagen_portada"]["name"];
                    $extension10 = explode(".", $extension10);
                    $poxixion_extenxion10 = count($extension10);
                    $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                    $extension10 = $extension10[$poxixion_extenxion10];
                    $extension = strtolower($extension10);



                    $nombre = $idReal;
                    $filename = $nombre . "." . $extension;
                    $directorio = '../../contenido/' . $nombre;
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $directorio = $directorio . '/img_p';
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $files = glob($directorio . '/*'); //obtenemos todos los nombres de los ficheros
                    foreach ($files as $file) {
                        //echo $file;
                        if (is_file($file)) {
                            unlink($file); //elimino el fichero
                        }
                    }

                    $filename = $filename . rand(1, 1000000000).uniqid();

                    $source = $_FILES["imagen_portada"]["tmp_name"];
                    $target_path = $directorio . '/' . $filename;
                    $file = $_FILES['imagen_portada']['tmp_name'];
                    $source_properties = getimagesize($file);
                    $image_type = $source_properties[2];
                    $continuarUpcorrect = false;
                    if ($image_type == IMAGETYPE_JPEG) {

                        $xten = "jpg";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_GIF) {

                        $xten = "gif";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_PNG) {

                        $xten = "png";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } else {
                        $result = array('suceso' => 'Formato De Archivo No Permitido', 'ruta' => '');
                        $upCorrect = false;
                        $continuarUpcorrect = false;
                    }

                    if ($continuarUpcorrect == true) {

                        if ($upCorrect) {
                            //insertar nombre en db 
                            //require_once('rutaMain.php');

                            $directorio_final = str_replace('../', '', $directorio_final);
                            //$directorio_final = $rutaPrincipalArchivos . $directorio_final;
                            $data = $directorio_final;
                            $consultaDataIMAGEN = "UPDATE `contenidos` SET `imagen_portada` = '" . $directorio_final . "' WHERE id = '" . $idReal . "';";
                            $check2 = mysqli_query($con, $consultaDataIMAGEN);
                        }
                    }
                }
            }

            if ($tipo_contenido == 1 or $tipo_contenido == 2) {


                $nombre = $idReal;
                $filename = $nombre . "." . $extension;
                $directorio = '../../contenido/' . $nombre;
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

                $directorio = $directorio . '/files';
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

               


                $contador =  0;
                $adjuntos = $rowAdjuntos["adjuntos"];
                foreach ($_FILES["files"] as $file) {
                    $contador = $contador + 1;
                    $file = $_FILES['files']["tmp_name"][$contador - 1];


                    if (!empty($file) && isset($file)) {

                        $directorio2 = $directorio . '/' .  uniqid() . uniqid();
                        if (!file_exists($directorio2)) {
                            mkdir($directorio2, 0777);
                        }


                        $nombre_final = $_FILES['files']["name"][$contador - 1];


                        $extension101 = $_FILES["imagen_portada"]["name"];
                        $extension101 = explode(".", $extension101);
                        $poxixion_extenxion101 = count($extension101);
                        $poxixion_extenxion101 = $poxixion_extenxion101 - 1;
                        $extension101 = $extension101[$poxixion_extenxion101];
                        $extension1 = strtolower($extension101);


                        if (
                            $extension1 != 'php' && $extension1 != 'py' && $extension1 != 'pyw' && $extension1 != '.htaccess'
                            && $extension1 != 'htaccess' && $extension1 != 'html'
                        ) {
                            $nombre_final = str_replace(',', '', $nombre_final);
                            $directorio_final = $directorio2 . '/' . $nombre_final;
                            $upCorrect = move_uploaded_file($file, $directorio_final);


                            // adjuntar cada ruta para agregar a ruta de agregdos
                            if (empty($adjuntos)) {
                                $adjuntos = $directorio_final;
                            } else {
                                $adjuntos = $adjuntos . ',' . $directorio_final;
                            }
                        }
                    }

                    $adjuntos = str_replace('../', '', $adjuntos);

                    if (!empty($adjuntos)) {
                        $consultaDataIMAGEN2 = "UPDATE `contenidos` SET `adjuntos` = '" . $adjuntos . "' WHERE id = '" . $idReal . "';";
                        $check22 = mysqli_query($con, $consultaDataIMAGEN2);
                    }


                    //var_dump($adjuntos);
                    //echo "<br>";

                }

                //archivos de contenido 
            }


            close_database($con);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        } else {
            $result = array('suceso' => 'error', 'mensaje' => $errorMesasge);
        }
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EliminarContenido"]) && $_POST["EliminarContenido"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idContenido = $_POST["idContenido"];


        $con = conection_database();
        $Sql_Query = "UPDATE `contenidos` SET `status` = '0' WHERE `id` = '" . $idContenido . "';";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);
        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["DeleteContenidoIndivi"]) && $_POST["DeleteContenidoIndivi"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idContenido = $_POST["idContenido"];
       

        $con = conection_database();
        $Sql_Query = "SELECT * FROM `contenidos` WHERE `id` = '" . $idContenido . "';";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);

        $nuevoAdjunto = $row["adjuntos"];

        // delete file 
        $Ruta = $_POST["Ruta"];
        $Ruta = '../../'. $Ruta;
        $delete= unlink($Ruta);
        //var_dump($delete);


        // ahora delete de la base de datos
        $busqueda =','.$_POST["Ruta"]; 
        $nuevoAdjunto = str_replace($busqueda,'', $nuevoAdjunto);
        $nuevoAdjunto = str_replace($_POST["Ruta"],'', $nuevoAdjunto);


        $Sql_Query2 = "UPDATE `contenidos` SET adjuntos='". $nuevoAdjunto."' WHERE `id` = '" . $idContenido . "';";
        $check2 = mysqli_query($con, $Sql_Query2);

        //var_dump();

        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["CrearRutaMiniatura"]) && $_POST["CrearRutaMiniatura"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $nombre_adjunto_info = $_POST["nombre_adjunto_info"];
        // $idContenido = $_POST["miniatura_imagen_adjunto"];
        $idContenido = $_POST["idContenido"];
        $Ruta = $_POST["Ruta"];





        $con = conection_database();

        $Sql_Query = "SELECT * FROM metadata_adjuntos WHERE idContenido='" . $idContenido . "' AND Ruta='" . $Ruta . "'";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);


        if (empty($row["id"])) {
            $Sql_Query = "INSERT INTO `metadata_adjuntos` (`idContenido`, `Ruta`, `miniatura_imagen_adjunto`, `nombre_adjunto_info`) VALUES ('" . $idContenido . "', '" . $Ruta . "', '', '" . $nombre_adjunto_info . "');";
            $check = mysqli_query($con, $Sql_Query);
            $lastid = mysqli_insert_id($con);
        } else {

            $Sql_Query = "UPDATE metadata_adjuntos SET nombre_adjunto_info='" . $nombre_adjunto_info . "' WHERE id='" . $row["id"] . "'";
            $check = mysqli_query($con, $Sql_Query);
            $lastid = $row["id"];
        }





        if ($_FILES['miniatura_imagen_adjunto']['name']) {
            if (!$_FILES['miniatura_imagen_adjunto']['error']) {


                //verificar el tama;o y el tipo de archivo


                $nameFinal = uniqid();
                $nameFinal = $nameFinal . md5(rand(100, 200));
                $idUsuario = $idReal;

                $extension10 = $_FILES["miniatura_imagen_adjunto"]["name"];
                $extension10 = explode(".", $extension10);
                $poxixion_extenxion10 = count($extension10);
                $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                $extension10 = $extension10[$poxixion_extenxion10];
                $extension = strtolower($extension10);

                $ext = $extension;
                $file = $_FILES['miniatura_imagen_adjunto']['tmp_name'];
                $sizeTam = filesize($file);
                //echo $sizeTam;
                if (
                    $ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'gif'
                ) {
                    $filename = $nameFinal . '.' . $ext;
                    $idRanduuid =  uniqid() . uniqid();
                    $idRanduuid = trim($idRanduuid);
                    $carpeta = '../../assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid;

                    if (!file_exists($carpeta)) {
                        mkdir($carpeta, 0777);
                    }
                    $destination =  $carpeta . '/' . $filename; //change this directory
                    $location = $_FILES["miniatura_imagen_adjunto"]["tmp_name"];
                    move_uploaded_file($location, $destination);
                    $suceso = 'ok';
                    // https://biblioteca.misionerosurbanosdejesucristo.org/
                    $message = '../assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid . '/' . $filename; //change this URL
                    $message = 'assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid . '/' . $filename; //change this URL
                    //$message = '../assets/imagenesSubidasAdmin/' . $idRanduuid . '/' . $filename; //change this URL


                    $Sql_Query2 = "UPDATE metadata_adjuntos SET miniatura_imagen_adjunto='" . $message . "' WHERE id='" . $lastid . "'";
                    $check2 = mysqli_query($con, $Sql_Query2);
                } else {
                    $suceso = 'Error';
                    $message = 'Recuerda que solo pueden ser png o jpg';
                }
            } else {
                $suceso = 'Error';
                $message = 'Ooops!  El Archivo Tuvo El Siguiente Error:  ' . $_FILES['file']['error'];
            }
        }


        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};