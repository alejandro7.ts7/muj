<?php
include('../connection/conect.php');
include('../include/functions.php');

$keyword = strval($_POST['query']);
$search_param = "{$keyword}%";
$conn = conection_database_myqli();

$sql = $conn->prepare("SELECT DISTINCT(tipo_pagina_interes) FROM contenidos WHERE tipo_pagina_interes LIKE ? AND tipo_pagina_interes IS NOT NULL");
$sql->bind_param("s", $search_param);
$sql->execute();
$result = $sql->get_result();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $countryResult[] = $row["tipo_pagina_interes"];
    }
    echo json_encode($countryResult);
}
$conn->close();
