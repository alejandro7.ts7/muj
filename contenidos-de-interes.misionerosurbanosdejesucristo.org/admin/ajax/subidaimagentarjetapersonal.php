<?php
session_start();
include('../connection/conect.php');
include('../include/functions.php');


if (isset($_POST["SubirImagenTarjeta"]) && $_POST["SubirImagenTarjeta"] == "SMD69") {

    $userid = $_POST["userid"];


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {
        if (isset($_SESSION["adminMisionerosUrbanos"][0])) {
            $idReal = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);
            if ($_FILES['file']['name']) {
                if (!$_FILES['file']['error']) {


                    //verificar el tama;o y el tipo de archivo


                    $nameFinal = uniqid();
                    $nameFinal = $nameFinal . md5(rand(100, 200));
                    $idUsuario = $idReal;

                    $extension10 = $_FILES["file"]["name"];
                    $extension10 = explode(".", $extension10);
                    $poxixion_extenxion10 = count($extension10);
                    $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                    $extension10 = $extension10[$poxixion_extenxion10];
                    $extension = strtolower($extension10);

                    $ext = $extension;
                    $file = $_FILES['file']['tmp_name'];
                    $sizeTam = filesize($file);
                    //echo $sizeTam;
                    if ($sizeTam <= (1024 * 1024)) {
                        if ($ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'gif') {
                            $filename = $nameFinal . '.' . $ext;
                            $idRanduuid =  uniqid();
                            $idRanduuid = trim($idRanduuid);
                            $carpeta = '../../assets/imagenesSubidasAdmin/' . $idRanduuid;

                            if (!file_exists($carpeta)) {
                                mkdir($carpeta, 0777);
                            }
                            $destination =  $carpeta . '/' . $filename; //change this directory
                            $location = $_FILES["file"]["tmp_name"];
                            move_uploaded_file($location, $destination);
                            $suceso = 'ok';
                            $message = 'https://contenidos-de-interes.misionerosurbanosdejesucristo.org/assets/imagenesSubidasAdmin/' . $idRanduuid . '/' . $filename; //change this URL
                            //$message = '../assets/imagenesSubidasAdmin/' . $idRanduuid . '/' . $filename; //change this URL
                            
                        } else {
                            $suceso = 'Error';
                            $message = 'Recuerda que solo pueden ser png o jpg';
                        }
                    } else {
                        $suceso = 'Error';
                        $message = 'Recuerda que el peso maximo es de 1 mb';
                    }
                } else {
                    $suceso = 'Error';
                    $message = 'Ooops!  El Archivo Tuvo El Siguiente Error:  ' . $_FILES['file']['error'];
                }
            }

            $datos = array('suceso' => $suceso, "mensaje" => $message);
            $obj = json_encode($datos);
            echo $obj;
        }
    }
}
