<?php
session_start();
include('../connection/conect.php');
include('../include/functions.php');


function crearCotenidoHtmlEmailEnvio($idContenido){
    $con = conection_database();
    $Sql_Query1 = "SELECT id FROM `emails` WHERE id_formulario='". $idContenido."'";
    $check1 = mysqli_query($con, $Sql_Query1);
    $row1 = mysqli_fetch_assoc($check1);

    if(empty($row1["id"])){
        $fecha = date('Y-m-d H:i:s');

        $contenidoEmail ='Gracias Por Registrarte';

        $hosting = 'https://experiencia.misionerosurbanosdejesucristo.org/images/logo.png';
        $contenidoEmail = '<p>
            
            
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width,initial-scale=1">
                <meta name="x-apple-disable-message-reformatting">
                <title></title>
                <!--[if mso]>
                <noscript>
                    <xml>
                        <o:OfficeDocumentSettings>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                        </o:OfficeDocumentSettings>
                    </xml>
                </noscript>
                <![endif]-->
                <style>
                    table, td, div, h1, p {font-family: Arial, sans-serif;}
                </style>
            
            
                <span></span></p><table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                    <tbody><tr>
                        <td align="center" style="padding:0;">
                            <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                                <tbody><tr>
                                    <td align="center" style="padding:40px 0 30px 0;background:#70bbd9;">
                                        <img src="'. $hosting. '" alt="" width="300" style="height:auto;display:block;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:36px 30px 42px 30px;">
                                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                                            <tbody><tr>
                                                <td style="padding:0 0 36px 0;color:#153643;">
                                                    <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Bienvenido //%NOMBRE//% //%APELLIDO//%</h1><h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;"><span style="letter-spacing: -0.04em;">Te Has Registrado En La Experiencia //%NAMEEXPERIENCIA//%</span><br></h1><h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;"><br></h1><h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;"><br>Fecha: //%FECHAEXPERIENCIA//%<br>Hora: //%HORAEXPERIENCIA//%</h1><p style="font-size: 24px; margin: 0px 0px 20px; font-family: Arial, sans-serif;"><br></p><p style="font-size: 24px; margin: 0px 0px 20px; font-family: Arial, sans-serif;">Datos de registro&nbsp;</p><p style="font-size: 24px; margin: 0px 0px 20px; font-family: Arial, sans-serif;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">NOMBRE://%NOMBRE//%</label><br style="color: rgb(82, 100, 132); font-size: 14px;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">APELLIDO://%APELLIDO//%</label><br style="color: rgb(82, 100, 132); font-size: 14px;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">CORREO ELECTRONICO://%EMAIL//%</label><br style="color: rgb(82, 100, 132); font-size: 14px;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">WHATSAPP://%WHATSAPP//%</label><br style="color: rgb(82, 100, 132); font-size: 14px;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">PA&Iacute;S://%PAIS//%</label><br style="color: rgb(82, 100, 132); font-size: 14px;"><label class="form-control-label text-uppercase" style="font-size: 14px; color: rgb(0, 0, 0);">CIUDAD://%CIUDAD//%</label><br></p><p style="font-size: 24px; margin: 0px 0px 20px; font-family: Arial, sans-serif;"><br></p></td></tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:30px;background:#6EC1E4;">
                                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                                            <tbody><tr>
                                                <td style="padding:0;width:50%;" align="left">
                                                    <p style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#000;">
                                                        <a style="color:#000;" target="_blank" href="https://misionerosurbanosdejesucristo.org/">Misioneros Urbanos de Jesucristo 2023</a>
                                                    </p>
                                                </td>
                                                
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            
            <p></p>';

        $Sql_Query2 = "INSERT INTO `emails` (`id_formulario`, `fecha`, `contenido`) VALUES ('". $idContenido."', '". $fecha."', '". $contenidoEmail."');";
        $check2 = mysqli_query($con, $Sql_Query2);
    }


    
   

    close_database($con);
}

if (isset($_POST["CrearContenidoInformacion"]) && $_POST["CrearContenidoInformacion"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';
    $errorMesasge = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $tipo_contenido = $_POST["tipo_contenido"];
        $pagina_interes = $_POST["pagina_interes"];
        $fecha_evento = $_POST["fecha_evento"];
        $hora_evento = $_POST["hora_evento"];
        $nombre_evento = $_POST["nombre_evento"];

        $fecha_evento2 = $_POST["fecha_evento2"];
        $hora_evento2 = $_POST["hora_evento2"];
        $nombre_evento2 = $_POST["nombre_evento2"];
        $tipo_pagina_interes = $_POST["tipo_pagina_interes"];
        $posicion_contenido_usuario = $_POST["posicion_contenido_usuario"];
        $activaFromulario = 0;
        $activaFromularioPreguntas = 0;

        if(isset($_POST["activaFromulario"])){
            if($_POST["activaFromulario"]==1){
                $activaFromulario =1;
            }

        }

        if (isset($_POST["activaFromularioPreguntas"])) {
            if ($_POST["activaFromularioPreguntas"] == 1) {
                $activaFromularioPreguntas = 1;
            }
        }



        $desc_form = $_POST["desc_form"];
        $links_video = $_POST["links_video"];
        $c1 = $_POST["c1"];
        $c1_activo = $_POST["c1_activo"];
        $c2 = $_POST["c2"];
        $c2_activo = $_POST["c2_activo"];
        $c3 = $_POST["c3"];
        $c3_activo = $_POST["c3_activo"];
        $c4 = $_POST["c4"];
        $c4_activo = $_POST["c4_activo"];
        $c5 = $_POST["c5"];
        $c5_activo = $_POST["c5_activo"];
        $c6 = $_POST["c6"];
        $c6_activo = $_POST["c6_activo"];
        $c7 = $_POST["c7"];
        $c7_activo = $_POST["c7_activo"];
        $c8 = $_POST["c8"];
        $c8_activo = $_POST["c8_activo"];


        $titulo = '';
        $tipo_publicacion = '';
        $imagen_portada = '';
        $tipo_publicacion2 = '';
        $author = '';
        $resumen = '';
        $url_new = '';


        if(!empty($tipo_pagina_interes)){
            $tipo_pagina_interes = $tipo_pagina_interes;
        }else {
            $tipo_pagina_interes ='';
        }


        $continuar = true; 

        if($tipo_contenido==4){
            if(empty($pagina_interes)){
                $continuar = false;
                $errorMesasge = 'Recuerda Que La Url Es Necesaria';
            }else {

                $urlparts = parse_url($pagina_interes);

                $scheme = $urlparts['scheme'];

                if ($scheme === 'https' OR $scheme === 'http') {
                    //echo ("$url es una URL valida");
                } else {
                    $continuar = false;
                    $errorMesasge = 'Recuerda Que La Url Debe Tener Http O Https';
                }
            }

            if($continuar==true){
                $contenido = $pagina_interes;
                $titulo = $contenido;
            }

            

        }

        

        $fecha2 = '';

        if ($tipo_contenido == 5) {
            $fecha2 = $fecha_evento.' '. $hora_evento;
            $contenido = $nombre_evento;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo1"];
        }

        if ($tipo_contenido == 3) {
            $fecha2 = $fecha_evento2 . ' ' . $hora_evento2;
            $contenido = $nombre_evento2;
            $titulo = $contenido;

            $url_new = $_POST["url_videoinfo2"];
        }

        if($continuar==true){

            $con = conection_database();

            $contenido = mysqli_real_escape_string($con, $contenido);
            $titulo = mysqli_real_escape_string($con, $titulo);
           
            $fecha = date('Y-m-d H:i:s');

            if(empty($fecha2)){
                $fecha2 = date('Y-m-d H:i:s');
            }


            if ($tipo_contenido == 1 OR $tipo_contenido == 2) {
                $titulo = $_POST["titulo_coleccion"];
                $titulo = mysqli_real_escape_string($con, $titulo);

                $author = $_POST["autores_coleccion"];
                $author = mysqli_real_escape_string($con, $author);

                $resumen = $_POST["desc_collecion_resumen"];
                $resumen = mysqli_real_escape_string($con, $resumen);


                if($tipo_contenido==2){
                    $tipo_publicacion = '';

                    $tipo_publicacion2 = $_POST["tipo_coleccion"];
                    $tipo_publicacion2 = mysqli_real_escape_string($con, $tipo_publicacion2);
                } else if ($tipo_contenido == 1) {
                    $tipo_publicacion2 = '';

                    $tipo_publicacion = $_POST["tipo_coleccion"];
                    $tipo_publicacion = mysqli_real_escape_string($con, $tipo_publicacion);
                }
               


                $fecha2 = $_POST["fecha_coleccion"];
                $contenido = $_POST["desc_collecion"];
                $contenido = nl2br($contenido);
                $contenido = mysqli_real_escape_string($con, $contenido);


                if($contenido== '<p><br></p>'){
                    $contenido='';
                }

                // upload imagen de portada 
               
            }



            $creador = desencriptar_datos_id($_SESSION["adminMisionerosUrbanos"][0]);
            

            $Sql_Query = "INSERT INTO `contenidos` (`fecha_publicacion`, `fecha_contenido`, `tipo`, `titulo`, `contenido`, `video1`, `video2`, `video3`, `adjuntos`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `meta_author`, `status`,`tipo_publicacion`,`imagen_portada`,`tipo_pagina_interes`,`tipo_publicacion2`,`creador`,`author`,`resumen`,`url_new`,`pedir_formulario`,`posicionamiento`,`desc_form`,`links_video`,`c1`,`c1_activo`,`c2`,`c2_activo`,`c3`,`c3_activo`,`c4`,`c4_activo`,`c5`,`c5_activo`,`c6`,`c6_activo`,`c7`,`c7_activo`,`c8`,`c8_activo`,`activaFromularioPreguntas`) VALUES ( '". $fecha."', '". $fecha2."', '". $tipo_contenido."', '".$titulo."', '". $contenido."', '', '', '', '', '', '', '', '', '', '1','". $tipo_publicacion."','". $imagen_portada."','". $tipo_pagina_interes."','". $tipo_publicacion2."','". $creador."','". $author."','". $resumen."','". $url_new."','". $activaFromulario."','". $posicion_contenido_usuario."','". $desc_form."','". $links_video."','". $c1."','". $c1_activo. "','" . $c2 . "','" . $c2_activo . "','" . $c3 . "','" . $c3_activo . "','" . $c4 . "','" . $c4_activo . "','" . $c5 . "','" . $c5_activo . "','" . $c6 . "','" . $c6_activo . "','" . $c7 . "','" . $c7_activo . "','" . $c8 . "','" . $c8_activo . "','". $activaFromularioPreguntas."');";
            $check = mysqli_query($con, $Sql_Query);
            //$row = mysqli_fetch_assoc($check);
            $idReal = mysqli_insert_id($con);

            if ($activaFromulario == 1) {
                crearCotenidoHtmlEmailEnvio($idReal);
            }

            if($tipo_contenido==1 OR $tipo_contenido == 2 OR $tipo_contenido == 4){
                if (!empty($_FILES["imagen_portada"]["tmp_name"])) {



                    $extension10 = $_FILES["imagen_portada"]["name"];
                    $extension10 = explode(".", $extension10);
                    $poxixion_extenxion10 = count($extension10);
                    $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                    $extension10 = $extension10[$poxixion_extenxion10];
                    $extension = strtolower($extension10);



                    $nombre = $idReal;
                    $filename = $nombre . "." . $extension;
                    $directorio = '../../contenido/' . $nombre;
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $directorio = $directorio.'/img_p';
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }


                    $filename = $filename . rand(1, 1000000000);

                    $source = $_FILES["imagen_portada"]["tmp_name"];
                    $target_path = $directorio . '/' . $filename;
                    $file = $_FILES['imagen_portada']['tmp_name'];
                    $source_properties = getimagesize($file);
                    $image_type = $source_properties[2];
                    $continuarUpcorrect = false;
                    if ($image_type == IMAGETYPE_JPEG) {

                        $xten = "jpg";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_GIF) {

                        $xten = "gif";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_PNG) {

                        $xten = "png";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } else {
                        $result = array('suceso' => 'Formato De Archivo No Permitido', 'ruta' => '');
                        $upCorrect = false;
                        $continuarUpcorrect = false;
                    }

                    if ($continuarUpcorrect == true) {

                        if ($upCorrect) {
                            //insertar nombre en db 
                            //require_once('rutaMain.php');

                            $directorio_final = str_replace('../', '', $directorio_final);
                            //$directorio_final = $rutaPrincipalArchivos . $directorio_final;
                            $data = $directorio_final;
                            $consultaDataIMAGEN = "UPDATE `contenidos` SET `imagen_portada` = '" . $directorio_final . "' WHERE id = '" . $idReal . "';";
                            $check2 = mysqli_query($con, $consultaDataIMAGEN);
                        } 
                    }
                }
            }

            if ($tipo_contenido == 1 OR $tipo_contenido == 2) {


                $nombre = $idReal;
                $filename = $nombre . "." . $extension;
                $directorio = '../../contenido/' . $nombre;
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

                $directorio = $directorio . '/files';
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }



                $contador =  0;
                $adjuntos = '';
                foreach ($_FILES["files"] as $file) {
                    $contador = $contador+1;
                    $file = $_FILES['files']["tmp_name"][$contador - 1];


                    if(!empty($file) && isset($file)){

                        $directorio2 = $directorio . '/'. uniqid() . uniqid();
                        if (!file_exists($directorio2)) {
                            mkdir($directorio2, 0777);
                        }

                     
                        $nombre_final = $_FILES['files']["name"][$contador - 1];


                        $extension101 = $_FILES["imagen_portada"]["name"];
                        $extension101 = explode(".", $extension101);
                        $poxixion_extenxion101 = count($extension101);
                        $poxixion_extenxion101 = $poxixion_extenxion101 - 1;
                        $extension101 = $extension101[$poxixion_extenxion101];
                        $extension1 = strtolower($extension101);


                        if($extension1 != 'php' && $extension1 != 'py' && $extension1 != 'pyw' && $extension1 != '.htaccess'
                        && $extension1 != 'htaccess' && $extension1 != 'html' ){
                            $nombre_final = str_replace(',', '', $nombre_final);
                            $directorio_final = $directorio2 . '/' . $nombre_final;
                            $upCorrect = move_uploaded_file($file, $directorio_final);


                            // adjuntar cada ruta para agregar a ruta de agregdos
                            if (empty($adjuntos)) {
                                $adjuntos = $directorio_final;
                            } else {
                                $adjuntos = $adjuntos . ',' . $directorio_final;
                            }
                        }


                        

                    }

                    $adjuntos = str_replace('../', '', $adjuntos);

                    if(!empty($adjuntos)){
                        $consultaDataIMAGEN2 = "UPDATE `contenidos` SET `adjuntos` = '" . $adjuntos . "' WHERE id = '" . $idReal . "';";
                        $check22 = mysqli_query($con, $consultaDataIMAGEN2);
                    }

                    
                    //var_dump($adjuntos);
                    //echo "<br>";
                    
                }

                //archivos de contenido 
            }


            close_database($con);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        }else {
            $result = array('suceso' => 'error', 'mensaje' => $errorMesasge);
        }

    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EditarContenidoInformacion"]) && $_POST["EditarContenidoInformacion"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';
    $errorMesasge = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idInfo = $_POST["idInfo"];
        $tipo_contenido = $_POST["tipo_contenido"];
        $pagina_interes = $_POST["pagina_interes"];
        $fecha_evento = $_POST["fecha_evento"];
        $hora_evento = $_POST["hora_evento"];
        $nombre_evento = $_POST["nombre_evento"];

        $fecha_evento2 = $_POST["fecha_evento2"];
        $hora_evento2 = $_POST["hora_evento2"];
        $nombre_evento2 = $_POST["nombre_evento2"];
        $tipo_pagina_interes = $_POST["tipo_pagina_interes"];
        $posicion_contenido_usuario = $_POST["posicion_contenido_usuario"];

        $activaFromulario = 0;
        $activaFromularioPreguntas = 0;

        if (isset($_POST["activaFromulario"])) {
            if ($_POST["activaFromulario"] == 1) {
                $activaFromulario = 1;
            }
        }

        if (isset($_POST["activaFromularioPreguntas"])) {
            if ($_POST["activaFromularioPreguntas"] == 1) {
                $activaFromularioPreguntas = 1;
            }
        }


        $desc_form = $_POST["desc_form"];
        $links_video = $_POST["links_video"];
        $c1 = $_POST["c1"];
        $c1_activo = $_POST["c1_activo"];
        $c2 = $_POST["c2"];
        $c2_activo = $_POST["c2_activo"];
        $c3 = $_POST["c3"];
        $c3_activo = $_POST["c3_activo"];
        $c4 = $_POST["c4"];
        $c4_activo = $_POST["c4_activo"];
        $c5 = $_POST["c5"];
        $c5_activo = $_POST["c5_activo"];
        $c6 = $_POST["c6"];
        $c6_activo = $_POST["c6_activo"];
        $c7 = $_POST["c7"];
        $c7_activo = $_POST["c7_activo"];
        $c8 = $_POST["c8"];
        $c8_activo = $_POST["c8_activo"];



        $titulo = '';
        $tipo_publicacion = '';
        $imagen_portada = '';
        $tipo_publicacion2 = '';

        $author = '';
        $resumen = '';
        $url_new = '';


        if (!empty($tipo_pagina_interes)) {
            $tipo_pagina_interes = $tipo_pagina_interes;
        } else {
            $tipo_pagina_interes = '';
        }


        $continuar = true;

        if ($tipo_contenido == 4) {
            if (empty($pagina_interes)) {
                $continuar = false;
                $errorMesasge = 'Recuerda Que La Url Es Necesaria';
            } else {

                $urlparts = parse_url($pagina_interes);

                $scheme = $urlparts['scheme'];

                if ($scheme === 'https' or $scheme === 'http') {
                    //echo ("$url es una URL valida");
                } else {
                    $continuar = false;
                    $errorMesasge = 'Recuerda Que La Url Debe Tener Http O Https';
                }
            }

            if ($continuar == true) {
                $contenido = $pagina_interes;
                $titulo = $contenido;
            }
        }



        $fecha2 = '';

        if ($tipo_contenido == 5) {
            $fecha2 = $fecha_evento . ' ' . $hora_evento;
            $contenido = $nombre_evento;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo1"];
        }

        if ($tipo_contenido == 3) {
            $fecha2 = $fecha_evento2 . ' ' . $hora_evento2;
            $contenido = $nombre_evento2;
            $titulo = $contenido;
            $url_new = $_POST["url_videoinfo2"];
        }

        if ($continuar == true) {

            $con = conection_database();

            $contenido = mysqli_real_escape_string($con, $contenido);
            $titulo = mysqli_real_escape_string($con, $titulo);
           
            $fecha = date('Y-m-d H:i:s');

            if (empty($fecha2)) {
                $fecha2 = date('Y-m-d H:i:s');
            }


            if ($tipo_contenido == 1 or $tipo_contenido == 2) {
                $titulo = $_POST["titulo_coleccion"];
                $titulo = mysqli_real_escape_string($con, $titulo);


                $author = $_POST["autores_coleccion"];
                $author = mysqli_real_escape_string($con, $author);

                $resumen = $_POST["desc_collecion_resumen"];
                $resumen = mysqli_real_escape_string($con, $resumen);



                if ($tipo_contenido == 2) {
                    $tipo_publicacion = '';

                    $tipo_publicacion2 = $_POST["tipo_coleccion"];
                    $tipo_publicacion2 = mysqli_real_escape_string($con, $tipo_publicacion2);
                } else if ($tipo_contenido == 1) {
                    $tipo_publicacion2 = '';

                    $tipo_publicacion = $_POST["tipo_coleccion"];
                    $tipo_publicacion = mysqli_real_escape_string($con, $tipo_publicacion);
                }


                $fecha2 = $_POST["fecha_coleccion"];
                $contenido = $_POST["desc_collecion"];
                $contenido = nl2br($contenido);
                $contenido = mysqli_real_escape_string($con, $contenido);

                if ($contenido == '<p><br></p>') {
                    $contenido = '';
                }

                // upload imagen de portada 

            }


            if($activaFromulario==1){

                // validar si ya esta creado
                $Sql_Query2AdjuntosIdFormulario = "SELECT * FROM emails  WHERE id_formulario='" . $idInfo . "'";
                $checkAdjunotsIdFormulario = mysqli_query($con, $Sql_Query2AdjuntosIdFormulario);
                $rowAdjuntosIdFormulario = mysqli_fetch_assoc($checkAdjunotsIdFormulario);

                if(empty($rowAdjuntosIdFormulario["id"])){
                    //crear email 
                    crearCotenidoHtmlEmailEnvio($idInfo);
                }
            }

            $Sql_Query2Adjuntos = "SELECT * FROM contenidos  WHERE id='" . $idInfo . "'";
            $checkAdjunots = mysqli_query($con, $Sql_Query2Adjuntos);
            $rowAdjuntos = mysqli_fetch_assoc($checkAdjunots);


         

            //,imagen_portada='". $imagen_portada. "'
            $Sql_Query2 = "UPDATE contenidos SET fecha_contenido='".$fecha2. "',tipo='".$tipo_contenido. "',titulo='". $titulo. "' , contenido='". $contenido. "',tipo_publicacion='". $tipo_publicacion. "' , tipo_pagina_interes='". $tipo_pagina_interes. "' , tipo_publicacion2='". $tipo_publicacion2. "',author='". $author. "',resumen='". $resumen. "', url_new='". $url_new. "' , pedir_formulario='". $activaFromulario. "',posicionamiento='". $posicion_contenido_usuario. "' , desc_form= '" . $desc_form . "', links_video= '" . $links_video . "', c1= '" . $c1 . "', c1_activo= '" . $c1_activo . "', c2= '" . $c2 . "', c2_activo= '" . $c2_activo . "',c3= '" . $c3 . "', c3_activo= '" . $c3_activo . "',c4= '" . $c4 . "', c4_activo= '" . $c4_activo . "',c5= '" . $c5 . "', c5_activo= '" . $c5_activo . "',c6= '" . $c6 . "', c6_activo= '" . $c6_activo . "',c7= '" . $c7 . "', c7_activo= '" . $c7_activo . "',c8= '" . $c8 . "', c8_activo= '" . $c8_activo . "',activaFromularioPreguntas='". $activaFromularioPreguntas."' WHERE id='". $idInfo."'";

            /*
            $Sql_Query = "INSERT INTO `contenidos` (`fecha_publicacion`, `fecha_contenido`, `tipo`, `titulo`, `contenido`, `video1`, `video2`, `video3`, `adjuntos`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `meta_author`, `status`,`tipo_publicacion`,`imagen_portada`,`tipo_pagina_interes`,`tipo_publicacion2`) VALUES ( '" . $fecha . "', '" . $fecha2 . "', '" . $tipo_contenido . "', '" . $titulo . "', '" . $contenido . "', '', '', '', '', '', '', '', '', '', '1','" . $tipo_publicacion . "','" . $imagen_portada . "','" . $tipo_pagina_interes . "','" . $tipo_publicacion2 . "');";
            */
            $check = mysqli_query($con, $Sql_Query2);
            //$row = mysqli_fetch_assoc($check);
            $idReal = $idInfo;


            if ($tipo_contenido == 1 or $tipo_contenido == 2  or $tipo_contenido == 4) {
                if (!empty($_FILES["imagen_portada"]["tmp_name"])) {



                    $extension10 = $_FILES["imagen_portada"]["name"];
                    $extension10 = explode(".", $extension10);
                    $poxixion_extenxion10 = count($extension10);
                    $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                    $extension10 = $extension10[$poxixion_extenxion10];
                    $extension = strtolower($extension10);



                    $nombre = $idReal;
                    $filename = $nombre . "." . $extension;
                    $directorio = '../../contenido/' . $nombre;
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $directorio = $directorio . '/img_p';
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777);
                    }

                    $files = glob($directorio . '/*'); //obtenemos todos los nombres de los ficheros
                    foreach ($files as $file) {
                        //echo $file;
                        if (is_file($file)) {
                            unlink($file); //elimino el fichero
                        }
                    }

                    $filename = $filename . rand(1, 1000000000).uniqid();

                    $source = $_FILES["imagen_portada"]["tmp_name"];
                    $target_path = $directorio . '/' . $filename;
                    $file = $_FILES['imagen_portada']['tmp_name'];
                    $source_properties = getimagesize($file);
                    $image_type = $source_properties[2];
                    $continuarUpcorrect = false;
                    if ($image_type == IMAGETYPE_JPEG) {

                        $xten = "jpg";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_GIF) {

                        $xten = "gif";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } elseif ($image_type == IMAGETYPE_PNG) {

                        $xten = "png";
                        $nombre_final = $filename . "_thump." . $xten;
                        $directorio_final = $directorio . '/' . $nombre_final;
                        $upCorrect = move_uploaded_file($file, $directorio_final);
                        $continuarUpcorrect = true;
                    } else {
                        $result = array('suceso' => 'Formato De Archivo No Permitido', 'ruta' => '');
                        $upCorrect = false;
                        $continuarUpcorrect = false;
                    }

                    if ($continuarUpcorrect == true) {

                        if ($upCorrect) {
                            //insertar nombre en db 
                            //require_once('rutaMain.php');

                            $directorio_final = str_replace('../', '', $directorio_final);
                            //$directorio_final = $rutaPrincipalArchivos . $directorio_final;
                            $data = $directorio_final;
                            $consultaDataIMAGEN = "UPDATE `contenidos` SET `imagen_portada` = '" . $directorio_final . "' WHERE id = '" . $idReal . "';";
                            $check2 = mysqli_query($con, $consultaDataIMAGEN);
                        }
                    }
                }
            }

            if ($tipo_contenido == 1 or $tipo_contenido == 2) {


                $nombre = $idReal;
                $filename = $nombre . "." . $extension;
                $directorio = '../../contenido/' . $nombre;
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

                $directorio = $directorio . '/files';
                if (!file_exists($directorio)) {
                    mkdir($directorio, 0777);
                }

               


                $contador =  0;
                $adjuntos = $rowAdjuntos["adjuntos"];
                foreach ($_FILES["files"] as $file) {
                    $contador = $contador + 1;
                    $file = $_FILES['files']["tmp_name"][$contador - 1];


                    if (!empty($file) && isset($file)) {

                        $directorio2 = $directorio . '/' .  uniqid() . uniqid();
                        if (!file_exists($directorio2)) {
                            mkdir($directorio2, 0777);
                        }


                        $nombre_final = $_FILES['files']["name"][$contador - 1];


                        $extension101 = $_FILES["imagen_portada"]["name"];
                        $extension101 = explode(".", $extension101);
                        $poxixion_extenxion101 = count($extension101);
                        $poxixion_extenxion101 = $poxixion_extenxion101 - 1;
                        $extension101 = $extension101[$poxixion_extenxion101];
                        $extension1 = strtolower($extension101);


                        if (
                            $extension1 != 'php' && $extension1 != 'py' && $extension1 != 'pyw' && $extension1 != '.htaccess'
                            && $extension1 != 'htaccess' && $extension1 != 'html'
                        ) {
                            $nombre_final = str_replace(',', '', $nombre_final);
                            $directorio_final = $directorio2 . '/' . $nombre_final;
                            $upCorrect = move_uploaded_file($file, $directorio_final);


                            // adjuntar cada ruta para agregar a ruta de agregdos
                            if (empty($adjuntos)) {
                                $adjuntos = $directorio_final;
                            } else {
                                $adjuntos = $adjuntos . ',' . $directorio_final;
                            }
                        }
                    }

                    $adjuntos = str_replace('../', '', $adjuntos);

                    if (!empty($adjuntos)) {
                        $consultaDataIMAGEN2 = "UPDATE `contenidos` SET `adjuntos` = '" . $adjuntos . "' WHERE id = '" . $idReal . "';";
                        $check22 = mysqli_query($con, $consultaDataIMAGEN2);
                    }


                    //var_dump($adjuntos);
                    //echo "<br>";

                }

                //archivos de contenido 
            }


            close_database($con);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        } else {
            $result = array('suceso' => 'error', 'mensaje' => $errorMesasge);
        }
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EliminarContenido"]) && $_POST["EliminarContenido"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idContenido = $_POST["idContenido"];


        $con = conection_database();
        $Sql_Query = "UPDATE `contenidos` SET `status` = '0' WHERE `id` = '" . $idContenido . "';";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);
        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["DeleteContenidoIndivi"]) && $_POST["DeleteContenidoIndivi"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idContenido = $_POST["idContenido"];
       

        $con = conection_database();
        $Sql_Query = "SELECT * FROM `contenidos` WHERE `id` = '" . $idContenido . "';";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);

        $nuevoAdjunto = $row["adjuntos"];

        // delete file 
        $Ruta = $_POST["Ruta"];
        $Ruta = '../../'. $Ruta;
        $delete= unlink($Ruta);
        //var_dump($delete);


        // ahora delete de la base de datos
        $busqueda =','.$_POST["Ruta"]; 
        $nuevoAdjunto = str_replace($busqueda,'', $nuevoAdjunto);
        $nuevoAdjunto = str_replace($_POST["Ruta"],'', $nuevoAdjunto);


        $Sql_Query2 = "UPDATE `contenidos` SET adjuntos='". $nuevoAdjunto."' WHERE `id` = '" . $idContenido . "';";
        $check2 = mysqli_query($con, $Sql_Query2);

        //var_dump();

        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};



if (isset($_POST["CreateHorario"]) && $_POST["CreateHorario"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];
        $horarioDia = $_POST["horarioDia"];
        $horarioHora = $_POST["horarioHora"];
        $fechaHora = $horarioDia.' '. $horarioHora;

        $con = conection_database();

        $Sql_Query2 = "INSERT INTO `horarios` (`id_formulario`, `fecha_begin`, `fecha_end`) VALUES ( '". $idFormulario."', '". $fechaHora."', NULL);";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EliminarHorario"]) && $_POST["EliminarHorario"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];
      

        $con = conection_database();

        $Sql_Query2 = "DELETE FROM horarios WHERE id='". $idFormulario."'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EditarHorario"]) && $_POST["EditarHorario"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();
        $horarioDia = $_POST["horarioDia"];
        $horarioHora = $_POST["horarioHora"];
        $fechaHora = $horarioDia . ' ' . $horarioHora;

        $Sql_Query2 = "UPDATE horarios SET fecha_begin='". $fechaHora."' WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};


if (isset($_POST["EditarEmailInformacionHtml"]) && $_POST["EditarEmailInformacionHtml"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';
    $errorMesasge = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idInfo = $_POST["idInfo"];
        $continuar = true;

       

        if ($continuar == true) {

            $con = conection_database();
            mysqli_set_charset($con, "utf8");
            $contenido = $_POST["codigo_email"];
            $contenido = mysqli_real_escape_string($con, $contenido);
           

            $Sql_Query2 = "UPDATE emails SET contenido='" . $contenido . "' WHERE id_formulario='" . $idInfo . "'";
            
            /*
            $Sql_Query = "INSERT INTO `contenidos` (`fecha_publicacion`, `fecha_contenido`, `tipo`, `titulo`, `contenido`, `video1`, `video2`, `video3`, `adjuntos`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `meta_author`, `status`,`tipo_publicacion`,`imagen_portada`,`tipo_pagina_interes`,`tipo_publicacion2`) VALUES ( '" . $fecha . "', '" . $fecha2 . "', '" . $tipo_contenido . "', '" . $titulo . "', '" . $contenido . "', '', '', '', '', '', '', '', '', '', '1','" . $tipo_publicacion . "','" . $imagen_portada . "','" . $tipo_pagina_interes . "','" . $tipo_publicacion2 . "');";
            */
            $check = mysqli_query($con, $Sql_Query2);
            //$row = mysqli_fetch_assoc($check);
            $idReal = $idInfo;


          


            close_database($con);

            $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
        } else {
            $result = array('suceso' => 'error', 'mensaje' => $errorMesasge);
        }
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};



if (isset($_POST["CrearRutaMiniatura"]) && $_POST["CrearRutaMiniatura"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $nombre_adjunto_info = $_POST["nombre_adjunto_info"];
        // $idContenido = $_POST["miniatura_imagen_adjunto"];
        $idContenido = $_POST["idContenido"];
        $Ruta = $_POST["Ruta"];

        



        $con = conection_database();

        $Sql_Query = "SELECT * FROM metadata_adjuntos WHERE idContenido='". $idContenido. "' AND Ruta='". $Ruta."'";
        $check = mysqli_query($con, $Sql_Query);
        $row = mysqli_fetch_assoc($check);


        if(empty($row["id"])){
            $Sql_Query = "INSERT INTO `metadata_adjuntos` (`idContenido`, `Ruta`, `miniatura_imagen_adjunto`, `nombre_adjunto_info`) VALUES ('" . $idContenido . "', '" . $Ruta . "', '', '" . $nombre_adjunto_info . "');";
            $check = mysqli_query($con, $Sql_Query);
            $lastid = mysqli_insert_id($con);
        }else {

            $Sql_Query = "UPDATE metadata_adjuntos SET nombre_adjunto_info='". $nombre_adjunto_info."' WHERE id='". $row["id"]."'";
            $check = mysqli_query($con, $Sql_Query);
            $lastid = $row["id"];
        }


       


        if ($_FILES['miniatura_imagen_adjunto']['name']) {
            if (!$_FILES['miniatura_imagen_adjunto']['error']) {


                //verificar el tama;o y el tipo de archivo


                $nameFinal = uniqid();
                $nameFinal = $nameFinal . md5(rand(100, 200));
                $idUsuario = $idReal;

                $extension10 = $_FILES["miniatura_imagen_adjunto"]["name"];
                $extension10 = explode(".", $extension10);
                $poxixion_extenxion10 = count($extension10);
                $poxixion_extenxion10 = $poxixion_extenxion10 - 1;
                $extension10 = $extension10[$poxixion_extenxion10];
                $extension = strtolower($extension10);

                $ext = $extension;
                $file = $_FILES['miniatura_imagen_adjunto']['tmp_name'];
                $sizeTam = filesize($file);
                //echo $sizeTam;
                if ($ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'gif'
                ) {
                    $filename = $nameFinal . '.' . $ext;
                    $idRanduuid =  uniqid() . uniqid();
                    $idRanduuid = trim($idRanduuid);
                    $carpeta = '../../assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid;

                    if (!file_exists($carpeta)) {
                        mkdir($carpeta, 0777);
                    }
                    $destination =  $carpeta . '/' . $filename; //change this directory
                    $location = $_FILES["miniatura_imagen_adjunto"]["tmp_name"];
                    move_uploaded_file($location, $destination);
                    $suceso = 'ok';
                    // https://biblioteca.misionerosurbanosdejesucristo.org/
                    $message = '../assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid . '/' . $filename; //change this URL
                    $message = 'assets/imagenesSubidasAdminAdjuntos/' . $idRanduuid . '/' . $filename; //change this URL
                    //$message = '../assets/imagenesSubidasAdmin/' . $idRanduuid . '/' . $filename; //change this URL


                    $Sql_Query2 = "UPDATE metadata_adjuntos SET miniatura_imagen_adjunto='" . $message . "' WHERE id='" . $lastid . "'";
                    $check2 = mysqli_query($con, $Sql_Query2);
                } else {
                    $suceso = 'Error';
                    $message = 'Recuerda que solo pueden ser png o jpg';
                }
            } else {
                $suceso = 'Error';
                $message = 'Ooops!  El Archivo Tuvo El Siguiente Error:  ' . $_FILES['file']['error'];
            }
        }
       

        close_database($con);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["CreateHorario_video"]) && $_POST["CreateHorario_video"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];
        $horarioDia = $_POST["horarioDia"];
        $horarioHora = $_POST["horarioHora"];
        $descHorario = $_POST["descHorario"];
        $fechaHora = $horarioDia . ' ' . $horarioHora;


        $horarioDia2 = $_POST["horarioDia2"];
        $horarioHora2 = $_POST["horarioHora2"];
        $fechaHora2 = $horarioDia2 . ' ' . $horarioHora2;

        $con = conection_database();

        $Sql_Query2 = "INSERT INTO `horarios_video` (`id_formulario`, `fecha_begin`, `fecha_end`,`descHorario`) VALUES ( '" . $idFormulario . "', '" . $fechaHora . "', '". $fechaHora2."','". $descHorario."');";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EliminarHorario_video"]) && $_POST["EliminarHorario_video"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();

        $Sql_Query2 = "DELETE FROM horarios_video WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["EditarHorario_video"]) && $_POST["EditarHorario_video"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();
        $horarioDia = $_POST["horarioDia"];
        $horarioHora = $_POST["horarioHora"];
        $fechaHora = $horarioDia . ' ' . $horarioHora;

        $horarioDia2 = $_POST["horarioDia2"];
        $horarioHora2 = $_POST["horarioHora2"];
        $fechaHora2 = $horarioDia2 . ' ' . $horarioHora2;
        $descHorario = $_POST["descHorario"];

        $Sql_Query2 = "UPDATE horarios_video SET fecha_begin='" . $fechaHora . "',fecha_end='" . $fechaHora2 . "',descHorario='". $descHorario."' WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["ObtenerHorario_video"]) && $_POST["ObtenerHorario_video"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();
        
    $Sql_Query2 = "SELECT * FROM  horarios_video  WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);
        $row2 = mysqli_fetch_assoc($check2);


        if ($row2["fecha_begin"] == '0000-00-00 00:00:00' || $d == '1969-12-31 19:00:00' || empty($row2["fecha_begin"]) || $row2["fecha_begin"] == null || $row2["fecha_begin"] == 'NULL') {
             $fecha = '';
            $hora = '';
        }else {
            $fecha = date('Y-m-d', strtotime($row2["fecha_begin"]));
            $hora = date('H:i:s', strtotime($row2["fecha_begin"]));
        }

        if ($row2["fecha_end"] == '0000-00-00 00:00:00' || $d == '1969-12-31 19:00:00' || empty($row2["fecha_end"]) || $row2["fecha_end"] == null || $row2["fecha_end"] == 'NULL') {
            $fecha2 = '';
            $hora2 = '';
        } else {
            $fecha2 = date('Y-m-d', strtotime($row2["fecha_end"]));
            $hora2 = date('H:i:s', strtotime($row2["fecha_end"]));
        }
        
       
        $descripcion = $row2["descHorario"];

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data, 'fecha'=> $fecha, 'hora'=> $hora, 'fecha2'=> $fecha2, 'hora2'=> $hora2, 'descripcion'=> $descripcion);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["ObtenerHorario_video_contenido_online"]) && $_POST["ObtenerHorario_video_contenido_online"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();

        $Sql_Query2 = "SELECT * FROM contenidos  WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);
        $row2 = mysqli_fetch_assoc($check2);





        $online_activo = $row2["online_activo"];
        $desc_activo = $row2["desc_activo"];

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data, 'online_activo' => $online_activo, 'desc_activo' => $desc_activo);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["InactivarConferenciaPermanente"]) && $_POST["InactivarConferenciaPermanente"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();
       

        $Sql_Query2 = "UPDATE contenidos SET online_activo='',desc_activo='' WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["ActivarConferenciaPermanente"]) && $_POST["ActivarConferenciaPermanente"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];
        $descHorario3 = $_POST["descHorario3"];


        $con = conection_database();


        $Sql_Query2 = "UPDATE contenidos SET online_activo='1',desc_activo='". $descHorario3."' WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["RespondidaPreguntaUsuario"]) && $_POST["RespondidaPreguntaUsuario"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();

        $Sql_Query2 = "UPDATE preguntas_usuarios SET respondida='1' WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, "formulario" => $formulario, "metodo_pago" => $metodo_pago, "key_data" => $key_data);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["ObtenerDatosDestacados"]) && $_POST["ObtenerDatosDestacados"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();

        $Sql_Query2 = "SELECT * FROM  contenidos  WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);
        $row2 = mysqli_fetch_assoc($check2);

        $mensaje ='ok';
        $submensaje ='ok';

        $diasActivos = $row2["destacados"];

        $lunes = 0;
        $martes = 0;
        $miercoles = 0;
        $jueves = 0;
        $viernes = 0;
        $sabado = 0;
        $domingo = 0;
        $misterio = 0;

        if (strpos($diasActivos, "1") !== false) {
            $lunes = 1;
        }
        if (strpos($diasActivos, "2") !== false) {
            $martes = 1;
        }
        if (strpos($diasActivos, "3") !== false) {
            $miercoles = 1;
        }
        if (strpos($diasActivos, "4") !== false) {
            $jueves = 1;
        }
        if (strpos($diasActivos, "5") !== false) {
            $viernes = 1;
        }
        if (strpos($diasActivos, "6") !== false) {
            $sabado = 1;
        }
        if (strpos($diasActivos, "7") !== false) {
            $domingo = 1;
        }
        if (strpos($diasActivos, "8") !== false) {
            $misterio = 1;
        }

        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje, 'lunes'=> $lunes, 'martes'=> $martes, 'miercoles'=> $miercoles, 'jueves'=> $jueves, 'viernes'=> $viernes, 'sabado'=> $sabado, 'domingo'=> $domingo, 'misterio'=> $misterio);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};

if (isset($_POST["GuardarDatosDescatados"]) && $_POST["GuardarDatosDescatados"] == 'SMD69') {

    $userid = $_POST["userid"];
    $submensaje = '';
    $formulario = '';
    $key_data = '';


    if ($userid == $_SESSION["adminMisionerosUrbanos"][0]) {

        $idFormulario = $_POST["idFormulario"];


        $con = conection_database();

       

       

        $lunes = $_POST["lunes"];
        $martes = $_POST["martes"];
        $miercoles = $_POST["miercoles"];
        $jueves = $_POST["jueves"];
        $viernes = $_POST["viernes"];
        $sabado = $_POST["sabado"];
        $domingo = $_POST["domingo"];
        $misterio = $_POST["misterio"];

        $stringContenido = $lunes.',' . $martes . ',' . $miercoles . ',' . $jueves . ',' . $viernes . ',' . $sabado . ',' . $domingo.','. $misterio ;


        $Sql_Query2 = "UPDATE contenidos SET destacados='". $stringContenido."'  WHERE id='" . $idFormulario . "'";
        $check2 = mysqli_query($con, $Sql_Query2);
        //$row2 = mysqli_fetch_assoc($check2);

        $mensaje = 'ok';
        $submensaje = 'ok';



        $result = array('suceso' => 'ok', 'mensaje' => $mensaje, 'submensaje' => $submensaje);

        close_database($con);
    } else {
        $result = array('suceso' => 'error', 'mensaje' => 'Error En Datos#1', 'Error');
    }
    $obj = json_encode($result);

    echo $obj;
};