 <?php
    session_start();
    ?>
 <!-- main header @s -->
 <div class="nk-header nk-header-fixed is-light">
     <div class="container-fluid">
         <div class="nk-header-wrap">
             <div class="nk-menu-trigger d-xl-none ml-n1">
                 <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
             </div>
             <div class="nk-header-brand d-xl-none">
                 <a class="navbar-brand" onclick="home(event)" style="color:#000;font-weight:800;font-size:20px"><img style="width: auto;max-height: 60px;" src="images/logo2.png" alt="logo"> </a>
             </div><!-- .nk-header-brand -->
             <div class="nk-header-news d-none d-xl-block">
                 <div class="nk-news-list">
                     <a class="nk-news-item" href="#">
                         <div class="nk-news-icon">
                             <em class="icon ni ni-card-view"></em>
                         </div>
                         <div class="nk-news-text">
                             <div style="display: none;" id="loadingDataShowLoader">
                                 <div class="spinner-border" role="status">
                                     <span class="sr-only">Cargando...</span>
                                 </div>
                                 Cargando Informacion...
                             </div>
                         </div>

                     </a>
                 </div>
             </div><!-- .nk-header-news -->
             <div class="nk-header-tools">
                 <ul class="nk-quick-nav">
                     <li class="dropdown user-dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                             <div class="user-toggle">
                                 <div class="user-avatar sm">
                                     <em class="icon ni ni-user-alt"></em>
                                 </div>
                                 <div class="user-info d-none d-md-block">
                                     <div class="user-status"></div>
                                     <div class="user-name dropdown-indicator">Admin</div>
                                 </div>
                             </div>
                         </a>
                         <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                             <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                 <div class="user-card">
                                     <div class="user-avatar">
                                         <span>AB</span>
                                     </div>
                                     <div class="user-info">
                                         <span class="lead-text">Admin</span>
                                     </div>
                                 </div>

                                 <div class="dropdown-inner">
                                     <ul class="link-list">
                                         <li><a href="logout.php"><em class="icon ni ni-signout"></em><span>Cerrar sesión</span></a></li>
                                     </ul>
                                 </div>
                             </div>
                     </li><!-- .dropdown -->

                 </ul><!-- .nk-quick-nav -->
             </div><!-- .nk-header-tools -->
         </div><!-- .nk-header-wrap -->
     </div><!-- .container-fliud -->
 </div>
 <!-- main header @e -->