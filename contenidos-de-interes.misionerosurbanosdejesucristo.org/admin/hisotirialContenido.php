<?php
session_start();
$_SESSION["adminHistorialPointMUJ"] = 3;
//require_once('include/function_admin.php');

?>
<link href="lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
<script src="lib/datatables/js/jquery.dataTables.js"></script>
<script src="lib/datatables-responsive/js/dataTables.responsive.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Historial de contenido</h3>
                        <div class="nk-block-des text-soft">
                            <p></p>
                        </div>
                    </div><!-- .nk-block-head-content -->

                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <div class="row g-gs">
                    <div class="col-md-12">
                        <div class="card  card-full">
                            <table id="datatable1_28" class="table card-text">
                                <thead>
                                    <tr>
                                        <th>Fecha creacion</th>
                                        <th>Tipo contenido</th>
                                        <th>Titulo</th>
                                        <th>Posteador </th>
                                        <th>Editar </th>
                                        <th>Eliminar </th>

                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- .card -->
                    </div><!-- .col -->



                </div>
            </div>
        </div>
    </div>
</div>
<!-- content @e -->

<script>
    // self executing function here
    (function() {
        $("#loadingDataShowLoader").css('display', 'none');
    })();




    $('#datatable1_28').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/scripts/server_processing_t1.php",
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: '_MENU_ Resultados/Por Pagina',
        },
        "order": [
            [0, "DESC"]
        ],
    });


    /*
    ,
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    */


    function eliminarContenido(idContenido) {
        var userid = '<?php echo $_SESSION["adminMisionerosUrbanos"][0]; ?>';
        Swal({
            title: 'Eliminar Contenido',
            text: 'Estas Seguro De Eliminar Este Contenido ?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#6baafe',
            cancelButtonColor: '#6baafe',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowOutsideClick: false,
            allowEscapeKey: false
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "ajax/contenidoData.php",
                    data: {
                        "idContenido": idContenido,
                        "userid": userid,
                        "EliminarContenido": 'SMD69'
                    },
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {
                            Swal({
                                title: 'Contenido Eliminado',
                                text: 'Contenido Eliminado Correctamente',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#6baafe',
                                cancelButtonColor: '#6baafe',
                                confirmButtonText: 'ok',
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        } else {
                            cargar_swal('error', loginData.mensaje, 'Error');
                        }
                    }
                });


            }
        });
    }
</script>