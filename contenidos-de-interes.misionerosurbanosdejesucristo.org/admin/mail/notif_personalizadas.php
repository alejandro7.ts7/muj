<?php
ini_set('display_errors', 'Off');
require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';
require '../connection/conect3.php';


//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


function is_valid_email_correo_envio($str)
{

    //$result = 0 ;		
    $matches = null;


    if (filter_var($str, FILTER_VALIDATE_EMAIL)) {
        //echo("$str is a valid email address");
        $result = true;
    } else {
        $result = false;
        //echo("$str is not a valid email address");
    }

    if ($result == false) {
        $str = strtolower($str);

        if (strpos($str, "hotmail.") or strpos($str, "gmail.com") or strpos($str, "yahoo.") or strpos($str, "outlook.") or strpos($str, "yandex.") or strpos($str, "aol.")) {
            $result = true;
        }
    }

    return $result;
}



function consultar_usuario_info($id_usuarioVendedor)
{

    $con = conection_database_email();
    if($id_usuarioVendedor==0){
        $Sql_Query = "SELECT * FROM `adminservice` WHERE id='1' ";
    }else {
        $Sql_Query = "SELECT * FROM `userservice` WHERE id='" . $id_usuarioVendedor . "' ";
    }
    $sql = mysqli_query($con, $Sql_Query);
    $row = mysqli_fetch_assoc($sql);

    

    $data = $row;

    

    close_database_email($con);
    return $data;
}
function consultar_configuracion_envio_mail($idConfiguracion)
{

    $con = conection_database_email();
    $Sql_Query = "SELECT * FROM `config_data` WHERE config_id='" . $idConfiguracion . "' ";
    $sql = mysqli_query($con, $Sql_Query);
    $row = mysqli_fetch_assoc($sql);
    $data = array("config_id" => $row["config_id"], "valor" => $row["valor"], "activo" => $row["activo"], "valor_nombre" => $row["valor_nombre"], "valor_hora" => $row["valor_hora"], "activo_publico" => $row["activo_publico"]);
    close_database_email($con);
    return $data;
}


function enviar_mensaje_por_smtp_mail_jet($correo_usuario, $tipoEnvio, $usernameData, $contenido)
{

    $isValidEmail = is_valid_email_correo_envio($correo_usuario);

    if ($isValidEmail == true) {
        $configMail = consultar_configuracion_envio_mail(42);

        if ($configMail["activo"] == 1) {
            //aqui envio con servidor smtp de gmail

            $cuerpo = obtener_cuerpo($tipoEnvio, $usernameData, $contenido);

            $tituloGeneral = 'Misioneros Urbanos de Jesucristo';
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            $correo_envio = $configMail["valor"];
            $mail->SMTPDebug = 0;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $configMail["valor"];               // SMTP username
            $mail->Password = $configMail["valor_nombre"];                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;
            //$mail->setFrom($correo_envio);
            $mail->SetFrom($correo_envio, $tituloGeneral);
            $mail->addAddress($correo_usuario);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $tituloGeneral;
            $mail->Body    = $cuerpo;
            $mail->AltBody = $cuerpo;
            $mail->send();
            //echo 'Message has been sent';
        } elseif ($configMail["activo"] == 0) {
            $cuerpo = obtener_cuerpo($tipoEnvio, $usernameData, $contenido);
            $tituloGeneral = 'Misioneros Urbanos de Jesucristo';
            $MailSTMPPROPIO = "noreply@tradexchange.com.co";
            $PASSWSMTP = "3@6j^^.ay8u&";
            $HOSTSMTP = "single-7051.banahosting.com";
            $mail = new PHPMailer(true);
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = $HOSTSMTP;
            $mail->Port = 465; // or 587
            $mail->IsHTML(true);
            $mail->Username = $MailSTMPPROPIO;
            $mail->Password = $PASSWSMTP;
            $mail->SetFrom($MailSTMPPROPIO, $tituloGeneral);
            $mail->Subject = $tituloGeneral;
            $mail->Body = $cuerpo;
            $mail->AltBody = $cuerpo;
            $mail->AddAddress($correo_usuario);
            $mail->Send();
        }
    }
}


function enviar_mensaje_por_smtp_mail_jet2OnlyContent($correo_usuario, $contenido)
{

    $isValidEmail = is_valid_email_correo_envio($correo_usuario);

    if ($isValidEmail == true) {
        $configMail = consultar_configuracion_envio_mail(42);

        if ($configMail["activo"] == 1) {
            //aqui envio con servidor smtp de gmail

            $cuerpo = obtener_cuerpo2($correo_usuario, $contenido);

            $tituloGeneral = 'TradeXchange';
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            $correo_envio = $configMail["valor"];
            $mail->SMTPDebug = 0;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $configMail["valor"];               // SMTP username
            $mail->Password = $configMail["valor_nombre"];                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;
            //$mail->setFrom($correo_envio);
            $mail->SetFrom($correo_envio, $tituloGeneral);
            $mail->addAddress($correo_usuario);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $tituloGeneral;
            $mail->Body    = $cuerpo;
            $mail->AltBody = $cuerpo;
            $mail->send();
            //echo 'Message has been sent';
        } elseif ($configMail["activo"] == 0) {
            $cuerpo = obtener_cuerpo2($correo_usuario, $contenido);
            $tituloGeneral = 'TradeXchange';
            $MailSTMPPROPIO = "noreply@tradexchange.com.co";
            $PASSWSMTP = "3@6j^^.ay8u&";
            $HOSTSMTP = "single-7051.banahosting.com";
            $mail = new PHPMailer(true);
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = $HOSTSMTP;
            $mail->Port = 465; // or 587
            $mail->IsHTML(true);
            $mail->Username = $MailSTMPPROPIO;
            $mail->Password = $PASSWSMTP;
            $mail->SetFrom($MailSTMPPROPIO, $tituloGeneral);
            $mail->Subject = $tituloGeneral;
            $mail->Body = $cuerpo;
            $mail->AltBody = $cuerpo;
            $mail->AddAddress($correo_usuario);
            $mail->Send();
        }
    }
}

function obtener_cuerpo($tipo, $username, $contenido)
{

    if ($tipo == 1) {
        $result = '<h1></h1><b>BIENVENIDO A TRADEXCHANGE</b><br><br></h1>
            <img src="https://tradexchange.com.co/images/logo-dark.png" style="height:40px;width:190px;">
            <br><br>
            <p>Hola ' . $username . '</p>
            <p>
                ' . $contenido . '
            </p>
            <p>
                Saludos<br>
                Equipo TradeXChange
            </p>';
    }


    return $result;
}


function obtener_cuerpo2($email, $contenido)
{

    $result = '<h1></h1><b>BIENVENIDO A TRADEXCHANGE</b><br><br></h1>
            <img src="https://tradexchange.com.co/images/logo-dark.png" style="height:40px;width:190px;">
            <br><br>
            <p>Hola ' . $email . '</p>
            <p>
                ' . $contenido . '
            </p>
            <p>
                Saludos<br>
                Equipo TradeXChange
            </p>';


    return $result;
}
