<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Misioneros Urbanos de Jesucristo">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Misioneros Urbanos de Jesucristo">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="images/favicon.png">
    <!-- Page Title  -->
    <title>Misioneros Urbanos de Jesucristo</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="assets/css/dashlite.css?ver=2.4.0">
    <link id="skin-default" rel="stylesheet" href="assets/css/theme.css?ver=2.4.0">
    <link href="lib/SpinKit/css/spinkit.css" rel="stylesheet">
    <link href="swal/sweetalert2.min.css" rel="stylesheet">
</head>

<body class="nk-body npc-crypto bg-white pg-auth">
    <!-- app body @s -->
    <div class="nk-app-root">
        <div class="nk-split nk-split-page nk-split-md">
            <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white">
                <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                    <a href="#" class="toggle btn-white btn btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                </div>
                <div class="nk-block nk-block-middle nk-auth-body">
                    <div class="brand-logo pb-5">
                        <a class="navbar-brand" href="login.php" style="color:#000;font-weight:800;font-size:20px"><img style="width:60px;height:40px;" src="images/logo2.png" alt="logo"> Misioneros Urbanos de Jesucristo</a>
                    </div>
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h5 class="nk-block-title">Iniciar Sesion </h5>
                            <div class="nk-block-des">
                                <p>Bienvenido Administrador .</p>
                            </div>
                        </div>
                    </div><!-- .nk-block-head -->

                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="default-01">Email O Usuario</label>
                        </div>
                        <input type="text" id="username" class="form-control form-control-lg" id="default-01" placeholder="Email O Usuario">
                    </div><!-- .foem-group -->
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password">Contraseña</label>
                        </div>
                        <div class="form-control-wrap">
                            <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                            </a>
                            <input type="password" class="form-control form-control-lg" id="password" placeholder="Contraseña">
                        </div>
                    </div><!-- .foem-group -->

                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password">Contraseña 2</label>
                        </div>
                        <div class="form-control-wrap">
                            <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password2">
                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                            </a>
                            <input type="password" class="form-control form-control-lg" id="password2" placeholder="Contraseña 2">
                        </div>
                    </div><!-- .foem-group -->
                    <div class="form-group">
                        <button onclick="login_user()" class="btn btn-lg btn-primary btn-block">
                            <div id="DaTaShow">Iniciar Sesion</div>
                            <div id="DataWait" style="display: none;" class="sk-three-bounce">
                                Por Favor Espere
                                <div class="sk-child sk-bounce1 bg-gray-800"></div>
                                <div class="sk-child sk-bounce2 bg-gray-800"></div>
                                <div class="sk-child sk-bounce3 bg-gray-800"></div>
                                <div class="sk-child sk-bounce3 bg-gray-800"></div>
                            </div>
                        </button>


                    </div>


                </div><!-- .nk-block -->

            </div><!-- .nk-split-content -->
            <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- .nk-split-content -->
        </div><!-- .nk-split -->
    </div><!-- app body @e -->
    <!-- JavaScript -->
    <script src="assets/js/bundle.js?ver=2.4.0"></script>
    <script src="assets/js/scripts.js?ver=2.4.0"></script>

    <script src="lib/jquery/js/jquery.js"></script>
    <script src="swal/swalpersonalizado.js"></script>
    <script src="swal/sweetalert2.min.js"></script>
    <script type="text/javascript">
        function aparecer_cargando() {

            $("#DaTaShow").css("display", "none");
            $("#DataWait").css("display", "block");

        }

        function desaparecer_cargando() {

            $("#DaTaShow").css("display", "block");
            $("#DataWait").css("display", "none");

        }

        function login_user() {

            var user_name = $("#username").val();
            var user_password = $("#password").val();
            var user_password_segunda = $("#password2").val();

            if (user_name != '' && user_password != '') {
                aparecer_cargando();
                $.ajax({
                    type: "POST",
                    url: "ajax/login_admin_user.php",
                    data: {
                        "user_name": user_name,
                        "user_password": user_password,
                        "loginData": 1,
                        "user_password_segunda": user_password_segunda
                    },
                    success: function(loginData) {
                        desaparecer_cargando();
                        if (loginData == 'ok') {
                            location.href = "index.php";
                            //window.open("index.php");

                        } else {
                            //alert(loginData);
                            cargar_swal('error', loginData, 'Error Ingresando');
                        }
                    }
                });

            } else {
                if (user_name == '' && user_password == '') {
                    cargar_swal("info", "", "usuario y contraseña vacia");
                } else if (user_name == '') {
                    cargar_swal("info", "", "usuario vacio");
                } else if (user_password == '') {
                    cargar_swal("info", "", "contraseña vacia");
                }
            }



        };



        $(document).ready(function() {

            $("#password2").on("keydown", function(e) {
                if (e.which == 13) {
                    login_user();
                }
            });
        });
    </script>

</body>

</html>