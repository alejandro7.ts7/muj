<?php
function EnviarMensajeNotificacionFCB($tituloEnvio,$mensajeEnvio,$urlEnvio){
    // Set the URL of the FCM API endpoint
    $url = 'https://fcm.googleapis.com/fcm/send';

    // Set the server key for authentication
    $server_key = 'AAAAj33H3-U:APA91bF26FjKLAs-30g2rTkQmspmgpP7BOFBg_xYU8laCxLWXzy0P4EbPIk3MbYg_tnLtrL2HEmn9xslu-VX-1aHiCBSU2JnOuzik5_fvOqTqeEBzGdDyucHgn-yQPRORuuySUQSRwVW';

    // Set the message payload
    $topic = 'muj';
    $message = array(
        'data' => array(
            'title' => $tituloEnvio,
            'body' => $mensajeEnvio,
            'icon' => 'https://experiencia.misionerosurbanosdejesucristo.org/img/logo.png',
            'click_action' => $urlEnvio
        ),
        "to" => '/topics/' . $topic,
    );

    /*
[
        'fhmAMvpTLx4xODPP54w7bH:APA91bERpwYz1z-JpIhuxGJfhDbt-sj2DMYq9UM9x_e9T9n8LPFjXF5Ap3s9RWbI7LV-VZw1B6Mf3b7LgQ-awEpVeEN1PzIX8tFL3xhLVdTVr--iz-uw0z_TAK5zlm_jsKZGrdKZKlfM'
  ]
*/
    // Set additional cURL options
    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_POST => true,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $server_key,
            'Content-Type: application/json',
        ),
        CURLOPT_POSTFIELDS => json_encode($message),
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => 1
    );

    // Create a new cURL resource and set the options
    $curl = curl_init();
    curl_setopt_array($curl, $options);

    // Send the HTTP request and get the response
    curl_exec($curl);
    /*
    // Check for errors
    if ($response === false) {
        // echo 'Error sending message: ' . curl_error($curl);
    } else {
        // echo 'Message sent successfully';
    }
    */

    // Close the cURL resource
    curl_close($curl);
}
