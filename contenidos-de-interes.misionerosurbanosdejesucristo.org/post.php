<?php
session_start();
date_default_timezone_set('America/Bogota');
require_once 'include/functions_connect.php';
$id = $_GET["id"];
$informacionContenido = consultar_contenido_informativo($id);
require_once 'cl/Mobile_Detect.php';
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
} else {
    $is_mobile = false;
}
//var_dump($informacionContenido);
// aqui hacer el select option
$categoriaExperiencias = '';
$obtenerCategorias = obtener_categorias_data();
$buscarOptions = '';
$contadorAllcategory = 0;
foreach ($obtenerCategorias as $key => $value) {

    //
    $categoriaConsultar = total_contenido_categoria($value);
    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }

    $contadorAllcategory = $contadorAllcategory + 1;

    // <span>(' . $categoriaConsultar . ')</span>
    $categoriaExperiencias = $categoriaExperiencias . '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;"  href="index.php?category=' . $value . '" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-book mr-3 text-black" style="color:#009BDF;"></i> ' . $value . ' </a></li>';
}
$categoriaExperiencias = '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;" href="index.php?category=" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-book mr-3 text-black" style="color:#009BDF;"></i> Todos los contenidos <span style="margin-right: 10px;">&nbsp;&nbsp;(' . $contadorAllcategory . ')</a></li>' . $categoriaExperiencias;

if ($informacionContenido["tipo"] == 1) {

    $numberRand = rand(7, 10);
    $categoria1 = '<p><strong>Categoria :</strong> ' . $informacionContenido["tipo_publicacion"] . '</p>';

    $categoria1 = '';
    $categoria2 = '';

    $icono = '<div  style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;"style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
} elseif (
    $informacionContenido["tipo"] == 2
) {
    $numberRand = rand(3, 6);
    $categoria1 = '<p><strong>Categoria :</strong> ' . $informacionContenido["tipo_publicacion2"] . '</p>';

    $categoria1 = '';
    $categoria2 = '';

    $icono = '<div  style="max-height: 60px;max-width: 60px;position: absolute;z-index: 100;"style="top: -13px;border-radius: 7px;" ><img src="images/b/' . $numberRand . '.png"></div>';
}

if (!empty($informacionContenido["imagen_portada"])) {
    $imagenPortada = $informacionContenido["imagen_portada"];
} else {
    $rutaDefault = 'images/default.jpg';
    $imagenPortada = $rutaDefault;
}

//$informacionContenido["adjuntos"] ='';
$archivosAdjuntos = '';

//var_dump($informacionContenido["adjuntos"]);

if (!empty($informacionContenido["adjuntos"])) {
    $ExplodeContenid = explode(',', $informacionContenido["adjuntos"]);

    foreach ($ExplodeContenid as $key => $value) {

        $consultarMeta = obtener_info_emetadata_usuario($informacionContenido["id"], $value);
        $infoMinuatura = $consultarMeta["miniatura_imagen_adjunto"];
        $infoNameminiatura = $consultarMeta["nombre_adjunto_info"];

        $nameShow = '';
        if (!empty($infoNameminiatura)) {
            $nameShow = $infoNameminiatura;
        } else {
            $nameShow = getName($value);
        }

        if (!empty($infoMinuatura)) {
            // poner imagen minuatura

            if ($is_mobile == true) {

                $boton = '<div class="col-lg-3 col-sm-3" style="margin-top: 10px; cursor: pointer;margin-top: 40px;border-radius:25px;padding-left: 0px;padding-right: 7px !important; ">
							<a target="_blank" href="' . $value . '" style="border-radius:25px;display: flex;justify-content: center;align-items: center;">
								<div class="card bd-0" style="width:190px;box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border-radius:25px;">
									<img style="width:100%;height:120px;filter: blur(0px);border-top-right-radius:25px;border-top-left-radius:25px;" class="img-fluid" src="' . $infoMinuatura . '" alt="Image">
									<div class="card-body bd bd-t-0" style="height: 40px;border-bottom-left-radius:25px;border-bottom-right-radius:25px;text-align: center;padding-top: 10px;font-size: 1.2rem;padding-left: 0px;padding-right: 0px;">
									' . $nameShow . '

									</div>



								</div><!-- card --></a>
								</div>';
            } else {
                $boton = '<div class="col-lg-3 col-sm-3" style="margin-top: 10px; cursor: pointer;margin-top: 40px;border-radius:25px;padding-left: 0px;">
							<a target="_blank" href="' . $value . '" style="border-radius:25px;display: flex;justify-content: center;align-items: center;">
								<div class="card bd-0" style="width:190px;box-shadow: rgba(0, 0, 0, 1) 0px 1px 4px;border-radius:25px;">
									<img style="width:100%;height:120px;filter: blur(0px);border-top-right-radius:25px;border-top-left-radius:25px;" class="img-fluid" src="' . $infoMinuatura . '" alt="Image">
									<div class="card-body bd bd-t-0" style="height: 40px;border-bottom-left-radius:25px;border-bottom-right-radius:25px;text-align: center;padding-top: 10px;font-size: 1.2rem;padding-left: 0px;padding-right: 0px;">
									' . $nameShow . '

									</div>



								</div><!-- card --></a>
								</div>';
            }
        } else {
            $boton = '<a target="_blank" href="' . $value . '" style="margin-bottom: 6px;cursor:pointer;color: #288feb;text-decoration: underline;"   type="submit" >Ver ' . $nameShow . '</a>';
        }

        // var_dump($consultarMeta);

        /*

         */

        $archivosAdjuntos = $archivosAdjuntos . $boton;

        // mostrar 2 botones
        // ver contenido
        // eliminar contenido
    }

    if (!empty($archivosAdjuntos)) {
        $archivosAdjuntos = '<div class="row">' . $archivosAdjuntos . '</div>';
    }
}

function filtrar_contenido_informacion($DataContenido)
{
    $DataContenido = $DataContenido;

    $regex = '/https?\:\/\/[^\",]+/i';
    $matches = array();
    preg_match_all($regex, $DataContenido, $matches);

    foreach ($matches[0] as $key => $value) {
        # code...
        //echo $value;
        //echo "<br>";
        //var_dump($matches[0]);
        $findme = '.pdf';
        $pos = strpos($value, $findme);
        // Nótese el uso de ===. Puesto que == simple no funcionará como se espera
        // porque la posición de 'a' está en el 1° (primer) caracter.
        if ($pos !== false) {
            //echo "La cadena '$findme' fue encontrada en la cadena '$value'";
            $obtenrSoloNombre = explode('/', $value);
            $conteo = count($obtenrSoloNombre);
            $obtenrSoloNombre = $obtenrSoloNombre[$conteo - 1];

            $templateRemplace = '<p><a title="' . $obtenrSoloNombre . '" href="' . $value . '">' . $obtenrSoloNombre . '</a></p>';

            // aqui reemplazar contenido

            // '. $value.'
            $DataContenido = str_replace($templateRemplace, '<embed src="' . $value . '" width="100%" height="600px"
 type="application/pdf"><br>', $DataContenido);
        }
    }

    //$DataContenido ='Hola Mundo';
    return $DataContenido;
}




include_once 'include/ip.php';
$obterIpDelUsuario = getRealIPAfiliadoVi();
$obterIpDelUsuario = explode(',', $obterIpDelUsuario);
$obterIpDelUsuario = $obterIpDelUsuario[0];
$registraAcceso = registrar_acceeso_ip($obterIpDelUsuario, $_GET["id"]);
// $obterIpDelUsuario = '191.95.158.96';

//$informacionIpPais = ConsultarPorApiPais($obterIpDelUsuario);
//$informacionIpPais = json_decode($informacionIpPais, true);
//var_dump($informacionIpPais["country_code2"]);

$datosPaiRegistro = devolver_id_from_country($obterIpDelUsuario);

// var_dump($datosPaiRegistro);
$isopaisName = $datosPaiRegistro["iso"];
$paisName = $datosPaiRegistro["pais"];
$paisName2 = $datosPaiRegistro["pais2"];

$timeZone = $datosPaiRegistro["timezone"];

$timezon = explode("UTC", $timeZone);
$timezon2 = trim($timezon[1]);
$signo = substr($timezon2, 0, 1);
$tiempo = str_replace('-', '', $timezon2);
$tiempo = str_replace('+', '', $timezon2);
$tiempo =  explode(":", $tiempo);
$tiempo = $tiempo[0];
if (substr($tiempo, 0, 1) === "0") {
    $tiempo = substr($tiempo, 1);
}
if ($signo == '-') {
    $signo = '+';
} elseif ($signo == '+') {
    $signo = '-';
}

$timeZone = $signo . $tiempo;


//$ip = '191.95.158.96';
//$timezone = geoip_time_zone_by_country_and_region('CO');
//echo $timezone;

//$infoTimezone = consultarApiTimezone($obterIpDelUsuario);
//$timeZone = $infoTimezone["timezone"];
//$timeZone = 'Europe/Madrid';

$optionsFechasExperiencias = obtenerFechasExperieicnias_v3($informacionContenido["id"], $timeZone);
$tamanoPantalla1 = 12;
$tamanoPantalla2 = 0;
$showFormulario = 'none';

$eventosDia = $optionsFechasExperiencias;

//var_dump($informacionContenido);

if ($informacionContenido["pedir_formulario"] == 1) {
    if (!empty($optionsFechasExperiencias)) {
        $tamanoPantalla1 = 8;
        $tamanoPantalla2 = 4;
        $showFormulario = 'none';
    }
}

//echo $showFormulario;

//var_dump($datosPaiRegistro);

$showFormulario = 'none';

function obtener_tipo_video($url)
{

    $xervidor_de_video = '';
    $data_url_final = '';
    $key = 'youtube';
    $key2 = 'vimeo';
    $key3 = 'youtu.be';
    $data_url_final = '';
    $url = $url;
    if (strpos($url, $key) == false && strpos($url, $key3) == false) {
        //aqui no exixte verificar xi exite vimeo;
        if (!strpos($url, $key2) == false) {
            //echo "Exixte Vimeo";
            $data_url_final = explode("/", $url);
            $numero_total_re = count($data_url_final);
            $numero_total_re = $numero_total_re - 1;
            $data_url_final = $data_url_final[$numero_total_re];
            $xervidor_de_video = $key2;
        }
    } else {
        if (strpos($url, $key3) !== false) {

            //aqui exixte youtu.be
            $data_url_final = explode("youtu.be/", $url);
            $data_url_final = $data_url_final[1];
            //$data_url_final = explode("&", $data_url_final);
            //$data_url_final = $data_url_final[0];
            $xervidor_de_video = $key;
        } else {

            //aqui exixte youtube
            $data_url_final = explode("=", $url);
            $data_url_final = $data_url_final[1];
            $data_url_final = explode("&", $data_url_final);
            $data_url_final = $data_url_final[0];
            $xervidor_de_video = $key;
        }
    }
    if ($xervidor_de_video == 'youtube') {
        if (
            empty($data_url_final) or $data_url_final == 'share'
        ) {
            //echo $url;
            $data_url_final = explode("share", $url);

            $url = $data_url_final[0];
            $data_url_final = explode("?", $url);
            $url = $data_url_final[0];
            //echo $url;
            $data_url_final = explode("/", $url);
            $data_url_final = $data_url_final[4];
            $data_url_final = explode("&", $data_url_final);
            $data_url_final = $data_url_final[0];
            // puede ser shorts
            //echo $data_url_final;
        }
    }

    $result = array("server" => $xervidor_de_video, "link" => $data_url_final);

    return $result;
}

$fameInformacion = '';
if (!empty($informacionContenido["links_video"])) {

    $infoEveryVideo = explode(',', $informacionContenido["links_video"]);

    foreach ($infoEveryVideo as $key) {
        # code...
        $tipoVideo = obtener_tipo_video($key);

        if ($is_mobile == true) {
            $is_mobile_height = '190';
            $porcentajeWidht = '100';
        } else {
            $is_mobile_height = '300';
            $porcentajeWidht = '100';
        }
        if ($tipoVideo["server"] == 'youtube' || $tipoVideo["server"] == 'youtu.be') {
            if (empty($fameInformacion)) {
                $fameInformacion = '<div class="col-lg-4 col-sm-4" style="margin-bottom: 15px;padding-left: 0px;"><div class="d-flex justify-content-center" style="justify-content: center;display: flex;"><iframe style="filter: blur(0px);border-top-right-radius: 25px;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;
" width="' . $porcentajeWidht . '%" height="' . $is_mobile_height . '" src="https://www.youtube.com/embed/' . $tipoVideo["link"] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div>';
            } else {
                $fameInformacion = $fameInformacion . '<div class="col-lg-4 col-sm-4" style="margin-bottom: 15px;padding-left: 0px;"><div class="d-flex justify-content-center" style="justify-content: center;display: flex;"><iframe style="filter: blur(0px);border-top-right-radius: 25px;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;
" width="' . $porcentajeWidht . '%" height="' . $is_mobile_height . '" src="https://www.youtube.com/embed/' . $tipoVideo["link"] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div>';
            }
        } elseif ($tipoVideo["server"] == 'vimeo') {
            if (empty($fameInformacion)) {

                $fameInformacion = '<div class="col-lg-4 col-sm-4" style="margin-bottom: 15px;padding-left: 0px;"><div class="d-flex justify-content-center" style="justify-content: center;display: flex;"><iframe style="filter: blur(0px);border-top-right-radius: 25px;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;
" width="' . $porcentajeWidht . '%" height="' . $is_mobile_height . '" src="https://player.vimeo.com/video/' . $tipoVideo["link"] . '" frameborder="0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div></div>';
            } else {

                $fameInformacion = $fameInformacion . '<div class="col-lg-4 col-sm-4" style="margin-bottom: 15px;padding-left: 0px;"><div class="d-flex justify-content-center" style="justify-content: center;display: flex;"><iframe style="filter: blur(0px);border-top-right-radius: 25px;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;
" width="' . $porcentajeWidht . '%" height="' . $is_mobile_height . '" src="https://player.vimeo.com/video/' . $tipoVideo["link"] . '" frameborder="0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div></div>';
            }
        }
    }
}

if (!empty($fameInformacion)) {
    $fameInformacion = '<div class="row">' . $fameInformacion . '</div>';
}

$categoriaEncontrada = 0;
$tieneCategoriaActual = '';
if (!empty($informacionContenido["tipo_publicacion"])) {
    $categoriaEncontrada = 1;
    $tieneCategoriaActual = $informacionContenido["tipo_publicacion"];
}

if (empty($tieneCategoriaActual)) {
    if (!empty($informacionContenido["tipo_publicacion2"])) {
        $categoriaEncontrada = 2;
        $tieneCategoriaActual = $informacionContenido["tipo_publicacion2"];
    }
}
if ($categoriaEncontrada > 0) {
    $preguntarCuantosExistenConLaMismaCategoria = total_contenido_categoria($tieneCategoriaActual);
    if ($preguntarCuantosExistenConLaMismaCategoria == 1) {
        $categoriaEncontrada = 0;
        $tieneCategoriaActual = '';
    }
}

$anteriorExperiencia = 0;
$siguienteExperiencia = 0;
if (empty($tieneCategoriaActual)) {

    $con = conection_database();
    $queryResultados = "SELECT * FROM `contenidos` WHERE  status='1' AND (tipo='1' OR tipo='2') ORDER BY posicionamiento ASC";
    $sqlPaginate2Rw = mysqli_query($con, $queryResultados);
    $encontradorAnterior = 1;
    $encontradoSiguiente = 0;
    while ($row1Paginate2Rw = mysqli_fetch_assoc($sqlPaginate2Rw)) {

        // echo $id;
        // echo $row1Paginate2Rw["id"];
        if ($encontradoSiguiente == 2) {
            $encontradoSiguiente = 0;
            $siguienteExperiencia = $row1Paginate2Rw["id"];
        }

        if ($encontradorAnterior == 1) {
            if (trim($row1Paginate2Rw["id"]) == trim($id)) {
                // echo "Encontrador Actual : ";
                $encontradorAnterior = 0;
            } else {
                $encontradorAnterior = 1;
            }
            if ($encontradorAnterior == 1) {
                $anteriorExperiencia = $row1Paginate2Rw["id"];
            }
        }

        if (trim($row1Paginate2Rw["id"]) == trim($id)) {
            // echo "Encontrador Actual : ";
            $encontradoSiguiente = 2;
        }

        /*
echo $row1Paginate2Rw["id"];
echo "<br>";
 */
    }
} else {
    // echo "Tiene categoria y buscar por las experiencias en la misma categoria";
    // echo $categoriaEncontrada;
    // echo $tieneCategoriaActual;
    $con = conection_database();

    if ($categoriaEncontrada == 1) {
        $queryResultados = "SELECT * FROM `contenidos` WHERE tipo_publicacion='" . $tieneCategoriaActual . "' AND  status='1' AND (tipo='1' OR tipo='2') ORDER BY posicionamiento ASC";
    } elseif ($categoriaEncontrada == 2) {

        $queryResultados = "SELECT * FROM `contenidos` WHERE tipo_publicacion2='" . $tieneCategoriaActual . "' AND  status='1' AND (tipo='1' OR tipo='2') ORDER BY posicionamiento ASC";
    }


    $sqlPaginate2Rw = mysqli_query($con, $queryResultados);
    $encontradorAnterior = 1;
    $encontradoSiguiente = 0;
    while ($row1Paginate2Rw = mysqli_fetch_assoc($sqlPaginate2Rw)) {

        // echo $id;
        // echo $row1Paginate2Rw["id"];
        if ($encontradoSiguiente == 2) {
            $encontradoSiguiente = 0;
            $siguienteExperiencia = $row1Paginate2Rw["id"];
        }

        if ($encontradorAnterior == 1) {
            if (trim($row1Paginate2Rw["id"]) == trim($id)) {
                // echo "Encontrador Actual : ";
                $encontradorAnterior = 0;
            } else {
                $encontradorAnterior = 1;
            }
            if ($encontradorAnterior == 1) {
                $anteriorExperiencia = $row1Paginate2Rw["id"];
            }
        }

        if (trim($row1Paginate2Rw["id"]) == trim($id)) {
            // echo "Encontrador Actual : ";
            $encontradoSiguiente = 2;
        }

        /*
echo $row1Paginate2Rw["id"];
echo "<br>";
 */
    }
}




// echo $anteriorExperiencia;
// echo "<br>";
// echo $siguienteExperiencia;
// echo $fameInformacion;

// horarios de videoconferencias
// obtener horarios de inicio y horarios finales

$horarioInicio = '';
$horarioFinal = '';
$textoDescripcion = '';
$horarioActivo = false;
$proximoHorario = true;
$con = conection_database();
$queryResultadosHorariosEnVivo = "SELECT * FROM `horarios_video` WHERE `id_formulario` = " . $informacionContenido["id"] . " ORDER BY `horarios_video`.`fecha_begin` ASC";
$sqlPaginate2RwHorariosEnVivo = mysqli_query($con, $queryResultadosHorariosEnVivo);


date_default_timezone_set('Etc/GMT' . $timeZone);
$fechaActualHorario = date('Y-m-d H:i:s');
date_default_timezone_set('America/Bogota');
$buscarHorarioEnVivo = true;

while ($row1Paginate2RwHorariosEnVivo = mysqli_fetch_assoc($sqlPaginate2RwHorariosEnVivo)) {

    $fechaFinalizacionVacia = false;
    if ($buscarHorarioEnVivo == true) {

        if ($row1Paginate2RwHorariosEnVivo["fecha_end"] == '0000-00-00 00:00:00' || $row1Paginate2RwHorariosEnVivo["fecha_end"] == '1969-12-31 19:00:00' || empty($row1Paginate2RwHorariosEnVivo["fecha_end"]) || $row1Paginate2RwHorariosEnVivo["fecha_end"] == null || $row1Paginate2RwHorariosEnVivo["fecha_end"] == 'NULL') {
            $fechaFinalizacionVacia = true;
        }

        $the_date1 = strtotime($row1Paginate2RwHorariosEnVivo["fecha_begin"]);
        $the_date2 = strtotime($row1Paginate2RwHorariosEnVivo["fecha_end"]);
        date_default_timezone_set('Etc/GMT' . $timeZone);

        $row1Paginate2RwHorariosEnVivo["fecha_begin"] = date("Y-m-d H:i:s", $the_date1);
        $row1Paginate2RwHorariosEnVivo["fecha_end"] = date("Y-m-d H:i:s", $the_date2);

        if (!empty($row1Paginate2RwHorariosEnVivo["fecha_begin"])) {
            $horarioActivo = true;
            $horarioInicio = $row1Paginate2RwHorariosEnVivo["fecha_begin"];

            // echo $fechaActualHorario;
            // echo $row1Paginate2RwHorariosEnVivo["fecha_begin"];
            // echo $row1Paginate2RwHorariosEnVivo["fecha_end"];

            // echo $row1Paginate2RwHorariosEnVivo["fecha_end"];

            if ($fechaFinalizacionVacia == true) {

                //echo "Aqui";
                $horarioActivo = true;
                $horarioInicio = $row1Paginate2RwHorariosEnVivo["fecha_begin"];
                $textoDescripcion = $row1Paginate2RwHorariosEnVivo["descHorario"];
                $horarioFinal = '3023-05-24 16:30:00';
                $buscarHorarioEnVivo = false;
            } else {

                if (strtotime($row1Paginate2RwHorariosEnVivo["fecha_begin"]) >= strtotime($fechaActualHorario)) {
                    // echo "Aqui1";

                    $horarioActivo = false;
                    $horarioInicio = '';
                    $textoDescripcion = '';
                    $horarioFinal = '';
                    $buscarHorarioEnVivo = true;
                } else {

                    // echo "Finalizar reunion";
                    if (strtotime($row1Paginate2RwHorariosEnVivo["fecha_end"]) <= strtotime($fechaActualHorario)) {
                        $horarioActivo = false;
                        $horarioInicio = '';
                        $textoDescripcion = '';
                        $horarioFinal = '';
                        $buscarHorarioEnVivo = true;
                    } else {
                        // echo "Aqui2";
                        $horarioActivo = true;
                        $horarioInicio = $row1Paginate2RwHorariosEnVivo["fecha_begin"];
                        $textoDescripcion = $row1Paginate2RwHorariosEnVivo["descHorario"];
                        $horarioFinal = $row1Paginate2RwHorariosEnVivo["fecha_end"];
                        $buscarHorarioEnVivo = false;
                    }
                }
            }
        }

        date_default_timezone_set('America/Bogota');
    }
}
//date_default_timezone_set('America/Bogota');

//echo $horarioInicio;

if ($horarioActivo == true) {
    $proximoHorario = false;
} else {
    $proximoHorario = true;
}

if ($proximoHorario == true) {
    $proximoHorario = false;
    // echo "no hay horario activo actual";
    $sqlPaginate2RwHorariosEnVivo2 = mysqli_query($con, $queryResultadosHorariosEnVivo);

    date_default_timezone_set('Etc/GMT' . $timeZone);
    $fechaActualInfo = date('Y-m-d H:i:s');
    date_default_timezone_set('America/Bogota');
    while ($row1Paginate2RwHorariosEnVivo2 = mysqli_fetch_assoc($sqlPaginate2RwHorariosEnVivo2)) {

        $the_date1 = strtotime($row1Paginate2RwHorariosEnVivo2["fecha_begin"]);
        $the_date2 = strtotime($row1Paginate2RwHorariosEnVivo2["fecha_end"]);
        date_default_timezone_set('Etc/GMT' . $timeZone);
        $row1Paginate2RwHorariosEnVivo2["fecha_begin"] = date("Y-m-d H:i:s", $the_date1);
        $row1Paginate2RwHorariosEnVivo2["fecha_end"] = date("Y-m-d H:i:s", $the_date2);

        if (strtotime($row1Paginate2RwHorariosEnVivo2["fecha_begin"]) >= strtotime($fechaActualInfo)) {
            if ($row1Paginate2RwHorariosEnVivo["fecha_end"] == '0000-00-00 00:00:00' || $row1Paginate2RwHorariosEnVivo["fecha_end"] == '1969-12-31 19:00:00' || empty($row1Paginate2RwHorariosEnVivo["fecha_end"]) || $row1Paginate2RwHorariosEnVivo["fecha_end"] == null || $row1Paginate2RwHorariosEnVivo["fecha_end"] == 'NULL') {
                $proximoHorario = true;
                $horarioInicio = $row1Paginate2RwHorariosEnVivo2["fecha_begin"];
                $textoDescripcion = $row1Paginate2RwHorariosEnVivo2["descHorario"];
            } else {
                if (strtotime($row1Paginate2RwHorariosEnVivo2["fecha_end"]) > strtotime($fechaActualInfo)) {
                } else {
                    $proximoHorario = true;
                    $horarioInicio = $row1Paginate2RwHorariosEnVivo2["fecha_begin"];
                    $textoDescripcion = $row1Paginate2RwHorariosEnVivo2["descHorario"];
                }
            }
        }

        date_default_timezone_set('America/Bogota');
        //var_dump($row1Paginate2RwHorariosEnVivo2["fecha_begin"]);
    }
}
// var_dump($proximoHorario);
// echo $timeZone;

$horaVideoConferencia = $horarioInicio;
$horaVideoConferencia_2 = $horarioInicio;
// var_dump($horaVideoConferencia);
$the_date2 = strtotime($horaVideoConferencia);
//date_default_timezone_set($timeZone);
$horaVideoConferencia_2 = date("Y-m-d H:i:s", $the_date2);
$horaVideoConferencia = date("Y-m-d H:i:s", $the_date2);

//date_default_timezone_set('America/Bogota');
//echo $horaVideoConferencia;

if ($proximoHorario == true) {

    function getDayName2($dayOfWeek)
    {

        switch ($dayOfWeek) {
            case 6:
                return 'Sábado';
            case 0:
                return 'Domingo';
            case 1:
                return 'Lunes';
            case 2:
                return 'Martes';
            case 3:
                return 'Miércoles';
            case 4:
                return 'Jueves';
            case 5:
                return 'Viernes';
            default:
                return '';
        }
    }

    function getMontName2($dayOfMontth)
    {

        switch ($dayOfMontth) {

            case 1:
                return 'enero';
            case 2:
                return 'febrero';
            case 3:
                return 'marzo';
            case 4:
                return 'abril';
            case 5:
                return 'mayo';
            case 6:
                return 'junio';
            case 7:
                return 'julio';
            case 8:
                return 'agosto';
            case 9:
                return 'septiembre';
            case 10:
                return 'octubre';
            case 11:
                return 'noviembre ';
            case 12:
                return 'diciembre';

            default:
                return '';
        }
    }
    $dayName2 = getDayName2(date('w', strtotime($horaVideoConferencia)));
    $moNnthame2 = getMontName2(date('n', strtotime($horaVideoConferencia)));

    $horaVideoConferencia_2 = $dayName2 . ' ' . date('j', strtotime($horaVideoConferencia)) . ' de ' . $moNnthame2 . ' de ' . date('Y', strtotime($horaVideoConferencia)) . ' - hora: ' . date('H:i', strtotime($horaVideoConferencia));


    $horaVideoConferencia = $dayName2 . ' ' . date('j', strtotime($horaVideoConferencia)) . ' de ' . $moNnthame2 . ' de ' . date('Y', strtotime($horaVideoConferencia)) . ' - hora: ' . date('H:i', strtotime($horaVideoConferencia));
} else {
    $horaVideoConferencia_2 = date("H:i", strtotime($horaVideoConferencia));
    $horaVideoConferencia = date("H:i", strtotime($horaVideoConferencia));
}

$numeroAleatorioCopy = '_' . uniqid();

if ($horarioActivo == false) {
    if (empty($informacionContenido["online_activo"])) {
    } else if ($informacionContenido["online_activo"] == 1) {
        $horarioActivo = true;
        $textoDescripcion = $informacionContenido["desc_activo"];
        $horaVideoConferencia = '';
        $horaVideoConferencia_2 = '';
    }
}
$numeroIdentificarConferencia = $informacionContenido["id"];

$urlCompleta = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];




?>


<!DOCTYPE html>
<html lang="es" translate="no">

<head>
    <!-- Required meta tags   -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-language" content="es_ES">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Contenido de interes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="css/data_image.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="swal/sweetalert2.min.css">
    <link href="lib/fonti/css/all.css" rel="stylesheet">
    <!--load all styles -->
    <!-- Arribaema -->
    <link rel="icon" href="images/favicon.png" sizes="192x192" />
    <link href="admin/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="lib/fullcall/fullcalendar.css">


    <link rel="stylesheet" type="text/css" href="lib/event-calendar-evo/evo-calendar/css/evo-calendar2.css">
    <link rel="stylesheet" type="text/css" href="lib/event-calendar-evo/evo-calendar/css/evo-calendar.royal-navy2.min.css">




    <style>
        table.dataTable tbody tr {
            background-color: transparent;
        }

        .has-fixed.is-shrink .header-main {
            padding: 0px 0;
        }

        .header-main {
            padding: 2px 0;
        }

        #home-v1-header-carousel .carousel-caption {
            top: 36%;
        }

        label {
            color: #fff;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: #fff !important;
        }

        .post-center-content {
            /* background-color: #fff; */
            /* -webkit-box-shadow: 0 0 14px 0 rgb(0 0 0 / 7%); */
            /*-moz-box-shadow: 0 0 14px 0 rgba(0, 0, 0, 0.07);
        /* box-shadow: 0 0 14px 0 rgb(0 0 0 / 7%); */
            display: inline-block;
            margin: 0 -15px;
            padding: 34px 15px 0 30px;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }


        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .single-book-box {
            padding-top: 5px !important;
        }

        .single-book-box .post-detail .btn {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }


        .event-list .active {
            background-color: #fff;
            -webkit-box-shadow: 0 3px 12px -4px rgba(0, 0, 0, .65);
            box-shadow: 0 3px 12px -4px rgba(0, 0, 0, .65);
            color: #009BDF;
        }

        .royal-navy .event-list .active:hover>.event-info>p {
            color: #009BDF;
        }

        .royal-navy .event-container>.event-info>p {
            color: #000;
        }

        .btn-primary {
            color: #fff;
        }
    </style>

    <style>
        @import url('https://fonts.googleapis.com/css?family=Nunito');


        .slim-header.with-sidebar .slim-sidebar-menu {
            margin-right: 0px !important;
        }


        .zoom:hover {
            transform: scale(1.1);
            /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }



        .itemdonar .sidebar-nav-link:hover,
        .sidebar-nav-link:focus {
            background-color: #f8f9fa;
        }



        .text-dark {
            color: #808080 !important;
        }



        #breadcrumb li:last-child a {
            padding-left: 5px;
            padding-right: 5px;
        }

        .section-wrapper {
            border-radius: 15px;
        }

        .event-info .event-desc {
            text-transform: lowercase;
        }

        .event-info .event-desc:first-letter {
            text-transform: uppercase;
        }
    </style>
    <!-- Stylesheet -->
    <link href="style.css" rel="stylesheet" type="text/css" />


    <link href="admin/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="lib/fullcall/fullcalendar.css">
    <style>
        .slim-header.with-sidebar .slim-logo {
            width: 205px;
            margin-right: 10px;
            margin-left: -340px;
        }

        .container-fluid {
            margin-right: 0px;
            margin-left: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }


        .nav-links .page-numbers.prev:hover {
            border-radius: 15px;
            background-color: #00adef;
            padding-right: 15px;
            color: #fff;
        }


        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .single-book-box {
            border: 10px solid #c2c2c2;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .books-gird .single-book-box {
            opacity: 1;
        }

        .books-gird ul li:nth-child(3n) .single-book-box .post-detail {
            left: 0px;
            right: 66px;
        }

        .books-gird .single-book-box .post-detail {
            top: 0;
            transform: none;
            width: 100%;
            left: 0;
            position: relative;
            padding: 30px 15px;
        }

        table.dataTable tbody tr {
            background-color: transparent;
        }

        .has-fixed.is-shrink .header-main {
            padding: 0px 0;
        }

        .header-main {
            padding: 2px 0;
        }

        #home-v1-header-carousel .carousel-caption {
            top: 36%;
        }

        label {
            color: #fff;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: #fff !important;
        }

        .sidebar .widget-sub-title::after {
            content: '';
        }

        input {
            border-radius: 15px;
        }

        .form-control {
            border-radius: 15px;
        }

        .single-book-box {
            padding-top: 5px;
        }

        .single-book-box .post-detail .btn {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }

        .row {
            margin-left: 0px;
            margin-right: 0px;
        }

        .form-control {
            border: 3px solid #f4f4f4 !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            height: 56px !important;
            /*padding: 5px 10px !important;*/
        }

        .booksmedia-detail-box {
            width: 100%;
        }

        .sidebar-nav-sub .nav-sub-link.active {
            background-color: #ffffff;
        }


        .margenLis {
            border-style: solid;
            border-color: #d3d3d3;
            border-width: 1.15px;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }

        .evo-calendar {
            border-radius: 25px;
        }

        .calendar-events {
            display: none;
        }

        .event-container>.event-icon>div.event-bullet-event,
        .event-indicator>.type-bullet>div.type-event {
            background-color: #288feb;
        }

        .event-container {
            border-style: solid;
            border-color: #288feb;
        }

        .event-container::before {
            background-color: transparent !important;
        }

        html {
            font-family: "Nunito" !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-weight: 500;
        }

        body {
            font-family: "Nunito" !important;
            margin: 0;
            font-weight: 500;
            color: #808080;
        }

        a,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        li,
        font {
            color: #808080;
            font-family: "Nunito" !important;
        }

        * {
            font-family: "Nunito";
        }

        .royal-navy .calendar-sidebar>span#sidebarToggler {

            background-color: #29daff !important;
            -webkit-box-shadow: 0 5px 10px -3px #29daff;
            box-shadow: 0 5px 10px -3px #29daff;
        }

        <?php if ($is_mobile == true) { ?>.col-xs-1,
        .col-sm-1,
        .col-md-1,
        .col-lg-1,
        .col-xs-2,
        .col-sm-2,
        .col-md-2,
        .col-lg-2,
        .col-xs-3,
        .col-sm-3,
        .col-md-3,
        .col-lg-3,
        .col-xs-4,
        .col-sm-4,
        .col-md-4,
        .col-lg-4,
        .col-xs-5,
        .col-sm-5,
        .col-md-5,
        .col-lg-5,
        .col-xs-6,
        .col-sm-6,
        .col-md-6,
        .col-lg-6,
        .col-xs-7,
        .col-sm-7,
        .col-md-7,
        .col-lg-7,
        .col-xs-8,
        .col-sm-8,
        .col-md-8,
        .col-lg-8,
        .col-xs-9,
        .col-sm-9,
        .col-md-9,
        .col-lg-9,
        .col-xs-10,
        .col-sm-10,
        .col-md-10,
        .col-lg-10,
        .col-xs-11,
        .col-sm-11,
        .col-md-11,
        .col-lg-11,
        .col-xs-12,
        .col-sm-12,
        .col-md-12,
        .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-left: 1px !important;
            padding-right: 1px !important;
        }


        <?php } ?>@keyframes spinner-border {
            to {
                transform: rotate(360deg);
            }
        }

        .spinner-border {
            display: inline-block;
            width: 20px;
            height: 20px;
            vertical-align: text-bottom;
            border: 20px solid currentColor;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner-border .75s linear infinite !important;
            animation: spinner-border .75s linear infinite !important;
        }

        .text-primary {
            color: #808080 !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" />


    <style>
        .jaas-container {
            border-radius: 25px;
        }

        #jitsiConferenceFrame0 {
            border-radius: 25px;
        }

        <?php if ($is_mobile == true) { ?>.premeeting-screen-avatar {
            display: none !important;
        }

        .css-1y6sf0m-overflowMenuDrawer {
            height: 320px !important;
        }




        <?php } ?>.swal2-show {
            border-radius: 25px !important;
        }



        .evo-calendar {
            position: relative;
            background-color: #fbfbfb;
            color: #5a5a5a;
            width: 100%;
            -webkit-box-shadow: 0 10px 50px -20px #116ed1;
            box-shadow: 0 10px 50px -20px #116ed1;
            margin: 0 auto;
            overflow: hidden;
            z-index: 1;
        }

        .btn-primary:hover {
            background-color: #116ed1;
            border-color: #116ed1;
            color: #fff;
        }

        .nav-links .page-numbers:hover,
        .nav-links .page-numbers.current {
            color: #fff !important;
            background-color: #009BDF !important;
            border-color: #009BDF !important;
        }

        .margenLis:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-sub .nav-sub-link.active:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-item.with-sub>.sidebar-nav-link::after {
            content: '\f3d0';
            font-family: 'Ionicons';
            margin-left: auto;
            position: relative;
            opacity: 1;
            color: #000;
        }

        .linunito {
            font-family: "Nunito" !important;
            /*color: #009BDF !important;*/
            list-style-type: none;
        }

        .linunito:hover {
            font-family: "Nunito" !important;
            color: #009BDF !important;
        }
    </style>
    <script type="text/javascript" defer>
        // (function() {
        //   var options = {
        //     whatsapp: "573194475236", // WhatsApp number
        //     call_to_action: "Contáctenos", // Call to action
        //     position: "right", // Position may be 'right' or 'left'
        //   };
        //   var proto = document.location.protocol,
        //     host = "whatshelp.io",
        //     url = proto + "//static." + host;
        //   var s = document.createElement('script');
        //   s.type = 'text/javascript';
        //   s.async = true;
        //   s.src = url + '/widget-send-button/js/init.js';
        //   s.onload = function() {
        //     WhWidgetSendButton.init(host, proto, options);
        //   };
        //   var x = document.getElementsByTagName('script')[0];
        //   x.parentNode.insertBefore(s, x);
        // })();
    </script>

    <link rel="stylesheet" href="intlTelInput.css">




</head>

<body>

    <div class="slim-header with-sidebar" style="background-color: #fff;color:#000;">
        <div class="container-fluid">
            <?php include('htmlinclude/siv33.php'); ?>
        </div><!-- container-fluid -->
    </div><!-- slim-header -->


    <div class="slim-body">
        <?php include('htmlinclude/siv22.php'); ?>

        <div class="slim-mainpanel" style="background-color: #f0f2f7;">

            <?php if ($is_mobile == true) { ?>
                <div class="container" style="padding-top: 5px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 26px;padding-left: 5px !important;padding-right: 5px !important;padding-bottom: 5px !important;">
                <?php } else { ?>
                    <div class="container" style="padding-top: 25px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 50px;">
                    <?php } ?>




                    <div id="cuadrocentralcontenido" class="cuadrocentralcontenido" style="border-radius:15px;">

                        <div id="content" class="site-content">
                            <div id="primary" class="content-area">
                                <main id="main" class="site-main">
                                    <div class="books-media-gird">
                                        <div class="container-fluid">
                                            <div style="display:none;" class="row">
                                                <!-- Start: Search Section -->
                                                <section class="search-filters" style="margin-bottom: 20px;">
                                                    <div class="container-fluid">

                                                    </div>
                                                </section>
                                                <!-- End: Search Section -->
                                            </div>
                                            <div class="row">


                                                <?php if ($is_mobile == true) { ?>

                                                    <div class="col-md-12" style="border-radius: 25px;padding: 0px;margin: 15px;">
                                                    <?php } else { ?>

                                                        <div class="col-md-12" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 30px;">
                                                        <?php } ?>



                                                        <div class="filter-options margin-list" style="margin-bottom: 0px;">
                                                            <div class="row">


                                                                <div class="booksmedia-detail-box">

                                                                    <a onclick="regresarAtras()" style="border-radius: 25px;padding-top: 6px;padding-bottom: 6px;padding-left: 15px;padding-right: 15px;color:#fff;border-left-width: 10px;margin-left: 10px;margin-bottom: 30px;" class="btn btn-primary">Regresar</a>
                                                                    <a onclick="recomendarUsuarioNew()" style="border-radius: 25px;padding-top: 6px;padding-bottom: 6px;padding-left: 15px;padding-right: 15px;color:#fff;border-left-width: 10px;margin-left: 10px;margin-bottom: 30px;position: absolute;right: 20px;" class="btn btn-primary">Recomendar</a>

                                                                    



                                                                    <?php if ($horarioActivo == true) { ?>

                                                                        <div class="row" style="<?php echo ($is_mobile == false) ? 'margin-right: 15px;margin-left: 15px;' : ''; ?>">

                                                                            <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 10px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;width: 100%;" class="table-tabs" id="responsiveTabs">

                                                                                <h1 style="text-transform:initial;font-weight: 500;color:#009BDF;text-align:center;word-break: break-word;"><?php echo $informacionContenido["titulo"]; ?> online </h1>

                                                                                <br>


                                                                                <div class="d-flex justify-content-center videoClassInfo" style="width:100%;word-break: break-word;">
                                                                                    <h1>Cargando <?php echo strtolower($informacionContenido["titulo"]); ?> online &nbsp;&nbsp;</h1>
                                                                                    <div class="spinner-border text-primary" role="status">

                                                                                    </div>
                                                                                </div>



                                                                                <div class="row">


                                                                                    <div class="col-lg-9 col-12">




                                                                                        <div class="d-flex justify-content-center" style="width:100%">


                                                                                            <div id="jaas-container<?php echo $numeroAleatorioCopy; ?>" class="jaas-container" style="width:100%"></div>

                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-lg-3 col-12 videoClassInfo2" style="word-break: break-word;display:none;">


                                                                                        <?php if ($is_mobile == true) { ?>

                                                                                            <br><br>
                                                                                        <?php } ?>

                                                                                        <?php if (!empty($horaVideoConferencia_2)) { ?>
                                                                                            <h3 style="text-align: center;word-break: break-word;">Hora de inicio <?php echo strtolower($informacionContenido["titulo"]); ?> <h1 style="text-align: center;color: #009BDF;word-break: break-word;"><?php echo $horaVideoConferencia_2; ?></h1>
                                                                                            </h3>
                                                                                            <br>

                                                                                        <?php } ?>
                                                                                        <div style="word-break: break-word;text-align: justify;font-family: Nunito !important;"><?php echo $textoDescripcion; ?></div>

                                                                                    </div>

                                                                                </div>




                                                                                <script src='https://8x8.vc/vpaas-magic-cookie-37e7411103304c2e8ea5d73ed2586069/external_api.js' async></script>



                                                                                <script type="text/javascript">
                                                                                    <?php if ($is_mobile == false) { ?>

                                                                                        var alturaVideoConferencia = 500;
                                                                                        var anchoVideoConferencia = 100;
                                                                                    <?php } else { ?>
                                                                                        var alturaVideoConferencia = 550;
                                                                                        var anchoVideoConferencia = 100;



                                                                                    <?php } ?>


                                                                                    var nombreData = '<?php echo str_replace(" ", "", $informacionContenido["titulo"]) . '_' . $numeroIdentificarConferencia; ?>';
                                                                                    window.onload = () => {
                                                                                        const api = new JitsiMeetExternalAPI("8x8.vc", {
                                                                                            roomName: "Videoconferencia_" + nombreData,
                                                                                            width: anchoVideoConferencia + '%',
                                                                                            height: alturaVideoConferencia,
                                                                                            lang: 'es',

                                                                                            configOverwrite: {
                                                                                                disableDeepLinking: true,
                                                                                                APP_NAME: '<?php echo $informacionContenido["titulo"]; ?>',
                                                                                                resolution: 1080,
                                                                                                toolbarButtons: [
                                                                                                    'microphone', 'camera',
                                                                                                    'fullscreen',
                                                                                                    'closedcaptions', 'desktop', 'chat',
                                                                                                    'raisehand', 'invite',
                                                                                                    'hangout',
                                                                                                    'embedmeeting',
                                                                                                    'livestreaming',
                                                                                                    'sharedvideo', 'shareaudio', 'settings',
                                                                                                    'videoquality', 'filmstrip', 'feedback', 'stats', 'shortcuts',
                                                                                                    'tileview', 'select-background', 'download', 'help', 'mute-everyone', 'mute-video-everyone', 'security'
                                                                                                ],
                                                                                                constraints: {
                                                                                                    video: {
                                                                                                        maxBitrate: 1000000, // 1 Mbps
                                                                                                    },
                                                                                                },
                                                                                                disableSimulcast: false,
                                                                                                videoQuality: {
                                                                                                    preferredCodec: 'VP9',
                                                                                                    maxBitratesVideo: {
                                                                                                        low: 500000, // 500 Kbps
                                                                                                        standard: 1500000, // 1.5 Mbps
                                                                                                        high: 5000000, // 5 Mbps
                                                                                                    },
                                                                                                },
                                                                                            },
                                                                                            interfaceConfigOverwrite: {
                                                                                                disableDeepLinking: true,
                                                                                                APP_NAME: '<?php echo $informacionContenido["titulo"]; ?>',
                                                                                                resolution: 1080,
                                                                                                toolbarButtons: [
                                                                                                    'microphone', 'camera',
                                                                                                    'fullscreen',
                                                                                                    'closedcaptions', 'desktop', 'chat',
                                                                                                    'raisehand', 'invite',
                                                                                                    'hangout',
                                                                                                    'embedmeeting',
                                                                                                    'livestreaming',
                                                                                                    'sharedvideo', 'shareaudio', 'settings',
                                                                                                    'videoquality', 'filmstrip', 'feedback', 'stats', 'shortcuts',
                                                                                                    'tileview', 'select-background', 'download', 'help', 'mute-everyone', 'mute-video-everyone', 'security'
                                                                                                ],
                                                                                                constraints: {
                                                                                                    video: {
                                                                                                        maxBitrate: 1000000, // 1 Mbps
                                                                                                    },
                                                                                                },
                                                                                                disableSimulcast: false,
                                                                                                videoQuality: {
                                                                                                    preferredCodec: 'VP9',
                                                                                                    maxBitratesVideo: {
                                                                                                        low: 500000, // 500 Kbps
                                                                                                        standard: 1500000, // 1.5 Mbps
                                                                                                        high: 5000000, // 5 Mbps
                                                                                                    },
                                                                                                },
                                                                                            },

                                                                                            parentNode: document.querySelector('#jaas-container' + '<?php echo $numeroAleatorioCopy; ?>'),
                                                                                        });








                                                                                        api.getRoomsInfo().then(rooms => {
                                                                                            // alert('iniciada');
                                                                                            setTimeout(() => {

                                                                                                $(".videoClassInfo").css('display', 'none');
                                                                                                $(".videoClassInfo2").css('display', 'block');

                                                                                                $(".videoClassInfo").removeClass('d-flex');
                                                                                                $(".videoClassInfo").removeClass('justify-content-center');

                                                                                            }, 1000);



                                                                                        });



                                                                                        api.executeCommand('displayName', '');
                                                                                        // Configura la calidad del video en alta resolución
                                                                                        api.executeCommand('setVideoQuality', 'high');



                                                                                    }
                                                                                </script>
                                                                            </div>

                                                                        </div>

                                                                    <?php } else if ($proximoHorario == true) { ?>


                                                                        <div class="row" style="<?php echo ($is_mobile == false) ? 'margin-right: 15px;margin-left: 15px;' : ''; ?>">

                                                                            <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 10px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;width: 100%;" class="table-tabs" id="responsiveTabs">

                                                                                <h1 style="text-transform:initial;color:#009BDF;text-align:center;word-break: break-word;"><?php echo $informacionContenido["titulo"]; ?> online</h1>

                                                                                <br>






                                                                                <div class="row">


                                                                                    <div class="col-lg-12 col-12">




                                                                                        <h3 style="text-align: center;word-break: break-word;">Horario próximo encuentro online: <h1 style="text-align: center;color: #009BDF;word-break: break-word;font-weight: 500;"><?php echo $horaVideoConferencia_2; ?></h1>
                                                                                        </h3>
                                                                                        <br>
                                                                                        <div style="word-break: break-word;text-align: justify;font-family: Nunito !important;"><?php echo $textoDescripcion; ?></div>


                                                                                    </div>


                                                                                    <div class="col-lg-12 col-12">
                                                                                        <h1 style="text-align: center;color: #009BDF;word-break: break-word;font-weight: 500;">Intenciones para la Santa Misa</h1>


                                                                                        <textarea style="min-height: 140px!important;" rows="5" placeholder="¿Tienes alguna intención para la próxima Santa Misa?" class="form-control" name="intencion_santa_misa" id="intencion_santa_misa" data-lt-tmp-id="lt-740435" spellcheck="false" data-gramm="false"></textarea>

                                                                                        <a onclick="crear_intencion_santa_misa()" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;cursor:pointer;width: 100%;margin-top: 15px;" class="btn btn-primary">Enviar</a>

                                                                                    </div>



                                                                                    <div class="col-lg-12 col-12" id="listadoProximasIntenciones" style="max-height: 400px;overflow: auto;">
                                                                                    </div>


                                                                                </div>







                                                                            </div>

                                                                        </div>


                                                                    <?php } ?>






                                                                    <div class="row" style="<?php echo ($is_mobile == false) ? 'margin-right: 15px;margin-left: 15px;' : ''; ?>">

                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;width:100%" class="table-tabs" id="responsiveTabs">





                                                                            <?php if ($is_mobile == true) { ?>
                                                                                <div class="detailed-box" style="">
                                                                                <?php } else { ?>

                                                                                    <div class="detailed-box" style="border-left-width: 15px;margin-left: 15px;margin-right: 15px;">
                                                                                    <?php } ?>

                                                                                    <div class="col-xs-12 col-sm-5 col-md-3 border-radius: 15px;">
                                                                                        <div class="post-thumbnail">
                                                                                            <?php //echo $icono;
                                                                                            ?>
                                                                                            <img style="width: 100%;border-radius: 15px;" src="<?php echo $imagenPortada; ?>" alt="Book Image">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-7 col-md-9" style="padding-right: 0px;border-radius: 15px;">
                                                                                        <div class="post-center-content" style="width: 100%;background-color: transparent;box-shadow: none;border-radius: 15px;background-color: #fff;padding-top: 15px;padding-left: 10px;padding-right: 10px;">




                                                                                            <h2 style="text-transform:initial;font-weight: 500;color:#009BDF;"><?php echo $informacionContenido["titulo"]; ?></h2>


                                                                                            <?php if (!empty($informacionContenido["author"])) { ?>
                                                                                                <p><strong>Autor :</strong> <?php echo $informacionContenido["author"]; ?></p>

                                                                                            <?php } ?>

                                                                                            <?php if (!empty($informacionContenido["fecha_contenido"]) && $informacionContenido["fecha_contenido"] != '0000-00-00 00:00:00') { ?>
                                                                                                <p><strong>Fecha publicado:</strong> <?php echo date('Y-m-d', strtotime($informacionContenido["fecha_contenido"])); ?></p>
                                                                                            <?php } ?>

                                                                                            <?php echo $categoria1; ?>

                                                                                            <br>
                                                                                            <?php
                                                                                            $mainContent = nl2br($informacionContenido["resumen"]);
                                                                                            echo $pqueDescription = '<p style="word-break:break-word;height: 60px;text-align:justify;font-family: Nunito !important;">' . $mainContent . '</p>';
                                                                                            ?>


                                                                                        </div>
                                                                                    </div>

                                                                                    </div>
                                                                                </div>


                                                                                <div class="clearfix" style="background: #fff;"></div>
                                                                        </div>
                                                                        <div class="clearfix" style="background: #fff;"></div>

                                                                        <div class="row">

                                                                            <?php if ($is_mobile == false) { ?>



                                                                                <div class="col-lg-12 col-12" style="border-radius: 25px;">

                                                                                    <?php if (!empty($informacionContenido["contenido"] && $informacionContenido["contenido"] != '<p><br></p>' && $informacionContenido["contenido"] != '<p>&nbsp;</p>' && $informacionContenido["contenido"] != '<p></p>' && $informacionContenido["contenido"] != '<p>')) { ?>
                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;word-break: break-word;text-align: justify;font-family: Nunito !important;" class="table-tabs" id="responsiveTabs">
                                                                                            <?php echo filtrar_contenido_informacion($informacionContenido["contenido"]); ?>
                                                                                        </div>

                                                                                    <?php } ?>


                                                                                    <?php if (!empty($fameInformacion)) { ?>
                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;" class="table-tabs" id="responsiveTabs">
                                                                                            <h4 style="color: #009BDF">Videos pregrabados:</h4>
                                                                                            <br><br>
                                                                                            <?php echo $fameInformacion; ?>
                                                                                        </div>
                                                                                    <?php } ?>

                                                                                    <?php if (!empty($archivosAdjuntos)) { ?>


                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">
                                                                                            <h4 style="color: #009BDF">Archivos adjuntos:</h4>
                                                                                            <?php echo $archivosAdjuntos; ?>
                                                                                        </div>
                                                                                    <?php } ?>


                                                                                </div>


                                                                                <div style="display:<?php echo $showFormulario; ?>;" class="col-lg-12 col-12" style="border-radius: 25px;">

                                                                                    <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;" class="table-tabs" id="responsiveTabs">
                                                                                        <div class="form-group">
                                                                                            <h4 style="color: #009BDF;font-weight: bold;"><?php echo $informacionContenido["desc_form"] . ((!empty($informacionContenido["desc_form"]) ? ':' : '')); ?></h4>
                                                                                        </div>



                                                                                        <div class="row">





                                                                                            <div class="col-lg-4 col-12">

                                                                                                <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">


                                                                                                    <div class="form-group">
                                                                                                        <label for="fechaUsuarioRegistrar" class="text-dark"><strong style="word-break: break-word;color: #009BDF;font-weight: bold;"> Selecciona una fecha para registrarte en la experiencia </strong></label>
                                                                                                        <div id="evoCalendar"></div>


                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>





                                                                                            <div class="col-lg-8 col-12" style="margin-top: 80px;">

                                                                                                <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">

                                                                                                    <div class="row">


                                                                                                        <div class="col-12" style="padding-left: 0px;">
                                                                                                            <p style="color: #009BDF;font-weight: bold;">Escribe tus datos:</p>
                                                                                                        </div>

                                                                                                        <?php if ($informacionContenido["c1_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="nombreUsuarioRegistrar" name="nombreUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c1"]; ?>">
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        <?php } else { ?>
                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="nombreUsuarioRegistrar" name="nombreUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu nombre">
                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c2_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="apellidoUsuarioRegistrar" name="apellidoUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c2"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>
                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="apellidoUsuarioRegistrar" name="apellidoUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu apellido">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c3_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">
                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="emailUsuarioRegistrar" name="emailUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c3"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>

                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="emailUsuarioRegistrar" name="emailUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu correo electrónico">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c4_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="phoneUsuarioRegistrar" name="phoneUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c4"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>


                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="phoneUsuarioRegistrar" name="phoneUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu whatsApp / móvil">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c5_activo"] == 1) { ?>

                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <select onchange="cambiar_horario_de_experiencias()" style="border-radius: 25px;" class="form-control" id="paisUsuarioRegistrar" name="paisUsuarioRegistrar">
                                                                                                                        <option></option>
                                                                                                                    </select>

                                                                                                                </div>

                                                                                                                <div style="display:none;" class="form-group">

                                                                                                                    <select style="border-radius: 25px;" class="form-control" id="estadoUsuarioRegistrar" name="estadoUsuarioRegistrar">
                                                                                                                        <option></option>
                                                                                                                    </select>

                                                                                                                </div>

                                                                                                            </div>

                                                                                                        <?php } else { ?>


                                                                                                            <div style="display:none" class="form-group">

                                                                                                                <select onchange="cambiar_horario_de_experiencias()" style="border-radius: 25px;" class="form-control" id="paisUsuarioRegistrar" name="paisUsuarioRegistrar">
                                                                                                                    <option></option>
                                                                                                                </select>

                                                                                                            </div>

                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <select style="border-radius: 25px;" class="form-control" id="estadoUsuarioRegistrar" name="estadoUsuarioRegistrar">
                                                                                                                    <option></option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c6_activo"] == 1) { ?>

                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="ciudadUsuarioRegistrar" name="ciudadUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c6"]; ?>">
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        <?php } else { ?>



                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="ciudadUsuarioRegistrar" name="ciudadUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu ciudad">
                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c7_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1" name="campo_editable_1" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c7"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>



                                                                                                            <div style="display:none" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1" name="campo_editable_1" aria-describedby="emailHelp" placeholder="Campo 1">
                                                                                                            </div>
                                                                                                        <?php } ?>


                                                                                                        <?php if ($informacionContenido["c8_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2" name="campo_editable_2" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c8"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>




                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2" name="campo_editable_2" aria-describedby="emailHelp" placeholder="Campo 2">
                                                                                                            </div>
                                                                                                        <?php } ?>

                                                                                                        <div class="col-lg-12 col-12">
                                                                                                            <div class="form-check">
                                                                                                                <input style="border-radius: 25px;height: 15px;width: 25px;margin-top: 1.5px;" type="checkbox" class="form-check-input" id="terminosYCondicionesAceptados" name="terminosYCondicionesAceptados">
                                                                                                                <label class="form-check-label" for="terminosYCondicionesAceptados" style="color:#808080;">&nbsp;<font style="color:#808080;" onclick="checkCuadroPrivacidad()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acepto</font> la política de <a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">privacidad de datos</a></label>
                                                                                                            </div>
                                                                                                            <div class="d-flex justify-content-center text-center">
                                                                                                                <button style="border-radius: 15px;cursor:pointer;margin-top: 15px;width:100%" onclick="registrarse_experriencia_espiritual(event)" type="submit" class="btn btn-primary">Registrarme</button>
                                                                                                            </div>
                                                                                                        </div>


                                                                                                    </div>


                                                                                                </div>






                                                                                            </div>


                                                                                        </div>
































                                                                                    </div>


                                                                                </div>

                                                                            <?php } elseif ($is_mobile == true) { ?>



                                                                                <div class="col-lg-12 col-12" style="border-radius: 25px;">


                                                                                    <?php if (!empty($informacionContenido["contenido"] && $informacionContenido["contenido"] != '<p><br></p>' && $informacionContenido["contenido"] != '<p>&nbsp;</p>' && $informacionContenido["contenido"] != '<p></p>' && $informacionContenido["contenido"] != '<p>')) { ?>
                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;margin-bottom: 10px;word-break: break-word;text-align: justify;font-family: Nunito !important;" class="table-tabs" id="responsiveTabs">
                                                                                            <?php echo filtrar_contenido_informacion($informacionContenido["contenido"]); ?>
                                                                                        </div>

                                                                                    <?php } ?>

                                                                                    <?php if (!empty($fameInformacion)) { ?>
                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;" class="table-tabs" id="responsiveTabs">
                                                                                            <h4 style="color: #009BDF">Videos pregrabados:</h4>
                                                                                            <br><br>
                                                                                            <?php echo $fameInformacion; ?>
                                                                                        </div>
                                                                                    <?php } ?>

                                                                                    <?php if (!empty($archivosAdjuntos)) { ?>


                                                                                        <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">
                                                                                            <h4 style="color: #009BDF">Archivos adjuntos:</h4>
                                                                                            <?php echo $archivosAdjuntos; ?>
                                                                                        </div>
                                                                                    <?php } ?>


                                                                                </div>

                                                                                <div class="col-lg-<?php echo $tamanoPantalla2; ?> col-12" style="border-radius: 25px;">

                                                                                    <div style="display:<?php echo $showFormulario; ?>;box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding-bottom: 20px;padding-top: 20px;padding-right: 10px;padding-left: 10px;word-break: break-all;border-radius: 25px;margin-bottom: 10px;" class="table-tabs" id="responsiveTabs">
                                                                                        <div class="form-group">

                                                                                            <h4 style="color: #009BDF;font-weight: bold;"><?php echo $informacionContenido["desc_form"] . ((!empty($informacionContenido["desc_form"]) ? ':' : '')); ?>:</h4>

                                                                                        </div>


                                                                                        <div class="row">

                                                                                            <div class="d-flex justify-content-center">


                                                                                                <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">

                                                                                                    <div class="col-12">
                                                                                                        <div class="form-group">
                                                                                                            <label for="fechaUsuarioRegistrar" class="text-dark"><strong style="word-break: break-word;color: #009BDF;font-weight: bold;"> Selecciona una fecha para registrarte en la experiencia </strong></label>


                                                                                                            <div id="evoCalendar"></div>




                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>





                                                                                            <div class="col-lg-6 col-12">

                                                                                                <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;" class="table-tabs" id="responsiveTabs">

                                                                                                    <div class="row">
                                                                                                        <div class="col-12" style="padding-left: 0px;">
                                                                                                            <p style="color: #009BDF;font-weight: bold;">Escribe tus datos:</p>
                                                                                                        </div>

                                                                                                        <?php if ($informacionContenido["c1_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="nombreUsuarioRegistrar" name="nombreUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c1"]; ?>">
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        <?php } else { ?>
                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="nombreUsuarioRegistrar" name="nombreUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu nombre">
                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c2_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="apellidoUsuarioRegistrar" name="apellidoUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c2"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>
                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="apellidoUsuarioRegistrar" name="apellidoUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu apellido">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c3_activo"] == 1) { ?>
                                                                                                            <div class="col-lg-6 col-12">
                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="emailUsuarioRegistrar" name="emailUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c3"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>

                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="emailUsuarioRegistrar" name="emailUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu correo electrónico">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c4_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="phoneUsuarioRegistrar" name="phoneUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c4"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>


                                                                                                            <div class="form-group">
                                                                                                                <input style="border-radius: 25px;" type="hidden" class="form-control" id="phoneUsuarioRegistrar" name="phoneUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu whatsApp / móvil">
                                                                                                            </div>
                                                                                                        <?php } ?>




                                                                                                        <?php if ($informacionContenido["c5_activo"] == 1) { ?>

                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <select onchange="cambiar_horario_de_experiencias()" style="border-radius: 25px;" class="form-control" id="paisUsuarioRegistrar" name="paisUsuarioRegistrar">
                                                                                                                        <option></option>
                                                                                                                    </select>

                                                                                                                </div>

                                                                                                                <div style="display:none;" class="form-group">

                                                                                                                    <select style="border-radius: 25px;" class="form-control" id="estadoUsuarioRegistrar" name="estadoUsuarioRegistrar">
                                                                                                                        <option></option>
                                                                                                                    </select>

                                                                                                                </div>

                                                                                                            </div>

                                                                                                        <?php } else { ?>


                                                                                                            <div style="display:none" class="form-group">

                                                                                                                <select onchange="cambiar_horario_de_experiencias()" style="border-radius: 25px;" class="form-control" id="paisUsuarioRegistrar" name="paisUsuarioRegistrar">
                                                                                                                    <option></option>
                                                                                                                </select>

                                                                                                            </div>

                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <select style="border-radius: 25px;" class="form-control" id="estadoUsuarioRegistrar" name="estadoUsuarioRegistrar">
                                                                                                                    <option></option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c6_activo"] == 1) { ?>

                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="ciudadUsuarioRegistrar" name="ciudadUsuarioRegistrar" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c6"]; ?>">
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        <?php } else { ?>



                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="ciudadUsuarioRegistrar" name="ciudadUsuarioRegistrar" aria-describedby="emailHelp" placeholder="Tu ciudad">
                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <?php if ($informacionContenido["c7_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">

                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1" name="campo_editable_1" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c7"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>



                                                                                                            <div style="display:none" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1" name="campo_editable_1" aria-describedby="emailHelp" placeholder="Campo 1">
                                                                                                            </div>
                                                                                                        <?php } ?>


                                                                                                        <?php if ($informacionContenido["c8_activo"] == 1) { ?>


                                                                                                            <div class="col-lg-6 col-12">


                                                                                                                <div class="form-group">

                                                                                                                    <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2" name="campo_editable_2" aria-describedby="emailHelp" placeholder="<?php echo $informacionContenido["c8"]; ?>">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        <?php } else { ?>




                                                                                                            <div style="display:none;" class="form-group">

                                                                                                                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2" name="campo_editable_2" aria-describedby="emailHelp" placeholder="Campo 2">
                                                                                                            </div>
                                                                                                        <?php } ?>



                                                                                                        <div class="col-lg-12 col-12">
                                                                                                            <div class="form-check">
                                                                                                                <input style="border-radius: 25px;height: 15px;width: 25px;margin-top: 1.5px;" type="checkbox" class="form-check-input" id="terminosYCondicionesAceptados" name="terminosYCondicionesAceptados">
                                                                                                                <label class="form-check-label" for="terminosYCondicionesAceptados" style="color: #808080;word-break: break-word;">&nbsp;<font onclick="checkCuadroPrivacidad()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acepto</font> la política de <a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">privacidad de datos</label>
                                                                                                            </div>
                                                                                                            <div class="d-flex justify-content-center text-center">
                                                                                                                <button style="border-radius: 15px;cursor:pointer;margin-top: 15px;" onclick="registrarse_experriencia_espiritual(event)" type="submit" class="btn btn-primary">Registrarme</button>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>














                                                                                        </div>


                                                                                    </div>



                                                                                <?php } ?>









                                                                                <div class="col-lg-12 col-12">
                                                                                    <div class="row" style="width:100%">
                                                                                        <div class="col-md-12 col-lg-12 col-12" style="box-shadow: rgb(157 156 156) 0px 1px 4px;border-radius: 25px;padding: 30px;">

                                                                                            <div class="row">


                                                                                                <?php if ($is_mobile == false) { ?>

                                                                                                    <?php if (!empty($anteriorExperiencia)) { ?>
                                                                                                        <div class="col-lg-4 col-4 col-xs-4">
                                                                                                            <div class="d-flex justify-content-center text-center" style="justify-content: left !important">
                                                                                                                <a target="_self" href="post.php?id=<?php echo $anteriorExperiencia; ?>" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;" class="btn btn-primary">Contenido anterior</a>
                                                                                                                <br><br>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <?php } ?>


                                                                                                    <div class="col-lg-4 col-4 col-xs-4">
                                                                                                        <div class="d-flex justify-content-center text-center">
                                                                                                            <a target="_self" href="index.php" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;" class="btn btn-primary">Todos los contenidos</a>
                                                                                                            <br><br>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <?php if (!empty($siguienteExperiencia)) { ?>
                                                                                                        <div class="col-lg-4 col-4 col-xs-4">
                                                                                                            <div class="d-flex justify-content-center text-center" style="justify-content: right !important">
                                                                                                                <a target="_self" href="post.php?id=<?php echo $siguienteExperiencia; ?>" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;" class="btn btn-primary">Siguiente contenido</a>
                                                                                                                <br><br>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <?php } ?>

                                                                                                <?php } else { ?>

                                                                                                    <?php if (!empty($anteriorExperiencia)) { ?>

                                                                                                        <div class="col-12 col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 10px;">
                                                                                                            <div class="d-flex justify-content-center text-center">

                                                                                                                <a target="_self" href="post.php?id=<?php echo $anteriorExperiencia; ?>" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 15px;padding-right: 15px;color:#fff;" class="btn btn-primary">Contenido anterior</a>
                                                                                                                <br><br>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    <?php } ?>



                                                                                                    <div class="col-12 col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 10px;">
                                                                                                        <div class="d-flex justify-content-center text-center">

                                                                                                            <a target="_self" href="index.php" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;" class="btn btn-primary">Todos los contenidos</a>
                                                                                                            <br><br>
                                                                                                        </div>

                                                                                                    </div>


                                                                                                    <?php if (!empty($siguienteExperiencia)) { ?>

                                                                                                        <div class="col-12 col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 10px;">

                                                                                                            <div class="d-flex justify-content-center text-center">

                                                                                                                <a target="_self" href="post.php?id=<?php echo $siguienteExperiencia; ?>" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;" class="btn btn-primary">Siguiente contenido</a>
                                                                                                                <br><br>

                                                                                                            </div>

                                                                                                        </div>

                                                                                                    <?php } ?>

                                                                                                <?php } ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                </div>




                                                                        </div>



                                                                    </div>
                                                                </div>


                                                            </div>


                                                        </div>
                                                        </div>


                                                    </div>
                                </main>
                            </div>
                        </div>
                    </div>


                    </div><!-- container -->



                    <?php include('htmlinclude/siv44.php'); ?>
                </div><!-- slim-mainpanel -->
        </div><!-- slim-body -->




        <script src="lib/jquery/js/jquery.js"></script>

        <script src="lib/popper.js/js/popper.js"></script>
        <script src="lib/bootstrap/js/bootstrap.js"></script>
        <script src="lib/jquery.cookie/js/jquery.cookie.js"></script>
        <script src="lib/d3/js/d3.js"></script>
        <script src="lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>
        <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

        <script src="js/ResizeSensor.js"></script>
        <script src="js/slim.js"></script>

        <script src="lib/fullcall/lib/jquery.min.js"></script>
        <script src="lib/fullcall/lib/moment.min.js"></script>
        <script src="lib/fullcall/fullcalendar.js"></script>


        <script type="text/javascript" src="js/countries.js"></script>
        <script src='admin/lib/fullcall/locale/es.js'></script>
        <script src="intlTelInput.js"></script>
        <script src="admin/swal/swalpersonalizado.js"></script>
        <script src="admin/swal/sweetalert2.min.js"></script>


        <script>
            function regresarAtras() {


                var TieneCategoria = '<?php echo $categoriaEncontrada; ?>';
                var TipoCategoria = '<?php echo $tieneCategoriaActual; ?>';

                if (TieneCategoria == 0 || TieneCategoria == '0') {
                    history.back();
                } else {
                    urlCategoria = 'index.php?category=' + TipoCategoria;
                    window.location.href = urlCategoria;
                }


            }
        </script>

        <?php if ($is_mobile == true) { ?>
            <script>
                // Verificar si el navegador admite la API Wake Lock
                if ('wakeLock' in navigator) {
                    let wakeLock = null;

                    // Solicitar el bloqueo de activación
                    const requestWakeLock = async () => {
                        try {
                            wakeLock = await navigator.wakeLock.request('screen');

                            // El bloqueo de activación se ha adquirido con éxito
                            console.error('Bloqueo de activación adquirido');
                        } catch (error) {
                            // No se pudo adquirir el bloqueo de activación
                            console.error('Error al adquirir el bloqueo de activación:', error);
                        }
                    };

                    // Liberar el bloqueo de activación
                    const releaseWakeLock = () => {
                        if (wakeLock) {
                            wakeLock.release();
                            wakeLock = null;
                            console.error('Bloqueo de activación liberado');
                        }
                    };

                    // Evento para solicitar el bloqueo de activación cuando la página está activa
                    document.addEventListener('visibilitychange', () => {
                        if (document.visibilityState === 'visible') {
                            requestWakeLock();
                        } else {
                            releaseWakeLock();
                        }
                    });

                    // Solicitar el bloqueo de activación inicial
                    requestWakeLock();
                }
            </script>
        <?php } ?>


        <script type="text/javascript">
            function listadoProximasIntenciones() {

                $("#listadoProximasIntenciones").html('');

                var form_data = new FormData();
                form_data.append("ObetenerProximasIntencionesUsuarios", 'SMD69');

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "admin/ajax/contenidoDataEmail.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {
                        if (loginData.suceso == 'ok') {


                            var contenidoHtmlInteciones = ``;
                            // Convierte el string JSON en un objeto JavaScript

                            if (loginData.mensaje.length > 0) {
                                contenidoHtmlInteciones = contenidoHtmlInteciones + `<br><h1 style="text-align: center;color: #009BDF;word-break: break-word;font-weight: 500;">Intenciones</h1>`;
                            }



                            for (var i = 0; i < loginData.mensaje.length; i++) {
                                var item = loginData.mensaje[i];
                                //console.log(); // Imprime cada objeto del JSON en la consola
                                // Realiza otras acciones con los datos obtenidos
                                contenidoHtmlInteciones = contenidoHtmlInteciones + `<div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 5px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;word-break: break-word;text-align: justify;font-family: Nunito !important;" class="table-tabs" id="responsiveTabs">
                                                                                            <p style="margin-bottom: 0px;">${item.intencion}</p>                                                                                        </div>`;
                            }

                            $("#listadoProximasIntenciones").html('').html(contenidoHtmlInteciones);

                        } else {
                            cargar_swal('info', loginData.mensaje, 'Advertencia');


                        }
                    }
                });

            }
            listadoProximasIntenciones();


            function crear_intencion_santa_misa() {
                var intencion_santa_misa = $("#intencion_santa_misa").val();
                if (intencion_santa_misa != '') {
                    var form_data = new FormData();
                    form_data.append("registrarIntecionesSantaMisa", 'SMD69');
                    form_data.append("intencion_santa_misa", intencion_santa_misa);


                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "admin/ajax/contenidoDataEmail.php",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        success: function(loginData) {
                            if (loginData.suceso == 'ok') {
                                $("#intencion_santa_misa").val('');
                                listadoProximasIntenciones();
                                Swal({
                                    title: '',
                                    html: '<h2>Muchas gracias por registrar tu intención.</h2><br><h4>Por María, ¡quédate en nosotros Jesús!</h4>',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#009BDF',
                                    cancelButtonColor: '#009BDF',
                                    confirmButtonText: 'ok',
                                    cancelButtonText: 'No',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {

                                    } else {
                                        cargar_swal('info', loginData.mensaje, 'Advertencia');


                                    }
                                });
                            } else {
                                cargar_swal('info', loginData.mensaje, 'Advertencia');


                            }
                        }
                    });

                } else {
                    cargar_swal('info', 'Recuerda que debes ingresar tu intención', 'Advertencia');
                }
            }

            $('#paisUsuarioRegistrar').on('change', function() {



                var form_data = new FormData();
                form_data.append("obtenerHorariosPorPaises", 'SMD69');
                form_data.append("pais", this.value);
                form_data.append("idContenido", '<?php echo $informacionContenido["id"]; ?>');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "admin/ajax/contenidoDataEmail.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(loginData) {

                        /*
                        $("#evoCalendar").evoCalendar('destroy');
                        $("#evoCalendar").html('');

                        $('#evoCalendar').evoCalendar({
                            theme: 'Royal Navy',
                            format: 'mm/dd/yyyy',
                            titleFormat: 'MM yyyy',
                            eventHeaderFormat: 'mm/dd/yyyy',
                            language: 'es',
                            eventListToggler: false,
                            todayHighlight: false,
                            sidebarToggler: false,
                            sidebarDisplayDefault: false,
                        });
                        */




                        var informacion = loginData.datos.split('},{');
                        informacion.forEach(element => {
                            element = element.replace(/&quot;/ig, '"');
                            element = element.replace("{", "");
                            element = element.replace("},", "");
                            element = element.replace("}", "");
                            element = '{' + element + '}';
                            var informacion2 = JSON.parse(element);


                            $("#evoCalendar").evoCalendar('removeCalendarEvent', informacion2.id);



                            //console.log(informacion2.date);
                            $("#evoCalendar").evoCalendar('addCalendarEvent', [{
                                id: informacion2.id,
                                name: informacion2.name,
                                date: informacion2.date,
                                badge: "",
                                description: informacion2.description,
                                type: "event"
                            }]);

                            // add events
                            /*
                            $("#evoCalendar").evoCalendar('addCalendarEvent', [{
                                id: informacion2.id,
                                name: informacion2.name,
                                date: informacion2.date,



                            }]);
                            */

                            /*
                            console.log({
                                id: informacion2.id,
                                name: informacion2.name,
                                date: informacion2.date,
                                badge: "",
                                description: informacion2.description,
                                type: "event"

                            })*/
                        });



                        //console.log(loginData.datos);
                        // var informacion = loginData.datos;
                        // var myFavShowArray = JSON.parse(informacion);
                        // console.log(myFavShowArray)






                    }
                });

            });


            var movilPhonePais;
            <?php if ($showFormulario == 'block') { ?>
                //alert('hello');
                // alert('aqui');
                movilPhonePais = $("#phoneUsuarioRegistrar").intlTelInput({
                    autoHideDialCode: false,
                    autoPlaceholder: "polite",
                    initialCountry: "<?php echo $isopaisName; ?>",
                    separateDialCode: true,
                    utilsScript: "utils.js",
                });



            <?php } ?>


            function registrarse_experriencia_espiritual(event) {
                event.preventDefault();
                if ($('#terminosYCondicionesAceptados').is(':checked')) {
                    var terminosYCondiciones = 1;
                } else {
                    var terminosYCondiciones = 0;
                }

                var id_formulario = '<?php echo $informacionContenido["id"]; ?>';

                if (terminosYCondiciones == 1) {
                    var numero_phone = movilPhonePais.intlTelInput("getNumber");
                    var nombreUsuarioRegistrar = $("#nombreUsuarioRegistrar").val();
                    var apellidoUsuarioRegistrar = $("#apellidoUsuarioRegistrar").val();
                    var emailUsuarioRegistrar = $("#emailUsuarioRegistrar").val();
                    var paisUsuarioRegistrar = $("#paisUsuarioRegistrar").val();
                    var estadoUsuarioRegistrar = $("#estadoUsuarioRegistrar").val();
                    var ciudadUsuarioRegistrar = $("#ciudadUsuarioRegistrar").val();


                    var campo_editable_1 = $("#campo_editable_1").val();
                    var campo_editable_2 = $("#campo_editable_2").val();



                    var fechaUsuarioRegistrar = fechaParaRegistroDeUsuario;
                    var fechaUsuarioSeleccionar = fechaParaRegistroDeUsuario2;
                    var horaUsuarioSeleccionar = fechaParaRegistroDeUsuario3;


                    var conti = true;
                    var menSjg = '';




                    if (conti == true) {
                        if (nombreUsuarioRegistrar == null || nombreUsuarioRegistrar == '' || nombreUsuarioRegistrar == 0 || nombreUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El nombre es necesario'
                        }
                    }

                    if (conti == true) {
                        if (apellidoUsuarioRegistrar == null || apellidoUsuarioRegistrar == '' || apellidoUsuarioRegistrar == 0 || apellidoUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El apellido es necesario'
                        }
                    }

                    if (conti == true) {

                        if (emailUsuarioRegistrar == null || emailUsuarioRegistrar == '' || emailUsuarioRegistrar == 0 || emailUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El correo electrónico es necesario'
                        }
                    }

                    if (conti == true) {
                        if (numero_phone == null || numero_phone == '' || numero_phone == 0 || numero_phone == undefined) {
                            conti = false;
                            menSjg = 'El telefono de whatsApp/móvil es necesario'
                        }
                    }

                    if (conti == true) {
                        if (paisUsuarioRegistrar == null || paisUsuarioRegistrar == '' || paisUsuarioRegistrar == 0 || paisUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El pais es necesario'
                        }
                    }


                    if (conti == true) {
                        if (ciudadUsuarioRegistrar == null || ciudadUsuarioRegistrar == '' || ciudadUsuarioRegistrar == 0 || ciudadUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'La ciudad es necesaria'
                        }
                    }

                    if (conti == true) {
                        if (fechaUsuarioRegistrar == null || fechaUsuarioRegistrar == '' || fechaUsuarioRegistrar == 0 || fechaUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'Debes de elegir en la agenda, el día y la hora en la que quieres participar de la Experiencia Espiritual '
                        }
                    }



                    //alert(numero_phone);

                    if (conti == true) {
                        var form_data = new FormData();
                        form_data.append("registrarUsuarioExperiencia_v2_email", 'SMD69');
                        form_data.append("id_formulario", id_formulario);
                        form_data.append("numero_phone", numero_phone);
                        form_data.append("nombreUsuarioRegistrar", nombreUsuarioRegistrar);
                        form_data.append("apellidoUsuarioRegistrar", apellidoUsuarioRegistrar);
                        form_data.append("emailUsuarioRegistrar", emailUsuarioRegistrar);
                        form_data.append("paisUsuarioRegistrar", paisUsuarioRegistrar);
                        form_data.append("estadoUsuarioRegistrar", estadoUsuarioRegistrar);
                        form_data.append("ciudadUsuarioRegistrar", ciudadUsuarioRegistrar);
                        form_data.append("fechaUsuarioRegistrar", fechaUsuarioRegistrar);
                        form_data.append("fechaUsuarioSeleccionar", fechaUsuarioSeleccionar);
                        form_data.append("horaUsuarioSeleccionar", horaUsuarioSeleccionar);
                        form_data.append("campo_editable_1", campo_editable_1);
                        form_data.append("campo_editable_2", campo_editable_2);
                        form_data.append("nameExperiencie", '<?php echo $informacionContenido["titulo"]; ?>');

                        // show loading

                        Swal.fire({
                            title: 'Registrando información...',
                            html: ``,
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            type: 'info',
                        });
                        Swal.showLoading();

                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: "admin/ajax/contenidoDataEmail.php",
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            success: function(loginData) {
                                if (loginData.suceso == 'ok') {
                                    $("#phoneUsuarioRegistrar").val('');
                                    $("#nombreUsuarioRegistrar").val('');
                                    $("#apellidoUsuarioRegistrar").val('');
                                    $("#emailUsuarioRegistrar").val('');
                                    $("#paisUsuarioRegistrar").val('');
                                    $("#estadoUsuarioRegistrar").val('');
                                    $("#ciudadUsuarioRegistrar").val('');
                                    $("#fechaUsuarioRegistrar").val('');
                                    Swal({
                                        title: 'Usuario registrado',
                                        text: 'Usuario registrado correctamente. A tu correo electrónico será enviada la información.',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#009BDF',
                                        cancelButtonColor: '#009BDF',
                                        confirmButtonText: 'ok',
                                        cancelButtonText: 'No',
                                        allowOutsideClick: false,
                                        allowEscapeKey: false
                                    }).then((result) => {
                                        if (result.value) {
                                            location.reload();



                                        }
                                    });
                                } else if (loginData.suceso == 'ok2') {
                                    $("#phoneUsuarioRegistrar").val('');
                                    $("#nombreUsuarioRegistrar").val('');
                                    $("#apellidoUsuarioRegistrar").val('');
                                    $("#emailUsuarioRegistrar").val('');
                                    $("#paisUsuarioRegistrar").val('');
                                    $("#estadoUsuarioRegistrar").val('');
                                    $("#ciudadUsuarioRegistrar").val('');
                                    $("#fechaUsuarioRegistrar").val('');
                                    Swal({
                                        title: 'Usuario registrado',
                                        text: 'El correo electrónico ya se encuentra registrado para esta experiencia, por favor revisa tu correo electrónico, en este te fue enviada toda la información ',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#009BDF',
                                        cancelButtonColor: '#009BDF',
                                        confirmButtonText: 'ok',
                                        cancelButtonText: 'No',
                                        allowOutsideClick: false,
                                        allowEscapeKey: false
                                    }).then((result) => {
                                        if (result.value) {
                                            location.reload();



                                        }
                                    });
                                } else {
                                    cargar_swal('error', loginData.mensaje, 'Advertencia');
                                }
                            }
                        });

                    } else {
                        cargar_swal('info', menSjg, 'Advertencia');
                    }

                    // ajax
                } else {
                    cargar_swal('info', 'Recuerda que debes aceptar los términos y condiciones', 'Advertencia');
                }

                //alert('registro Expereiciancia ');
            }


            //

            /*










            */

            populateCountriesSelected("paisUsuarioRegistrar", "estadoUsuarioRegistrar", '<?php echo $paisName; ?>');
            // definir si es mobil

            <?php if ($showFormulario == 'block') { ?>
                //alert('hello');

                if ($("#paisUsuarioRegistrar").val('<?php echo $paisName; ?>') == '<?php echo $paisName; ?>') {
                    // alert('aqui');
                } else {
                    // populateCountriesSelected("paisUsuarioRegistrar", "estadoUsuarioRegistrar", <?php echo $paisName2; ?>);
                }


            <?php } ?>


            window.mobileCheck = function() {
                let check = false;
                (function(a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
                })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            };

            var data = mobileCheck();
            if (data == false) {
                //$(".carousel-inner").css('height', '900px')
            } else {
                $("#home-v1-header-carousel .carousel-caption").css('top', '62%');
                $(".desaparecer").css('display', 'none');
            }
        </script>


        <script src="admin/lib/datatables/js/jquery.dataTables.js"></script>
        <script src="admin/lib/datatables-responsive/js/dataTables.responsive.js"></script>
        <script>
            var movilPhonePais2;

            var recomendacionurl = 'https://api.whatsapp.com/send?phone=&text=Esta experiencia espiritual podría gustarte a ti también <?php echo $informacionContenido["titulo"]; ?>  <?php echo $urlCompleta; ?>';


            function recomendarUsuarioNew() {

                Swal.fire({
                    title: '',
                    html: `


                    


                    <div class="col-12">

    <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 0px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;"
        class="table-tabs" id="responsiveTabs">

        <div class="row">
            <div class="col-12" style="padding-left: 0px;">
                <a href="${recomendacionurl}" target="_blank" style="border-radius: 25px;padding-top: 8px;padding-bottom: 6px;padding-left: 10px;padding-right: 10px;color:#fff;cursor:pointer;width: 100%;margin-top: 15px;background-color:#009BDF;" class="btn btn-primary">Recomendar por whatsapp</a>
            </div>
        </div>
    </div>
    
    <hr>
    
    </div>
        
                    
                    <div class="col-12">

    <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 0px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;"
        class="table-tabs" id="responsiveTabs">

        <div class="row">
            <div class="col-12" style="padding-left: 0px;">
                <h3 style="color: #009BDF;font-weight: bold;">Recomendar por correo </h1>
            </div>

            <div class="col-lg-6 col-12">


                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="nombreUsuarioRegistrar_pr"
                        name="nombreUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Nombre">
                </div>

            </div>



            <div class="col-lg-6 col-12">

                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="apellidoUsuarioRegistrar_pr"
                        name="apellidoUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Apellido">
                </div>
            </div>




            <div class="col-lg-12 col-12">
                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="emailUsuarioRegistrar_pr"
                        name="emailUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Correo Electrónico">
                </div>
            </div>











            <div style="display:none" class="form-group">

                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1"
                    name="campo_editable_1" aria-describedby="emailHelp" placeholder="Campo 1">
            </div>






            <div style="display:none;" class="form-group">

                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2"
                    name="campo_editable_2" aria-describedby="emailHelp" placeholder="Campo 2">
            </div>



            <div class="col-lg-12 col-12">
                <div class="form-check">
                    <input style="border-radius: 25px;height: 15px;width: 25px;margin-top: 1.5px;" type="checkbox"
                        class="form-check-input" id="terminosYCondicionesAceptados_pr"
                        name="terminosYCondicionesAceptados_pr">
                    <label class="form-check-label" for="terminosYCondicionesAceptados_pr"
                        style="color: #808080;word-break: break-word;">&nbsp;<font onclick="checkCuadroPrivacidad()">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acepto</font> la política de <a target="_blank" href="#"
                            style="text-decoration: underline;color: #009BDF;">privacidad de datos</a></label><a
                        target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">
                    </a>
                </div>


                


                <h4 id="mensajeErrorData" style="color:#ff0000;"></h4>
                
               
            </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">

            </a>
        </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">
        </a>
    </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">

    </a>
</div>



`,

                    showCancelButton: false,
                    showConfirmButton: true,
                    confirmButtonColor: '#009BDF',
                    cancelButtonColor: '#009BDF',
                    confirmButtonText: 'Enviar recomendación por correo',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    preConfirm: () => {
                        return new Promise((resolve) => {
                            // Realizar el primer paso o acción después de hacer clic en el botón de confirmación
                            enviarPregunta2();
                            // console.log('Primer paso completado');
                            // ... Agrega tu código personalizado aquí

                            // Resuelve la promesa para mantener el popup abierto
                            //resolve();
                        });
                    }
                });

            }

            function crear_repgunta() {

                var pregunta_por_responder = $("#pregunta_por_responder").val();
                if (pregunta_por_responder != '') {
                    Swal.fire({
                        title: '',
                        html: `<div class="col-12">

    <div style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 0px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 20px;"
        class="table-tabs" id="responsiveTabs">

        <div class="row">
            <div class="col-12" style="padding-left: 0px;">
                <h1 style="color: #009BDF;font-weight: bold;">Información personal</h1>
            </div>

            <div class="col-lg-6 col-12">


                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="nombreUsuarioRegistrar_pr"
                        name="nombreUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Nombre">
                </div>

            </div>



            <div class="col-lg-6 col-12">

                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="apellidoUsuarioRegistrar_pr"
                        name="apellidoUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Apellido">
                </div>
            </div>




            <div class="col-lg-6 col-12">
                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="emailUsuarioRegistrar_pr"
                        name="emailUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Correo Electrónico">
                </div>
            </div>






            <div class="col-lg-6 col-12">

                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="phoneUsuarioRegistrar_pr"
                        name="phoneUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="WhatsApp / Móvil">
                </div>
            </div>





            <div class="col-lg-6 col-12">

                <div class="form-group">

                    <select style="border-radius: 25px;"
                        class="form-control" id="paisUsuarioRegistrar_pr" name="paisUsuarioRegistrar_pr">

                        
                    </select>

                </div>

                <div style="display:none;" class="form-group">

                    <select style="border-radius: 25px;" class="form-control" id="estadoUsuarioRegistrar_pr"
                        name="estadoUsuarioRegistrar_pr">
                        <option></option>
                    </select>

                </div>

            </div>





            <div class="col-lg-6 col-12">


                <div class="form-group">

                    <input style="border-radius: 25px;" type="text" class="form-control" id="ciudadUsuarioRegistrar_pr"
                        name="ciudadUsuarioRegistrar_pr" aria-describedby="emailHelp" placeholder="Ciudad">
                </div>

            </div>






            <div style="display:none" class="form-group">

                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_1"
                    name="campo_editable_1" aria-describedby="emailHelp" placeholder="Campo 1">
            </div>






            <div style="display:none;" class="form-group">

                <input style="border-radius: 25px;" type="text" class="form-control" id="campo_editable_2"
                    name="campo_editable_2" aria-describedby="emailHelp" placeholder="Campo 2">
            </div>



            <div class="col-lg-12 col-12">
                <div class="form-check">
                    <input style="border-radius: 25px;height: 15px;width: 25px;margin-top: 1.5px;" type="checkbox"
                        class="form-check-input" id="terminosYCondicionesAceptados_pr"
                        name="terminosYCondicionesAceptados_pr">
                    <label class="form-check-label" for="terminosYCondicionesAceptados_pr"
                        style="color: #808080;word-break: break-word;">&nbsp;<font onclick="checkCuadroPrivacidad()">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acepto</font> la política de <a target="_blank" href="#"
                            style="text-decoration: underline;color: #009BDF;">privacidad de datos</a></label><a
                        target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">
                    </a>
                </div>


                


                <h4 id="mensajeErrorData" style="color:#ff0000;"></h4>
                
               
            </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">

            </a>
        </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">
        </a>
    </div><a target="_blank" href="#" style="text-decoration: underline;color: #009BDF;">

    </a>
</div>



`,
                        onOpen: () => {
                            populateCountriesSelected("paisUsuarioRegistrar_pr", "estadoUsuarioRegistrar_pr", '<?php echo $paisName; ?>');
                            movilPhonePais2 = $("#phoneUsuarioRegistrar_pr").intlTelInput({
                                autoHideDialCode: false,
                                autoPlaceholder: "polite",
                                initialCountry: "<?php echo $isopaisName; ?>",
                                separateDialCode: true,
                                utilsScript: "utils.js",
                            });

                        },
                        showCancelButton: false,
                        showConfirmButton: true,
                        confirmButtonColor: '#009BDF',
                        cancelButtonColor: '#009BDF',
                        confirmButtonText: 'Enviar',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        preConfirm: () => {
                            return new Promise((resolve) => {
                                // Realizar el primer paso o acción después de hacer clic en el botón de confirmación
                                enviarPregunta();
                                // console.log('Primer paso completado');
                                // ... Agrega tu código personalizado aquí

                                // Resuelve la promesa para mantener el popup abierto
                                //resolve();
                            });
                        }
                    });
                } else {
                    cargar_swal('info', 'Recuerda que debes ingresar tu pregunta', 'Advertencia');
                }
            }




            function enviarPregunta() {
                if ($('#terminosYCondicionesAceptados_pr').is(':checked')) {
                    var terminosYCondiciones = 1;
                } else {
                    var terminosYCondiciones = 0;
                }

                var id_formulario = '<?php echo $informacionContenido["id"]; ?>';

                if (terminosYCondiciones == 1) {
                    var numero_phone = movilPhonePais2.intlTelInput("getNumber");
                    var nombreUsuarioRegistrar = $("#nombreUsuarioRegistrar_pr").val();
                    var apellidoUsuarioRegistrar = $("#apellidoUsuarioRegistrar_pr").val();
                    var emailUsuarioRegistrar = $("#emailUsuarioRegistrar_pr").val();
                    var paisUsuarioRegistrar = $("#paisUsuarioRegistrar_pr").val();
                    var estadoUsuarioRegistrar = $("#estadoUsuarioRegistrar_pr").val();
                    var ciudadUsuarioRegistrar = $("#ciudadUsuarioRegistrar_pr").val();
                    var preguntar_usuario = $("#pregunta_por_responder").val();


                    var campo_editable_1 = '';
                    var campo_editable_2 = '';



                    var fechaUsuarioRegistrar = fechaParaRegistroDeUsuario;
                    var fechaUsuarioSeleccionar = fechaParaRegistroDeUsuario2;
                    var horaUsuarioSeleccionar = fechaParaRegistroDeUsuario3;


                    var conti = true;
                    var menSjg = '';




                    if (conti == true) {
                        if (nombreUsuarioRegistrar == null || nombreUsuarioRegistrar == '' || nombreUsuarioRegistrar == 0 || nombreUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El nombre es necesario'
                        }
                    }

                    if (conti == true) {
                        if (apellidoUsuarioRegistrar == null || apellidoUsuarioRegistrar == '' || apellidoUsuarioRegistrar == 0 || apellidoUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El apellido es necesario'
                        }
                    }

                    if (conti == true) {

                        if (emailUsuarioRegistrar == null || emailUsuarioRegistrar == '' || emailUsuarioRegistrar == 0 || emailUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El correo electrónico es necesario'
                        }
                    }

                    if (conti == true) {
                        if (numero_phone == null || numero_phone == '' || numero_phone == 0 || numero_phone == undefined) {
                            conti = false;
                            menSjg = 'El telefono de whatsApp/móvil es necesario'
                        }
                    }

                    if (conti == true) {
                        if (paisUsuarioRegistrar == null || paisUsuarioRegistrar == '' || paisUsuarioRegistrar == 0 || paisUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'El pais es necesario'
                        }
                    }


                    if (conti == true) {
                        if (ciudadUsuarioRegistrar == null || ciudadUsuarioRegistrar == '' || ciudadUsuarioRegistrar == 0 || ciudadUsuarioRegistrar == undefined) {
                            conti = false;
                            menSjg = 'La ciudad es necesaria'
                        }
                    }




                    //alert(numero_phone);

                    if (conti == true) {
                        var form_data = new FormData();
                        form_data.append("registrarUsuarioExperiencia_v2_email_pregunta", 'SMD69');
                        form_data.append("id_formulario", id_formulario);
                        form_data.append("numero_phone", numero_phone);
                        form_data.append("nombreUsuarioRegistrar", nombreUsuarioRegistrar);
                        form_data.append("apellidoUsuarioRegistrar", apellidoUsuarioRegistrar);
                        form_data.append("emailUsuarioRegistrar", emailUsuarioRegistrar);
                        form_data.append("paisUsuarioRegistrar", paisUsuarioRegistrar);
                        form_data.append("estadoUsuarioRegistrar", estadoUsuarioRegistrar);
                        form_data.append("ciudadUsuarioRegistrar", ciudadUsuarioRegistrar);
                        form_data.append("preguntar_usuario", preguntar_usuario);




                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: "admin/ajax/contenidoDataEmail.php",
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            success: function(loginData) {
                                if (loginData.suceso == 'ok') {
                                    Swal({
                                        title: '',
                                        html: '<h2>Muchas gracias por enviarnos tus inquietudes, pronto te daremos respuesta al correo y WhasApp registrados</h2><br><h4>Por María, ¡quédate en nosotros Jesús!</h4>',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#009BDF',
                                        cancelButtonColor: '#009BDF',
                                        confirmButtonText: 'ok',
                                        cancelButtonText: 'No',
                                        allowOutsideClick: false,
                                        allowEscapeKey: false
                                    }).then((result) => {
                                        if (result.value) {
                                            $("#pregunta_por_responder").val('');
                                            resolve()



                                        }
                                    });
                                } else {
                                    $("#mensajeErrorData").html(loginData.mensaje)

                                }
                            }
                        });

                    } else {
                        $("#mensajeErrorData").html(menSjg)
                    }

                    // ajax
                } else {
                    $("#mensajeErrorData").html('Recuerda que debes aceptar los términos y condiciones')

                }

                $(".swal2-confirm").prop('disabled', false);
            }

        //     function enviarPregunta2() {


        //         var id_formulario = '<?php echo $informacionContenido["id"]; ?>';

        //         var numero_phone = '';
        //         var nombreUsuarioRegistrar = $("#nombreUsuarioRegistrar_pr").val();
        //         var apellidoUsuarioRegistrar = $("#apellidoUsuarioRegistrar_pr").val();
        //         var emailUsuarioRegistrar = $("#emailUsuarioRegistrar_pr").val();
        //         var paisUsuarioRegistrar = '';
        //         var estadoUsuarioRegistrar = '';
        //         var ciudadUsuarioRegistrar = '';
        //         var preguntar_usuario = '';


        //         var campo_editable_1 = '';
        //         var campo_editable_2 = '';



        //         var fechaUsuarioRegistrar = fechaParaRegistroDeUsuario;
        //         var fechaUsuarioSeleccionar = fechaParaRegistroDeUsuario2;
        //         var horaUsuarioSeleccionar = fechaParaRegistroDeUsuario3;


        //         var conti = true;
        //         var menSjg = '';




        //         if (conti == true) {
        //             if (nombreUsuarioRegistrar == null || nombreUsuarioRegistrar == '' || nombreUsuarioRegistrar == 0 || nombreUsuarioRegistrar == undefined) {
        //                 conti = false;
        //                 menSjg = 'El nombre es necesario'
        //             }
        //         }

        //         if (conti == true) {
        //             if (apellidoUsuarioRegistrar == null || apellidoUsuarioRegistrar == '' || apellidoUsuarioRegistrar == 0 || apellidoUsuarioRegistrar == undefined) {
        //                 conti = false;
        //                 menSjg = 'El apellido es necesario'
        //             }
        //         }

        //         if (conti == true) {

        //             if (emailUsuarioRegistrar == null || emailUsuarioRegistrar == '' || emailUsuarioRegistrar == 0 || emailUsuarioRegistrar == undefined) {
        //                 conti = false;
        //                 menSjg = 'El correo electrónico es necesario'
        //             }
        //         }






        //         //alert(numero_phone);

        //         if (conti == true) {

        //             var nameExperience = '<?php echo $informacionContenido["titulo"]; ?>';
        //             var urlExperience = '<?php echo $urlCompleta; ?>';


        //             var form_data = new FormData();
        //             form_data.append("registrarUsuarioExperiencia_v2_email_pregunta_recomendaciones", 'SMD69');
        //             form_data.append("id_formulario", id_formulario);
        //             form_data.append("numero_phone", numero_phone);
        //             form_data.append("nombreUsuarioRegistrar", nombreUsuarioRegistrar);
        //             form_data.append("apellidoUsuarioRegistrar", apellidoUsuarioRegistrar);
        //             form_data.append("emailUsuarioRegistrar", emailUsuarioRegistrar);
        //             form_data.append("paisUsuarioRegistrar", paisUsuarioRegistrar);
        //             form_data.append("estadoUsuarioRegistrar", estadoUsuarioRegistrar);
        //             form_data.append("ciudadUsuarioRegistrar", ciudadUsuarioRegistrar);
        //             form_data.append("preguntar_usuario", preguntar_usuario);
        //             form_data.append("nameExperience", nameExperience);
        //             form_data.append("urlExperience", urlExperience);

        //             Swal.fire({
        //                 title: 'Enviando recomendación...',
        //                 html: ``,
        //                 allowOutsideClick: false,
        //                 allowEscapeKey: false,
        //                 type: 'info',
        //             });
        //             Swal.showLoading();


        //             $.ajax({
        //                 type: "POST",
        //                 dataType: 'json',
        //                 url: "admin/ajax/EnviaRecomendacion.php",
        //                 cache: false,
        //                 contentType: false,
        //                 processData: false,
        //                 data: form_data,
        //                 success: function(loginData) {
        //                     if (loginData.suceso == 'ok') {
        //                         Swal({
        //                             title: '',
        //                             html: '<h2>Muchas gracias por enviar tu recomendación a una persona que le pueda servir de ayuda</h2><br><h4>Por María, ¡quédate en nosotros Jesús!</h4>',
        //                             type: 'success',
        //                             showCancelButton: false,
        //                             confirmButtonColor: '#009BDF',
        //                             cancelButtonColor: '#009BDF',
        //                             confirmButtonText: 'ok',
        //                             cancelButtonText: 'No',
        //                             allowOutsideClick: false,
        //                             allowEscapeKey: false
        //                         }).then((result) => {
        //                             if (result.value) {
        //                                 resolve()



        //                             }
        //                         });
        //                     } else {
        //                         $("#mensajeErrorData").html(loginData.mensaje)

        //                     }
        //                 }
        //             });

        //         } else {
        //             $("#mensajeErrorData").html(menSjg)
        //         }

        //         $(".swal2-confirm").prop('disabled', false);
        //     }

        <?php


function enviarPregunta2() {
    //use PHPMailer\PHPMailer\PHPMailer;
   // use PHPMailer\PHPMailer\SMTP;
 //   use PHPMailer\PHPMailer\Exception;
    
    require 'admin/mail/src/PHPMailer.php';
    require 'admin/mail/src/SMTP.php';
    require 'admin/mail/src/Exception.php';
    
    // Crea una nueva instancia de PHPMailer
    $mail = new PHPMailer(true);
    
    try {
        // Configura el servidor SMTP
        $mail->isSMTP();
        $mail->Host = 'single-7051.banahosting.com'; // Cambia esto por la dirección de tu servidor SMTP
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@tradexchange.com.co'; // Cambia esto por tu dirección de correo electrónico
        $mail->Password = '3@6j^^.ay8u&'; // Cambia esto por tu contraseña de correo electrónico
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port = 465; // El puerto puede variar según el servidor SMTP
    
        // Configura el remitente y el destinatario
        $mail->setFrom('contenidos@misionerosurbanosdejesucristo.com', 'Misioneros Urbanos de Jesucristo');
        $mail->addAddress('edvargaspe@gmail.com', 'Alejandro Torres');
    
        // Configura el asunto y el mensaje
        $mail->Subject = 'Ejemplo de correo desde PHPMailer';
        $mail->Body = 'Este es un correo de prueba enviado desde PHPMailer.';
    
        // Envía el correo
        $mail->send();
        echo 'Correo enviado con éxito.';
    } catch (Exception $e) {
        echo 'Error al enviar el correo: ' . $mail->ErrorInfo;
    }
}
?>


        // </script>

        <script>
            $('html,body').animate({
                scrollTop: $(".search-filters").offset().top
            }, 'slow');

            function checkCuadroPrivacidad() {
                //$("#terminosYCondicionesAceptados").prop("checked", true);
            }
        </script>
        <script src="lib/event-calendar-evo/evo-calendar/js/evo-calendar2.js"></script>
        <script>
            var fechaParaRegistroDeUsuario;
            var fechaParaRegistroDeUsuario2;
            var fechaParaRegistroDeUsuario3;

            function capitalizarPrimeraLetra(str) {
                str = str.toLowerCase()
                //alert(str)
                return str.charAt(0).toUpperCase() + str.slice(1);
            }


            <?php if ($showFormulario == 'block') { ?>


                $("#sidebarToggler").click(function(e) {
                    e.preventDefault();
                });



                $(".icon-button").click(function(e) {
                    e.preventDefault();
                });

                myEvents = [<?php echo $eventosDia; ?>],

                    // initialize your calendar, once the page's DOM is ready
                    $(document).ready(function() {
                        /* {
                            settingName: settingValue
                        } */


                        <?php if ($is_mobile == true) { ?>

                            $('#evoCalendar').evoCalendar({
                                theme: 'Royal Navy',
                                format: 'mm/dd/yyyy',
                                titleFormat: 'MM yyyy',
                                eventHeaderFormat: 'mm/dd/yyyy',
                                calendarEvents: myEvents,
                                language: 'es',
                                eventListToggler: false,
                                todayHighlight: false,
                                sidebarToggler: true,
                                sidebarDisplayDefault: true,


                            });
                            $(".evo-calendar").removeClass('event-hide');
                            $("#eventListToggler").css('display', 'none');

                        <?php } else { ?>
                            $('#evoCalendar').evoCalendar({
                                theme: 'Royal Navy',
                                format: 'mm/dd/yyyy',
                                titleFormat: 'MM yyyy',
                                eventHeaderFormat: 'mm/dd/yyyy',
                                calendarEvents: myEvents,
                                language: 'es',
                                eventListToggler: false,
                                todayHighlight: false,
                                sidebarToggler: true,
                                sidebarDisplayDefault: true,


                            });

                            function validar() {
                                var ExisteCalendafio = $(".calendar-initialized").hasClass('evo-calendar');
                                if (ExisteCalendafio == false) {
                                    console.log(ExisteCalendafio);
                                    setTimeout(validar, 1000);
                                } else {
                                    //alert('existe');
                                    $(".calendar-initialized").addClass('sidebar-hide')

                                }
                            }
                            setTimeout(validar, 1000);


                        <?php } ?>


                        $('#evoCalendar').on('selectDate', function(event, activeEvent) {

                            var contenido = $(".calendar-events").html();



                            Swal({
                                title: '',
                                html: `<h4>Elige el día y la hora que quieres participar en la Experiencia Espiritual:</h4>` +
                                    contenido,
                                type: 'info',
                                showCancelButton: true,
                                showConfirmButton: false,
                                confirmButtonColor: '#009BDF',
                                cancelButtonColor: '#009BDF',
                                confirmButtonText: 'Confirmar',
                                cancelButtonText: 'Cerrar',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {


                                }
                            });

                            $(".event-container").on("click", function(event) {
                                var horaCompleta = $(this).data('eventIndex');
                                var OtroDato = $(this).find(".event-title").html();
                                var OtroDato2 = $(this).find(".event-desc").html();





                                // Domingo 21 De Agosto Del 2023
                                // 2023-05-20 23:41:00
                                // Hora : 1:41 PM
                                // console.log(activeEvent.description)
                                // console.log(activeEvent.id)
                                // console.log(activeEvent.name)

                                Swal({
                                    title: 'Confirmar selección de fecha:',
                                    html: '<h4 style="text-align:center;"> <br> Fecha: ' + capitalizarPrimeraLetra(OtroDato2) + '<br>' + OtroDato + '</h4>',
                                    type: 'info',
                                    showCancelButton: true,
                                    confirmButtonColor: '#009BDF',
                                    cancelButtonColor: '#009BDF',
                                    confirmButtonText: 'Confirmar',
                                    cancelButtonText: 'Cancelar',
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    if (result.value) {
                                        fechaParaRegistroDeUsuario = horaCompleta;
                                        fechaParaRegistroDeUsuario2 = OtroDato2;
                                        fechaParaRegistroDeUsuario3 = OtroDato;

                                        $(".event-container").removeClass('active');
                                        $(".event-container").addClass('active');
                                        //$(".event-container").find("[data-event-index='" + activeEvent.id + "']").addClass('active');

                                    }
                                });


                                //swal.close();
                                //var Content = $('.event-container ').html()
                                //console.log(Content);
                                //console.log($(this).data()[0])
                                //console.log(event)
                            });

                            // alert(2);
                        });

                        // selectEvent
                        $('#evoCalendar').on('selectEvent', function(event, activeEvent) {



                            // console.log(activeEvent)
                            // show alert
                            //
                            //fechaParaRegistroDeUsuario = activeEvent.id;

                            // Domingo 21 De Agosto Del 2023
                            // 2023-05-20 23:41:00
                            // Hora : 1:41 PM
                            // console.log(activeEvent.description)
                            // console.log(activeEvent.id)
                            // console.log(activeEvent.name)
                            //console.log(activeEvent.description)

                            Swal({
                                title: 'Confirmar selección de fecha:',
                                html: '<h4 style="text-align:center;"> <br> Fecha: ' + activeEvent.description + '<br>' + activeEvent.name + '</h4>',
                                type: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#009BDF',
                                cancelButtonColor: '#009BDF',
                                confirmButtonText: 'Confirmar',
                                cancelButtonText: 'Cancelar',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then((result) => {
                                if (result.value) {
                                    fechaParaRegistroDeUsuario = activeEvent.id;
                                    fechaParaRegistroDeUsuario2 = activeEvent.description;
                                    fechaParaRegistroDeUsuario3 = activeEvent.name;

                                    $(".event-container").removeClass('active');
                                    $(".event-container").addClass('active');
                                    //$(".event-container").find("[data-event-index='" + activeEvent.id + "']").addClass('active');

                                }
                            });


                            // console.log(event);
                            // console.log(activeEvent.id);
                            // code here...
                        });


                    })



            <?php } ?>
        </script>


        <div id="smart-button-container">
            <div style="text-align: center;">
                <div id="paypal-button-container"></div>
            </div>
        </div>



        <script>
            function abrirMenu() {
                if ($("body").hasClass("show-sidebar")) {
                    //alert('1');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Mostrar Menú');
                    $("body").removeClass("show-sidebar");

                } else {
                    //alert('2');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Ocultar Menu')
                    $("body").addClass("show-sidebar");

                }
            }

            $("#slimSidebarMenu").on("click", function() {
                if ($("body").hasClass("show-sidebar")) {
                    //alert('1');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Mostrar Menú');
                    // $("body").removeClass("show-sidebar");

                } else {
                    //alert('2');
                    $("#textoMenuShow2").html('').html('&nbsp;&nbsp;Ocultar Menu')
                    // $("body").addClass("show-sidebar");

                }
            });
        </script>



</body>

</html>