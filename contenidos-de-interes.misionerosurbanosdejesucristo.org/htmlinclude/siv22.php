<div class="slim-sidebar">
    <label class="sidebar-label" style="text-transform:initial;font-size: 15px;">Menú</label>

    <ul class="nav nav-sidebar">

        <li class="sidebar-nav-item">
            <a href="index.php" id="cuadroRoundData1" class="sidebar-nav-link ">
                <i class="fas fa-home mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Inicio </span></a>
        </li>



        <li class="sidebar-nav-item with-sub">


            <a href="" class="sidebar-nav-link "><i class="fas fa-book  mr-3 text-black"></i>
                <span class="text-black" style="color: #000;font-size: 1.3rem;">Contenidos de interés &nbsp;&nbsp;</span></a>

            <ul class="nav sidebar-nav-sub" style="display: block;margin-left: 0px;">
                <?php echo $categoriaExperiencias; ?>










            </ul>
        </li>





        <li class="sidebar-nav-item">
            <a href="pinteres.php" id="cuadroRoundData2" class="sidebar-nav-link ">
                <i class="fas fa-file-alt mr-3 text-black mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Páginas de interés </span></a>
        </li>

        <li class="sidebar-nav-item">
            <a href="calendario1.php" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-newspaper mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Noticias </span></a>
        </li>

        <li class="sidebar-nav-item">
            <a href="calendario2.php" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-users mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Programación actividades </span></a>
        </li>

        <li class="sidebar-nav-item">
            <a href="https://misionerosurbanosdejesucristo.org/" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-cross mr-3 text-black mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Página principal </span></a>
        </li>



        <li class="sidebar-nav-item">
            <a href="https://misionerosurbanosdejesucristo.org/quienes-somos/" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-info-circle mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Quiénes Somos</span></a>
        </li>

        <li class="sidebar-nav-item">
            <a href="https://misionerosurbanosdejesucristo.org/blog/" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-blog mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Blog</span></a>
        </li>

        <li class="sidebar-nav-item">
            <a href="https://edu.misionerosurbanosdejesucristo.org/login.php" id="cuadroRoundData3" class="sidebar-nav-link ">
                <i class="fas fa-graduation-cap mr-3 text-black"></i>
                <span class="text-black" style="color: #000; cursor: pointer;">Acceso al Diplomado</span></a>
        </li>




        <div class="sidebar-nav-item" style="background-color: #FADB0A !important;color: #025373 !important;">
            <a target="_blank" href=" https://misionerosurbanosdejesucristo.org/donar/" id="cuadroRoundData3" class="sidebar-nav-link2 " style="padding: 0 20px;height: 42px;display: flex;align-items: center;justify-content: flex-start;color: #025373;border-bottom: 1px solid #dee2e6;">
                <i class="fas fa-hand-holding-medical mr-3 text-black" style="color: #025373 !important;"></i>
                <span class="text-black" style="color: #025373 !important; cursor: pointer;">Quiero donar</span></a>
        </div>

















    </ul>
</div><!-- slim-sidebar -->