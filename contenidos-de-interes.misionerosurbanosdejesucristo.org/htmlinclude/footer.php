<!-- Start: Footer -->

<footer class="site-footer">

    <div class="sub-footer" style="background-color: #F6F2F0;">
        <div class="container">
            <div class="row" style="text-align:center;">

                <div class="footer-text col-12">

                    <div class="d-flex justify-content-center">

                        <h3 style="color: #000;">¿Necesitas ayuda con esta Experiencia? Escríbenos</h2>



                    </div>

                    <div class="d-flex justify-content-center">

                        <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-shrink elementor-repeater-item-b682d3e" href="https://api.whatsapp.com/send/?phone=573005782938&amp;text=Hola+Misioneros%2C+%2Aquiero+conocer+información+del+contenido+de+interes+%2C+por+favor%2A.+Mi+nombre+es%3A&amp;app_absent=0" target="_blank">
                            <span class="elementor-screen-only"></span>
                            <i style="color:#fff;font-size: 30px;background-color: #00d084;border-radius: 50%;padding: 7px;" class="fab fa-whatsapp"></i> </a>

                        <a class="elementor-icon elementor-social-icon elementor-social-icon-envelope elementor-animation-shrink elementor-repeater-item-d6b2487" href="mailto:info@misionerosurbanosdejesucristo.org" target="_blank">
                            <span class="elementor-screen-only"></span>
                            <i style="color:#fff;font-size: 30px;background-color: #EA4335;border-radius: 50%;padding: 7px;" class="fas fa-envelope"></i> </a>
                    </div>








                </div>


            </div>
        </div>
</footer>


<footer class="site-footer">

    <div class="sub-footer">
        <div class="container">
            <div class="row" style="text-align:center;">




                <div class="footer-text col-12">

                    <div class="d-flex justify-content-center">

                        <a href="https://misionerosurbanosdejesucristo.org/">
                            <img style="text-align: center;max-height: 80px;" src="images/logo.png" alt="">
                        </a>



                    </div>

                    <p><a style="text-align: center;color:#000;" target="_blank" href="https://misionerosurbanosdejesucristo.org/">Copyright © 2022 Misioneros Urbanos de Jesucristo</a></p>


                </div>
            </div>
        </div>
</footer>
<!-- End: Footer -->

<!-- jQuery Latest Version 1.x -->
<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>

<!-- jQuery UI -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

<!-- jQuery Easing -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!-- Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- Mobile Menu -->
<script type="text/javascript" src="js/mmenu.min.js"></script>

<!-- Harvey - State manager for media queries -->
<script type="text/javascript" src="js/harvey.min.js"></script>

<!-- Waypoints - Load Elements on View -->
<script type="text/javascript" src="js/waypoints.min.js"></script>

<!-- Facts Counter -->
<script type="text/javascript" src="js/facts.counter.min.js"></script>

<!-- MixItUp - Category Filter -->
<script type="text/javascript" src="js/mixitup.min.js"></script>

<!-- Owl Carousel -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script>

<!-- Accordion -->
<script type="text/javascript" src="js/accordion.min.js"></script>

<!-- Responsive Tabs -->
<script type="text/javascript" src="js/responsive.tabs.min.js"></script>

<!-- Responsive Table -->
<script type="text/javascript" src="js/responsive.table.min.js"></script>

<!-- Masonry -->
<script type="text/javascript" src="js/masonry.min.js"></script>

<!-- Carousel Swipe -->
<script type="text/javascript" src="js/carousel.swipe.min.js"></script>

<!-- bxSlider -->
<script type="text/javascript" src="js/bxslider.min.js"></script>

<!-- Custom Scripts -->
<script type="text/javascript" src="js/main.js"></script>


<script>
    var lastScrollTop = 0;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        /* 
        if (st > lastScrollTop) {
            //$("").css("display", "none");
            $(".navbar-wrapper").stop();
            $(".navbar-wrapper").animate({
                opacity: 0
            }, {
                duration: 500,
                complete: function() {
                    $(".navbar-nav a").css('display', 'none');
                    $(".navbar-nav a").css('opacity', '0');
                    //console.log('ok');
                }
            });


        } else {
            //console.log('up')
            $(".navbar-wrapper").stop();
            $(".navbar-wrapper").animate({
                opacity: 1
            }, {
                duration: 1,
                complete: function() {
                    
                    $(".navbar-nav a").animate({opacity: 1},3);
                    $(".navbar-nav a").css('display', 'block');
                    //console.log('ok2');
                }
            })
            //$("header").css("display", "block");
        }
        lastScrollTop = st;
        */
    });
</script>