<?php

require_once('cl/Mobile_Detect.php');
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
    $hexigMAcima = '100%';
} else {
    $hexigMAcima = '200px';
    $is_mobile = false;
}

?>
<div class="slim-header-left">


    <?php if ($is_mobile == true) { ?>
        <h2 class="slim-logo"><a class="navbar-brand" href="https://diplomado.misionerosurbanosdejesucristo.org" style="font-size:2.6rem;color:#fff;">
                <img style="max-height: 70px;" src="img/logo.png">
            </a></h2>

        <?php if ($is_mobile == true) { ?>
            <a href="" id="slimSidebarMenu" class="slim-sidebar-menu" style="background-color: #fff;color: #000;margin-right: 0px !important;margin-left: 20px;"><span></span></a>
            <font onclick="abrirMenu()" id="textoMenuShow2">&nbsp;&nbsp;Mostrar Menú</font>


        <?php } ?>

    <?php } else { ?>
        <h2 class="slim-logo" style="
    position: absolute;
    left: -15px;
    top: -18px;
    margin: 0px;
"><a class="navbar-brand" href="https://diplomado.misionerosurbanosdejesucristo.org" style="font-size:2.6rem;color:#fff;">
                <img style="max-height: 70px;" src="img/logo.png">
            </a></h2>

    <?php } ?>





</div><!-- slim-header-left -->
<div class="slim-header-right">


    <?php if ($is_mobile == false) { ?>


        <li class="linunito" style="margin: 8px;"><a href="index.php">Inicio</a></li>

        <li class="linunito" style="margin: 8px;"><a href="pinteres.php">Páginas de interés</a></li>
        <li class="linunito" style="margin: 8px;"><a href="calendario1.php">Noticias MUJ</a></li>
        <li class="linunito" style="margin: 8px;"><a href="calendario2.php">Programación actividades MUJ</a></li>
        <li class="linunito" style="margin: 8px;"><a href="https://misionerosurbanosdejesucristo.org/">Página principal MUJ</a></li>


        <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/quienes-somos/">Quiénes Somos</a></li>

        <li class="linunito" style="margin: 3px;"><a href="https://misionerosurbanosdejesucristo.org/blog/">Blog</a></li>
        <li class="linunito" style="margin: 3px;"><a href="https://edu.misionerosurbanosdejesucristo.org/login.php">Acceso al Diplomado</a></li>

        <li class="linunito" style="margin: 8px;"><a style="
    
    width: 181.08px;
    height: 47px;
    color: #025373;
    background: #FADB0A;
    padding: 12px;
    z-index: 1000;
    border-radius: 30px;
    text-align: center;
    cursor: pointer;
    transition: all 0.5s;
    margin-left: 30px;
    display: grid;
    " href="https://misionerosurbanosdejesucristo.org/donar/" target="_blank">
                <span>
                    Quiero donar
                </span>
            </a></li>

    <?php } ?>



</div><!-- header-right -->

<style>
    .fc-toolbar .fc-button {
        background-color: #009BDF;
    }

    .table thead>tr>th,
    .table thead>tr>td,
    .table tfoot>tr>th,
    .table tfoot>tr>td {
        background-color: #009BDF;
        color:#fff;
    }
</style>