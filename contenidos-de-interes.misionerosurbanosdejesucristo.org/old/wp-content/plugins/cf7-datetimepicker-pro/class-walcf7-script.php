<?php

class walcf7_Script_Controller{

    public function enque_scripts(){
		add_action('wp_enqueue_scripts', array($this, 'include_scripts_styles'));
    }

    public function include_scripts_styles(){
        global $post;
		
		$data = get_option('walcf7_options');
		
		$dtp_language = !empty($data['dtp_language'])?$data['dtp_language']:'en';
		
		if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
		  $dtp_language = ICL_LANGUAGE_CODE; // wpml language detection
		}

		$theme = !empty($data['dtp_theme'])?$data['dtp_theme']:'default';
		$dtp_masking = !empty($data['dtp_masking'])?$data['dtp_masking']:'';
		$format = !empty($data['dtp_date_format'])?$data['dtp_date_format']:'Y-m-d';
		$dayOfWeekStart = !empty($data['dayOfWeekStart'])?$data['dayOfWeekStart']:0;
		$dtp_disabled_date = !empty($data['dtp_disabled_date'])?$data['dtp_disabled_date']:'enabled';
		
		if($dtp_disabled_date == 'disabled_bt'){
			$disabled_date = 0;
		}else{
			$disabled_date = 'enabled';
		}
		$dtp_disable_today = !empty($data['dtp_disable_today'])?$data['dtp_disable_today']:false;
		$dtp_weekend = !empty($data['dtp_weekend'])?$data['dtp_weekend']:'0, 6';
		$dtp_disable_weekend = !empty($data['dtp_disable_weekend'])?$data['dtp_disable_weekend']:false;
		$dtp_disable_special = !empty($data['dtp_disable_special'])?$data['dtp_disable_special']:'';
		$max_date = !empty($data['max_date'])?$data['max_date']:false;
		$min_date = !empty($data['min_date'])?$data['min_date']:false;
		
		$time_steps = !empty($data['time_steps'])?$data['time_steps']:'60';
		$dtp_time_format = !empty($data['dtp_time_format'])?$data['dtp_time_format']:'H:i';
		$default_time = !empty($data['default_time'])?$data['default_time']:'09:00';
		
		$min_time = !empty($data['min_time'])?$data['min_time']:'';
		$max_time = !empty($data['max_time'])?$data['max_time']:'';
		
		$show_ampm = $data['dtp_ampm'];
		$ampm_format = !empty($data['dtp_ampm_format'])?$data['dtp_ampm_format']:'A';
		$ampm = !empty($data['dtp_ampm'])?' '.$ampm_format:'';
				
						
        $config_array = array(
			'dtp_language' => $dtp_language,
			'theme' => $theme,
			'mask' => $dtp_masking,
			'format' => $format,
			'time_steps' => $time_steps,
			'time_format' => $dtp_time_format,
			'default_time' => $default_time,
			'show_ampm' => $show_ampm ,
			'ampm' => $ampm,
			'dayOfWeekStart' => $dayOfWeekStart,
			'disabled_date' => $disabled_date,
			'dtp_weekend' => $dtp_weekend,
			'dtp_disable_today' => $dtp_disable_today,
			'dtp_disable_weekend' => $dtp_disable_weekend,
			'dtp_disable_special' => $dtp_disable_special,
			'max_date' => $max_date,
			'min_date' => $min_date,
			'min_time' => $min_time,
			'max_time' => $max_time,
        );			
		
		global $wp_query; 
		$page_id = $wp_query->post->ID;	
		
		$pages = !empty($data['load_in_page'])?$data['load_in_page']:'';
		
		if(empty($pages)){
			wp_register_script(
				'walcf7-datepicker-js', 
				WALCF7_DTP_URI.'assets/js/jquery.datetimepicker.full.js', 
				array("jquery"), false, true);
			wp_enqueue_style(
				'walcf7-datepicker-css', 
				WALCF7_DTP_URI.'assets/css/jquery.datetimepicker.min.css', false, '1.0.0','all');
				
			wp_register_script( 'walcf7_datepicker', WALCF7_DTP_URI. 'assets/js/datetimepicker.js', array( 'jquery'), false, true );
			wp_localize_script('walcf7_datepicker', 'walcf7_conf', $config_array);	
			wp_enqueue_script('walcf7-datepicker-js');
			wp_enqueue_script('walcf7_datepicker');
				
		}else{
			$page_arr = array();
			$all_pages = explode(",", $pages);
			foreach($all_pages as $key => $val){
				array_push($page_arr, trim($val));
			}
			if (in_array($page_id, $page_arr)){
				wp_register_script(
					'walcf7-datepicker-js', 
					WALCF7_DTP_URI.'assets/js/jquery.datetimepicker.full.js', 
					array("jquery"), false, true);
				wp_enqueue_style(
					'walcf7-datepicker-css', 
					WALCF7_DTP_URI.'assets/css/jquery.datetimepicker.min.css', false, '1.0.0','all');
					
				wp_register_script( 'walcf7_datepicker', WALCF7_DTP_URI. 'assets/js/datetimepicker.js', array( 'jquery'), false, true );
				wp_localize_script('walcf7_datepicker', 'walcf7_conf', $config_array);	
				wp_enqueue_script('walcf7-datepicker-js');
				wp_enqueue_script('walcf7_datepicker');
				
			}
		}
    }
}
?>