;(function ($) {
	"use strict";
	$(document).ready(function () {
		function get_type(dd, mm, yyyy){
			var format = walcf7_conf.format;
			if(format == 'Y-m-d'){
				return yyyy + '-' + mm + '-' + dd;
			}else if(format == 'd-m-Y'){
				return dd + '-' + mm + '-' + yyyy;
			}else{
				return mm + '-' + dd + '-' + yyyy;
			}
		}
		function get_todate(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();			
			if(dd<10) {
				dd = '0'+dd
			} 
			if(mm<10) {
				mm = '0'+mm
			} 
			today = get_type(dd, mm, yyyy);
			return today;
		}

		function get_tomorrow(){
			var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			var dd = tomorrow.getDate()
			var mm = tomorrow.getMonth() + 1
			var yyyy = tomorrow.getFullYear()
			if(dd<10) {
				dd = '0'+dd
			} 
			if(mm<10) {
				mm = '0'+mm
			} 
			tomorrow = get_type(dd, mm, yyyy);
			return tomorrow;
		}
		function allowed_time(){
			var min_time = walcf7_conf.min_time;
			var max_time = walcf7_conf.max_time;
			var steps = parseInt(walcf7_conf.time_steps);
			var allowTime = [];
			if(min_time != ''){
				var min_hrmin =  min_time.split(":");
				var max_hrmin =  max_time != ''? max_time.split(":"): '';
				var min_hr = parseInt(min_hrmin[0]);
				var min_min = min_hrmin[1] != '00'? parseInt(min_hrmin[1]): 0;
				var max_hr = max_hrmin[0] != ''? parseInt(max_hrmin[0]): 23;
				var max_min = max_hrmin[1] != '00'? parseInt(max_hrmin[1]): 0;
								
				var x = steps; //minutes interval
				var st = min_hr*60 + min_min ; // start time	
				var et = max_hr*60 + max_min; // end time			
				//loop to increment the time and push results in array
				var itime = min_hr*60 + min_min;
				for (var i = 0; st <= et ; i++) {
				  var hh = Math.floor(st/60);
				  var mm = (st%60); // getting minutes of the hour in 0-55 format
				  allowTime[i] = ("0" + hh).slice(-2) + ':' + ("0" + mm).slice(-2);
				  st = st + x;
				}

				return allowTime;
				
			}else{
				return [];
			}
		}
		
		function get_date_by_day(day_num){
			var d = new Date();
			
			d.setDate(d.getDate() + parseInt(day_num)); 
			
			var dd = d.getDate();
			var mm = d.getMonth() + 1;
			var yyyy = d.getFullYear();
			if(dd<10) {
				dd = '0'+dd
			} 
			if(mm<10) {
				mm = '0'+mm
			}
			
			return  yyyy + '/' + mm + '/' + dd;
		}
		
		
		$('.walcf7-datetimepicker').each(function(index, element) {
			var todate = get_todate();
			var tomorrow = get_tomorrow();
			var disabled_dates = [];
			if(walcf7_conf.dtp_disable_today){
				disabled_dates.push(todate);
			}
			if(walcf7_conf.dtp_disable_special!=''){
				var disable_special = walcf7_conf.dtp_disable_special.split(",");
				for(var x in disable_special){
					disabled_dates.push(disable_special[x].trim());
				}
			}
			var max_date = walcf7_conf.max_date!= ''? Date.parse(get_date_by_day(walcf7_conf.max_date)):'';
			var min_date = walcf7_conf.min_date!= ''? Date.parse(get_date_by_day(walcf7_conf.min_date)):'';
			var default_time = walcf7_conf.default_time != ''? walcf7_conf.default_time: '09:00';
			var dpt_lang = walcf7_conf.dtp_language!= ''? walcf7_conf.dtp_language:'en';
			if(dpt_lang != 'en'){
				jQuery.datetimepicker.setLocale(dpt_lang);
			}
			$(this).datetimepicker({
				dayOfWeekStart : 1,
				yearStart: '1900',
				lang: dpt_lang,
				formatDate: walcf7_conf.format,
				formatTime: walcf7_conf.time_format+walcf7_conf.ampm,
				defaultTime: default_time+walcf7_conf.ampm,
				format: walcf7_conf.format+' '+walcf7_conf.time_format+walcf7_conf.ampm,
				startDate: todate,
				theme: walcf7_conf.theme,
				disabledDates: disabled_dates,
				dayOfWeekStart: walcf7_conf.dayOfWeekStart,
				minDate: walcf7_conf.disabled_date!= 'enabled'?parseInt(walcf7_conf.disabled_date):false,
				step: parseInt(walcf7_conf.time_steps),
				allowTimes: allowed_time(),
				validateOnBlur: false,
				onGenerate: function( ct, $input ){
					var $this = $(this);
					var weekend = walcf7_conf.dtp_weekend.split(",");
					if(walcf7_conf.dtp_disable_weekend && weekend != ''){
						for (var x in weekend) {	
							$this.find('.xdsoft_day_of_week'+weekend[x].trim()).addClass('xdsoft_disabled');
						}
					}
					
					if(max_date != ''){
						$this.find('.xdsoft_date').each(function(index, element) {
							var dd = parseInt($(this).attr('data-date'));
							var mm = parseInt($(this).attr('data-month'))+1;
							var yyyy = parseInt($(this).attr('data-year'));
							
							var each_date = Date.parse(yyyy + '/' + mm + '/' + dd);
							if(each_date > max_date){
								$(this).addClass('xdsoft_disabled');
							}
						});
					}
					if(min_date != ''){
						$this.find('.xdsoft_date').each(function(index, element) {
							var dd = parseInt($(this).attr('data-date'));
							var mm = parseInt($(this).attr('data-month'))+1;
							var yyyy = parseInt($(this).attr('data-year'));
							var each_date = Date.parse(yyyy + '/' + mm + '/' + dd);
							if(each_date < min_date){
								$(this).addClass('xdsoft_disabled');
							}
						});
					}
				}						
			});
		});
		
		$('.walcf7-datepicker').each(function(index, element) {
			var todate = get_todate();
			var tomorrow = get_tomorrow();
			var disabled_dates = [];
			if(walcf7_conf.dtp_disable_today){
				disabled_dates.push(todate);
			}
			if(walcf7_conf.dtp_disable_special!=''){
				var disable_special = walcf7_conf.dtp_disable_special.split(",");
				for(var x in disable_special){
					disabled_dates.push(disable_special[x].trim());
				}
			}
			var max_date = walcf7_conf.max_date!= ''? Date.parse(get_date_by_day(walcf7_conf.max_date)):'';
			var min_date = walcf7_conf.min_date!= ''? Date.parse(get_date_by_day(walcf7_conf.min_date)):'';
			
			var dpt_lang = walcf7_conf.dtp_language!= ''? walcf7_conf.dtp_language:'en';
			if(dpt_lang != 'en'){
				jQuery.datetimepicker.setLocale(dpt_lang);
			}
			$(this).datetimepicker({
				dayOfWeekStart : 1,
				yearStart: '1900',
				lang: dpt_lang,
				timepicker:false,
				format: walcf7_conf.format,
				formatDate: walcf7_conf.format,
				dayOfWeekStart: walcf7_conf.dayOfWeekStart,
				minDate: walcf7_conf.disabled_date!= 'enabled'?parseInt(walcf7_conf.disabled_date):false,
				theme: walcf7_conf.theme,
				disabledDates: disabled_dates,
				startDate: todate,
				validateOnBlur: false,
				onGenerate: function( ct, $input ){
					var $this = $(this);
					var weekend = walcf7_conf.dtp_weekend.split(",");
					if(walcf7_conf.dtp_disable_weekend && weekend != ''){
						for (var x in weekend) {	
							$this.find('.xdsoft_day_of_week'+weekend[x].trim()).addClass('xdsoft_disabled');
						}
					}
					
					if(max_date != ''){
						$this.find('.xdsoft_date').each(function(index, element) {
							var dd = parseInt($(this).attr('data-date'));
							var mm = parseInt($(this).attr('data-month'))+1;
							var yyyy = parseInt($(this).attr('data-year'));
							
							var each_date = Date.parse(yyyy + '/' + mm + '/' + dd);
							if(each_date > max_date){
								$(this).addClass('xdsoft_disabled');
							}
						});
					}
					if(min_date != ''){
						$this.find('.xdsoft_date').each(function(index, element) {
							var dd = parseInt($(this).attr('data-date'));
							var mm = parseInt($(this).attr('data-month'))+1;
							var yyyy = parseInt($(this).attr('data-year'));
							var each_date = Date.parse(yyyy + '/' + mm + '/' + dd);
							if(each_date < min_date){
								$(this).addClass('xdsoft_disabled');
							}
						});
					}
				}
			});
		});
		
		$('.walcf7-timepicker').each(function(index, element) {
			var default_time = walcf7_conf.default_time != ''? walcf7_conf.default_time: '09:00';
			$(this).datetimepicker({
				datepicker:false, 
				format: walcf7_conf.time_format+walcf7_conf.ampm,
				formatTime: walcf7_conf.time_format+walcf7_conf.ampm,
				step: parseInt(walcf7_conf.time_steps),
				theme: walcf7_conf.theme,
				defaultTime: default_time+walcf7_conf.ampm,
				allowTimes: allowed_time(),
				validateOnBlur: false,
				onGenerate: function( ct, $input ){
					$input.prop('readonly', true);
					var $this = $(this);
					$this.find('.xdsoft_time').removeClass('xdsoft_disabled');
				}
			});
		});
		
	});
}(jQuery));

