<?php
/**
 * Plugin Name: Date Time Picker for Contact Form 7 (Pro)
 * Plugin URI: https://wpapplab.com/
 * Description: This plugin can be used to display date and time picker into Contact Form 7 text input field by using css class. This plugin specifically designed to work with Contact Form 7. Other form plugins are not tested.
 * Version: 1.1.1
 * Author: Ruhul Amin
 * Author URI: https://mircode.com
 * Text Domain: walcf7-datetimepicker
 *
 * @package WAL Demo
 */

define( 'WALCF7_DTP_FILE', __FILE__ );
define( 'WALCF7_DTP_BASE', plugin_basename( WALCF7_DTP_FILE ) );
define( 'WALCF7_DTP_DIR', plugin_dir_path( WALCF7_DTP_FILE ) );
define( 'WALCF7_DTP_URI', plugins_url( '/', WALCF7_DTP_FILE ) );

if( !defined('CF7_PATH') )
	define( 'CF7_PATH', plugin_dir_path(__FILE__) );
if( !defined('CF7_URL') )
	define( 'CF7_URL', plugin_dir_url(__FILE__ ) );


require_once 'admin/class-walcf7-options.php';

class walcf7_Apps {
	
	 public function __construct() {
		 
    }
	
    public function initialize_controllers() {

        require_once 'class-walcf7-activation.php';
        $activation_controller = new walcf7_Activation_Controller();
        $activation_controller->initialize_activation_hooks();
		
    }

    public function initialize_app_controllers() {

		require_once 'class-walcf7-script.php';
        $script_controller = new walcf7_Script_Controller();
        $script_controller->enque_scripts();
    }	
}

$walcf7_app = new walcf7_Apps();
$walcf7_app->initialize_controllers();

function load_walcf7(){
	$walcf7_init = new walcf7_Apps();
	$walcf7_init->initialize_app_controllers();
}

add_action('init', 'load_walcf7');
