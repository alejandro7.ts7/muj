<?php

if ( ! class_exists( 'walcf7Options' ) ) {
	class walcf7Options {
			
		function __construct() {
			$this->create_walcf7_Options();
		}
		
		public function create_walcf7_Options() {
			require_once(CF7_PATH . "admin/admin-page-class.php");
			/**
			* configure your admin page
			*/
			$config = array(    
				'menu'           => 'settings',             //sub page to settings page
				'page_title'     => 'CF7 Date Time',       //The name of this page 
				'capability'     => 'edit_posts',         // The capability needed to view the page 
				'option_group'   => 'walcf7_options',       //the name of the option to create in the database
				'id'             => 'walcf7_admin_page',   // meta box id, unique per page
				'fields'         => array(),    // list of fields (can be added by field arrays)
				'local_images'   => false,   // Use local or hosted images (meta box images for add/remove)
				'use_with_theme' => false //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
			);  
			
			/**
			* instantiate your admin page
			*/
			
			$options_panel = new CF7_Admin_Page_Class($config);
			$options_panel->OpenTabs_container('');
			
			/**
			* define your admin page tabs listing
			*/
			$options_panel->TabsListing(array(
			'links' => array(
			  'options_1' =>  __('Style Options','apc'),
			  'options_7' =>  __('Import Export','apc'),
			)
			));
			
			/**
			* Open admin page first tab
			*/
			$options_panel->OpenTab('options_1');
			
			//title
			$options_panel->Title(__("Style Options","apc"));
			
			$options_panel->addSelect(
				'dtp_language',
				array(
					'en' => 'English',
					'ar' => 'Arabic',
					'az' => 'Azerbaijanian (Azeri)',
					'bg' => 'Bulgarian',
					'bs' => 'Bosanski',
					'ca' => 'Català',
					'ch' => 'Simplified Chinese',
					'cs' => 'Čeština',
					'da' => 'Dansk',
					'de' => 'German',
					'el' => 'Ελληνικά',
					'en-GB' => 'English (British)',
					'es' => 'Spanish',
					'et' => 'Eesti',
					'eu' => 'Euskara',
					'fa' => 'Persian',
					'fi' => 'Finnish (Suomi)',
					'fr' => 'French',
					'gl' => 'Galego',
					'he' => 'Hebrew (עברית)',
					'hr' => 'Hrvatski',
					'hu' => 'Hungarian',
					'id' => 'Indonesian',
					'it' => 'Italian',
					'ja' => 'Japanese',
					'ko' => 'Korean (한국어)',
					'kr' => 'Korean',
					'lt' => 'Lithuanian (lietuvių)',
					'lv' => 'Latvian (Latviešu)',
					'mk' => 'Macedonian (Македонски)',
					'mn' => 'Mongolian (Монгол)',
					'nl' => 'Dutch',
					'no' => 'Norwegian',
					'pl' => 'Polish',
					'pt' => 'Portuguese',
					'pt-BR' => 'Português(Brasil)',
					'ro' => 'Romanian',
					'ru' => 'Russian',
					'se' => 'Swedish',
					'sk' => 'Slovenčina',
					'sl' => 'Slovenščina',
					'sq' => 'Albanian (Shqip)',
					'sr' => 'Serbian Cyrillic (Српски)',
					'sr-YU' => 'Serbian (Srpski)',
					'sv' => 'Svenska',
					'th' => 'Thai',
					'tr' => 'Turkish',
					'uk' => 'Ukrainian',
					'vi' => 'Vietnamese',
					'zh' => 'Simplified Chinese (简体中文)',
					'zh-TW' => 'Traditional Chinese (繁體中文)',
				),
				array(
					'name'=> __('Language','apc'),
					'std'=> array('en'), 
					'desc' => __('Month and day name will be translated only','apc')
				)
			);
			
			$options_panel->addRadio(
				'dtp_theme',
				array(
					'default'=>'Default',
					'dark'=>'Dark',
				),
				array(
					'name'=> __('Theme color','apc'),
					'std'=> array('default'), 
					'desc' => __('','apc')
				)
			);
			$options_panel->addRadio(
				'dtp_disabled_date',
				array(
					'enabled'=>'Enable all',
					'disabled_bt'=>'Disable before today',
				),
				array(
					'name'=> __('Disable Selecting Date.','apc'),
					'std'=> array('enabled'), 
					'desc' => __('','apc')
				)
			);
			$options_panel->addCheckbox('dtp_disable_today',array('name'=> __('Disable today?','apc'), 'std' => false, 'desc' => __('Default: false','apc')));
			$options_panel->addRadio(
				'dtp_date_format',
				array(
					'Y-m-d'=>'Y-m-d',
					'd-m-Y'=>'d-m-Y',
					'm-d-Y'=>'m-d-Y',
				),
				array(
					'name'=> __('Date Format','apc'),
					'std'=> array('d-m-Y'), 
					'desc' => __('Y = Full Year, m = month 01 to 12, d = day 01 to 31, M = Jan to Dec, F = January to December ','apc')
				)
			);
			$options_panel->addRadio(
				'dayOfWeekStart',
				array(
					'0'=>'Sunday',
					'1'=>'Monday',
					'2'=>'Tuesday',
					'3'=>'Wednesday',
					'4'=>'Thursday',
					'5'=>'Friday',
					'6'=>'Saturday',
				),
				array(
					'name'=> __('Day of Week Start?','apc'),
					'std'=> array('0'), 
					'desc' => __('','apc')
				)
			);
			
			$options_panel->addText('dtp_weekend', array('name'=> __('Add Weekend','apc'), 'std'=> '0', 'desc' => __('0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday , Use only Number seperated by comma [,] Example: 0, 6 means Sunday and Saturday is weekend.','apc')));
			$options_panel->addCheckbox('dtp_disable_weekend',array('name'=> __('Disabled Weekend?','apc'), 'std' => false, 'desc' => __('Default: false','apc')));
			$options_panel->addText('dtp_disable_special', array('name'=> __('Disable Specific date?','apc'), 'std'=> '', 'desc' => __('You can put specific date to disable it. Date Format must be similar to above 3er point, Example: if you select above date format Y-m-d then add date like 2020-10-24, 2020-10-26. You can add multiple date seperated by comma [,]','apc')));
			
			//is_numeric
			$options_panel->addText('max_date',
			array(
			  'name'     => __('Maximum Selectable Date','apc'),
			  'std'      => '',
			  'desc'     => __("Put only number. Example value 7 means user can select date from coming 7 days only. Select Value between 1 to 365. Default: false","apc"),
			  'validate' => array(
				  'numeric' => array(
					  'param' => '',
					  'message' => __("must be numeric (number) value","apc")
				  ),
				  
				  'maxvalue' => array(
					  'param' => '365',
					  'message' => __("Value cannot be more than 356","apc")
				  )
			  )
			)
			);
			
			//is_numeric
			$options_panel->addText('min_date',
			array(
			  'name'     => __('Minimum Selectable Date','apc'),
			  'std'      => '',
			  'desc'     => __("Put only number. Example value 7 means user can not select any past date before today + coming 7 days from today. Can select only days after 7 days. Select Value between 1 to 365. Default: false","apc"),
			  'validate' => array(
				  'numeric' => array(
					  'param' => '',
					  'message' => __("must be numeric (number) value","apc")
				  ),
				  
				  'maxvalue' => array(
					  'param' => '365',
					  'message' => __("Value cannot be more than 356","apc")
				  )
			  )
			)
			);

			$options_panel->addRadio(
				'dtp_time_format',
				array(
					'H:i'=>'24 hr',
					'h:i'=>'12 hr',
				),
				array(
					'name'=> __('Time Format','apc'),
					'std'=> array('H:i'), 
					'desc' => __('Default: 24 hr','apc')
				)
			);
			
			$options_panel->addCheckbox('dtp_ampm',array('name'=> __('Show AM/PM?','apc'), 'std' => false, 'desc' => __('Default: false','apc')));
			$options_panel->addRadio(
				'dtp_ampm_format',
				array(
					'a'=>'am/pm',
					'A'=>'AM/PM',
				),
				array(
					'name'=> __('AM/PM Format?','apc'),
					'std'=> array('a'), 
					'desc' => __('Default: am/pm','apc')
				)
			);
			
			$options_panel->addText('default_time', array('name'=> __('Default Time','apc'), 'std'=> '', 'desc' => __('Highlight starting time of the day. Keep the time format similar to example. Example: 09:00  ','apc')));
			
			//is_numeric
			$options_panel->addText('time_steps',
			array(
			  'name'     => __('Time Step?','apc'),
			  'std'      => 15,
			  'desc'     => __("Time Interval of selecting time. time value can be 2, 5, 10, 15, 20, 30, 60 or between 2 to 60 minutes. Default: 15","apc"),
			  'validate' => array(
				  'numeric' => array(
					  'param' => '',
					  'message' => __("must be numeric (number) value","apc")
				  ),
				  'minvalue' => array(
					  'param' => '2',
					  'message' => __("Value cannot be less than 2","apc")
				  ),
				  'maxvalue' => array(
					  'param' => '61',
					  'message' => __("Value cannot be more than 60","apc")
				  )
			  )
			)
			);
			
			$options_panel->addText('min_time', array('name'=> __('Minimum Selectable Time','apc'), 'std'=> '', 'desc' => __('Keep the time format to 24 hour example. Example: 09:00 means 9 am 22:00 means 10 pm. You can use minutes also like 23:30 . Min value 00:00','apc')));
			$options_panel->addText('max_time', array('name'=> __('Maximum Selectable Time','apc'), 'std'=> '', 'desc' => __('Keep the time format to 24 hour example. Example: 09:00 means 9 am 22:00 means 10 pm. You can use minutes also like 23:30 . Max value 23:59 ','apc')));
			$options_panel->addText('load_in_page', array('name'=> __('Load script only pages specified?','apc'), 'std'=> '', 'desc' => __('Type page ids where you want to load the script. For multiple page, type each page id seperated by comma (,). Example: 10, 15, 200','apc')));

			/**
			* Close first tab
			*/   
			$options_panel->CloseTab();
			
			
			/**
			* Open admin page 7th tab
			*/
			$options_panel->OpenTab('options_7');
			
			//title
			$options_panel->Title(__("Import Export","apc"));
			
			/**
			* add import export functionallty
			*/
			$options_panel->addImportExport();
			
			/**
			* Close 7th tab
			*/
			$options_panel->CloseTab();
			$options_panel->CloseTab();
			
			//Now Just for the fun I'll add Help tabs

		}
	
	}
	new walcf7Options();
}

?>