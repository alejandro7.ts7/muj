<?php

class walcf7_Activation_Controller {

    public function initialize_activation_hooks() {
        register_activation_hook("cf7-datetimepicker-pro/cf7-datetimepicker-pro.php", array($this, 'execute_activation_hooks'));
    }

    public function execute_activation_hooks() {
		if ( is_plugin_active( 'cf7-datetimepicker/cf7-datetimepicker.php' ) ) {
			//plugin is activated
			deactivate_plugins( 'cf7-datetimepicker/cf7-datetimepicker.php' );
		} 
        // Do activate Stuff now.
		        
    }
	
}

?>