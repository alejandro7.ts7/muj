msgid ""
msgstr ""
"Project-Id-Version: Automatic YouTube Gallery\n"
"POT-Creation-Date: 2021-12-23 16:17+0530\n"
"PO-Revision-Date: 2021-12-23 16:17+0530\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e;_n\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/admin.php:82
msgid "Invalid API Key"
msgstr ""

#: admin/admin.php:83
msgid "Cleared"
msgstr ""

#: admin/admin.php:98 admin/admin.php:139
msgid "Build Gallery"
msgstr ""

#: admin/admin.php:111 block/block.php:37 block/block.php:75
#: premium/widget/widget.php:35 premium/widget/widget.php:110
msgid "Automatic YouTube Gallery"
msgstr ""

#: admin/admin.php:112
msgid "YouTube Gallery"
msgstr ""

#: admin/admin.php:122 admin/admin.php:123
msgid "Dashboard"
msgstr ""

#: admin/settings.php:59 admin/settings.php:60
msgid "Settings"
msgstr ""

#: admin/settings.php:103 includes/functions.php:160
msgid "General"
msgstr ""

#: admin/settings.php:104
msgid "Gallery"
msgstr ""

#: admin/settings.php:105
msgid "Player"
msgstr ""

#: admin/settings.php:106 admin/settings.php:137 includes/functions.php:173
msgid "Live Stream"
msgstr ""

#: admin/settings.php:122
msgid "General Settings"
msgstr ""

#: admin/settings.php:127
msgid "Gallery Settings"
msgstr ""

#: admin/settings.php:132
msgid "Player Settings"
msgstr ""

#: admin/settings.php:156 admin/templates/api-key.php:18
msgid "Youtube API Key"
msgstr ""

#: admin/settings.php:158 admin/templates/api-key.php:27
#: includes/youtube-api.php:59
#, php-format
msgid ""
"Follow <a href=\"%s\" target=\"_blank\">this guide</a> to get your own API "
"key."
msgstr ""

#: admin/settings.php:170
msgid "Fallback Message"
msgstr ""

#: admin/settings.php:171
msgid ""
"Enter your Custom HTML message that should be displayed when there is no "
"video streaming live."
msgstr ""

#: admin/settings.php:448
msgid "Choose File"
msgstr ""

#: admin/settings.php:497
msgid "Select a page"
msgstr ""

#: admin/templates/api-key.php:13
msgid "You must create an API Key to build dynamic galleries from YouTube."
msgstr ""

#: admin/templates/api-key.php:22
msgid "Proceed"
msgstr ""

#: admin/templates/builder.php:80
msgid "Generate Shortcode"
msgstr ""

#: admin/templates/builder.php:89
msgid ""
"\"Automatic YouTube Gallery\" provides several methods to build your "
"gallery. Choose one of the following methods best suited for you,"
msgstr ""

#: admin/templates/builder.php:90
msgid ""
"Use the shortcode builder in this page to build your gallery shortcode, then "
"add it in your POST/PAGE."
msgstr ""

#: admin/templates/builder.php:91
#, php-format
msgid ""
"Use our \"Automatic YouTube Gallery\" <a href=\"%s\" target=\"_blank"
"\">Gutenberg block</a> to build the gallery directly in your POST/PAGE."
msgstr ""

#: admin/templates/builder.php:92
msgid ""
"Use our \"Automatic YouTube Gallery\" widget to add the gallery in your "
"website sidebars. This is a PRO feature."
msgstr ""

#: admin/templates/builder.php:104
msgid ""
"Congrats! copy the shortcode below and paste it in your POST/PAGE where you "
"need the gallery,"
msgstr ""

#: admin/templates/dashboard.php:14
msgid "Welcome to \"Automatic YouTube Gallery\""
msgstr ""

#: admin/templates/dashboard.php:17
msgid ""
"Create responsive, modern & dynamic video galleries by simply adding a "
"YouTube USERNAME, CHANNEL, PLAYLIST, SEARCH TERM or a custom list of YouTube "
"URLs."
msgstr ""

#: admin/templates/dashboard.php:20
#, php-format
msgid "Version %s"
msgstr ""

#: admin/templates/settings.php:38 admin/templates/settings.php:41
msgid "Delete Cache"
msgstr ""

#: admin/templates/settings.php:43
msgid "Delete all of the YouTube API data cached by the plugin."
msgstr ""

#: block/block.php:74
msgid "Create automated YouTube galleries."
msgstr ""

#: block/block.php:76
msgid "Selected Color"
msgstr ""

#: block/block.php:77
msgid ""
"Waiting to finish your block configuration. This delay is to avoid frequent "
"YouTube API calls."
msgstr ""

#: includes/functions.php:164
msgid "Source Type"
msgstr ""

#: includes/functions.php:168
msgid "Playlist"
msgstr ""

#: includes/functions.php:169
msgid "Channel"
msgstr ""

#: includes/functions.php:170
msgid "Username"
msgstr ""

#: includes/functions.php:171 includes/functions.php:205
msgid "Search Terms"
msgstr ""

#: includes/functions.php:172
msgid "Single Video"
msgstr ""

#: includes/functions.php:174
msgid "Custom Videos List"
msgstr ""

#: includes/functions.php:181
msgid "YouTube Playlist ID (or) URL"
msgstr ""

#: includes/functions.php:182 includes/functions.php:190
#: includes/functions.php:198 includes/functions.php:206
#: includes/functions.php:214 includes/functions.php:222
msgid "Example"
msgstr ""

#: includes/functions.php:189
msgid "YouTube Channel ID (or) URL"
msgstr ""

#: includes/functions.php:197
msgid "YouTube Account Username"
msgstr ""

#: includes/functions.php:213
msgid "YouTube Video ID (or) URL"
msgstr ""

#: includes/functions.php:221
msgid "YouTube Video IDs (or) URLs"
msgstr ""

#: includes/functions.php:224
msgid "Enter one video per line"
msgstr ""

#: includes/functions.php:230
msgid "Order Videos by"
msgstr ""

#: includes/functions.php:234
msgid "Date"
msgstr ""

#: includes/functions.php:235
msgid "Rating"
msgstr ""

#: includes/functions.php:236
msgid "Relevance"
msgstr ""

#: includes/functions.php:237 premium/widget/templates/admin.php:15
msgid "Title"
msgstr ""

#: includes/functions.php:238
msgid "View Count"
msgstr ""

#: includes/functions.php:245
msgid "Number of Videos"
msgstr ""

#: includes/functions.php:246
msgid ""
"Specifies the maximum number of videos that will appear in this gallery. Set "
"to 0 for the maximum amount (500)."
msgstr ""

#: includes/functions.php:255
msgid "Refresh the data every (Cache time)"
msgstr ""

#: includes/functions.php:256
msgid ""
"IMPORTANT! Specifies how frequently the plugin should check your YouTube "
"source for any new update. We recommend keeping this value as \"Page load\" "
"when you test the gallery and strongly suggest to change this value as \"Day"
"\" or to a greater value before pushing the gallery live."
msgstr ""

#: includes/functions.php:259
msgid "Page load"
msgstr ""

#: includes/functions.php:260
msgid "15 minutes"
msgstr ""

#: includes/functions.php:261
msgid "30 minutes"
msgstr ""

#: includes/functions.php:262
msgid "Hour"
msgstr ""

#: includes/functions.php:263
msgid "Day"
msgstr ""

#: includes/functions.php:264
msgid "Week"
msgstr ""

#: includes/functions.php:265
msgid "Month"
msgstr ""

#: includes/functions.php:273
msgid "Gallery (optional)"
msgstr ""

#: includes/functions.php:277
msgid "Player (optional)"
msgstr ""

#: includes/functions.php:398
msgid "Select Theme"
msgstr ""

#: includes/functions.php:399
#, php-format
msgid ""
"<a href=\"%s\">Upgrade Pro</a> for more themes (Popup, Slider, Playlister)."
msgstr ""

#: includes/functions.php:402
msgid "Classic"
msgstr ""

#: includes/functions.php:409
msgid "Columns"
msgstr ""

#: includes/functions.php:410
msgid ""
"Enter the number of columns you like to have in the gallery. Maximum of 12."
msgstr ""

#: includes/functions.php:419
msgid "Videos per page"
msgstr ""

#: includes/functions.php:420
msgid "Enter the number of videos to show per page. Maximum of 50."
msgstr ""

#: includes/functions.php:429
msgid "Thumbnail Ratio"
msgstr ""

#: includes/functions.php:440 includes/functions.php:611
msgid "Show Video Title"
msgstr ""

#: includes/functions.php:441
msgid "Check this option to show the video title in each gallery item."
msgstr ""

#: includes/functions.php:448
msgid "Video Title Length"
msgstr ""

#: includes/functions.php:449
msgid ""
"Enter the number of characters you like to show in the title. Set 0 to show "
"the whole title."
msgstr ""

#: includes/functions.php:458
msgid "Show Video Excerpt (Short Description)"
msgstr ""

#: includes/functions.php:459
msgid ""
"Check this option to show the short description of a video in each gallery "
"item."
msgstr ""

#: includes/functions.php:466
msgid "Video Excerpt Length"
msgstr ""

#: includes/functions.php:467
msgid ""
"Enter the number of characters you like to have in the video excerpt. Set 0 "
"to show the whole description."
msgstr ""

#: includes/functions.php:476
msgid "Pagination"
msgstr ""

#: includes/functions.php:477
msgid "Check this option to show the pagination."
msgstr ""

#: includes/functions.php:484
msgid "Pagination Type"
msgstr ""

#: includes/functions.php:487
msgid "Pager"
msgstr ""

#: includes/functions.php:488
msgid "Load More"
msgstr ""

#: includes/functions.php:516 public/public.php:67
msgid "Show More"
msgstr ""

#: includes/functions.php:600
msgid "Player Ratio"
msgstr ""

#: includes/functions.php:612
msgid ""
"Check this option to show the current playing video title on the bottom of "
"the player."
msgstr ""

#: includes/functions.php:619
msgid "Show Video Description"
msgstr ""

#: includes/functions.php:620
msgid ""
"Check this option to show the current playing video description on the "
"bottom of the player."
msgstr ""

#: includes/functions.php:627
msgid "Autoplay"
msgstr ""

#: includes/functions.php:628
msgid ""
"Specifies whether the initial video will automatically start to play when "
"the player loads."
msgstr ""

#: includes/functions.php:635
msgid "Autoplay Next Video"
msgstr ""

#: includes/functions.php:636
msgid ""
"Specifies whether to play the next video in the list automatically after "
"previous one end."
msgstr ""

#: includes/functions.php:643
msgid "Loop"
msgstr ""

#: includes/functions.php:644
msgid ""
"In the case of a single video player, plays the initial video again and "
"again. In the case of a gallery, plays the entire list in the gallery and "
"then starts again at the first video."
msgstr ""

#: includes/functions.php:651
msgid "Show Player Controls"
msgstr ""

#: includes/functions.php:652
msgid "Uncheck this option to hide the video player controls."
msgstr ""

#: includes/functions.php:659
msgid "Hide YouTube Logo"
msgstr ""

#: includes/functions.php:660
msgid ""
"Lets you prevent the YouTube logo from displaying in the control bar. Note "
"that a small YouTube text label will still display in the upper-right corner "
"of a paused video when the user's mouse pointer hovers over the player."
msgstr ""

#: includes/functions.php:667
msgid "Force Closed Captions"
msgstr ""

#: includes/functions.php:668
msgid ""
"Show captions by default, even if the user has turned captions off. The "
"default behavior is based on user preference."
msgstr ""

#: includes/functions.php:675
msgid "Show Annotations"
msgstr ""

#: includes/functions.php:676
msgid "Choose whether to show annotations or not."
msgstr ""

#: includes/functions.php:683
msgid "Player Language"
msgstr ""

#: includes/functions.php:685
#, php-format
msgid ""
"Specifies the player's interface language. Set the field's value to an <a "
"href=\"%s\" target=\"_blank\">ISO 639-1 two-letter language code.</a>"
msgstr ""

#: includes/functions.php:694
msgid "Default Captions Language"
msgstr ""

#: includes/functions.php:696
#, php-format
msgid ""
"Specifies the default language that the player will use to display captions. "
"Set the field's value to an <a href=\"%s\" target=\"_blank\">ISO 639-1 two-"
"letter language code.</a>"
msgstr ""

#: includes/youtube-api.php:59
msgid "YouTube API Key Missing."
msgstr ""

#: includes/youtube-api.php:70
msgid "YouTube Playlist ID (or) URL is required."
msgstr ""

#: includes/youtube-api.php:78 includes/youtube-api.php:132
msgid "YouTube Channel ID (or) URL is required."
msgstr ""

#: includes/youtube-api.php:96
msgid "YouTube Account Username is required."
msgstr ""

#: includes/youtube-api.php:116
msgid "Cannot search an empty string. A search term is required."
msgstr ""

#: includes/youtube-api.php:124
msgid "Atleast one YouTube Video ID (or) URL is required."
msgstr ""

#: includes/youtube-api.php:140
msgid "YouTube Video ID (or) URL is required."
msgstr ""

#: includes/youtube-api.php:284 includes/youtube-api.php:576
msgid "No videos found matching your query."
msgstr ""

#: premium/admin/admin.php:105
msgid "Inline"
msgstr ""

#: premium/admin/admin.php:106
msgid "Popup"
msgstr ""

#: premium/admin/admin.php:107
msgid "Slider"
msgstr ""

#: premium/admin/admin.php:108
msgid "Playlister"
msgstr ""

#: premium/admin/admin.php:118
msgid "Arrows"
msgstr ""

#: premium/admin/admin.php:119
msgid "Check this option to enable Prev/Next Arrows."
msgstr ""

#: premium/admin/admin.php:126
msgid "Arrow Size"
msgstr ""

#: premium/admin/admin.php:127
msgid "Enter the arrow size in pixels."
msgstr ""

#: premium/admin/admin.php:136
msgid "Arrow BG Color"
msgstr ""

#: premium/admin/admin.php:144
msgid "Arrow Icon Color"
msgstr ""

#: premium/admin/admin.php:152
msgid "Arrow Radius"
msgstr ""

#: premium/admin/admin.php:153
msgid "Enter the amount of arrow radius in pixels."
msgstr ""

#: premium/admin/admin.php:162
msgid "Arrow Top Offset"
msgstr ""

#: premium/admin/admin.php:163
msgid "Enter the arrow position from the slider's top edge in percentage (%)."
msgstr ""

#: premium/admin/admin.php:172
msgid "Arrow Left Offset"
msgstr ""

#: premium/admin/admin.php:173
msgid "Enter the left arrow position from the slider's left edge in pixels."
msgstr ""

#: premium/admin/admin.php:182
msgid "Arrow Right Offset"
msgstr ""

#: premium/admin/admin.php:183
msgid "Enter the right arrow position from the slider's right edge in pixels."
msgstr ""

#: premium/admin/admin.php:192
msgid "Dots"
msgstr ""

#: premium/admin/admin.php:193
msgid "Check this option to show dot indicators."
msgstr ""

#: premium/admin/admin.php:200
msgid "Dot Size"
msgstr ""

#: premium/admin/admin.php:201
msgid "Enter the dot size in pixels."
msgstr ""

#: premium/admin/admin.php:210
msgid "Dot Color"
msgstr ""

#: premium/admin/admin.php:218
msgid "Playlist Position"
msgstr ""

#: premium/admin/admin.php:222
msgid "Right"
msgstr ""

#: premium/admin/admin.php:223
msgid "Bottom"
msgstr ""

#: premium/admin/admin.php:230
msgid "Playlist Color"
msgstr ""

#: premium/admin/admin.php:234
msgid "Light"
msgstr ""

#: premium/admin/admin.php:235
msgid "Dark"
msgstr ""

#: premium/widget/widget.php:38
msgid "Displays automated YouTube video gallery."
msgstr ""

#: public/public.php:68
msgid "Show Less"
msgstr ""

#: public/templates/gallery-thumbnail.php:35
msgid "active"
msgstr ""

#: public/templates/pagination-loadmore.php:15
msgid "More Videos"
msgstr ""

#: public/templates/pagination-pager.php:15
msgid "Prev"
msgstr ""

#: public/templates/pagination-pager.php:20
msgid "of"
msgstr ""

#: public/templates/pagination-pager.php:25
msgid "Next"
msgstr ""
