<?php

/**
 * Gallery: Thumbnail.
 *
 * @link    https://plugins360.com
 * @since   1.0.0
 *
 * @package Automatic_YouTube_Gallery
 */

$title = ayg_get_gallery_thumb_title( $video, $attributes );
$excerpt = ayg_get_gallery_thumb_excerpt( $video, $attributes );
$description = ayg_get_player_description( $video, $attributes );

$container_class = 'ayg-thumbnail';

if ( ! empty( $title ) || ! empty( $excerpt ) ) {
    $container_class .= ' ayg-has-caption';
}

if ( true === $is_active ) {
    $container_class .= ' ayg-active';
}
?>

<div class="<?php echo $container_class; ?>" data-id="<?php echo esc_attr( $video->id ); ?>" data-title="<?php echo esc_attr( $video->title ); ?>">
    <div class="ayg-thumbnail-image-wrapper">
        <?php the_ayg_gallery_thumbnail_image( $video, $attributes ); ?>

        <svg class="ayg-icon ayg-thumbnail-icon-play" width="32" height="32" viewBox="0 0 32 32" fill="#fff">
            <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM12 9l12 7-12 7z"></path>
        </svg>

        <div class="ayg-thumbnail-active" style="display: none;"><?php esc_html_e( 'active', 'automatic-youtube-gallery' ); ?></div>
    </div>

    <div class="ayg-thumbnail-caption">
        <?php if ( ! empty( $title ) ) : ?> 
            <div class="ayg-thumbnail-title"><?php echo esc_html( $title ); ?></div>
        <?php endif; ?> 

        <?php if ( ! empty( $excerpt ) ) : ?>
            <div class="ayg-thumbnail-excerpt"><?php echo wp_kses_post( $excerpt ); ?></div>
        <?php endif; ?>

        <?php if ( ! empty( $description ) ) : ?>  
            <div class="ayg-thumbnail-description" style="display: none;"><?php echo wp_kses_post( make_clickable( $description ) ); ?></div>
        <?php endif; ?>
    </div>           
</div>
