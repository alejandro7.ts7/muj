<?php

/**
 * Theme: Live Stream.
 *
 * @link    https://plugins360.com
 * @since   1.6.4
 *
 * @package Automatic_YouTube_Gallery
 */

$livestream_settings = get_option( 'ayg_livestream_settings' );
?>

<div class="ayg ayg-theme-livestream">
    <!-- Player -->
    <div class="ayg-player">
        <div class="ayg-player-wrapper" style="padding-bottom: <?php echo esc_attr( ayg_get_player_ratio( $attributes ) ); ?>;">
            <iframe id="ayg-player-<?php echo esc_attr( ayg_get_uniqid() ); ?>" style="display: none;" class="ayg-player-iframe" width="100%" height="100%" src="<?php echo esc_url( ayg_get_player_embed_url( $response, $attributes ) ); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    
    <!-- Fallback Message -->
    <div class="ayg-fallback-message" style="display: none;">
        <?php echo wp_kses_post( $livestream_settings['fallback_message'] ); ?>
    </div>
</div>

