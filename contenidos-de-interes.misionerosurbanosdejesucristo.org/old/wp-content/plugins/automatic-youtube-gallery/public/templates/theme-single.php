<?php

/**
 * Theme: Classic.
 *
 * @link    https://plugins360.com
 * @since   1.0.0
 *
 * @package Automatic_YouTube_Gallery
 */

$data_params = array(    
    'player_title'       => (int) $attributes['player_title'],
    'player_description' => (int) $attributes['player_description']
);

$featured_video = $videos[0]; // Featured Video
$featured_video_title = ayg_get_player_title( $featured_video, $attributes ); // Featured Video Title
$featured_video_description = ayg_get_player_description( $featured_video, $attributes ); // Featured Video Description
?>

<div class="ayg ayg-theme-single" data-params='<?php echo wp_json_encode( $data_params ); ?>'>
    <!-- Player -->
    <div class="ayg-player">
        <?php the_ayg_player( $featured_video, $attributes ); ?>

        <?php if ( ! empty( $featured_video_title ) || ! empty( $featured_video_description ) ) : ?>
            <div class="ayg-player-caption">
                <?php if ( ! empty( $featured_video_title ) ) : ?>    
                    <h2 class="ayg-player-title"><?php echo esc_html( $featured_video_title ); ?></h2>  
                <?php endif; ?>

                <?php if ( ! empty( $featured_video_description ) ) : ?>  
                    <div class="ayg-player-description"><?php echo wp_kses_post( make_clickable( $featured_video_description ) ); ?></div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
