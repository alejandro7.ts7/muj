<?php
session_start();
require_once('include/functions_connect.php');

require_once('cl/Mobile_Detect.php');
$detect_mobile = new Mobile_Detect;
$is_mobile = false;
if ($detect_mobile->isMobile()) {
    $is_mobile = true;
    $hexigMAcima = '100%';
} else {
    $hexigMAcima = '200px';
    $is_mobile = false;
}


$obtenerCategorias = obtener_categorias_data();
//var_dump($obtenerCategorias);
$palabraBusquedaFinderDi = '';
$categoriaFiltradaInfoD1 = '';
$mostrarCategoriasEnMovil = true;

if ($is_mobile == true) {
    if (isset($_GET["category"])) {
        if (!empty($_GET["category"])) {
            $categoriaData = $_GET["category"];
            $mostrarCategoriasEnMovil = false;
            //echo "Buscar por categoria : " . $categoriaData;
        }
    }
}

if (isset($_GET["tipoList"])) {
    if (!empty($_GET["tipoList"])) {
        $tipoList = $_GET["tipoList"];
    }
} else {
    if ($is_mobile == false) {
        $tipoList = 0;
    } else {
        $tipoList = 0;
    }
}

$keyWords = '';
if (isset($_GET["keywords"])) {
    if (!empty($_GET["keywords"])) {
        $keyWords = $_GET["keywords"];
        $palabraBusquedaFinderDi = 'sobre ' . $keyWords;
        //echo "Buscar por palabra : " . $keyWords;
    }
}

$categoriaData = '';
if (isset($_GET["category"])) {
    if (!empty($_GET["category"])) {
        $categoriaData = $_GET["category"];
        $categoriaFiltradaInfoD1 = $categoriaData;
        //echo "Buscar por categoria : " . $categoriaData;
    }
}


// aqui hacer el select option 
$buscarOptions = '';
foreach ($obtenerCategorias as $key => $value) {

    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }
}

$buscarOptions2 = '';
$buscarOptions2Movil = '';
$contadorAllcategory = 0;
$categoriaExperiencias = '';
$contadorAllcategory = 0;
foreach ($obtenerCategorias as $key => $value) {

    //
    $categoriaConsultar = total_contenido_categoria($value);
    if (!empty($categoriaData)) {
        //echo $categoriaData;

        if (trim($value) == trim($categoriaData)) {
            $buscarOptions = $buscarOptions . '<option selected value="' . $value . '">' . $value . '</option>';
        } else {
            $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
        }
    } else {
        $buscarOptions = $buscarOptions . '<option value="' . $value . '">' . $value . '</option>';
    }

    $contadorAllcategory = $contadorAllcategory + 1;

    // <span>(' . $categoriaConsultar . ')</span>
    $categoriaExperiencias = $categoriaExperiencias . '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;"  href="index.php?category=' . $value . '" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-book mr-3 text-black" style="color:#009BDF;"></i> ' . $value . ' </a></li>';
}
$categoriaExperiencias = '<li class="margenLis nav-sub-item"><a style="cursor:pointer;color:#000000;padding-left: 0px;padding-right: 0px;margin-left: 5px;" href="index.php?category=" class="nav-sub-link active">&nbsp;&nbsp;&nbsp;<i class="fas fa-book mr-3 text-black" style="color:#009BDF;"></i> Todos los contenidos <span style="margin-right: 10px;">&nbsp;&nbsp;(' . $contadorAllcategory . ')</a></li>' . $categoriaExperiencias;


$buscarOptions2 = '<li><h4 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas los contenidos <span>(' . $contadorAllcategory . ')</span></a></h4></li>' . $buscarOptions2;

$buscarOptions2Movil = '<h3 style="margin-bottom: 8px;font-size: 1.6rem;"><a style="cursor:pointer;color:#288feb;" href="index.php?category=">Todas los contenidos <span>(' . $contadorAllcategory . ')</span></a></h3>' . $buscarOptions2Movil;
//echo $contadorAllcategory;

$con = conection_database();
// aqui hacer la pagincacion 

close_database($con);

// include mobile detect




?>


<!DOCTYPE html>
<html lang="es" translate="no">

<head>
    <!-- Required meta tags   -->
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-language" content="es_ES">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Páginas de interés</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="css/data_image.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="swal/sweetalert2.min.css">
    <link href="lib/fonti/css/all.css" rel="stylesheet">
    <!--load all styles -->
    <!-- Arribaema -->
    <link rel="icon" href="images/favicon.png" sizes="192x192" />


    <style>
        @import url('https://fonts.googleapis.com/css?family=Nunito');


        .slim-header.with-sidebar .slim-sidebar-menu {
            margin-right: 0px !important;
        }


        .zoom:hover {
            transform: scale(1.1);
            /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }

        .zoom2:hover {
            transform: scale(1.06);
            /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }


        .itemdonar .sidebar-nav-link:hover,
        .sidebar-nav-link:focus {
            background-color: #f8f9fa;
        }




        #breadcrumb li:last-child a {
            padding-left: 5px;
            padding-right: 5px;
        }

        .section-wrapper {
            border-radius: 15px;
        }

        .sidebar-nav-sub .nav-sub-link.active {
            background-color: #ffffff;
        }


        .margenLis {
            border-style: solid;
            border-color: #d3d3d3;
            border-width: 1.15px;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
        }
    </style>
    <!-- Stylesheet -->
    <link href="style.css" rel="stylesheet" type="text/css" />


    <link href="admin/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="lib/fullcall/fullcalendar.css">
    <style>
        .slim-header.with-sidebar .slim-logo {
            width: 205px;
            margin-right: 10px;
            margin-left: -340px;
        }

        .container-fluid {
            margin-right: 0px;
            margin-left: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }


        .nav-links .page-numbers.prev:hover {
            border-radius: 15px;
            background-color: #00adef;
            padding-right: 15px;
            color: #fff;
        }


        select.form-control {
            -webkit-appearance: menulist !important;
            -moz-appearance: menulist !important;
            -ms-appearance: menulist !important;
            -o-appearance: menulist !important;
            appearance: menulist !important;
        }

        .single-book-box {
            border: 10px solid #c2c2c2;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .books-gird .single-book-box {
            opacity: 1;
        }

        .books-gird ul li:nth-child(3n) .single-book-box .post-detail {
            left: 0px;
            right: 66px;
        }

        .books-gird .single-book-box .post-detail {
            top: 0;
            transform: none;
            width: 100%;
            left: 0;
            position: relative;
            padding: 30px 15px;
        }

        table.dataTable tbody tr {
            background-color: transparent;
        }

        .has-fixed.is-shrink .header-main {
            padding: 0px 0;
        }

        .header-main {
            padding: 2px 0;
        }

        #home-v1-header-carousel .carousel-caption {
            top: 36%;
        }

        label {
            color: #fff;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            color: #fff !important;
        }

        .sidebar .widget-sub-title::after {
            content: '';
        }

        input {
            border-radius: 15px;
        }

        .form-control {
            border-radius: 15px;
        }

        .single-book-box {
            padding-top: 5px;
        }

        .single-book-box .post-detail .btn {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }

        .row {
            margin-left: 0px;
            margin-right: 0px;
        }

        .form-control {
            border: 3px solid #f4f4f4 !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            height: 56px !important;
            padding: 5px 10px !important;
        }

        html {
            font-family: "Nunito" !important;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-weight: 500;
        }

        body {
            font-family: "Nunito" !important;
            margin: 0;
            font-weight: 500;
            color: #808080;
        }

        a,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        li,
        font {
            color: #808080;
            font-family: "Nunito" !important;
        }

        .text-dark {
            color: #808080 !important;
        }

        * {
            font-family: "Nunito";
        }

        .btnSearch:hover {
            background-color: #116ed1 !important;
            border-color: #116ed1 !important;
            color: #fff;
        }

        .btnSearch:active {
            background-color: #116ed1 !important;
            border-color: #116ed1 !important;
            color: #fff;
        }

        .books-list {
            margin-top: 100px
        }

        .nav-links .page-numbers:hover,
        .nav-links .page-numbers.current {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .nav-links .page-numbers.next:hover {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .nav-links .page-numbers.prev:hover {
            color: #fff;
            background-color: #146abb;
            border-color: #1363b0;
        }

        .btn-primary:hover {
            background-color: #116ed1;
            border-color: #116ed1;
            color: #fff;
        }

        .nav-links .page-numbers:hover,
        .nav-links .page-numbers.current {
            color: #fff !important;
            background-color: #009BDF !important;
            border-color: #009BDF !important;
        }

        .margenLis:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-sub .nav-sub-link.active:hover {
            text-decoration: none;
            background-color: #eeeeee;
        }

        .sidebar-nav-item.with-sub>.sidebar-nav-link::after {
            content: '\f3d0';
            font-family: 'Ionicons';
            margin-left: auto;
            position: relative;
            opacity: 1;
            color: #000;
        }

        .linunito {
            font-family: "Nunito" !important;
            /*color: #009BDF !important;*/
            list-style-type: none;
        }

        .linunito:hover {
            font-family: "Nunito" !important;
            color: #009BDF !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" />

    <script type="text/javascript" defer>
        // (function() {
        //   var options = {
        //     whatsapp: "573194475236", // WhatsApp number
        //     call_to_action: "Contáctenos", // Call to action
        //     position: "right", // Position may be 'right' or 'left'
        //   };
        //   var proto = document.location.protocol,
        //     host = "whatshelp.io",
        //     url = proto + "//static." + host;
        //   var s = document.createElement('script');
        //   s.type = 'text/javascript';
        //   s.async = true;
        //   s.src = url + '/widget-send-button/js/init.js';
        //   s.onload = function() {
        //     WhWidgetSendButton.init(host, proto, options);
        //   };
        //   var x = document.getElementsByTagName('script')[0];
        //   x.parentNode.insertBefore(s, x);
        // })(); 
    </script>






</head>

<body>

    <div class="slim-header with-sidebar" style="background-color: #fff;color:#000;">
        <div class="container-fluid">
            <?php include('htmlinclude/siv33.php'); ?>
        </div><!-- container-fluid -->
    </div><!-- slim-header -->


    <div class="slim-body">
        <?php include('htmlinclude/siv22.php'); ?>

        <div class="slim-mainpanel" style="background: #f0f2f7;">

            <?php if ($is_mobile == true) { ?>
                <div class="container" style="padding-top: 5px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 26px;padding-left: 5px !important;padding-right: 5px !important;padding-bottom: 5px !important;">
                <?php } else { ?>
                    <div class="container" style="padding-top: 25px;/* margin-left: 10px; *//* margin-right: 10px; */width: 100%;/* background-color: #000; */background-image: linear-gradient(to bottom, rgb(255 255 255) 0%, rgb(255 255 255) 100%), linear-gradient(to bottom, #f0f2f7 0%, #f0f2f7 100%);background-clip: content-box, padding-box;border-radius: 50px;">
                    <?php } ?>




                    <div id="cuadrocentralcontenido" class="cuadrocentralcontenido" style="border-radius:15px;">

                        <div id="content" class="site-content">
                            <div id="primary" class="content-area">
                                <main id="main" class="site-main">
                                    <div class="books-media-gird">
                                        <div class="container-fluid">
                                            <div style="display:none;" class="row">
                                                <!-- Start: Search Section -->
                                                <section class="search-filters" style="margin-bottom: 20px;">
                                                    <div class="container-fluid">

                                                    </div>
                                                </section>
                                                <!-- End: Search Section -->
                                            </div>
                                            <div class="row">

                                                <?php if ($is_mobile == true) { ?>

                                                    <?php if (1 == 2 && $mostrarCategoriasEnMovil == true) { ?>
                                                        <div class="col-md-3">
                                                            <div class="widget widget_related_search open" data-accordion>
                                                                <!--<h4 class="widget-title" data-control>Búsqueda relacionada</h4>
                                             -->
                                                                <div data-accordion>
                                                                    <h1 style="margin-bottom: 1;padding-bottom: 0px;margin-bottom: 0px;" class="widget-sub-title" data-control>Contenidos</h1>
                                                                    <h6 style="margin-bottom: 10px;color:#000;">Selecciona un contenido</h6>
                                                                    <?php echo $buscarOptions2Movil; ?>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>


                                                        </div>
                                                    <?php } ?>
                                                    <div class="col-md-9" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 20px;margin: 15px;">
                                                        <div class="filter-options margin-list">
                                                            <div class="row">


                                                                <?php if ($is_mobile == true) { ?>

                                                                    <div class="col-md-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                                                                        <form action="index.php" method="get">



                                                                            <div class="col-12" style="padding-left: 0px;margin-top: 30px;margin-bottom: 30px;">
                                                                                <h1 style="word-break: break-word;font-size:3rem;text-align:center;color:#009BDF;">Páginas de interés </h1>
                                                                            </div>


                                                                            <div class="col-lg-12 col-sm-12">



                                                                                <p class="desaparecer2" style="font-size: 14px;text-align: justify;">Nos esforzamos por hacer realidad el máximo deseo de
                                                                                    nuestro Dios, “que todos los hombres se salven”. Por eso
                                                                                    te recomendamos estas páginas web para que
                                                                                    continuemos aprendiendo y creciendo juntos desde “la
                                                                                    Verdad”, amando a Dios por encima de todo y
                                                                                    compartiendo un interés genuino por la salvación de todas
                                                                                    las almas. </p>

                                                                                <table id="datatable1_evento" class="table card-text">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Enlaces (Url) de interés:</th>


                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>

                                                                                    </tbody>
                                                                                </table>



                                                                            </div>





                                                                        </form>

                                                                    </div>







                                                                <?php } else if ($is_mobile == false) { ?>




                                                                <?php } ?>


                                                            </div>
                                                        </div>









                                                    </div>


                                                <?php } else { ?>
                                                    <div class="col-md-12" style="box-shadow: rgb(0 0 0) 0px 1px 4px;border-radius: 25px;padding: 0px;padding-left: 25px;padding-top: 20px;padding-bottom: 20px;">
                                                        <div class="filter-options margin-list">
                                                            <div class="row">

                                                                <div class="col-12" style="padding-left: 0px;margin-top: 30px;margin-bottom: 30px;">
                                                                    <h1 style="word-break: break-word;font-size:7rem;text-align:center;color:#009BDF;">Páginas de interés </h1>
                                                                </div>






                                                                <div class="col-md-12 col-sm-12" style="padding-left: 0px;">
                                                                    <div class="container-fluid" style="box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;padding: 20px;word-break: break-all;border-radius: 25px;background-color: #fff;margin-bottom: 10px;word-break: break-word;text-align: justify;">




                                                                        <div class="filter-box" style="border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;">

                                                                            <div class="col-lg-12 col-sm-12">
                                                                                <div class="d-flex justify-content-center " style="text-align:center;">
                                                                                    <label class="desaparecer2 section-title" style="color:#009BDF;text-align:center;">Páginas de interés</label>
                                                                                </div>



                                                                                <div class="col-12">
                                                                                    <p class="desaparecer2" style="font-size: 14px;text-align: justify;">Nos esforzamos por hacer realidad el máximo deseo de
                                                                                        nuestro Dios, “que todos los hombres se salven”. Por eso
                                                                                        te recomendamos estas páginas web para que
                                                                                        continuemos aprendiendo y creciendo juntos desde “la
                                                                                        Verdad”, amando a Dios por encima de todo y
                                                                                        compartiendo un interés genuino por la salvación de todas
                                                                                        las almas. </p>

                                                                                    <table id="datatable1_evento" class="table card-text">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th>Enlaces (Url) de interés:</th>


                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>





                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div>



                                                            </div>
                                                        </div>








                                                    </div>


                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                </main>
                            </div>
                        </div>
                    </div>


                    </div><!-- container -->



                    <?php include('htmlinclude/siv44.php'); ?>
                </div><!-- slim-mainpanel -->
        </div><!-- slim-body -->




        <script src="lib/jquery/js/jquery.js"></script>

        <script src="lib/popper.js/js/popper.js"></script>
        <script src="lib/bootstrap/js/bootstrap.js"></script>
        <script src="lib/jquery.cookie/js/jquery.cookie.js"></script>
        <script src="lib/d3/js/d3.js"></script>
        <script src="lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>
        <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

        <script src="js/ResizeSensor.js"></script>
        <script src="js/slim.js"></script>

        <script src="swal/swalpersonalizado.js"></script>
        <script src="swal/sweetalert2.min.js"></script>

        <script src="admin/lib/datatables/js/jquery.dataTables.js"></script>
        <script src="admin/lib/datatables-responsive/js/dataTables.responsive.js"></script>
        <script>
            $('#datatable1_evento').DataTable({
                "bLengthChange": false,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "ajax": "admin/ajax/scripts/server_processing_evento4.php",
                responsive: true,
                "language": {
                    "processing": "Procesando...",
                    "lengthMenu": "Resultados por página _MENU_ ",
                    "zeroRecords": "No se encontraron resultados",
                    "emptyTable": "Ningún dato disponible",
                    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    "search": "Buscar:",
                    "infoThousands": ",",
                    "loadingRecords": "Cargando...",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                },


            });
        </script>


        <script>
            function regresarAtras() {


                history.back();


            }
        </script>



        <div id="smart-button-container">
            <div style="text-align: center;">
                <div id="paypal-button-container"></div>
            </div>
        </div>










</body>

</html>